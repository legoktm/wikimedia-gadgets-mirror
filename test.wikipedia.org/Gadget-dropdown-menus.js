/*********************************************************************
**                ***WARNING: GLOBAL GADGET FILE***                 **
**         any changes to this file will affect many users          **
**          please discuss changes on the talk page or at           **
**             [[Wikipedia talk:Gadget]] before editing             **
**     (consider dropping the script author a note as well...)      **
**                                                                  **
**********************************************************************
**             Script:        Dropdown menus                        **
**             Author:        MusikAnimal, Haza-w                   **
**      Documentation:        [[User:MusikAnimal/MoreMenu]]         **
**                                                                  **
**  For version information, see corresponding files at:            **
**   Vector skin:  [[MediaWiki:Gadget-dropdown-menus-vector.js]]    **
**   Non-vector:   [[MediaWiki:Gadget-dropdown-menus-nonvector.js]] **
**                                                                  **
*********************************************************************/

if ( mw.config.get( 'skin' ) === 'vector' ) {
	mw.loader.load( 'ext.gadget.dropdown-menus-vector' );
} else {
	mw.loader.load( 'ext.gadget.dropdown-menus-nonvector' );
}