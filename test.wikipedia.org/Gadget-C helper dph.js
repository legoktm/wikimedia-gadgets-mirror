C.dph = {
  dialog: null,
  config: {
  	'request-page': 'User:0x010C/dph',
    'modal-title': 'Chelper - Demander une purge de l\'historique',
    'submit': 'valider la requête',
    'c-tag-checkbox-class': 'c-tag-checkbox',
    'c-dph-radio-class': 'c-dph-radio',
    'notify-pending': 'Dépôt de la requête sur [[Wikipédia:Demande de purge d\'historique]] en cours.',
    'notify-sucess': 'La demande de purge d\'historique a été correctement envoyé',
    'notify-not-selected': 'Vous n\'avez pas sélectionné la version d\'ajout et/ou la version du retrait du copyvio.',
    'notify-not-a-page': 'Impossible de demander la purge de l\'historique d\'une page inexistante.',
    'single-add-summary': 'Ajout de $1',
    'multi-add-summary': 'Ajout de $1 et $2',
    'template-link-summary': '{{[[Modèle:$1|$1]]}}',
    'load-more-label': '(charger la suite)',
    'copyvio-label': 'Contenu copié',
    'history-label': 'Historique',
    'history-help': 'Cocher dans la première colonne la version insérant le copyvio, et dans la seconde la version où le copyvio a été retiré',
  },
  history_container: null,
  rvcontinue: null,

  init: function() {
  },
  launch: function() {
  	if(mw.config.get('wgArticleId') === 0) {
    	mw.notify(C.dph.config['notify-not-a-page'], {title:'C-helper', type:'error'});
    	return;
  	}
	if(this.dialog === null) {
		this.build_dialog();
	}
	this.dialog.dialog("open");
  },
  "build_dialog": function() {
    this.dialog = $('<div/>', {title:C.dph.config['modal-title']});
    var form = $('<form/>');
    form.append($("<h3/>").html(C.dph.config['copyvio-label']));
    form.append($("<input/>", {type:"text", id:"C-dph-copyvio", placeholder:"https://www.exemple.com"}));
    form.append($("<hr/>"));
    form.append($("<h3/>").html(C.dph.config['history-label']).append(C.util.construct_help_icon(C.dph.config['history-help'])));
    this.dialog.append(form);

    //Create sections
    C.dph.history_container = $("<div/>", {id:"C-dph-history-container"});
    form.append(C.dph.history_container);
    
    this.dialog.dialog({
      autoOpen: false,
      height: 400,
      width: 600,
      modal: true,
      buttons: [
        {
          text: C.dph.config['submit'],
          click: function() {
            C.dph.dialog.dialog("close");
            C.dph.validate();
          },
        },
      ],
    dialogClass: 'c-helper-dialog',
    });
    this.load_more();
  },
  "load_more": function() {
	$("#C-dph-load-more").remove();
  	C.dph.history_container.append($("<div/>", {id:"C-dph-wait", style:"text-align: center;"}).append($("<img/>", {src:"//upload.wikimedia.org/wikipedia/commons/b/b4/Loading_Animation.gif"})));
  	var data = {
		action: 'query',
		format: 'json',
		prop: 'revisions',
		titles: mw.config.get('wgPageName'),
		rvprop: 'ids|timestamp|user|size|comment',
		rvlimit: 500,
	};
	if(C.dph.rvcontinue !== null)
		data.rvcontinue = C.dph.rvcontinue;
  	$.ajax({
		url: mw.util.wikiScript('api'),
		data: data,
		dataType: 'json'
	}).then(function(data) {
		revisions = data.query.pages[mw.config.get('wgArticleId')].revisions;
		var ul = $("<ul/>");
		for(i=0;i<revisions.length;i++) {
			var li = $("<li/>");
			r = revisions[i];
			content = '([[Special:Diff/'+r.revid+'/cur|actu]] | [[Special:Diff/'+r.revid+'|diff]]) '+r.timestamp+' [[User:'+r.user+'|'+r.user+']] ([[User talk:'+r.user+'|discuter]] | [[Special:Contributions/'+r.user+'|contributions]]) <small>('+r.size+' octets)</small> ('+r.comment+')';
			li.append($('<input/>', {type:'radio', class:C.dph.config['c-dph-radio-class'], name:'C-dph-first-revision', content:content, value:i}));
			li.append($('<input/>', {type:'radio', class:C.dph.config['c-dph-radio-class'], name:'C-dph-last-revision', content:content, value:i}));
			li.append(C.util.parse_wikicode(content));
			ul.append(li);
		}
		$("#C-dph-wait").remove();
		C.dph.history_container.append(ul);
		console.log(data);
		if(data.hasOwnProperty("continue")) {
			console.log(data['continue']);
    		C.dph.history_container.append($("<a/>", {id:"C-dph-load-more", href:"#"}).text("(load more)").click(function(){C.dph.load_more();}));
			C.dph.rvcontinue = data['continue'].rvcontinue;
		}
		else {
			C.dph.rvcontinue = null;
		}
		$('.'+C.dph.config['c-dph-radio-class']).change(function() {
			if($(this).is(":checked")) {
				var value = parseInt($(this).attr("value"));
				if($(this).attr("name") == 'C-dph-first-revision') {
					$('input[name="C-dph-last-revision"]').each(function(){
						if(parseInt(this.value) < value)
							$(this).attr('disabled', false);
						else
							$(this).attr('disabled', true);
					});
				}
				else {
					$('input[name="C-dph-first-revision"]').each(function(){
						if(parseInt(this.value) > value)
							$(this).attr('disabled', false);
						else
							$(this).attr('disabled', true);
					});
				}
			}
		});
	});
  },
  validate: function() {
  	mw.notify(C.dph.config['notify-pending'], {title:'C-helper', type:'warning', autoHide:false, tag:'C-dph'});
  	first = $('input[name=C-dph-first-revision]:checked');
  	last = $('input[name=C-dph-first-revision]:checked');
  	if(first.length === 0 || last.length === 0) {
  		mw.notify(C.dph.config['notify-not-selected'], {title:'C-helper', type:'error', autoHide:false, tag:'C-dph'});
  		return;
  	}
	//<nowiki>
  	content = "{{RA début|traitée=|date=<!--~~~~~-->}}\n"
  		+ "'''Contenu copié''' : "+$("#C-dph-copyvio").val()+"\n\n"
  		+ "'''Historique''' :\n"
  		+ "* ''Retrait'' : "+last.attr('content')+"\n"
  		+ "* ''Premier ajout'' : "+first.attr('content')+"\n"
  		+ "Merci ~~~~"
  		+ "{{RA fin}}";
  	//</nowiki>
  	C.util.new_section(C.dph.config['request-page'], "{{a-court|"+mw.config.get('wgPageName')+"}}", content, "Demande de purge de [["+C.dph.config['request-page']+"]]", function() {
  		mw.notify(C.dph.config['notify-sucess'], {title:'C-helper', type:'info', autoHide:false, tag:'C-dph'});
  	});
  },
};

C.modules.dph.callback = C.dph;