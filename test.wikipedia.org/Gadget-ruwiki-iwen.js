(function () {
	var wgLangPrefs = mw.config.get('wgLangPrefs');
	if(! $.isArray(wgLangPrefs) ) {
		wgLangPrefs = Array();
	}
	if ($.inArray("en", wgLangPrefs) == -1) {
		wgLangPrefs.push("en");
	}
	mw.config.set('wgLangPrefs', wgLangPrefs);
}());