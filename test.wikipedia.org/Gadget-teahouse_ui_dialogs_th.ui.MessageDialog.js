
/*!
 * Teahouse user interface MessageDialog class.
 */

/**
 * Dialog displaying a message to a user
 *
 * @class
 * @extends OO.ui.MessageDialog
 *
 * @constructor
 * @param {Object} [config] Configuration options
 */

mw.teahouse.util.registerType( 'th.ui.MessageDialog' );

th.ui.MessageDialog = function ThUiMessageDialog( config ) {
	th.ui.MessageDialog.super.call( this, config );
};
OO.inheritClass( th.ui.MessageDialog, OO.ui.MessageDialog );

th.ui.MessageDialog.static.actions = [
	{ action: 'yes', label: mw.message('th-dialog-msg-btn-yes').plain() },
	{ action: 'no', label: mw.message('th-dialog-msg-btn-no').plain() }
];
/**
 * @inheritdoc
 */
th.ui.MessageDialog.prototype.initialize = function () {
	// Parent method
	th.ui.MessageDialog.super.prototype.initialize.call( this );

	//Create new panel
	this.$message = $('<div>').addClass( 'oo-ui-messageDialog-message' );

	this.text.$element.append( this.$message );
};

/**
 * @inheritdoc
 */
th.ui.MessageDialog.prototype.getSetupProcess = function ( data ) {
	data = data || {};

	// Parent method
	return th.ui.MessageDialog.super.prototype.getSetupProcess.call( this, data )
		.next( function () {

			//We hide the base class' OO.ui.LabelWidget
			this.message.$element.hide();

			this.$message.empty();
			this.$message.append(data.message);
		}, this );
};