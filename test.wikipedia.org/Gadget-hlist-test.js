/* Helper script for .hlist class in common.css
 * Maintainer: [[User:Edokter]]
 */
 
if ( $.client.profile().name == 'msie' ) {
  /* Add pseudo-selector class to last child list items in IE 8 */
  if ( $.client.profile().versionBase == '8' ) {
    $( '.hlist' ).find( 'dd:last-child, dt:last-child, li:last-child' )
      .addClass( 'hlist-last-child' );
  }
  /* Generate interpuncts and parens for IE < 8 */
  if ( $.client.profile().versionBase < '8' ) {
    $( '.hlist' ).find( 'dt + dd, dt + dt' ).prev()
      .append( '<b>:</b> ' );
    $( '.hlist' ).find( 'dd + dd, dd + dt, li + li' ).prev()
      .append( '<b>·</b> ' );
    $( '.hlist' ).find( 'dl dl, ol ol, ul ul' )
      .prepend( '( ' ).append( ') ' );
  }
}