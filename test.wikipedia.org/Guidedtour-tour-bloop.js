// Guided Tour to help users start editing
 
( function ( window, document, $, mw, gt ) {
gt.defineTour( {
        name: 'bloop',
        steps: [ {
                title: 'Ready to help?',
                description: 'This page needs basic copyediting – improving the grammar, style, tone, or spelling – to make it clear and easy to read. This tour will show you the steps to take.',
                overlay: true,
                buttons: [ {
                        action: 'next'
                } ],
 
                // shouldSkip is a general tool to skip a step based on their progress towards some goal.  This is used to ensure this step will not be shown once they have started editing.
                // shouldSkip always returns true to skip, or false otherwise.
                shouldSkip: function() {
                        return gt.hasQuery( { action: 'edit' } );
                }
        },  {
                title: 'Click \'Edit\'',
                description: 'This will let you make changes to any part of the page, when you\'re ready.',
                attachTo: '#ca-edit',
                position: 'bottom',
                // Same as above.  We plan to make this less repetitive in the future.
                shouldSkip: function() {
                        return gt.hasQuery( { action: 'edit' } );
                }
        }, {
                title: 'Preview (optional)',
                description: 'Clicking \'Preview\' allows you to check what the page will look like with your changes. Just don\'t forget to save.',
                attachTo: '#wpPreview',
                position: 'top',
                // Normally, the guider will automatically close when the user clicks outside the boundaries.  This can be turned off on a case-by-case basis, as it is here.
                // In this case, it is turned off since the user is likely to click the edit box before seeing the guider at the bottom of the screen.
                closeOnClickOutside: false,
                // This is the opposite of the above.  We skip if they are *not* editing.
                shouldSkip: function() {
                        return !gt.hasQuery( { action: 'edit' } );
                }
        }, {
                title: 'You\'re almost finished!',
                description: 'Click \'Save\' and your changes will be visible.',
                attachTo: '#wpSave',
                position: 'top',
 
                // Normally, guiders will close when the user clicks outside them.  But you may want to disable this in some cases.
                // For example, since people normally click on the edit box, we are disabling them on steps that are on the edit screen.
                closeOnClickOutside: false,
 
                // We skip if they are not doing a preview, show changes, etc.
                shouldSkip: function() {
                        return !gt.hasQuery( { action: 'submit' } );
                }
        }, {
                title: 'Looking for more to do?',
                description: '[[Special:GettingStarted|Getting Started]] is updated every hour with new pages.',
                onShow: gt.parseDescription,
                overlay: true,
 
                // We don't want to show this step if they didn't complete an edit.
                // Note that this requires they actually change something, not just click save.
                shouldSkip: function() {
                        return !gt.isPostEdit();
                },
                buttons: [ {
                        action: 'end'
                } ]
        } ]
} );
 
} (window, document, jQuery, mediaWiki, mediaWiki.guidedTour ) );