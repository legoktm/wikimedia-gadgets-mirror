mw.loader.using( ['mediawiki.util', 'mediawiki.page.startup', 'jquery.ui.dialog'], function () {


  var myForm = document.createElement('form');
  myForm.innerHTML = '<label for="test-textarea">Free input</label><textarea id="test-textarea" placeholder="Hello, come and type here..." rows="10"></textarea><hr/><input type="submit">';

  jQuery(document).ready(function ($) {
    $('#clicker-dialog').show().click(function () {
      var $wrapper = $('<div>').append(
        $(myForm).clone()
      );
      $wrapper.dialog({
        title: 'My widget',
        minWidth: 500
      });
    });

    $('#clicker-prependfield').show().click(function () {
      var $wrapper = $('<fieldset>').hide().append(
        $('<legend>').text('My widget'),
        $(myForm).clone()
      );
      mw.util.$content.prepend($wrapper);
      $wrapper.slideDown();
    });

  });



});