//Tim's mass blocking tool
//Adapted from [[User:Animum/massdelete.js]]

function doMassBlock() {
    document.getElementById("wpMassBlockSubmit").disabled = true;
    var users = document.getElementById("wpMassBlockUsers").value.split("\n");
    if(users.length == 0) return;
    var wpMassBlockReasons = document.getElementById("wpMassBlockReasons").value, wpMassBlockReason = document.getElementById("wpMassBlockReason").value, blocked = 0, talkpageedited = 0, userpageedited = 0, failed = new Array(), error = new Array();
    var wpMassBlockAnononly = document.getElementById("wpMassBlockAnononly").checked, wpMassBlockNocreate = document.getElementById("wpMassBlockNocreate").checked, wpMassBlockEmail = document.getElementById("wpMassBlockEmail").checked, wpMassBlockAutoblock = document.getElementById("wpMassBlockAutoblock").checked, wpMassBlockTalkpage = document.getElementById("wpMassBlockTalkpage").checked, wpMassBlockReblock = document.getElementById("wpMassBlockReblock").checked;
    var wpMassBlockMessage = document.getElementById("wpMassBlockMessage").value, wpMassBlockTag = document.getElementById("wpMassBlockTag").value, wpMassBlockExpiry = document.getElementById("wpMassBlockExpiry").value;
    var wpMassBlockSummaryTalk = document.getElementById("wpMassBlockSummaryTalk").value, wpMassBlockSummaryUser = document.getElementById("wpMassBlockSummaryUser").value, wpMassBlockExpiry = document.getElementById("wpMassBlockExpiry").value;
    var blocksettingstring = "&expiry=" + encodeURIComponent(wpMassBlockExpiry == ""? "indefinite" : wpMassBlockExpiry) + (wpMassBlockAnononly ? "&anononly=1" : "") + (wpMassBlockNocreate ? "&nocreate=1" : "") + (wpMassBlockAutoblock ? "&autoblock=1" : "") + (wpMassBlockEmail ? "&noemail=1" : "") + (wpMassBlockTalkpage ? "" : "&allowusertalk=1") + (wpMassBlockReblock ? "&reblock=1" : "");
    for(i=0;i<users.length;i++) {
        var user = users[i];
        if(user.length > 0) {
            var req = sajax_init_object();
            req.open("GET", wgScriptPath + "/api.php?format=json&action=query&prop=info&intoken=block&titles=User:" + encodeURIComponent(user), false);
            req.send(null);
            var response = eval("(" + req.responseText + ")").query.pages;
            for(var index in response) {
                var info = response[index];
                var blocktoken = info.blocktoken;
                var postdata = "format=json"
                             + "&action=block"
                             + "&user=" + encodeURIComponent(user)
                             + "&reason=" + encodeURIComponent(wpMassBlockReasons == "other" ? wpMassBlockReason : wpMassBlockReasons + (wpMassBlockReason ? ": " + wpMassBlockReason : ""))
                             +  blocksettingstring
                             + "&token=" + encodeURIComponent(blocktoken);
                var req = sajax_init_object();
                req.open("POST", wgScriptPath + "/api.php", false);
                req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                req.setRequestHeader("Content-length", postdata.length);
                req.send(postdata);
                if(eval("(" + req.responseText + ")")['block']) { //If blockd, update the blockd count and the button.
                    blocked++;
		    // now edit user and user talk.
                    if(wpMassBlockMessage != "") {
                        var postdata = "format=json&action=edit&watchlist=nochange&title=User%20talk:" + encodeURIComponent(user) + "&summary=" + encodeURIComponent(wpMassBlockSummaryTalk) 
                             + "&appendtext=" + encodeURIComponent(wpMassBlockMessage)
                             + "&token=" + encodeURIComponent(blocktoken);
                        var req = sajax_init_object();
                        req.open("POST", wgScriptPath + "/api.php", false);
                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        req.setRequestHeader("Content-length", postdata.length);
                        req.send(postdata);
                        if(eval("(" + req.responseText + ")")['edit']) { talkpageedited ++; }
                        else { //If not edited, add the title to the "failed" array and a description of the error to the "error" array.
                            failed.push("User talk:" + user);
                            error.push(eval("(" + req.responseText + ")").error.info);
                        }
		    }
                    if(wpMassBlockTag != "") {
                        var postdata = "format=json&action=edit&watchlist=nochange&title=User:" + encodeURIComponent(user) + "&summary=" + encodeURIComponent(wpMassBlockSummaryUser) 
                             + "&text=" + encodeURIComponent(wpMassBlockTag)
                             + "&token=" + encodeURIComponent(blocktoken);
                        var req = sajax_init_object();
                        req.open("POST", wgScriptPath + "/api.php", false);
                        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        req.setRequestHeader("Content-length", postdata.length);
                        req.send(postdata);
                        if(eval("(" + req.responseText + ")")['edit']) { userpageedited ++; }
                        else { //If not edited, add the title to the "failed" array and a description of the error to the "error" array.
                            failed.push("User talk:" + user);
                            error.push(eval("(" + req.responseText + ")").error.info);
                        }
		    }
                    document.getElementById("wpMassBlockSubmit").value = "(" + blocked + "/" + talkpageedited + "/" + userpageedited + ")";
                } else { //If not blocked, add the title to the "failed" array and a description of the error to the "error" array.
                    failed.push("Special:Block/" + user);
                    error.push(eval("(" + req.responseText + ")").error.info);
                }
            }
        }
        if(!users[i+1]) {
            document.getElementById("wpMassBlockSubmit").value = "(" + blocked + "/" + talkpageedited + "/" + userpageedited + ")";
            if(failed.length > 0) {
                var linkedList = "";
                for(x=0; x<failed.length; x++) {
                    linkedList += "<li><a href=\"" + wgScript + "?title=" + encodeURIComponent(failed[x]) + "\">" + failed[x] + "</a>: " + error[x] + "</li>"; //Links the titles in the "failed" array
                }
                document.getElementById("wpMassBlockFailedContainer").innerHTML += '<br /><b>Failed actions:</b><ul>' + linkedList + '</ul>';
            }
            
        }
    }
}
 
function massblockform() {
    var bodyContent = (skin == "cologneblue" ? "article" : "bodyContent");
    document.getElementsByTagName("h1")[0].textContent = "Tim's mass-blocking tool";
    document.title = "Tim's mass-blocking tool - Wikipedia, the free encyclopedia";
    document.getElementById(bodyContent).innerHTML = '<h3 id="siteSub">From Wikipedia, the free encyclopedia</h3><br /><br />'
        + '<form id="wpMassBlock" name="wpMassBlock">'
        + '<b>If you abuse this tool, it\'s <i>your</i> fault, not mine.</b>'
        + '<div id="wpMassBlockFailedContainer"></div>'
        + '<br /><br />'
            + 'Users to block (one on each line, please):<br />'
                + '<textarea tabindex="1" accesskey="," name="wpMassBlockUsers" id="wpMassBlockUsers" rows="10" cols="80"></textarea>'
            + 'Talk page message, if any (leave blank to leave no message):<br />'
                + '<textarea tabindex="2" accesskey="," name="wpMassBlockMessage" id="wpMassBlockMessage" rows="10" cols="80"></textarea>'
            + 'Replace user page text with (leave blank for no change):<br />'
                + '<textarea tabindex="3" accesskey="," name="wpMassBlockTag" id="wpMassBlockTag" rows="10" cols="80"></textarea>'
            + '<br /><br /><table style="background-color:transparent">'
                + '<tr><td>Common reasons:</td>'
                    + '<td><select id="wpMassBlockReasons">'
                        + '<optgroup label="Other reason">'
                            + '<option value="other">Other reason</option>'
                        + '</optgroup>'
                        + '<optgroup label="Common reasons">'
                            + '<option value="[[WP:Vandalism|Vandalism]]">[[WP:Vandalism|Vandalism]]</option>'
                            + '<option value="[[WP:Vandalism-only account|Vandalism-only account]] ">[[WP:Vandalism-only account|Vandalism-only account]] </option>'
                            + '<option value="Violations of the [[WP:BLP|Biographies of living persons policy]] ">Violations of the [[WP:BLP|Biographies of living persons policy]] </option>'
                            + '<option value="Using Wikipedia for [[WP:Spam|spam]] or [[WP:Advertising|advertising]] purposes">Using Wikipedia for [[WP:Spam|spam]] or [[WP:Advertising|advertising]] purposes</option>'
                            + '<option value="[[WP:Spam|Spam]] / [[WP:Advertising|advertising]]-only account">[[WP:Spam|Spam]] / [[WP:Advertising|advertising]]-only account</option>'
                            + '<option value="Creating [[WP:Attack page|attack]], [[WP:Patent nonsense|nonsense]] or other inappropriate pages">Creating [[WP:Attack page|attack]], [[WP:Patent nonsense|nonsense]] or other inappropriate pages</option>'
                            + '<option value="Creating [[WP:Copyright violations|copyright violations]]">Creating [[WP:Copyright violations|copyright violations]]</option>'
                            + '<option value="Abusing [[WP:Sock puppetry|multiple accounts]]">Abusing [[WP:Sock puppetry|multiple accounts]]</option>'
                            + '<option value="[[WP:Block#Evasion of blocks|Block evasion]]">[[WP:Block#Evasion of blocks|Block evasion]]</option>'
                            + '<option value="[[WP:No personal attacks|Personal attacks]] or [[WP:Harassment|harassment]]">[[WP:No personal attacks|Personal attacks]] or [[WP:Harassment|harassment]]</option>'
                            + '<option value="[[WP:Edit warring|Edit warring]]">[[WP:Edit warring|Edit warring]]</option>'
                            + '<option value="Violation of the [[WP:Three-revert rule|three-revert rule]] ">Violation of the [[WP:Three-revert rule|three-revert rule]] </option>'
                            + '<option value="[[WP:Disruptive editing|Disruptive editing]]">[[WP:Disruptive editing|Disruptive editing]]</option>'
                            + '<option value="[[WP:Long-term abuse|Long-term abuse]]">[[WP:Long-term abuse|Long-term abuse]]</option>'
                            + '<option value="Making [[WP:No legal threats|legal threats]]">Making [[WP:No legal threats|legal threats]]</option>'
                            + '<option value="[[WP:Arbitration enforcement|Arbitration enforcement]]">[[WP:Arbitration enforcement|Arbitration enforcement]]</option>'
                            + '<option value="Deliberately triggering the [[WP:Edit filter|Edit filter]]">Deliberately triggering the [[WP:Edit filter|Edit filter]]</option>'
                            + '<option value="Revoking talk page access: inappropriate use of user talk page while blocked">Revoking talk page access: inappropriate use of user talk page while blocked</option>'
                            + '<option value="\{\{anonblock}}">\{\{anonblock}}</option>'
                            + '<option value="\{\{schoolblock}}">\{\{schoolblock}}</option>'
                            + '<option value="\{\{blocked proxy}}">\{\{blocked proxy}}</option>'
                            + '<option value="\{\{usernameblock}}">\{\{usernameblock}}</option>'
                            + '<option value="\{\{UsernameHardBlocked}}">\{\{UsernameHardBlocked}}</option>'
                            + '<option value="\{\{spamusernameblock}}">\{\{spamusernameblock}}</option>'
                            + '<option value="\{\{softerblock}}">\{\{softerblock}}</option>'
                            + '<option value="\{\{checkuserblock}}">\{\{checkuserblock}}</option>'
                            + '<option value="\{\{Tor}}">\{\{Tor}}</option>'
                            + '<option value="\{\{Vaublock}}">\{\{Vaublock}}</option>'
                        + '</optgroup>'
                    + '</select></td></tr>'
            + '<tr><td>Other/additional reason:</td>'
                + '<td><input type="text" id="wpMassBlockReason" name="wpMassBlockReason" maxlength="255" /></td></tr>'
            + '<tr><td>Expiration time (blank for indefinite):</td>'
                + '<td><input type="text" id="wpMassBlockExpiry" name="wpMassBlockExpiry" maxlength="255" /></td></tr>'
            + '<tr><td>Edit summary for talk page edit:</td>'
                + '<td><input type="text" id="wpMassBlockSummaryTalk" name="wpMassBlockSummaryTalk" maxlength="255" /></td></tr>'
            + '<tr><td>Edit summary for user page edit:</td>'
                + '<td><input type="text" id="wpMassBlockSummaryUser" name="wpMassBlockSummaryUser" maxlength="255" /></td></tr>'
                + '<tr><td>Block anonymous users only (IPs only):</td><td><input type="checkbox" id="wpMassBlockAnononly" name="wpMassBlockAnononly" /></td></tr>'
                + '<tr><td>Enable autoblock (accounts only):</td><td><input type="checkbox" id="wpMassBlockAutoblock" name="wpMassBlockAutoblock" checked="checked" /></td></tr>'
                + '<tr><td>Block account creation:</td><td><input type="checkbox" id="wpMassBlockNocreate" name="wpMassBlockNocreate" checked="checked" /></td></tr>'
                + '<tr><td>Block email:</td><td><input type="checkbox" id="wpMassBlockEmail" name="wpMassBlockEmail" /></td></tr>'
                + '<tr><td>Remove talk page access:</td><td><input type="checkbox" id="wpMassBlockTalkpage" name="wpMassBlockTalkpage" /></td></tr>'
                + '<tr><td>Override existing blocks:</td><td><input type="checkbox" id="wpMassBlockReblock" name="wpMassBlockReblock" checked="checked" /></td></tr>'
                + '<tr><td><input type="button" id="wpMassBlockSubmit" name="wpMassBlockSubmit" value="Block" onclick="doMassBlock()" /></td></tr>'
        + '</form>';
    document.getElementById("wpMassBlockReasons").onchange = function() {
        var maxlength = (document.getElementById("wpMassBlockReasons").value == "other" ? 255 : 253-document.getElementById("wpMassBlockReasons").value.length); //It's 25e because of the two characters (": ") in addition to the selected summary.
        document.getElementById("wpMassBlockReason").setAttribute("maxlength", maxlength);
    }
}
 
if(wgNamespaceNumber == -1 && (wgPageName == "Special:Massblock" || wgPageName == "Special:MassBlock") && /sysop/.test(wgUserGroups)) $(massblockform);