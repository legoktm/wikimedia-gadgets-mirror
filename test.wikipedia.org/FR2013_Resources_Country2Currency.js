<script>
/**
 * NOTE: The following currency mapping is WMF-specific based on payment
 * provider availability, NOT necessarily the official currency of the country
 */
function getCurrency() {
    switch(Geo.country) {
        // Big 5 at the top for speed
        case "US": return "USD";
        case "CA": return "CAD";
        case "GB": return "GBP";
        case "AU": return "AUD";
        case "NZ": return "NZD";
        // Euro countries
        case "AD":
        case "AL":
        case "AM":
        case "AT":
        case "AW":
        case "AZ":
        case "BE":
        case "BY":
        case "CI":
        case "CY":
        case "DE":
        case "EE":
        case "ES":
        case "FI":
        case "FR":
        case "GF":
        case "GR":
        case "IE":
        case "IT":
        case "LT":
        case "LU":
        case "LV":
        case "LY":
        case "MC":
        case "ME":
        case "MG":
        case "MT":
        case "NL":
        case "PT":
        case "RE":
        case "RS":
        case "SI":
        case "SK":
        case "SM":
        case "SR":
        case "VA":
            return "EUR";
        // The rest
        case "AE": return "AED";
        case "AR": return "ARS";
        case "BA": return "BAM";
        case "BB": return "BBD";
        case "BD": return "BDT";
        case "BG": return "BGN";
        case "BH": return "BHD";
        case "BM": return "BMD";
        case "BO": return "BOB";
        case "BR": return "BRL";
        case "BZ": return "BZD";
        case "CH": return "CHF";
        case "CK": return "NZD";
        case "CL": return "CLP";
        case "CN": return "CNY";
        case "CO": return "COP";
        case "CR": return "CRC";
        case "CZ": return "CZK";
        case "DK": return "DKK";
        case "DO": return "DOP";
        case "DZ": return "DZD";
        case "EG": return "EGP";
        case "FJ": return "FJD";
        case "FO": return "DKK";
        case "GL": return "DKK";
        case "GT": return "GTQ";
        case "HK": return "HKD";
        case "HN": return "HNL";
        case "HR": return "HRK";
        case "HU": return "HUF";
        case "ID": return "IDR";
        case "IL": return "ILS";
        case "IN": return "INR";
        case "IS": return "ISK";
        case "JM": return "JMD";
        case "JO": return "JOD";
        case "JP": return "JPY";
        case "KE": return "KES";
        case "KI": return "AUD";
        case "KR": return "KRW";
        case "KZ": return "KZT";
        case "LB": return "LBP";
        case "LI": return "CHF";
        case "LK": return "LKR";
        case "MA": return "MAD";
        case "MK": return "MKD";
        case "MV": return "MVR";
        case "MW": return "GBP";
        case "MX": return "MXN";
        case "MY": return "MYR";
        case "NI": return "NIO";
        case "NO": return "NOK";
        case "NP": return "INR";
        case "NR": return "AUD";
        case "OM": return "OMR";
        case "PA": return "PAB";
        case "PE": return "PEN";
        case "PG": return "AUD";
        case "PH": return "PHP";
        case "PK": return "PKR";
        case "PL": return "PLN";
        case "PY": return "PYG";
        case "QA": return "QAR";
        case "RO": return "RON";
        case "RU": return "RUB";
        case "SA": return "SAR";
        case "SD": return "GBP";
        case "SE": return "SEK";
        case "SG": return "SGD";
        case "TH": return "THB";
        case "TM": return "RUB";
        case "TN": return "TND";
        case "TR": return "TRY";
        case "TT": return "TTD";
        case "TW": return "TWD";
        case "UA": return "UAH";
        case "UY": return "UYU";
        case "UZ": return "RUB";
        case "VE": return "VEF";
        case "VN": return "VND";
        case "VU": return "AUD";
        case "ZA": return "ZAR";
        // small multi-country currencies
        case "CW":
        case "SX":
            return "ANG";
        case "AG":
        case "DM":
        case "GD":
        case "KN":
        case "LC":
            return "XCD";
        case "BJ":
        case "BF":
        case "CI":
        case "GW":
        case "ML":
        case "NE":
        case "SN":
        case "TG":
            return "XOF";
        case "PF":
        case "NC":
        case "WF":
            return "XPF";
        // fall back to USD
        default:
            return "USD";
    }
}
</script>