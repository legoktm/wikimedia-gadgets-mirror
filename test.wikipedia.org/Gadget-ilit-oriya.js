/**
 * Beginning of Transliteration Tool
 * Author: Vaibhav Jain [[user:Vibhijain]]
 * added date: 2011-05-30
 */
importScriptURI('http://ilit.microsoft.com/bookmarklet/script/Oriya.js');

function transetup(event) {
	setDefaultSchmeIndex(readCookie("transToolIndex"));

	transliterate('searchInput', 'wpTextbox1', 'wpSummary', 'searchText', 'powerSearchText', 'wpNewTitle', 'wpReason', 'nsfrom', 'username', 'mwProtect-reason', 'nsto','wpText',  'wpUploadDescription', 'wpDestFile');
	addTransliterationOption( 'searchText', 'powerSearchText', 'wpNewTitle', 'wpReason', 'nsfrom', 'username', 'mwProtect-reason', 'nsto','wpText', 'wpUploadDescription', 'wpDestFile');
	transettings.checkbox.position = "before";
	addTransliterationOption( 'wpTextbox1', 'wpSummary' );
	addOptionsToSimpleSearch();
	initMultiScheme();
	translitStateSynWithCookie('searchInput', 'wpTextbox1', 'wpSummary', 'searchText', 'powerSearchText', 'wpNewTitle', 'wpReason', 'nsfrom', 'username', 'mwProtect-reason', 'nsto','wpText');
}