// importScript('User:Jim Carter/common.js');
// importScript('User:Jim Carter/vector.js');
// importScript('User:Jim Carter/common1.js');
if (mw.config.get('wgPageName') == 'User:Jim_Carter/Leadership_Development_Dialogue') {

$('#button1').click(function(){
    $('#tour').delay(2000).show(2000);
    $('#LeDe').hide(2000);
    $(".element").typed({
        strings: ["Hello, <span style=color:green>"+wgUserName+"</span>!^3000","<span style=color:black;font-size:35px;>Introduction:</span>"," <span style=font-size:35px;>F</span>or every leader one might identify,<br/> there are dozens within the Wikimedia movement who might<br/> also step up to lead, if given access to the right resources and encouragement.^3000","The Wikimedia Foundation and movement affiliates</br>already support and collaborate with leaders, mentors, and guides in the Wikimedia movement,</br>but there are also many people throughout the movement who don’t get direct support</br>for their leadership development activities from the Wikimedia Foundation or from movement affiliates.^3000","<span style=font-size:35px;>T</span>he Community Engagement department needs your ideas about:^1200 <br/> (1) <strong>How we can design opportunities for leaders that are inclusive,<br/>to help leaders develop skills and mentor one another.</strong> </br> (2) <strong>How we can describe leaders in our movement.^3000</strong> ","Your input will be included in upcoming training events such as <a href=\"https://meta.wikimedia.org/wiki/Category:Learning_Day_events#Learning_Days\">Learning Days</a>,</br>and also may be be used to design or expand leadership development activities</br>supported by the Wikimedia Foundation.^3000","<strong><span style=font-size:35px;>T</span>here are many ways to^1500 participate!</strong>","<span style=font-size:35px;>S</span>hare a new <span style=font-size:35px;>idea</span>, add to or <strong>comment</strong> on an existing idea, ask a question, or vote on words and labels :)^3000</br>We need feedback in two areas:^3000","(1)<strong> How can we design opportunities in a way that better supports leaders to learn from one another?</strong>^3000</br><span style=font-size:15px;color:grey;><p>The first aim of this consultation is to gain community input on how to design a peer academy for community members </br>who would like to guide their communities and mentor fellow Wikimedians. The peer academy would be a series of </br> regular events for community leaders that want to improve the skills they need to lead.<p></br/>Just as a university has several schools, degree tracks, and courses, in which all learners are enrolled but take part in <br/>different classes based on their various pathways; people in our movement have different interests and activities. We </br>would like to connect learners across different interests and activities, to communicate about opportunities for </br>leadership development, and be transparent about how Wikimedians can take part in this program. In turn, these </br>learners will become peer mentors when they learn ways to share their leadership skills with others in their </br>communities.^3000</span></br><p> <button class=\"mw-ui-button mw-ui-constructive\"><a href=\"User_talk:Jim Carter/Leadership_Development_Dialogue\" style=color:white;text-decoration:none;>Comment on talk page?</a></button>&emsp;&emsp;"/*<button class=\"mw-ui-button mw-ui-constructive\" onclick=\"conFunction()\">Continue reading?</button>"*/],
        typeSpeed: 10,
        backSpeed: -115,
        backDelay: 500,
        startDelay: 4000,
        showCursor: false
      });
});

function conFunction() {
	$(".element").hide(2000);
	$("#backG").delay(16000).show(2000);
	$(".element2").typed({
		strings: ["(2)<strong> How do we understand and describe peer mentoring and leadership in our movement?</strong>^3000<br/><span style=font-size:15px;color:grey;><p>We call community members who guide their communities and mentor their fellow Wikimedians “leaders”, but the word<br/>“leader” does not always translate well between languages and cultures. A second aim of this consultation is to gain</br>broader input on the language we use to talk about \"leader\", and to better understand what makes a leader in the </br>Wikimedia movement.<p></br>Small focus groups over the last year have shown that many community members share a similar basic understanding</br>of the leadership traits we want to encourage in our movement. If we can better understand what traits our leaders</br>share, we can encourage and develop these characteristics in new leaders. This could strengthen communities and </br>help them become more sustainable.^3000","<span style=font-size:35;color:grey>Thank you! Hope to hear your opinion at the talk page :)^3000</span></br><button class=\"mw-ui-button\" onclick=\"oriFunction()\">Read normally</button>"],
		typeSpeed: 50,
        backSpeed: -15,
        backDelay: 500,
        startDelay: 2000,
        showCursor: false
	});
}

function oriFunction() {
	$("#original").fadeIn();
	$("#full").hide();
}

$('#button2').click(function(){
	location.href = "//meta.wikimedia.org/wiki/Community_Engagement";
});

$("#button3").click(function(){
	$("#original").fadeIn();
	$("#full").hide();
});

} 
/* $(function(){
      $(".element").typed({
        strings: ["First sentence.", wgUserName+" is something we always say."],
        typeSpeed: 160,
        backDelay: 5000,
        showCursor: false
      });
  });
*/