if (skin == 'monobook' || skin=='simple') {
 var bottomSidebarSheet = bottomSidebarAppendCSS('\
#column-content {float:none; margin-left:0}\
#column-content #content {margin-left:2px}\
#column-one {position:static; padding-top: 10px}\
#p-logo {display:none}\
#p-personal ul {text-align:left}\
#p-cactions {left:0}\
.portlet {float:left}\
#p-lang {float:right}\
#footer {	margin-left: 0}')
 hookEvent('load', bottomSidebarAttachSwitcher)
}
function bottomSidebarAttachSwitcher(){
 var link = document.createElement('a')
 link.href = 'javascript:bottomSidebarSwitch()'
 link.appendChild(document.createTextNode('<>'))
 link.style.cssText = 'color:#797; font-weight:bold; font-size:15px; left:0px; top:0px; padding:2px; z-index:100; position:absolute'
 document.body.appendChild(link)
}
function bottomSidebarSwitch(){
 bottomSidebarSheet.disabled = !bottomSidebarSheet.disabled
}
function bottomSidebarAppendCSS(text){
 var s = document.createElement('style')
 s.setAttribute('type', 'text/css')
 if (s.styleSheet) s.styleSheet.cssText = text //IE
 else s.appendChild(document.createTextNode(text))
 document.getElementsByTagName('head')[0].appendChild(s)
 if (s.sheet) s = s.sheet //IE needs style, Safari needs sheet, Firefox could do both
 return s
}