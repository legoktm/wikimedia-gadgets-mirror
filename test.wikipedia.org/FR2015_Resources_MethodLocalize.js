/** MediaWiki:FR2015/Resources/MethodLocalize.js
  * Display the correct payment methods for current country. Used by both desktop and mobile banners.
  */

// Countries with no credit card option (currently none)
var noCC = [];
if ($.inArray(Geo.country, noCC) !== -1) {
    $(".paymentmethod-cc").remove();
}

// Can't do monthly credit card in India or Brazil
if (Geo.country === 'IN' || Geo.country === 'BR') {
    $(".paymentmethod-cc").addClass("no-monthly");
}

// Remove any leftover WorldPay
$(".paymentmethod-cc-wp").remove();

// Countries using Adyen for credit card
if (Geo.country === 'FR' || Geo.country === 'IL') {
    $(".paymentmethod-cc").remove();
    $(".paymentmethod-cc-adyen").show();
} else {
    $(".paymentmethod-cc-adyen").remove();
}

// Countries with no PayPal option
var noPP = ['IN', 'RU', 'SG', 'AE', 'QA', 'JP', 'OM', 'BD', 'BO', 'PA',
            'PE', 'PY', 'GT', 'JM', 'TT', 'DZ'];
if ($.inArray(Geo.country, noPP) !== -1) {
    $(".paymentmethod-pp").remove();
    $(".paymentmethod-pp-usd").remove();
}

// Countries where PayPal must be in USD
var ppUSD = ['BG', 'HR', 'LT', 'MK', 'RO', 'UA', 'SA', 'CN', 'ID', 'KR',
             'KZ', 'MY', 'VN', 'AR', 'CL', 'DO', 'CO', 'NI', 'UY', 'ZA',
             'BH', 'LB', 'VE', 'TR', 'IS', 'BA', 'MV', 'BB', 'BM', 'BZ',
             'CR', 'CW', 'SX', 'HN', 'KN', 'DM', 'AG', 'LC', 'GD', 'FJ',
             'TN', 'BJ', 'BF', 'CI', 'GW', 'ML', 'NE', 'SN', 'TG', 'BR'];
if ($.inArray(Geo.country, ppUSD) !== -1) {
    $(".paymentmethod-pp").remove();
    $(".paymentmethod-pp-usd").show();
} else {
    $(".paymentmethod-pp").show();
    $(".paymentmethod-pp-usd").remove();
}

// Show any extra local payment methods, or remove them if not needed
var extrapaymentmethods = {
    'amazon'     : ['US'],
    'bpay'       : ['AU'],
    'ideal'      : ['NL'],
    'yandex'     : ['RU'],
    'webmoney'   : ['RU'],
    'dd'         : ['AT', 'DE', 'ES', 'NL'],
    'enets'      : ['SG'],
    'sofort'     : [],
    'bt'         : ['AR', 'BR', 'CL', 'CO'],      // Bank Transfer (Astropay)
    'cash'       : ['AR', 'BR', 'CO', 'MX', 'UY'] // "Cash" methods (Astropay)
};

// Methods with different labels per country
if (Geo.country === 'AR') {
    $('.paymentmethod-bt button, button.paymentmethod-bt').html('Santander');
}
if (Geo.country === 'CO') {
    $('.paymentmethod-bt button, button.paymentmethod-bt').html('PSE Pagos');
}
if (Geo.country === 'UY') {
    $('.paymentmethod-cash button, button.paymentmethod-cash').html('RedPagos');
}
if (Geo.country === 'CL' && mw.config.get('wgUserLanguage') == 'es') {
    $('.paymentmethod-bt button, button.paymentmethod-bt').html('Trans. bancaria');
}

for (var method in extrapaymentmethods) {
    var $methodbutton = $('.paymentmethod-' + method);
    if ( $.inArray(Geo.country, extrapaymentmethods[method]) !== -1 ) { // country is in the list
        $methodbutton.show();
    } else {
        $methodbutton.remove();
    }
}