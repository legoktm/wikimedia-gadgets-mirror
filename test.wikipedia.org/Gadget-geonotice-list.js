/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Defines the list of notices to be shown to registered users based on their location
 *
 * Format is:
 *
 * ID: {
 *   begin: 'date',
 *   end: 'date',
 *   corners: [ [ lat, lon ], [ lat, lon ] ],
 *   text: 'message'
 * }
 *
 * There also is an option to use country instead of corners. For example:
 * ID: {
 *   begin: 'date',
 *   end: 'date',
 *   country: 'US',
 *   text: 'message'
 * }
 *
 * See http://dev.maxmind.com/geoip/legacy/codes/iso3166 for full list of country codes
 *
 * - There must be commas between the notices, but not after the last one.
 * - BE SURE TO ESCAPE YOUR APOSTROPHES (put a backslash in front of it, like this: \')!
 * - Use [[meta:w:en:link|link]] format for internal links, so that they work properly
 * on the other projects where the geonotice is displayed.
 */

window.GeoNotice = {
	i18n: {
		'gn-hideButton': 'Hide'
	}
};
window.GeoNotice.notices = {

NYCAprilwikiwed:
{ begin : '13 April 2015 0:00 UTC',
  end : '30 April 2015 0:00 UTC',
  corners : [ [42,-76], [40,-72] ],
  text : "Join the [[Wikipedia:Meetup/NYC|WikiWednesday Salon and Skill-Share NYC]] by Union Square at 7PM on April 29!"
},

UK20150427: /* last 8 chars of ID is date of last amendment in CCYYMMDD format - change this if making major amendment or adding a meetup; leave alone if minor amendment or removing a meetup */
{
   begin: '26 April 2015 17:00 UTC', /* yesterday's date - amend only if the ID was altered */
   end: '10 May 2015 17:00 UTC', /* set this to date of last meetup shown */
   corners:[ [60,-9], [50, 2] ],
   text: 'Interested in having a chat with fellow Wikipedians? There is a forthcoming meetup in: [[m:Meetup/London/93|London, 10 May]]!' /* try to limit this to four meetups, no more than one per town/city, and no more than four weeks in advance; shorten month names to three letters if four meetups are shown */
},

Ok150429:
{
	 begin: '10 April 2015 17:00 UTC',
	 end: '30 April 2015 17:00 UTC',
	 corners:[ [38.116084, -98.950281], [35.181555, -95.203943] ],
	 text: 'Want to help cover Artists from Oklahoma? Join us at Oklahoma State University by noting [[Wikipedia:Meetup/Stillwater/Oklahoma State University Edit-A-Thon 2015|joining us on the signup page]].'
},

VT150530:
{
	 begin: '29 April 2015 17:00 UTC',
	 end: '30 May 2015 17:00 UTC',
	 corners:[ [42.214604, -73.364688], [43.713651, -71.309096] ],
	 text: 'Interested in meeting other Wikipedians? Want to learn about more ways to be engaged in the Wikimedia community? Help us schedule a Wikipedia meetup during the week of May 24-30 by [[Wikipedia:Meetup/Brattleboro|responding here]].'
},
TEST:
{
   begin: '01 January 2002 00:00 UTC',
   end: '31 December 2100 00:00 UTC',
   corners:[ [-1000,-1000], [1000, 1000] ],
   text: 'This is a demonstration of the [[en:Wikipedia:Geonotice|geonotice]] [[en:WT:Gadget|gadget]].'
}

/*******************************************
 *             End of list.
 *           Edit only above!
 *
 *******************************************/
};