/**
 * Upload form of ptwikipdia
 *
 * @author: [[pt:User:Danilo.mac]]
 * @author: [[pt:User:Herder.wiki]]
 * @author: [[pt:User:!Silent]]
 */
/*jslint browser: true, white: true, devel: true, continue: true, regexp: true, plusplus: true, forin: true */
/*global mediaWiki, jQuery */

( function( mw, $ ) {
'use strict';

var uf = {},
	file = ( mw.config.get( 'wgUserLanguage' ) !== 'pt' ? 'arquivo' : 'ficheiro' );

/**
 * The fields of form
 *
 * @property: {rows: number, cols: number, desc: string, tip: string, defaulText: string (optional), list: array}
 */
uf.fields = [ {
		rows: 3,
		cols: 80,
		desc: 'Descrição:',
		tip: 'Descreva brevemente a imagem.'
	}, {
		rows: 2,
		cols: 80,
		desc: 'Fonte:',
		tip: 'Qual é a fonte deste ' + file + '?'
	}, {
		rows: 2,
		cols: 80,
		desc: 'Autor:',
		tip: 'Quem criou? Se mostra alguma obra artística, quem a criou?'
	}, {
		rows: 2,
		cols: 80,
		desc: 'Direitos:',
		tip: 'Quem é o detentor dos direitos de autor?'
	}, {
		cols: 50,
		desc: 'Artigo/anexo:',
		tip: 'Para qual artigo ou anexo da Wikipédia essa imagem é necessária?'
	}, {
		cols: 50,
		desc: 'Integral ou parte:',
		tip: 'Essa é a obra integral citada na fonte ou parte dela?'
	}, {
		desc: 'Código do carregamento:',
		list: [ ' - ', [
			'1.1 - Pessoas e institucionais',
			'1.2 - Acontecimentos',
			'1.3 - Produtos',
			'1.4 - Logotipos, marcas, símbolos e cartazes',
			'1.5 - Ícones',
			'1.6 - Selos e moedas',
			'1.7 - Fauna e flora',
			'1.8 - Mapas',
			'1.9 - Material de divulgação',
			'1.10 - Capturas de ecrã ou telas e artes visuais',
			'1.11 - Personagens',
			'1.12 - Imagens microscópicas em geral',
			'2 - Som',
			'3 - Texto'
		] ],
		tip: 'Sob qual ponto da política de uso restrito este ' + file + '?'
	}, {
		rows: 2,
		cols: 60,
		desc: 'Propósito:',
		tip: 'Qual é a importância para o artigo/anexo?',
		defaultText: 'Prover informação visual indispensável para a compreensão do artigo.'
	}, {
		rows: 2,
		cols: 60,
		desc: 'Insubstituível:',
		tip: 'Por que não existe material semelhante sob licença livre? Você procurou? Onde?',
		defaultText: 'Não há versões da imagem sob licença livre.'
	}, {
		rows: 3,
		cols: 60,
		desc: 'Outras informações:',
		tip: 'Se tiver alguma consideração adicional coloque aqui.',
		optional: true
	}, {
		cols: 20,
		desc: 'Permissão:',
		tip: 'Insira a permissão correta',
		optional: true
	}, {
		desc: 'Licença: ',
		condition: $.inArray( mw.util.getParamValue( 'fonte' ), [ 'prop', 'flickr' ] ) !== -1,
		list: [ ' = ', [
			'não = ©Todos os direitos reservados',
			'cc-by-nd = Creative Commons Attribution No-Derivs',
			'cc-by-nc = Creative Commons Attribution Non-Commercial',
			'cc-by-nc-sa = Creative Commons Attribution Non-Commercial Share-Alike',
			'cc-by-nc-nd = Creative Commons Attribution Non-Commercial No-Derivs'
		] ],
		tip: 'Sob qual licença esse ' + file + ' está sendo carregado?'
	}
];

/**
 * Creates the form
 */
uf.setupForm = function() {
	var i, j, temp, waitWikEdLoad, currentField, tagType,
		$loading = $( '#mw-upload-form' ),
		$table = $( '#mw-htmlform-description tbody' );

	//Remove default fields and wikEdInputWrapper
	$( '.mw-htmlform-field-HTMLTextAreaField' ).remove();
	$( '.mw-htmlform-field-Licenses' ).remove();
	$( '.mw-editTools' ).parent().parent().remove();
	$( '#editpage-specialchars' ).parent().parent().parent().remove();

	$( '<input />', {
		'type': 'hidden',
		'name': 'wpUploadDescription',
		'id': 'wpUploadDescription'
	} ).appendTo( $table );

	$loading.submit( ( function( oldSubmit ) {
		return function () {
			// First, let's construct the information template
			var	doSubmit = uf.upload();

			// Then call whatever onsubmit hook already was present. If this is HotCat's submission
			// handler, it will add the categories to the 'wpUploadDescription' field.
			if ( doSubmit && oldSubmit ) {
				if ( typeof oldSubmit === 'string') {
					doSubmit = eval( oldSubmit );
				} else if ( $.isFunction( oldSubmit ) ) {
					doSubmit = oldSubmit.apply( $loading[ 0 ], arguments );
				}
			}

			return doSubmit;
		};
	}( $loading[ 0 ].onsubmit ) ) );

	//Add the fields
	for ( i = 0; i < uf.fields.length; i++ ) {
		currentField = uf.fields[ i ];

		if ( currentField.condition !== undefined
			&& !currentField.condition
		) {
			continue;
		}

		if ( currentField.list ) {
			$( '<tr></tr>' ).append(
				$( '<td class="mw-label"></td>' ).append(
					$( '<label class="uf-label" for="uf-field-' + i + '"></label>' ).html( currentField.desc )
				),
				$( '<td class="mw-input"></td>' ).append(
					$( '<select class="uf-field" id="uf-field-' + i + '"></select>' )
						.append( $( '<option></option>' ) )
				)
			).appendTo( $table );

			for ( j = 0; j < currentField.list[ 1 ].length; j++ ) {
				temp = currentField.list[ 1 ][ j ].split( currentField.list[ 0 ] );

				$( '<option></option>' )
					.val( temp[ 0 ] )
					.html( currentField.list[ 0 ] === ' - '
							? currentField.list[ 1 ][ j ]
							: temp[ 1 ]
					).appendTo( $( '#uf-field-' + i  ) );
			}
		} else {
			tagType = ( !currentField.rows || currentField.rows === 1 ) ? 'input' : 'textarea';

			$( '<tr></tr>' ).append(
				$( '<td class="mw-label"></td>' ).append(
					$( '<label class="uf-label" for="uf-field-' + i + '"></label>' ).html( currentField.desc + ' ' )
				),
				$( '<td class="mw-input"></td>' ).append(
					$( '<' + tagType + '></' + tagType + '>' )
						.css( 'width', 'auto' )
						.attr( {
							'class': 'uf-field',
							'id': 'uf-field-' + i,
							'rows': currentField.rows || 1
						} ).attr( ( tagType === 'input' ? 'size' : 'cols' ), currentField.cols )
				)
			).appendTo( $table );

			if ( currentField.defaultText ) {
				$( '#uf-field-' + i ).val( currentField.defaultText );
			}
		}

		if ( currentField.tip ) {
			$( '#uf-field-' + i ).parent().append(
				$( '<div></div>' )
					.css( {
						'font-size': 'smaller',
						'minHeight': '25px'
					} )
					.html( currentField.tip )
			);
		}
	}
};

/**
 * Collect the image information e append in inpu#wpUploadDescription
 */
uf.upload = function () {
	var $this,
		text = '{' + '{Informação\n';

	$( '.uf-field' ).each( function( i ) {
		$this = $( this );

		if ( !uf.fields[ i ].optional && $this.val() === '' ) {
			$this.addClass( 'uf-fill-field' );
		} else {
			$this.removeClass( 'uf-fill-field' );
		}

		text += '| '
			+ ( uf.fields[ i ]
				? uf.fields[ i ].desc
				: $( '.uf-label' ).eq( i ).html()
			).replace( /[\/():].*/g, '' ).toLowerCase()
			+ ' = '
			+ $this.val()
			+ '\n';
	} );

	if ( $( '.uf-field' ).hasClass( 'uf-fill-field' ) ) {
		alert( 'Existem campos obrigatórios que não foram preenchidos.' );

		return false;
	}

	text += '}' + '}\n';

	$( '#wpUploadDescription' ).val( text );

	return true;
};

if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Upload' ) {
	$( uf.setupForm );

	//FIXME: move this to definiton of gadget
	mw.loader.load( '//commons.wikimedia.org/w/index.php?title=MediaWiki:Gadget-HotCat.js&action=raw&ctype=text/javascript&smaxage=21600&maxage=86400' );
}

}( mediaWiki, jQuery ) );