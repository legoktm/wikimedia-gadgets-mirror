//dynamically load Commons-style Edittools
function createEdittoolsLink(){
 if(window.doNotWantEditTools) return;
 //get div.mw-editTools
 var box = document.getElementById('wpTextbox1')
 while (box && box.className!='mw-editTools') box=box.nextSibling
 if (!box) return
 //create a link
 var lnk = document.createElement('a')
 lnk.href =  'javascript:loadCommonsTools()'
 lnk.title = 'Load Commons-style Edittools' 
 lnk.id = 'loadCommonsEdittoos'
 lnk.appendChild(document.createTextNode('[load edittools]'))
 lnk.style.cssText = 'float:right'
 box.appendChild(lnk)
}
function loadCommonsTools(){
 importScript('MediaWiki:Scripts/Edittools1.js')
 var lnk = document.getElementById('loadCommonsEdittoos')
 if (lnk) lnk.parentNode.removeChild(lnk)
}
$(createEdittoolsLink)


//Add Insert Buttons
function addInsertButton(img, speedTip, tagOpen, tagClose, sampleText){
 mwCustomEditButtons[mwCustomEditButtons.length] =
 {'imageFile': '//upload.wikimedia.org/' + img,
  'speedTip': speedTip,
  'tagOpen': tagOpen,
  'tagClose': tagClose,
  'sampleText': sampleText};
}

addInsertButton('wikipedia/commons/c/c8/Button_redirect.png','Redirect','#Redirect [[',']]',''); 
addInsertButton('wikipedia/commons/2/2a/Button_category_plus.png','Category','[[Category:',']]','');