// <source lang="javascript">
 
if (wgNamespaceNumber == 0 && wgAction == 'edit') {
    $(document).ready(function () {
		var text = document.getElementById ('wpTextbox1');
		if (!text) return;
		// Xoá thẻ "Bài do bot tạo"
		text.value = text.value.replace (/\{\{[bB]ài do bot tạo[^}]*\}\}\n*/g, "")
					  // Xoá khoảng trắng ở đầu và cuối trang
					  .replace(/^\s*((?:.|\n)+?)\s*$/, "$1");
 
		// Xoá khoảng trắng ở đầu và cuối trang
		text.value = $.trim( text.value || "" );
 
		// Set edit summary
                var add = "Đã kiểm tra bài do bot tạo";
                var summary = document.getElementById ('wpSummary');
                if (!summary) return;
                var val = $.trim( summary.value || "" );
                if (val.indexOf(add) != -1) return;
                summary.value = (val.length == 0) ? add : val + " " + add;
	});
}
// </source>