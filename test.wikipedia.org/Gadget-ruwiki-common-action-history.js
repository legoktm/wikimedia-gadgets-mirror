$( function() {
	// Добавить ссылки «Вернуть к» для автопатрулируемых, патрулирующих и администраторов
	var wgUserGroups = mw.config.get( 'wgUserGroups' );
	if( wgUserGroups && /editor|sysop/.test( wgUserGroups.join( '|' ) ) ) {
		addSubLink( '<a title="Добавить ссылки для возврата к старым версиям">Вернуть к ...</a>', 'HistoryRevertTo' );
	}
	
	// Функция для добавления ссылки в меню под заголовком
	function addSubLink(aa, toolname){
		$('#contentSub').append( ' · ', 
			$(aa)
				.attr('id',toolname).attr('href','#')
				.css('font-style','italic')
				.click ( function (e) {
					e.preventDefault();
					importMW( 'Tool/' + this.id);
				} )
		);
	}
	
	// Изменить отображение ссылок «Обновлено с моего последнего посещения»
	var upd = $('span.updatedmarker');
	upd
		.attr('title', upd.eq(0).text())
		.text('☆')
		.css('background','#bfb')
		.show();
} );