/*
 * Guided Tour to test guided tour features.
 */
 
// Copy the next two lines into your tour.
( function ( window, document, $, mw, gt ) {
 
// Declare a variable for use later
var pageName = 'Help:Guided tours/guider';
 
gt.defineTour( {
        /*
         * This is the name of the tour.  It must be lowercase, without any hyphen (-) or
         * period (.) characters.
         *
         * The page where you save an on-wiki tour must be named
         * MediaWiki:Guidedtour-tour-{name}.js , in this example MediaWiki:Guidedtour-tour-mytest.js
         */
        name: 'mytest',
 
        /*
         * Information defining each tour step, in order.
         */
        steps: [ {
                /*
                 * Show overlay at start of tour
                 */
 
                // Note that for on-wiki tours, we use title and description with the actual text.
                // The title appears in the title bar of the guider.
                title: 'Testing',
 
                // The description appears in the body
                description: 'This is a test of the description. Lorem ipsum dolor sit!',
 
                // This specifies that the guider appears in the center of the screen
                overlay: true,
                // This means there is just the okay button, which will go to
                // the next step unless the user ends the tour.
                buttons: [ {
                        action: 'next'
                } ]
        }, {
                /*
                 * Callout of left menu
                 */
                title: 'Test callouts',
                description: 'This is the community portal page.',
 
                // This positions the guider next to a page element, in this
                // case the portal link (which is "Community portal" on English
                // Wikipedia, but varies by site).
                // The string is a jQuery selector.  "#n-portal" means the HTML
                // element with this id attribute, and "a" means an a, or link,
                // element inside that.
                attachTo: '#n-portal a',
 
                // This means the guider shows to the right of the Community Portal link
                position: 'right',
 
                buttons: [ {
                        action: 'next'
                } ]
        }, {
                /*
                 * Test out mediawiki parsing
                 */
                title: 'Test MediaWiki parse',
                description: 'A guider in your on-wiki tour can contain wikitext using onShow and parseDescription. Use it to create a wikilink to the [[{{MediaWiki:Guidedtour-help-url}}|Guided tours documentation]]. Or an external link [https://github.com/tychay/mwgadget.GuidedTour to GitHub], for instance.',
 
                // This attaches to the search box, by ID.
                attachTo: '#searchInput',
                position: 'bottomRight',
 
                // This means the description will be parsed as wikitext before being shown.
                onShow: gt.parseDescription,
                buttons: [ {
                        action: 'next'
                } ]
        }, {
                /*
                 * Test out mediawiki description pages
                 */
                title: 'Test MediaWiki description pages',
 
                // In this case, we are putting a page name here, rather than text directly.
                description: pageName,
 
                overlay: true,
 
                // This means the wikitext will be loaded from the page name in the description field.
                onShow: gt.getPageAsDescription,
 
                buttons: [ {
                        // This makes a button which acts like a wikilink to 'Help:Guided tours/guider'
                        name: 'Go to description page',
                        onclick: function() {
                                window.location = mw.util.getUrl(pageName);
                                return false;
                        }
                }, {
                        // This makes the okay button on this step end the tour.
                        action: 'end'
                } ]
        } ]
} );
 
// The following should be the last line of your tour.
} (window, document, jQuery, mediaWiki, mediaWiki.guidedTour ) );