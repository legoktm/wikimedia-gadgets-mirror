/**
 * FastButtons's buttons list
 *
 * @see [[MediaWiki:Gadget-fastbuttons.js]]
 * @see [[MediaWiki:Gadget-fastbuttons.js/core.js]]
 * @class fastb.buttons
 */
/* jshint laxbreak:true */
/* global mediaWiki, jQuery, fastButtons, window */

( function ( mw, $, fastb, window ) {
'use strict';

mw.messages.set( {
	// General
	'fastb-FastButtons': 'FastButtons',
	'fastb-hideButton': 'Esconder',
	'fastb-hideButton-title': 'Esconde os botões do FastButtons. Para reavê-los na skin vector, basta clicar em "Mais".',
	'fastb-summarySufix': ', usando [[WP:FastButtons|FastButtons]]',
	'fastb-petScan': 'Procurar nesta categoria',
	'fastb-loading': 'Carregando...',
	'fastb-noRecentChange': 'Nenhuma alteração recente.',
	'fastb-noNewPage': 'Nenhuma página nova ainda não patrulhada ou que não seja um redirecionamento dentre as páginas criadas à no máximo, um mês.',
	'fastb-noScoredRecentChanges': 'Nenhuma edição para patrulhar ou desfazer.',
	'fastb-noER': 'Nenhuma página marcada para eliminação rápida.',
	'fastb-yes': 'Sim',
	'fastb-no': 'Não',
	'fastb-hours': '$1h$2min',
	'fastb-date': '$1 de $2 de $3', // $1 = day, $2 = month, $3 = year
	'fastb-none': 'Nenhum',
	'fastb-OK': 'OK',
	'fastb-cancel': 'Cancelar',
	'fastb-alreadyExistsEliminationTag': 'Já existe uma predefinição de eliminação nesta página.',
	'fastb-alreadyExistsThisTag': 'A predefinição "$1" já está incluída nesta página.',
	'fastb-alreadyExistsThisStub': 'Já existe um esboço nesta página.',
	'fastb-warning': 'Aviso',

	// Notify
	'fastb-notify-editSuccess': 'A página foi editada com sucesso.',
	'fastb-notify-moveSuccess': 'A página foi movida com sucesso.',
	'fastb-notify-sendWarning': 'Enviando a notificação para o criador...',
	'fastb-notify-editingPage': 'Editando a página...',
	'fastb-notify-patrollingPage': 'Patrulhando a página...',
	'fastb-notify-getPageContent': 'Obtendo o conteúdo da página...',
	'fastb-notify-creatingEliminationPage': 'Criando a página de eliminação...',
	'fastb-notify-editingSomePage': 'Editando a página "$1"...',
	'fastb-notify-archivingPE': 'Arquivando o pedido de eliminação anterior...',
	'fastb-notify-errorSufix': '<br />Se o problema persistir,'
		+ ' favor informar no <a href="' + mw.util.getUrl( 'WP:Café dos Programadores' ) + '">Café dos programadores</a>'
		+ ' ou em <a href="' + mw.util.getUrl( 'MediaWiki Discussão:Gadget-fastbuttons.js' ) + '">MediaWiki Discussão:Gadget-fastbuttons.js</a>.',
	'fastb-notify-apiErrorEdit': 'Não foi possível realizar a edição.<br />A API retornou o código de erro "$1": $2',
	'fastb-notify-editFail': 'Não foi possível realizar a edição devido a um erro desconhecido da API.',
	// Dialog
	'fastb-dialog-note': 'Se necessário, coloque uma observação.',
	'fastb-dialog-labelDefault': 'Observação',
	'fastb-dialog-placeholderDefault': 'Não é necessário assinar.',
	'fastb-dialog-pageSituation': 'Escreva um breve resumo das condições atuais da página',

	// Dialog [ESR prompt]
	'fastb-dialog-ESR-title-1': 'Qual a justificativa para a eliminação do arquivo?',
	'fastb-dialog-ESR-title-2': 'Eliminação semirrápida',
	'fastb-dialog-ESR-reason': 'Justificativa',
	'fastb-dialog-ESR-subject': 'Assunto',
	'fastb-dialog-ESR-other': 'Outro',
	'fastb-dialog-ESR-badTranslation': 'Má tradução',
	'fastb-dialog-ESR-VDA': 'Suspeita de violação dos direitos',
	'fastb-dialog-ESR-language': 'Idioma (somente a sigla)',
	'fastb-dialog-ESR-addComent': 'Comentário adicional',
	'fastb-dialog-ESR-sendWarning': 'Enviar um aviso para o criador da página',

	// Dialog [PE prompt]
	'fastb-dialog-PE-title': 'Página para eliminar',
	'fastb-dialog-PE-sendWarning': 'Enviar um aviso para o criador da página',
	'fastb-dialog-PE-type-1': 'Enviar um aviso de eliminação',
	'fastb-dialog-PE-type-2': 'Enviar um aviso de remoção de marcações de eliminação nas páginas',
	'fastb-dialog-PE-create': 'Criar a discussão para a eliminação da página',
	'fastb-dialog-PE-reason': 'Justificativa para eliminação. Não precisa assinar.',

	// Dialog [Requests prompt]
	'fastb-dialog-requests-argumentation': 'Argumentação',
	'fastb-dialog-requests-page': 'Página',
	'fastb-dialog-requests-user': 'Usuário',
	'fastb-dialog-requests-subject': 'Assunto',
	'fastb-dialog-requests-historyPageExtra': 'Página 2',
	'fastb-dialog-requests-dontSign': 'Não precisa assinar.',

	// Dialog [Merging prompt]
	'fastb-dialog-merging-title': 'Fusão',
	'fastb-dialog-merging-page1': 'Página que permanecerá',
	'fastb-dialog-merging-page2': 'Página que será fundida com a de cima',

	// Dialog [Maintenance prompt]
	'fastb-dialog-maintenance-title': 'Adicionar uma etiqueta de manutenção à página',
	'fastb-dialog-maintenance-sendWarn': 'Se possível, avisar ao criador a marcação da etiqueta na página.',
	'fastb-dialog-maintenance-sendWarnCases': 'Somente nos casos em que apenas o criador editou a página, e no caso dele ser um IP, somente se a página tiver sido criada a no máximo 24 horas.',

	// Page
	'fastb-page-ref': '$1 está referênciada',
	'fastb-page-cat': '$1 está categorizada',
	'fastb-page-iw': '$1 possui interwikis',
	'fastb-page-it': 'A página',
	'fastb-page-backlinks': 'Afluentes da página',
	'fastb-page-noBacklinks': 'Esta página ainda não possui afluentes',
	'fastb-page-quality': 'Qualidade',
	'fastb-page-qualityUnknown': 'desconhecida',
	'fastb-page-deletedEdit': 'edição eliminada',
	'fastb-page-deletedEdits': 'edições eliminadas',
	'fastb-page-size': 'Tamanho',
	'fastb-page-watchers': 'Vigilantes',
	'fastb-page-lastEdit': 'Última edição',
	'fastb-page-requestDeletion': 'Pedido de eliminação',
	'fastb-page-requestDeletionLink': 'Pedido de eliminação desta página',
	'fastb-page-neverProposedElimination': 'A página nunca foi proposta para eliminação',
	'fastb-page-notExist': 'A página não existe.',
	'fastb-page-pageDeleteDate': 'A página foi deletada às <b>$1</b>',
	'fastb-page-moreInfo': 'Mais informações',
	'fastb-page-log': 'Registros da página',
	'fastb-page-noEditPermission': 'Você não possui permissão para editar esta página.',

	// Page [move]
	'fastb-page-move-noPermissions': 'Você não possui permissão para mover esta página.',
	'fastb-page-move-moving': 'Movendo a página',
	'fastb-page-move-redirect': 'Criar um redirecionamento',
	'fastb-page-move-talk': 'Mover a página de discussão também, se aplicável',
	'fastb-page-move-reason': 'Motivo',
	'fastb-page-move-newTitle': 'Novo título',
	'fastb-page-move-buttonName': 'Mover a página',
	'fastb-page-move-buttonTitle': 'Move a página para o título informado',
	'fastb-page-move-mainDomain': '(Principal)',

	// User
	'fastb-user-anonFirstEdit': 'Primeira edição',
	'fastb-user-registryDate': 'Data de registro',
	'fastb-user-userFirstEdit': 'A data em questão se refere, na realidade, à primeira edição do usuário, pois não foi possível obter a data de registro original, por se tratar de uma conta muito antiga',
	'fastb-user-unkownRegisterDate': 'Não foi possível encontrar a data de registro do editor.',
	'fastb-user-undefined': 'indefinido',
	'fastb-user-priorTo': 'antes de',
	'fastb-user-groups': 'Grupos',
	'fastb-user-edits': 'Edições',
	'fastb-user-blocked': 'Editor <b>bloqueado</b>',
	'fastb-user-error': 'Ocorreu um erro ao tentar obter as informações do usuário',
	'fastb-user-notExist': 'O usuário <b>não existe</b>',
	'fastb-user-anonNoEdit': 'Ainda não foi realizada nenhuma edição com este endereço de IP',
	'fastb-user-anonLargeEdits': 'mais de $1',

	// Errors
	'fastb-error-unableGetList': 'não foi possível obter a lista "$1" através da API.',
	'fastb-error-backlinksNoData': 'a consulta dos afluentes da página não retornou nenhum dado.',
	'fastb-error-categoryIncompleteData': 'a consulta para se obter a qualidade da página através das categorias retornou dados incompletos.',
	'fastb-error-categoryNoData': 'a consulta para se obter a qualidade da página através das categorias não retornou nenhum dado.',
	'fastb-error-userInfoNoData': 'a consulta com as informações do usuário não retornou nenhum dado.',

	// Summary
	'fastb-summary-requestElimination': 'Página proposta para [[WP:ER|eliminação rápida]] (regra $1)',
	'fastb-summary-redirect': 'Feito redirecionamento para [[$1]]$2',
	'fastb-summary-addMessage': 'Adicionando mensagem com a predefinição "[[Predefinição:$1|$1]]"',
	'fastb-summary-stub': 'Página marcada como [[WP:EBC|esboço]]',
	'fastb-summary-addTag': 'Adicionando marcação',
	'fastb-summary-ESR': 'Página proposta para [[WP:ESR|eliminação semirrápida]]',
	'fastb-summary-creatingEliminationPage': 'Criando página de eliminação',
	'fastb-summary-elimination': 'Página proposta para [[WP:Eliminação por consenso|eliminação por consenso]]',
	'fastb-summary-inMaintenanceTag': 'Adicionando a página "$1"',
	'fastb-summary-archivingPE': 'Arquivando pedido de eliminação',
	'fastb-summary-newRequest': 'Adicionando novo pedido',
	'fastb-summary-addMergingPropose': 'Adicionando proposta de fusão',

	// Warn [elimination]
	'fastb-warn-elimination-summary-pageElimination': 'Aviso sobre a eliminação da página "[[$1]]"',
	'fastb-warn-elimination-summary-removeEliminationTag': 'Aviso sobre a remoção da marcação de eliminação da página',
	'fastb-warn-elimination-prompt-title': 'Enviar notificação',
	'fastb-warn-elimination-prompt-select-user': 'Para enviar um aviso de eliminação ao editor <a href="/wiki/User:$1">$1</a>, selecione uma opção abaixo',
	'fastb-warn-elimination-prompt-select-anon': 'Para enviar um aviso de eliminação ao usuário anônimo <a href="/wiki/Special:Contributions/$1">$1</a>, selecione uma opção abaixo',
	'fastb-warn-elimination-prompt-option-1': 'Enviar um aviso de eliminação',
	'fastb-warn-elimination-prompt-option-2': 'Enviar um aviso sobre a remoção da marcação de eliminação da página',
	'fastb-warn-elimination-prompt-option-3': 'Não enviar nenhum aviso',

	// Warn [maintenanceTags]
	'fastb-warn-maintenanceTags-summary': 'Aviso sobre a marcação da página "[[$1]]" com a predefinição "[[Predefinição:$2|$2]]"',
	'fastb-warn-maintenanceTags-prompt-title': 'Enviar notificação sobre a marcação de "$1"',
	'fastb-warn-maintenanceTags-prompt-content': 'Deseja notificar o editor <a href="/wiki/User:$1">$1</a>?'
} );

var nsNum = mw.config.get( 'wgNamespaceNumber' ),
	pageName = mw.config.get( 'wgPageName' ),
	userName =  ( mw.config.get( 'wgCanonicalSpecialPageName' ) !== 'Contributions' )
		? mw.config.get( 'wgTitle' ).split( '/' )[ 0 ]
		: window.decodeURI( mw.util.getUrl().split( '/' )[ 3 ] || mw.util.getParamValue( 'target' ) );

$.extend( fastb.buttons, {
	/**
	 * Submenu [Eliminação]
	 * @property {Object[]} elimination
	 */
	elimination: [ {
			action: function () {
				fastb.changeSubmenu( fastb.buttons.ER );
			},
			text: 'Rápida',
			title: 'Exibir regras para a eliminação rápida'
		}, {
			action: fastb.openPrompt.bind( fastb, 'ESR' ),
			text: 'Semirrápida',
			title: 'Exibe um prompt para propor a eliminação semirrápida da página',
			disable: $.inArray( nsNum, [ 0, 6 ] ) === -1
		}, {
			action: fastb.openPrompt.bind( fastb, 'PE' ),
			text: 'Consenso',
			title: 'Marcar para eliminação por consenso',
			disable: $.inArray( nsNum, [ 8, 828 ] ) !== -1
		}
	],

	/**
	 * Submenu [ER]
	 * @property {Object[]} ER
	 */
	ER: [ {
			action: 'ER|1',
			text: '1',
			title: 'Marcar subpágina do próprio usuário para eliminação',
			disable: true
		}, {
			action: 'ER|5',
			text: '5',
			title: 'Aparecimento recorrente (se o conteúdo for igual ao eliminado por consenso)'
		}, {
			action: 'ER|6',
			text: '6',
			title: 'Título é SPAM',
			disable: nsNum !== 0
		}, {
			action: 'ER|7',
			text: '7',
			title: 'Próprio criador reconhece que se enganou'
		}, {
			action: 'ER|8',
			text: '8',
			title: 'Eliminação temporária sem perda de histórico para resolver problemas técnicos',
			disable: $.inArray( 'sysop', mw.config.get( 'wgUserGroups' ) ) === -1
				&& $.inArray( 'eliminator', mw.config.get( 'wgUserGroups' ) ) === -1
		}, {
			action: 'ER|9',
			text: '9',
			title: 'Eliminar redirect, página sem histórico relevante (mover página redirecionada para cá)'
		}, {
			action: 'ER|10',
			text: '10',
			title: 'Domínio que não existe (WikipÉdia, AjUda)',
			disable: nsNum !== 0
		}, {
			action: 'ER|12',
			text: '12',
			title: 'Imagem (somente por quem a carregou)',
			disable: nsNum !== 6
		}, {
			action: 'ER|13',
			text: '13',
			title: 'Página sem histórico relevante que é violação flagrante de direitos autorais de outras páginas na internet',
			disable: nsNum !== 0
		}, {
			action: 'ER|14',
			text: '14',
			title: 'Ficheiro (arquivos) duplicados',
			disable: nsNum !== 6
		}, {
			action: 'ER|17',
			text: '17',
			title: 'Salto de domínio'
		}, {
			action: 'ER|18',
			text: '18',
			title: 'Discussão cujos artigos não existem',
			disable: nsNum !== 1
		}, {
			action: 'ER|20',
			text: '20',
			title: 'Impróprio'
		}, {
			action: 'ER|21',
			text: '21',
			title: 'Página de eliminação de um artigo antes de passados 6 meses do último consenso',
			disable: pageName.indexOf( 'Wikipédia:Páginas_para_eliminar/' ) !== 0
		}, {
			action: 'ER|A1',
			text: 'A1',
			title: 'Página com o título malformatado, absurdo, com palavras que não'
				+ ' o são, com erros devidos à má configuração do teclado, com codificação'
				+ ' incorreta do sistema ou que expressem domínios que não existem.',
			disable: nsNum !== 0
		}, {
			action: 'ER|A2',
			text: 'A2',
			title: 'Sem contexto',
			disable: nsNum !== 0
		}, {
			action: 'ER|A3',
			text: 'A3',
			title: 'Sem conteúdo',
			disable: nsNum !== 0
		}, {
			action: 'ER|A4',
			text: 'A4',
			title: 'Sem indicação de importância'
				+ ' (pessoas, animais, organizações, conteúdo web, eventos)',
			disable: nsNum !== 0
		}, {
			action: 'ER|A5',
			text: 'A5',
			title: 'Sem indicação de importância'
				+ ' (gravações musicais e livros)',
			disable: nsNum !== 0
		}, {
			action: 'ER|A6',
			text: 'A6',
			title: 'Artigo criado recentemente que duplica um tópico existente',
			disable: nsNum !== 0
		}, {
			action: 'ER|C1',
			text: 'C1',
			title: 'Categoria vazia, desnecessária ou substituída',
			disable: nsNum !== 14
		}, {
			action: 'ER|D1',
			text: 'D1',
			title: 'Discussão de página inexistente',
			disable: nsNum % 2 === 0
		}, {
			action: 'ER|D2',
			text: 'D2',
			title: 'Discussão de página para eliminação ou com histórico irrelevante',
			disable: nsNum % 2 === 0
		}, {
			action: 'ER|U1',
			text: 'U1',
			title: 'Uso impróprio da página de usuário',
			disable: $.inArray( nsNum, [ 2, 3 ] ) === -1
		}, {
			action: 'ER|U2',
			text: 'U2',
			title: 'Página de usuário criada por outro usuário',
			disable: nsNum !== 2
		}, {
			action: 'ER|P1',
			text: 'P1',
			title: 'Predefinição vazia, desnecessária ou substituída',
			disable: nsNum !== 10
		}, {
			action: 'ER|P2',
			text: 'P2',
			title: 'Predefinição que é uma deturpação das regras',
			disable: nsNum !== 10
		}, {
			action: 'ER|R1',
			text: 'R1',
			title: 'Redirecionamento indevido, desnecessário, sem afluentes, para páginas inexistente ou eliminadas'
		}, {
			action: 'ER|G1',
			text: 'G1',
			title: 'Eliminação técnica'
		}
	],

	/**
	 * Submenu [Manuteção]
	 * @property {Object[]} maintenance
	 */
	maintenance: [ {
			action: 'subst:s-fontes',
			templatename: 'Sem-fontes',
			text: 'Sem fontes',
			title: 'Página não cita nenhuma fonte ou referência',
			sum: 'Página marcada como sem fontes',
			warn: true
		}, {
			action: 'subst:s-fontes-bpv',
			templatename: 'Sem-fontes-bpv',
			text: 'Sem fontes BPV',
			title: 'Biografia de pessoa viva que não cita nenhuma fonte',
			sum: 'Página marcada como [[WP:BPV|biografia de pessoa viva]] sem fontes',
			warn: true
		}, {
			action: 'subst:s-notas',
			templatename: 'Sem notas',
			text: 'Sem notas',
			title: 'Existem fontes no final da página, mas não são citadas no corpo do artigo',
			sum: 'Página marcada como sem notas'
		}, {
			action: 'subst:m-notas',
			templatename: 'Mais notas',
			text: 'Mais notas',
			title: 'Página cita fontes fiáveis, mas não cobre todo o texto',
			sum: 'Página marcada que carece de mais notas'
		}, {
			action: 'subst:m-notas-bpv',
			templatename: 'Mais notas-bpv',
			text: 'Mais notas BPV',
			title: 'Biografia de pessoa viva que cita fontes, porém que não cobrem todo o texto',
			sum: 'Página marcada como [[WP:BPV|biografia de pessoa viva]] que carece de mais notas'
		}, {
			action: 'subst:fpr',
			templatename: 'Fontes primárias',
			text: 'Fonte primária',
			title: 'Artigo necessita de fontes secundárias fiáveis publicadas por terceiros',
			sum: 'Página marcada como sem fontes secundárias fiáveis'
		}, {
			action: 'subst:f-referências',
			templatename: 'Formatar referências',
			text: 'Formatar referências',
			title: 'Artigo contém referências que necessitam de formatação',
			sum: 'Página marcada que existem referências sem formatação'
		}, {
			action: 'subst:wkf',
			templatename: 'Wikificação',
			text: 'Wikificar',
			title: 'Não está formatado de acordo com o livro de estilo',
			sum: 'Página marcada para [[WP:WKF|wikificação]]'
		}, {
			action: 'subst:rec',
			templatename: 'Reciclagem',
			text: 'Reciclagem',
			title: 'Página precisa ser reciclada de acordo com o livro de estilo',
			sum: 'Página marcada para [[WP:RECI|reciclagem]]'
		}, {
			action: 'subst:s-cat',
			templatename: 'Sem cat',
			text: 'Sem categoria',
			title: 'Página não está em nenhuma categoria',
			sum: 'Página marcada como sem categoria',
			warn: true
		}, {
			action: 'Parcial',
			templatename: 'Parcial',
			text: 'Parcial',
			title: 'Artigo possui passagens que não respeitam o princípio da imparcialidade',
			sum: 'Página marcada como parcial'
		}, {
			action: 'subst:ctx',
			templatename: 'Contextualizar',
			text: 'Contexto',
			title: 'Página carece de contexto',
			sum: 'Página marcada como sem contexto'
		}, {
			action: 'subst:não-enc',
			templatename: 'Não enciclopédico',
			text: 'Não enciclopédico',
			title: 'A conteúdo da página é possivelmente apresentado de uma maneira não-enciclopédica',
			sum: 'Página marcada que possui conteúdo não-enciclopédico'
		}, {
			action: 'Publicidade',
			templatename: 'Publicidade',
			text: 'Publicidade',
			title: 'A conteúdo da página está possivelmente apresentado em formato publicitário',
			sum: 'Página marcada que possui conteúdo em formato publicitário'
		}, {
			action: 'Global',
			templatename: 'Global',
			text: 'Global',
			title: 'A conteúdo da página está redigido sob uma perspectiva majoritariamente brasileira, portuguesa ou lusófona',
			sum: 'Página marcada que possui um conteúdo redigido numa perspectiva regionalizada',
			prompt: 'Qual o tipo da perspectiva?',
			label: 'Digite o número correspondente:<br />1 = Lusofonia<br />2 = Brasil<br />3 = Portugal<br />Ou deixe em branco para utilizar a predefinição genérica[optional]'
		}, {
			action: 'Má tradução',
			templatename: 'Má tradução',
			text: 'Má tradução',
			title: 'A conteúdo da página está possivelmente mal traduzido',
			sum: 'Página marcada como má tradução'
		}, {
			action: 'Corrigir',
			templatename: 'publicidade',
			text: 'Corrigir',
			title: 'A conteúdo da página possivelmente precisa de correção ortográfico-gramatical',
			sum: 'Página marcada como carece de correção'
		}, {
			action: 'Revisão',
			templatename: 'Revisão',
			text: 'Revisão',
			title: 'A conteúdo da página está possivelmente inconsistente',
			sum: 'Página marcada para revisão'
		}, {
			action: 'Em manutenção',
			templatename: 'Em manutenção',
			text: 'Em manutenção',
			title: 'Marcar a página para uma manutenção emergencial, afim de evitar uma eliminação',
			sum: 'Página marcada para manutenção emergencial'
		}, {
			action: 'Desatualizado',
			templatename: 'Desatualizado',
			text: 'Desatualizado',
			title: 'A página contém um conteúdo que pode está desatualizado',
			sum: 'Página marcada como desatualizada'
		}, {
			action: 'Evento atual',
			templatename: 'Evento atual',
			text: 'Evento atual',
			title: 'Artigo sobre um evento atual',
			sum: 'Página marcada como evento atual'
		}, {
			action: 'subst:m-recente',
			templatename: 'Morte recente',
			text: 'Morte recente',
			title: 'Artigo sobre uma pessoa que morreu recentemente',
			sum: 'Página marcada como morte recente'
		}, {
			action: fastb.openPrompt.bind( fastb, 'merging' ),
			templatename: 'Fusão',
			text: 'Fusão',
			title: 'Página necessita de fusão',
		}
	],

	/**
	 * Submenu [Aviso]
	 * @property {Object[]} warn
	 */
	warn: [  {
			action: 'subst:bem-vindo(a)',
			text: 'BV',
			title: 'Bem-vindo(a) à Wikipédia',
			disable: mw.util.isIPAddress( userName )
		}, {
			action: 'subst:bv-av-registrado',
			text: 'Av-BV',
			title: 'Aviso sobre erro em artigo e boas-vindas para usuário(a) registrado',
			prompt: 'Aviso sobre qual artigo?',
			label: 'Artigo',
			disable: mw.util.isIPAddress( userName )
		}, {
			action: 'subst:bem-vindo IP',
			text: 'BV-IP',
			title: 'Boas-vindas para usuário(a) não registrado(a)',
			disable: !mw.util.isIPAddress( userName )
		}, {
			action: 'subst:bv-av',
			text: 'Av-BV-IP',
			title: 'Aviso sobre erro em artigo e boas-vindas para usuário(a) não registrado(a)',
			prompt: 'Aviso sobre qual artigo?',
			label: 'Artigo',
			disable: !mw.util.isIPAddress( userName )
		},{
			action: 'Vandalismo repetido',
			text: 'Vandalismo repetido',
			title: 'Adiciona uma marca informando que o IP em questão tem sido constantemente bloqueado.',
			disable: !mw.util.isIPAddress( userName )
		}, {
			action: 'subst:aviso-ER',
			text: 'Av-ER',
			title: 'Aviso sobre eliminação rápida',
			prompt: 'Qual página foi proposta para eliminação?',
			label: 'Página|2=Regra de eliminação'
		}, {
			action: 'subst:av-bv-ER',
			text: 'Av-BV-ER',
			title: 'Aviso sobre eliminação rápida + boas-vindas',
			prompt: 'Qual página foi proposta para eliminação?',
			label: 'Página|2=Regra de eliminação'
		}, {
			action: 'subst:aviso-ESR',
			text: 'Av-ESR',
			title: 'Aviso sobre eliminação semirrápida',
			prompt: 'Qual página foi proposta para eliminação?',
			label: 'Página'
		}, {
			action: 'subst:aviso-PE',
			text: 'Av-PE',
			title: 'Aviso sobre eliminação por consenso',
			prompt: 'Qual página foi proposta para eliminação?',
			label: 'Página'
		}, {
			action: 'subst:av-página de usuário',
			text: 'Av-PU',
			title: 'Considere refazer a página de usuário(a)'
		}, {
			action: 'subst:bv-propaganda',
			text: 'Propaganda + BV',
			title: 'Caro editor,por favor não faça propaganda, [...] Apesar disso, bem-vindo à Wikipédia'
		}, {
			action: 'subst:aviso',
			text: 'Av-erro em página',
			title: 'Aviso sobre erro em artigo',
			prompt: 'Aviso sobre qual artigo?',
			label: 'Artigo'
		}, {
			action: 'subst:aviso2',
			text: 'Av2',
			title: 'Aviso sobre vandalismo',
			prompt: 'Qual página foi vandalizada?',
			label: 'Página'
		}, {
			action: 'subst:aviso-vandalismo',
			text: 'Av-vandalismo',
			title: 'Não vandalize os artigos',
			prompt: 'Qual o nível do aviso (1 para primeiro, 2 para segundo, 3 para terceiro ou b para notificação de bloqueio)? E que página foi vandalizada?',
			label: 'Nível|2=Página'
		}, {
			action: 'subst:av-Bpv',
			text: 'Av-BPV',
			title: 'Não adicione conteúdo difamatório nos artigos sobre biografias de pessoas vivas',
			prompt: 'Qual o nível do aviso (1 para primeiro, 2 para segundo, 3 para terceiro ou b para notificação de bloqueio)? E em que página foi adicionado conteúdo difamatório?',
			label: 'Nível|2=Página'
		}, {
			action: 'subst:av-Remoção',
			text: 'Av-remoção',
			title: 'Não remova conteúdo dos artigos',
			prompt: 'Qual o nível do aviso (1 para primeiro, 2 para segundo, 3 para terceiro ou b para notificação de bloqueio)? E de qual página foi removido conteúdo?',
			label: 'Nível|2=Página'
		}, {
			action: 'subst:av-Spam',
			text: 'Av-spam',
			title: 'Não insira ligações externas inadequadas nos artigos',
			prompt: 'Qual o nível do aviso (1 para primeiro, 2 para segundo, 3 para terceiro ou b para notificação de bloqueio)? E em que página foram inseridas ligações externas inadequadas?',
			label: 'Nível|2=Página'
		}, {
			action: 'subst:av-teste',
			text: 'Av-teste',
			title: 'Não faça testes nos artigos',
			prompt: 'Qual o nível do aviso (1 para primeiro, 2 para segundo, 3 para terceiro ou b para notificação de bloqueio)? E em que página foram feitos testes?',
			label: 'Nível|2=Página'
		}, {
			action: 'subst:av-data',
			text: 'Av-data',
			title: 'Não insira seu nome e data de nascimento em páginas de datas',
			prompt: 'Em que página de data foram inseridos o nome e a data de nascimento?',
			label: 'Página'
		}, {
			action: 'subst:av-interwiki',
			text: 'Av-interwikis',
			title: 'Faltou adicionar os interwikis à página',
			prompt: 'Qual foi a página?',
			label: 'Página'
		}, {
			action: 'subst:av-categoria',
			text: 'Av-Categoria',
			title: 'Faltou adicionar categorias à página',
			prompt: 'Qual foi a página?',
			label: 'Página'
		}, {
			action: 'subst:assine',
			text: 'Assine',
			title: 'Faltou assinar o comentário',
			prompt: 'Qual foi o local?',
			label: 'Local'
		}, {
			action: 'subst:propaganda',
			text: 'Propaganda',
			title: 'Caro editor, por favor não faça propaganda...',
			prompt: 'Em que página foi feita propaganda?',
			label: 'Página'
		}, {
			action: 'subst:BSRE',
			text: 'BSRE',
			title: 'Aviso de biografia sem relevo enciclopédico',
			prompt: 'Qual artigo?',
			label: 'Artigo'
		}, {
			action: 'subst:cópia',
			text: 'Cópia',
			title: 'Aviso sobre artigo copiado de fonte externa/VDA',
			prompt: 'Qual a página da Wikipédia? E qual a URL da página copiada?',
			label: 'Página|2=URL'
		}, {
			action: 'subst:linguagem incorreta',
			text: 'Linguagem',
			title: 'Não insulte nem use linguagem inadequada em artigos ou discussões'
		}, {
			action: 'subst:ortografia',
			text: 'Ortografia',
			title: 'Não mude a versão da língua',
			prompt: 'Em qual artigo a versão da língua foi alterada?',
			label: 'Artigo'
		}, {
			action: 'subst:mostrar previsão',
			text: 'Salvamento sucessivo',
			title: 'Não faça salvamentos sucessivos, utilize o botão \'Mostrar previsão\'',
			prompt: 'Em que artigo foram feitos salvamentos sucessivos?',
			label: 'Página'
		}, {
			action: 'subst:não remova',
			text: 'Não remova',
			title: 'Não remova marcações de eliminação das páginas',
			prompt: 'Qual página em que a marcação de eliminação foi removida? Se desejar, pode especificar o tipo de marcação.',
			label: 'Página|2=Tipo de marcação[optional]',
			sum: 'Aviso sobre remoção de marcações de eliminação das páginas'
		}, {
			action: 'subst:autobiografia',
			text: 'Autobiografia',
			title: 'Não crie autobiografias',
			prompt: 'Qual autobiografia foi criada?',
			label: 'Página'
		}, {
			action: 'subst:cite fonte',
			text: 'Citar fontes',
			title: 'Faltou citar fontes à página',
			prompt: 'Qual foi a página?',
			label: 'Página'
		}, {
			action: 'subst:aviso-GE',
			text: 'Aviso-GE',
			title: 'A página foi protegida devido à guerra de edições',
			prompt: 'Qual página foi protegida?',
			label: 'Página'
		}, {
			action: 'subst:bloqueado',
			text: 'Bloqueado',
			title: 'Notificação de bloqueio quando o usuário NÃO está autorizado a usar a sua discussão',
			prompt: 'Especifique o tempo e o motivo do bloqueio.',
			label: 'Tempo|2=Motivo'
		}, {
			action: 'subst:bloqueado-disc',
			text: 'Bloqueado-disc',
			title: 'Notificação de bloqueio com discussão quando o usuário está autorizado a usar a sua discussão',
			prompt: 'Especifique o tempo e o motivo do bloqueio.',
			label: 'Tempo|2=Motivo'
		}, {
			action: 'subst:bloqueado-CPV',
			text: 'Bloqueado-CPV',
			title: 'Notificação de bloqueio para contas para vandalismo'
		}, {
			action: 'proxy',
			text: 'Proxy',
			title: 'Notificação de proxy bloqueado'
		}
	],

	/**
	 * Submenu [Busca]
	 * @property {Object[]} search
	 */
	search: [ {
			text: 'Google',
			desc: 'Pesquisar o título desta página no Google',
			url: '//www.google.com/search?&as_eq=wikipedia&as_epq='
		}, {
			text: 'Google notícias',
			desc: 'Pesquisar o título desta página no Google Notícias',
			url: '//google.com/search?tbm=nws&q='
		}, {
			text: 'Google livros',
			desc: 'Pesquisar o título desta página no Google Livros',
			url: '//books.google.com/books?&as_brr=0&as_epq='
		}, {
			text: 'Google acadêmico',
			desc: 'Pesquisar o título desta página no Google Acadêmico',
			url: '//scholar.google.com/scholar?q='
		}, {
			text: 'Bing',
			desc: 'Pesquisar o título desta página no Bing',
			url: '//www.bing.com/search?q='
		}
	],

	/**
	 * Submenu [PetScan]
	 * @property {Object[]} cat
	 */
	cat: [ {
			text: 'Sem fontes',
			desc: 'Procurar páginas desta categoria que precisam de fontes',
			url: '&templates_yes=Sem-fontes'
		}, {
			text: 'Revisão',
			desc: 'Procurar páginas desta categoria que precisam de revisão',
			url: '&templates_yes=Revis%C3%A3o'
		}, {
			text: 'Wikificação',
			desc: 'Procurar páginas desta categoria que precisam de wikificação',
			url: '&templates_yes=Wikifica%C3%A7%C3%A3o'
		}, {
			text: 'Menos de 1 000 bytes ou 4 links',
			desc: 'Procurar páginas desta categoria que possuem menos de 1 000 bytes ou 4 links',
			url: '&larger=1000&minlinks=4'
		}, {
			text: 'Menos de 500 bytes ou 2 links',
			desc: 'Procurar páginas desta categoria que possuem menos de 500 bytes ou 2 links',
			url: '&larger=500&minlinks=2'
		}
	],

	/**
	 * Submenu [Pedidos]
	 * @property {Object[]} requests
	 */
	requests: [ {
			action: 'request',
			text: 'Bloqueio',
			title: 'Requisitar o bloqueio de um usuário.',
			placeholder: 'Qual a justificativa para o pedido de bloqueio?',
			page: 'Wikipédia:Pedidos/Bloqueio'
		}, {
			action: 'request',
			text: 'Desproteção',
			title: 'Requisitar a desproteção de uma página.',
			placeholder: 'Qual a justificativa para o pedido de desproteção?',
			page: 'Wikipédia:Pedidos/Desproteção'
		}, {
			action: 'request',
			text: 'Edição',
			title: 'Requisitar uma alteração de uma página, pois devido ao fato de está protegida, não pode ser feita por você.',
			placeholder: 'Qual a alteração deseja fazer e por quê?',
			page: 'Wikipédia:Pedidos/Páginas protegidas'
		}, {
			action: 'request',
			text: 'Histórico',
			title: 'Requisitar a fusão do histórico de uma página com outra.',
			placeholder: 'Qual a justificativa para o pedido de fusão de históricos?',
			page: 'Wikipédia:Pedidos/Histórico'
		}, {
			action: 'request',
			text: 'Proteção',
			title: 'Requisitar a proteção de uma página.',
			placeholder: 'Qual a justificativa para o pedido de proteção?',
			page: 'Wikipédia:Pedidos/Proteção'
		}, {
			action: 'request',
			text: 'Restauro',
			title: 'Requisitar o restauro de uma página',
			placeholder: 'Qual a justificativa para o pedido de restauro?',
			page: 'Wikipédia:Pedidos/Restauro'
		}, {
			action: 'request',
			text: 'Supressão',
			title: 'Requisitar a supressão de uma ou mais edições de uma página.',
			page: 'Wikipédia:Pedidos/Supressão',
			placeholder: 'Quais edições devem ser suprimidas e por quê?'
		}, {
			action: 'request',
			text: 'Outro',
			title: 'Fazer um pedido sobre algo.',
			page: 'Wikipédia:Pedidos/Outros',
			placeholder: 'Esclareça o pedido.'
		}
	],

	/**
	 * User infomation
	 * @property {Object[]} userInfo
	 */
	userInfo: [ {
			href: mw.util.getUrl( 'Special:Contributions/' ) + userName,
			title: 'Abrir a lista de contribuições deste editor',
			text: 'Contribuições',
			disable: mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Contributions'
		}, {
			href: mw.util.getUrl( 'Special:Log/' ) + userName,
			title: 'Abrir a lista de registros deste editor',
			text: 'Registros'
		}, {
			href: mw.util.getUrl( 'Special:Log' ) + '?type=block&page=User%3A' + userName,
			title: 'Abrir a lista de registros de bloqueio deste editor',
			text: 'Registros de bloqueio'
		}, {
			href: 'https://tools.wmflabs.org/xtools-ec/?user=' + userName + '&lang=pt&wiki=wikipedia',
			title: 'Abrir a contagem de edições deste editor',
			text: 'Contador de edições'
		}, {
			href: 'https://tools.wmflabs.org/xtools/pages/?user=' + userName + '&lang=pt&wiki=wikipedia&namespace=all&redirects=none',
			title: 'Abrir a lista de páginas criadas por este editor',
			text: 'Páginas criadas'
		}
	],

	/**
	 * Recent Changes, new pages and watched pages
	 * @property {Object[]} list
	 */
	list:  [ {
			action: function () {
				fastb.callAPI( 'PV' );
			},
			text: 'Páginas vigiadas',
			title: 'Exibir páginas vigiadas que foram alteradas recentemente'
		}, {
			action: function () {
				fastb.callAPI( 'MR' );
			},
			text: 'Mudanças recentes',
			title: 'Exibir mudanças recentes feitas por IPs em páginas do domínio principal'
		}, {
			action: function () {
				fastb.callAPI( 'PN' );
			},
			text: 'Páginas novas',
			title: 'Exibir páginas novas que ainda não foram patrulhadas'
		}, {
			action: function () {
				fastb.callAPI( 'SCORES' );
			},
			text: 'Edições a revisar',
			title: 'Exibir edições com grandes chances de serem desfeitas e que ainda não foram patrulhadas'
		}, {
			action: function () {
				fastb.callAPI( 'ER' );
			},
			text: 'Eliminação rápida',
			title: 'Exibir as páginas que foram marcadas para eliminação rápida'
		}
	]
} );

/**
 * Default justifications for the ESR prompt
 * @property {Object[]} defaultJustifications_ESR
 */
fastb.defaultJustificationsESR = {
	'Arte': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Banda': [
		'Artigo sobre banda/grupo musical sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do'
			+ ' texto e atestem a notoriedade do artista ou de sua obra. Ver [[WP:V|princípio da verificabilidade]] e'
			+ ' [[WP:CDN|critérios de notoriedade]] (e o [[Wikipédia:Critérios de notoriedade/Música|critério específico para música]]). Páginas'
			+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',
		'ESR-banda'
	],

	'Cantores, álbuns e músicas':'Artigo relacionado à música sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do'
		+ ' texto e atestem a notoriedade do artista ou de sua obra. Ver [[WP:V|princípio da verificabilidade]] e'
		+ ' [[WP:CDN|critérios de notoriedade]] (e o [[Wikipédia:Critérios de notoriedade/Música|critério específico para música]]). Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Carnaval': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Carnaval|critério específico para carnaval]]). Páginas pessoais, sites'
		+ ' de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Ciência': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Ciências sociais': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Cinema': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Cinema, rádio e televisão|critério específico para cinema]]). Páginas'
		+ '  pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Educação': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Educação|critério específico para educação]]). Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Elementos de ficção': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Elementos de ficção|critério específico para elementos de ficção]]). Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Empresas, produtos e serviços': [
		'Artigo sobre organização sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações'
			+ ' do texto e atestem notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios'
			+ ' de notoriedade]] (e os [[Wikipédia:Critérios de notoriedade/Empresas, produtos e serviços|critérios específicos para'
			+ ' empresas, produtos e serviços]]). Páginas mantidas pela própria organização, blogues, fóruns e redes sociais (como'
			+ ' Facebook, Twitter, etc.) não são considerados fontes fiáveis.',
		'ESR-organização'
	],

	'Entretenimento': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Esporte/desporto': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Desporto|critério específico para desporto]]. Páginas pessoais, sites'
		+ ' de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Geografia': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Geografia|critério específico para geografia]]). Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Ginástica': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Ginástica|critério específico para ginástica]]). Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'História e sociedade': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Medicina': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Países': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]]. Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Pessoas': [
		'Biografia sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem a'
			+ ' notoriedade do biografado. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
			+ ' [[Wikipédia:Critérios de notoriedade/Biografias|critério específico para biografias]]). Páginas '
			+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são considerados fontes fiáveis.',
		'ESR-bio'
	],

	'Política': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem '
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Política|critério específico para política]]). Páginas'
		+ ' pessoais, fóruns, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.',

	'Rádio e televisão': 'Artigo sem [[WP:FF|fontes fiáveis]] e [[WP:FI|independentes]] que confirmem as afirmações do texto e atestem'
		+ ' notoriedade. Ver [[WP:V|princípio da verificabilidade]] e [[WP:CDN|critérios de notoriedade]] (e o'
		+ ' [[Wikipédia:Critérios de notoriedade/Cinema, rádio e televisão|critério específico para rádio e televisão]]). Páginas'
		+ ' pessoais, sites de fãs, blogues e redes sociais (como Facebook, Twitter, etc.) não são fontes fiáveis.'
};

}( mediaWiki, jQuery, fastButtons, window ) );

// [[Categoria:!Código-fonte de scripts|FastButtons/Lista de botões]]