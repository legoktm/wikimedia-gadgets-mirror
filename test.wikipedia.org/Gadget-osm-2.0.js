// Verwendung von OpenStreetMap in Wikipedia.
// (c) 2008-2012 by Magnus Manske + Prolineserver
// Released under GPL
 
osmHeadHight = 15;
 
 
function openStreetMapInit () {
 
 
  c = getElementsByClass ( 'coordinates', document, 'span') ;
  if (!c) return ;
  for ( i = 0 ; i < c.length ; i++ ) {
    geohack = 0;
    a = c[i].getElementsByTagName ( 'a' ) ;
    for ( j = 0 ; j < a.length ; j++ ) {
      h = a[j] ;
      if ( !h.href.match(/geohack/) ) continue ;
      geohack = 1 ;
      break ;
    }
    if ( geohack == 1 ) {
      h = a[j] ;
      geoParams = h.href.split('params=')[1] ;
      geoParams = geoParams.split('&')[0] ;
      if (c[i].id == 'coordinates' || c[i].id == 'text_coordinates' || c[i].id == '') {
        geoTitle = wgTitle;
      } else {
        geoTitle = c[i].id;
      }
      na = document.createElement ( 'a' ) ;
      na.href = 'javascript:openStreetMapToggle(\''+geoParams+'\',\''+geoTitle+'\')' ;
// For some reason this command is executed immediately
//      na.onclick = 'openStreetMapToggle(\''+geoParams+'\',\''+geoTitle+'\')' ;
      na.style.textDecoration = 'none';
      nimg = document.createElement ( 'img' ) ;
//      nimg.src = 'http://toolserver.org/~prolineserver/images/osm_logo_15.png';
      nimg.src = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/Openstreetmap_logo.svg/15px-Openstreetmap_logo.svg.png';

      nimg.width = 15;
      nimg.title = 'Auf OSM-Karte darstellen'
      na.appendChild ( nimg ) ;
 
 
      c[i].appendChild ( document.createTextNode ( ' ' ) ) ;
      c[i].appendChild ( na ) ;
      c[i].appendChild ( document.createTextNode ( ' ' ) ) ;
    }
  } 
}
 
function openStreetMapToggle (geoParams, geoTitle) {
 
  cs = document.getElementById ( 'contentSub' ) ;
  osm = document.getElementById ( 'osmdiv' ) ;
 
  if ( cs && osm ) {
    cs.removeChild ( osm ) ;
  }
 
 
  if( self.innerHeight ) {
    width = self.innerWidth ;
    height = self.innerHeight ;
  } else if( document.documentElement && document.documentElement.clientHeight ) {
    width = document.documentElement.clientWidth ;
    height = document.documentElement.clientHeight ;
  } else {
    width = document.body.clientWidth ;
    height = document.body.clientHeight ;
  }
 
  lat = geoParams.split('_')[0] ;
  lon = geoParams.split('_')[2] ;
 
  lat = (geoParams.split('_')[1] == 'N') ? lat : -lat ;
  lon = (geoParams.split('_')[3] == 'E') ? lon : -lon ;
 
  osm = document.createElement ( 'div' ) ;
  osm.id = 'osmdiv' ;
  osm.style.position = 'fixed' ;
  osm.style.display = 'block' ;
  osm.style.border = '1px solid #000' ;
  osm.style.width = (width-320) + 'px' ;
  osm.style.height = (height-150) + 'px' ;
  osm.style.background = '#ddd';
  osm.style.right = '75px' ;
  osm.style.top = '75px' ;
  osm.style.zIndex = 1 ;
 
  osmclose = document.createElement ( 'a' ) ;
  osmclose.href = 'javascript:openStreetMapClose()' ;
  osmclose.appendChild ( document.createTextNode ( 'Schließen' ) ) ;
 
  osmedit = document.createElement ( 'a' ) ;
  osmedit.href = 'http://openstreetmap.org/edit?lat='+lat+'&lon='+lon+'&zoom=15' ;
  osmedit.appendChild ( document.createTextNode ( 'Karte bearbeiten' ) ) ;
 
 
 
  head = document.createElement ( 'div' ) ;
  head.id = 'osmhead' ;
  head.style.height = osmHeadHight + 'px' ;
  head.appendChild ( osmclose ) ;
  head.appendChild ( document.createTextNode ( ', ' ) ) ;
  head.appendChild ( osmedit ) ;
  head.appendChild ( document.createTextNode ( ': ' ) ) ;
  head.appendChild ( document.createTextNode ( unescape(geoTitle) ) ) ;
 
 
 
  iframe = document.createElement ( 'iframe' ) ;
  url = 'http://toolserver.org/~kolossos/openlayers/kml-on-ol.php';
  url += '?lang=' + wgUserLanguage ;
  url += '&uselang=' + wgUserLanguage ;
  url += '&params=' + geoParams ;
  if ( geoTitle == wgTitle) {
    url += '&title=' + geoTitle ;
  } else {
    url += '&title=' + wgTitle ;
    url += '#' + geoTitle ;
  }

  iframe.id = 'openstreetmap' ;
 
  iframe.style.width = (width-320-5) + 'px' ;
  iframe.style.height = (height-150-5-osmHeadHight) + 'px' ;
  iframe.style.clear = 'both' ;
  iframe.src = url ;
  osm.appendChild ( head ) ;
  osm.appendChild ( iframe ) ;
  cs.appendChild ( osm ) ;
}
 
 
 
function openStreetMapClose () {
  cs = document.getElementById ( 'contentSub' ) ;
  osm = document.getElementById ( 'osmdiv' ) ;
 
  if ( cs && osm ) {
    cs.removeChild ( osm ) ;
    return ;
  }
}
 
$(openStreetMapInit);
 
 
 
 
// Stolen from http://www.anyexample.com/webdev/javascript/javascript_getelementsbyclass_function.xml
 
function getElementsByClass( searchClass, domNode, tagName) {
  if (domNode == null) domNode = document;
  if (tagName == null) tagName = '*';
  var el = new Array();
  var tags = domNode.getElementsByTagName(tagName);
  var tcl = " "+searchClass+" ";
  for(i=0,j=0; i<tags.length; i++) {
    var test = " " + tags[i].className + " ";
    if (test.indexOf(tcl) != -1)
      el[j++] = tags[i];
  }
  return el;
}