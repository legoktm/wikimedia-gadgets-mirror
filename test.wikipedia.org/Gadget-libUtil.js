/**
 * <nowiki>
 * This is a library for containing common used functions on Commons 
 *   BUT that could be also useful to other wikis
 *
 * Inclusion-scope: 
 *   "used by one or better more of the default or in one of the 5 most popular gadgets"
 *   "and is potentially useful for other tools on Commons"
 * Keep in mind: It's a library. Do not change any variable-state or the UI until a method is called.
 *
 * All things that are specific to Commons (as a media hoster) should go into [[MediaWiki:Gadget-libCommons.js]].
 * 
 * Coding convetion:
 * Do not self-refer inside a function using "this"; instead use "lc"
 * to allow reusing these functions in an altered scope
 *
 * Derived from [[MediaWiki:Gadgetprototype.js]]
 *
 * Invoke jsHint-validation on save.
 *
 * @rev 1 (2012-06-21)
 * @author Rillke, 2012
 */

/*global jQuery:false, mediaWiki:false*/
/*jshint curly:false*/

( function ( $, mw ) {
"use strict";

if (!mw.libs.commons) mw.libs.commons = {};
var lc = mw.libs.commons;

$.extend(mw.libs.commons, {
	/**
	* Some pages are related to a user. 
	* For example Special:Contributions/Rillke lists the contributions of [[User:Rillke]].
	* @author
	*      Rillke, one RegExp by DieBuche
	*
	* @example
	*      mw.libs.commons.guessUser();
	*
	* @context {mw.libs.commons} or any other
	* @return {string} The user name guessed from URL or wgPageName.
	*/
	guessUser: function() {
		switch (mw.config.get('wgNamespaceNumber')) {
			case 3: // User_talk
			case 2: // User
				return mw.config.get('wgPageName').match(/.*?\:(.*?)(\/.*)*$/)[1];
			case -1: // Special pages
				try {
					switch (mw.config.get('wgCanonicalSpecialPageName')) {
						case 'Contributions':
						case 'DeletedContributions':
						case 'Block':
						case 'CentralAuth':
							// Extract target from addressline
							if ( /Special\:(?:DeletedContributions|Contributions|Block|CentralAuth)\//.test(location.href) ) {
								return decodeURI(location.href.match(/Special\:(?:DeletedContributions|Contributions|Block|CentralAuth)\/(.*?)(?:&.*)?$/)[1]);
							} else if ( -1 !== location.href.indexOf(mw.config.get('wgScript')) ) {
								return mw.util.getParamValue('target');
							}
							break;
						case 'Userrights':
						case 'Listfiles':
						case 'Log':
							// Extract target from addressline
							if (mw.util.getParamValue('user')) {
								return mw.util.getParamValue('user');
							}
							if (mw.util.getParamValue('page')) { 
								if (/User:+./.test(mw.util.getParamValue('page'))) {
									return mw.util.getParamValue('page').replace("User:", "");
								}
							}
							if ( /Special\:(?:Log|ListFiles|UserRights)\//.test( location.href )) {
								return decodeURI(location.href.match(/Special\:(?:Log|ListFiles|UserRights)\/(.*?)(?:&.*)?$/)[1]);
							}
							break;
					}
				} catch (ex) { }
			break;
		}
	},
	/**
	* source: [[MediaWiki:Gadget-AjaxQuickDelete.js]]
	* Very simple date formatter.  Replaces the substrings "YYYY", "MM" and "DD" in a
	* given string with the UTC year, month and day numbers respectively.
	* Also replaces "MON" with the English full month name and "DAY" with the unpadded day.
	*
	* wgMonthNames can't be used because it is in Page content language which is
	* the user's language on Special:-pages
	*
	* @example
	*      mw.libs.commons.formatDate('year=YYYY|month=MON|day=DD');
	*
	* @param {string} fmt The (format) template-string
	* @param {Date} date A date to use
	* @param {Date} fallbackDate  If date was empty, use this date instead. If it's also empty the current date will be used.
	*
	* @return {string} A formatted date-string.
	*/
	monthNamesInSiteLang: ["gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre"],
	formatDate: function (fmt, date, fallbackDate) {
		var pad0 = function (s) {
				s = "" + s;
				return (s.length > 1 ? s : "0" + s);
			}; // zero-pad to two digits
		date = date || fallbackDate || new Date();
		fmt = fmt.replace(/YYYY/g, date.getUTCFullYear());
		fmt = fmt.replace(/MM/g, pad0(date.getUTCMonth() + 1));
		fmt = fmt.replace(/DD/g, pad0(date.getUTCDate()));
		fmt = fmt.replace(/MON/g, lc.monthNamesInSiteLang[date.getUTCMonth()]);
		fmt = fmt.replace(/DAY/g, date.getUTCDate());
		return fmt;
	},
	/**
	* Get the file title from a file source link
	*
	*
	* @example
	*      mw.libs.commons.titleFromImgSrc($('img').attr('src'));
	*
	* @param {string} src The image source (upload.wikimedia.org ...)
	*
	* @return {string} Either the file title (without File:-namespace), if successful or undefined
	*/
	titleFromImgSrc: function(src) {
		try {
			return decodeURIComponent(src).match(/\/[a-f0-9]\/[a-f0-9]{2}\/(\S+\.\S{2,5})\//)[1].replace(/_/g, ' ');
		} catch (ex) {
			try {
				return decodeURIComponent(src).match(/thumb\.php.*(?:\?|\&)f=(\S+\.\S{2,5})(?:\&.+)?$/)[1].replace(/_/g, ' ');
			} catch (ex2) {
				try {
					return decodeURIComponent(src).match(/\/[a-f0-9]\/[a-f0-9]{2}\/(\S+\.\S{2,5})$/)[1].replace(/_/g, ' ');
				} catch (ex3) {}
			}
		}
	}
});

}( jQuery, mediaWiki ));
// </nowiki>