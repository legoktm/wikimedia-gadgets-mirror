/**
 * Validate block rollbackers
 * @desc Prevents that rollbackers in ptwikipedia blocks autoconfirmed users and of exceed the block limit (1 day).
 * @author [[w:pt:User:!Silent]]
 * @date 15/apr/2012
 * @updated 14/sep/2016
 */
/* jshint laxbreak: true */
/* global mediaWiki, jQuery */

( function ( mw, $ ) {
'use strict';

mw.messages.set( {
	'vbr-noPermissionAutoconfirmed': 'Como Reversor, você não tem permissão para bloquear este usuário, pois ele ele é um autoconfirmado.',
	'vbr-noPermissionHimself': 'Como Reversor, você não tem permissão para bloquear a si mesmo.'
} );

var $target, queue;

/**
 * Verify if the user is an autoconfirmed
 * @return {jQuery.Deferred}
 */
function isAutoconfirmed() {
	var apiDeferred = $.Deferred();

	// Prevents more than one request
	if ( !queue ) {
		queue = 1;
	} else {
		return apiDeferred.promise();
	}

	$.getJSON( mw.util.wikiScript( 'api' ), {
		action: 'query',
		list: 'allusers',
		format: 'json',
		auprop: 'implicitgroups',
		aulimit: '1',
		aufrom: $target.val(),
		auto: $target.val()
	}, function ( data ) {
		apiDeferred.resolve(
			$.isEmptyObject( data.query.allusers )
				? false
				: ( $.inArray( 'autoconfirmed', data.query.allusers[ 0 ].implicitgroups ) !== -1 )
		);

		queue = 0;
	} );

	return apiDeferred.promise();
}

/**
 * Executes
 * @return {undefined}
 */
function validateBlockRollbackers() {
	if ( $target.val() === mw.config.get( 'wgUserName' ) ) {
		$( '#mw-content-text' ).html( mw.message( 'vbr-noPermissionHimself' ).plain() );
		return;
	}

	isAutoconfirmed().done( function ( confirmed ) {
		if ( confirmed ) {
			$( '#mw-content-text' ).html( mw.message( 'vbr-noPermissionAutoconfirmed' ).plain() );
		}
	} );

	$( '#mw-input-wpExpiry' ).find( 'option' ).each( function () {
		if ( $( this ).val().search( /(seconds?|minutes|hour|1 day)/ ) === -1 ) {
			$( this ).remove();
		}
	} );

	$( '#mw-input-wpReason' ).find( 'optgroup' ).eq( 1 ).remove();
	$( '#mw-input-wpReason' ).find( 'option' ).each( function () {
		if ( $( this ).val().indexOf( 'Vandalismo' ) === -1 ) {
			$( this ).remove();
		}
	} );

	$target.blur( function () {
		isAutoconfirmed().done( function ( confirmed ) {
			if ( confirmed ) {
				alert( mw.message( 'vbr-noPermission' + ( $target.val() === mw.config.get( 'wgUserName' ) ? 'Himself' : 'Autoconfirmed' ) ).plain() );
				$target.val( '' ).focus();
			}
		} );
	} );
}

$( function() {
	$target = $( '#mw-bi-target' );
	validateBlockRollbackers();
} );

}( mediaWiki, jQuery ) );

// [[Categoria:!Código-fonte de scripts|validateBlockRollbackers/core]]