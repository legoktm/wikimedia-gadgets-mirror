/**
 * Links destacados (https://pt.wikipedia.org/wiki/Wikipédia:Esplanada/anúncios?diff=prev&oldid=791903)
 * Adiciona uma das classes 'FA', 'FL' ou 'BOM' às ligações dos artigos destacados/bons das outras Wikipédias
 * @author: [[:fr:User:Aoineko]] (https://fr.wikipedia.org/wiki/MediaWiki:Monobook.js?oldid=3104050&diff=prev)
 * @source: https://en.wikipedia.org/wiki/MediaWiki:Common.js?oldid=23027352
 */
/*jslint browser: true, white: true, plusplus: true*/
/*global jQuery, mediaWiki */
( function ( $ ) {
	'use strict';
	function LinkFA() {
		var i, li, interwikiLinks;
		if ( document.getElementById( 'p-lang' ) ) {
			interwikiLinks = document.getElementById( 'p-lang' ).getElementsByTagName( 'li' );
			for ( i = 0; i < interwikiLinks.length; i++ ) {
				li = interwikiLinks[i];
				if ( document.getElementById( li.className + '-fa' ) ) {
					li.className += ' FA';
					li.title = 'Este artigo foi classificado como destacado.';
				} else if ( document.getElementById( li.className + '-fl' ) ) {
					li.className += ' FL';
					li.title = 'Este anexo foi classificado como destacado.';
				} else if ( document.getElementById( li.className + '-bom' ) ) {
					li.className += ' BOM';
					li.title = 'Este artigo foi classificado como bom.';
				}
			}
		}
	}
	$( LinkFA );
}( jQuery ) );