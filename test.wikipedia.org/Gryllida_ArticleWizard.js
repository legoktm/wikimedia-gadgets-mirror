//
//Hold it right there!
//You are editing a page from the MediaWiki namespace. Please don't leave any tests lying around for others to clean up!
// [[User:Gryllida]]

// UI PARTS
var Gryllida={gadget:{}};

Gryllida.gadget.openDialog = function (content, actionLabel,actionFunction, helpAction = 1, extraAction) {
	section = ArticleWizard.lastAction;
	currentPosition = ArticleWizard.positions[ArticleWizard.positions.length - 1];
	console.log('...opening dialog now...');
	
	function ProcessDialog( config ) {
	  ProcessDialog.super.call( this, config );
	}
	OO.inheritClass( ProcessDialog, OO.ui.ProcessDialog );
	if(!helpAction){
		ProcessDialog.static.title = 'Help';
	} else {
		ProcessDialog.static.title = 'Wikipedia Article Creation Wizard '+ section ; // title
	}
	console.log('...populating actions...');
	// ACTIONS GO HERE
	ProcessDialog.static.actions = [
		{ label: 'Back', action: 'back', icon: 'previous' }
	];
	console.log(currentPosition)
	if(currentPosition !== 0){
		$.each(Object.keys(currentPosition), function(j,value){
		ProcessDialog.static.actions.push({action: value, label: ArticleWizard.labels[value]});
		});
	}
	console.log('...actions done...');
	console.log(ProcessDialog.static.actions);
	ProcessDialog.prototype.initialize = function () {
		ProcessDialog.super.prototype.initialize.apply( this, arguments );
		this.content = ArticleWizard.dialogContents(section);
		this.$body.append( this.content.$element );
	};

	ProcessDialog.prototype.getBodyHeight = function () {
	  return this.content.$element.outerHeight( true );
	};
	var windowManager = new OO.ui.WindowManager();
		ProcessDialog.prototype.getActionProcess = function ( action ) {
	  var dialog = this;
	  if ( action ) {
	  	console.log('Action: '+action);
	  	switch(action){
	  		// Middle points
	  		case 'back':
	  			console.log('...POP! navigating from this...')
		    	console.log(ArticleWizard.positions)
	  			ArticleWizard.positions.pop();
	  			console.log('to this...');
				console.log(ArticleWizard.positions);
				console.log('...POPPED!...')
	  			currentPosition = ArticleWizard.positions[ArticleWizard.positions.length - 1];
	  			return new OO.ui.Process( function () {
		      		dialog.close( { action: action } );
		    	} );
		    // Exit points
		    default:
		    	console.log('...navigating from this...')
		    	console.log(ArticleWizard.positions)
				ArticleWizard.positions.push(currentPosition[action]);
				console.log('to this...');
				console.log(ArticleWizard.positions);
				ArticleWizard.lastAction = action;
	  			Gryllida.gadget.openDialog(ArticleWizard.dialogContents(action));
		    	break;
	  	}
	  }
	  return ProcessDialog.super.prototype.getActionProcess.call( this, action );
	};
	$( 'body' ).append( windowManager.$element );
	var size = '';
	if(helpAction){
		size='full';
	} else{
		size='small';
	}
	var processDialog = new ProcessDialog({
	  size: size,
	  escapable: 'true'
	});
	windowManager.addWindows( [ processDialog ] );
	windowManager.openWindow( processDialog );
	ArticleWizard.windowManager = windowManager;
	return processDialog;
};

// ARTICLE WIZARD CODE HERE
var ArticleWizard={};

ArticleWizard.dialogContents = function(section){
	content = new OO.ui.PanelLayout( { padded: true, expanded: false } );
	content.$element.append($(ArticleWizard.texts[section]));
	if(section=='pageexistcheck'){
		var textInput = new OO.ui.TextInputWidget( { 
	  		placeholder: 'New article name',
	  		maxLength: 50,
	  		autofocus: true
		} );
		content.$element.append( textInput.$element );
		resultsList = $('<ol></ol>');
		content.$element.append(resultsList);
		textInput.on('enter', function(){
			resultsList.filter('ol').empty();resultsList.filter('ol').append($('<li>Searching...</li>'));
			ArticleWizard.api.get({
				action: 'query',
				list: 'search',
				srsearch: textInput.value,
				srlimit: 10
			}
			).then(function(data){
				
				if(data.query.search.length === 0) {
					resultsList.filter('ol').empty();
				}else{
					resultsList.filter('ol').empty();
					$.each(data.query.search, function(index,value){
						var text = '<li><a href="'+value.title+'">'+value.title+'</a> - '+value.snippet+'</li>';
						resultsList.filter('ol').append($(text));
					});
				}
			});
			
		});
	}
	return content;
};

ArticleWizard.flow = {
	'pageexistcheck': {
		'pageexists': 0,
		'pagedoesnotexist': {
			'wiktionary': 0,
			'wikinews': 0,
			'commons': 0,
			'wikibooks': 0,
			'wikiquote': 0,
			'wikispecies': 0,
			'wikivoyage': 0,
			'wikipedia': {
				'yesverify': {
					'english': 0,
					'not-english': 0,
				},
				'req': 0,
				'help-verify': 0,
				'make-verifiable': 0,
			}
		}
	}
};

ArticleWizard.labels = {
	'welcome': '',
	'pageexistcheck': 'Next',
	'pageexists': 'The page already exists',
	'pagedoesnotexist': 'The page does not exist yet',
	'topiccheck': 'Next',
	'wiktionary': 'A word or phrase',
	'wikinews': 'A recent event',
	'commons': 'A picture',
	'wikibooks': 'A free e-book',
	'wikiquote': 'A famous quote',
	'wikispecies': 'A new species',
	'wikivoyage': 'A travel guide',
	'wikipedia': 'An encyclopedia article',
	'varify': 'Next',
	'yesverify': 'I am confident in my sources',
	'language': 'Next',
	'english': 'English',
	'go': 'Next',
	'not-english': 'A foreign language' ,
	'req': 'Request article creation by an independent volunteer',
	'help-verify': 'Get help with sources evaluation',
	'make-verifiable': 'How do I make the subject notable?'
};

ArticleWizard.sections=Object.keys(ArticleWizard.labels);
/*
ArticleWizard._getKeys=function(hash){
	$.each(Object.keys(hash), function(j,mykey){
		subhash = hash[mykey];
		if(subhash === 0){
			ArticleWizard.sections.push(mykey);
		} else {
			ArticleWizard._getKeys(subhash);
		}
	});
};

ArticleWizard._getKeys(ArticleWizard.flow);*/
ArticleWizard.sections.push('help');
ArticleWizard.sections.push('welcome');

ArticleWizard.texts={};

mw.loader.using(['mediawiki.api', 'oojs-ui', 'mediawiki.user'], function () {
	ArticleWizard.api = new mw.Api();
	console.log('got the api');
	$.each(ArticleWizard.sections, function(index,name){
		$.get('https://test.wikipedia.org/wiki/MediaWiki:Gryllida/ArticleWizard.js/'+name+'?action=render',
		function( data ) {
			console.log('added text '+name);
			ArticleWizard.texts[name]=data;
		});
	});
	ArticleWizard.positions = [];
	ArticleWizard.positions.push(ArticleWizard.flow);
	ArticleWizard.lastAction = 'welcome';
	console.log('opening dialog');
	Gryllida.gadget.openDialog();
	/*ArticleWizard.firstScreen=1;
	// 1. welcome
	ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogContentsWelcome(), "Next", function(){
		ArticleWizard.firstScreen=0;
		// 2. check that a page does not exist
		ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogContentsSearch(), "The page I want to create is not listed", function (){
			// 3. choose topic
			/*ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogTopic(), "Encyclopedia article", function (){
				// 4. notability intro, sources
				ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogInclusionIntro(), "I am confident in my sources", function (){
					// 5. copyright and tone
					ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogContentIntro(), "I will write neutrally and I will not copy paste from websites into Wikipedia", function (){
						// 6. Go
						ArticleWizard.dialog = Gryllida.gadget.openDialog(ArticleWizard.dialogGo(), "", function(){});
					});
				}, 1, [
					{label: 'Make the subject notable', action: 'not-notable', icon: 'redo'},
					{label: 'Get help with evaluating sources for notability', action: 'sources-help', icon: 'comment'},
					{label: 'Ask a volunteer to create the article', action: 'req', icon: 'comment'}
				]
				);
			}, 1, [
				{label: 'Word or phrase', icon: , action: 'wiktionary'},
				{label: 'Picture', icon: 'picture', action: 'commons'},
				{label: 'E-book (unpublished)', icon: 'add', action: 'wikibooks'},
				{label: 'Classic content (published)', icon: 'add', action: 'wikisource'},
				{label: 'A quote from a famous person', icon: 'comment', action: 'wikiquote'},
				{label: 'New species', icon: 'add', action: 'wikispecies'},
				{label: 'News article', icon: 'history', action: 'wikinews'},
				{label: 'Learning material', icon: 'add', action: 'wikiversity'},
				{label: 'Travel guide', icon: 'add', action: 'wikivoyage'}
			]);
		}, 1, [{action: 'editExistingPage', label: 'The page I want to create already exists', icon: 'check'}]);
	});*/

});