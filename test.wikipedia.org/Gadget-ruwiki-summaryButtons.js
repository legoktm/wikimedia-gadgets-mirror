console.log('Гаджет', $.now());

// Код из польской Википедии, изначально адаптированный для русскоязыного раздела Википедии by [[u:Alex Smotrov]]
// https://ru.wikipedia.org/w/index.php?diff=32005736&oldid=32004004&title=MediaWiki%3AEditpage.js&type=revision

function insertSummary( txt ) {
	if ( typeof txt !== 'string' ) {
		txt = this.title;
	}
	if (typeof summaryItemsSeparator == 'undefined') {
		window.summaryItemsSeparator = ',';
	}
	var val = $( '#wpSummary' ).val();
	var regExp = new RegExp( '(^|[,;.?!/]) ?' + mw.RegExp.escape(txt) );
	if ( regExp.test( val ) ) {
		return;
	}
	if ( /[^,; \/]$/.test( val ) ) {
		val += summaryItemsSeparator;
	}
	if ( /[^ ]$/.test( val ) ) {
		val += ' ';
	}
	$( '#wpSummary' ).val( val + txt );
}
 
function addSumButton( btn, txt ) {
	$( '<a class="mw-ui-button summaryButtons-button summaryButtons-button-default" title="' + txt + '">' + btn + '</a>' )
		.appendTo( '.summaryButtons-summaryGroup' )
		.click( insertSummary );
}
 
window.addSumButtonNew = window.addSumButtonNew || function ( btn, txt ) {
	$( '<a class="mw-ui-button summaryButtons-button summaryButtons-button-custom" title="' + txt + '">' + btn + '</a>' )
		.appendTo( '.summaryButtons-summaryGroup' )
		.click( insertSummary );
}

$( function() {
	if (typeof summaryButtons === 'undefined') {
		window.summaryButtons = {
			hideDefaultButtons: false,
		}
	}
	
	var frm = document.getElementById( 'editform' );
	if ( !mw.config.get( 'wgArticleId' ) || !frm || $( frm.wpSection ).val() === 'new' ) {
		return;
	}
	
	$( '<div id="userSummaryButtonsB" class="summaryButtons"></div>' ).insertAfter( '#wpSummary' );
	$( '<div class="mw-ui-button-group summaryButtons-summaryGroup"></div>' ).appendTo( '#userSummaryButtonsB' );
	if ( !summaryButtons.hideDefaultButtons ) {
		$.each(
			[
				'викиф|икация', 'оформл|ение', 'стил|евые правки', 'грам|матика',
				'вопрос', 'ответ', 'комм|ентарий', 'кат|егоризация',
				'к удал|ению', 'илл|юстрация', 'источ|ники', 'запр|ос источника',
				'доп|олнение', 'уточн|ение', 'испр|авление', 'обнов|ление', 'закр|ыто', 'итог'
			],
			function ( i, s ) {
				addSumButton( s.replace( /\|.*/, '' ), s.replace( /\|/, '' ) );
			}
		);
	}
	$( '<div class="summaryButtons-summaryInfo"><a class="mw-ui-button mw-ui-quiet summaryInfo-button" title="Информация о кнопках описания правок">?</a></div>' )
	.appendTo( '#userSummaryButtonsB' )
} );