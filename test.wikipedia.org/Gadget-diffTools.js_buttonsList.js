/**
 * Buttons of "Reversão e avisos"
 *
 * @see [[MediaWiki:Gadget-diffTools.js]]
 */
/* jshint laxbreak: true */
/*global mediaWiki, jQuery, diffTools */

( function ( mw, $, df ) {
'use strict';

var pageName = df.pageName;

mw.messages.set( {
	// General
	'df-getPageHistory': 'Consultando o histórico da página "<a href="$1">$2</a>"...',
	'df-notifyTitle': 'Reversão e avisos',
	'df-diffTools': 'Reversão e avisos',
	'df-OK': 'OK',
	'df-cancel': 'Cancelar',

	// Dialog
	'df-dialog-commentary': 'Comentário a acrescentar ao resumo da edição',
	'df-dialog-remainingChar': 'Caracteres restantes: $1',
	
	// Edit
	'df-edit-preparing': 'Preparando a edição da página "$1"...',
	'df-edit-summaryPrefix': 'Desfeita(s) uma ou mais edições de [[Special:Contribs/$1|$1]]',
	'df-edit-summarySufix': ' com [[WP:RA|Reversão e avisos]].',
	'df-edit-success': '<p>A página "$1" <a href="$2">foi editada</a> (<a href="$3">abrir</a>).</p>',

	// Errors
	'df-error-API': 'Erro da API: $1. $2',
	'df-error-pageNotExist':' A página "<a href="$1">$2</a> não existe.',
	'df-error-unknown':'Houve um erro inesperado ao usar a API do MediaWiki.',
	'df-error-requestFail': 'Houve um erro ao requisitar a edição da página.',
	'df-error-ajaxFail-1':'Houve um erro ao usar AJAX para consultar o conteúdo da página.',
	'df-error-ajaxFail-2': 'Houve um erro ao usar AJAX para editar a página.'
} );

$.extend( df.buttons, {
	'revert': {
		'description': false,

		'Opções': {
			'Reverter': {
				'desc': 'Desfaz as últimas edições do artigo, para retornar à'
					+ ' versão mostrada à esquerda',
				'sum': 'Desfeita(s) uma ou mais edições de [[Special:Contribs/$2|$2]],'
			},

			'+comentário': {
				'desc': 'Desfaz as últimas edições do artigo para retornar à'
					+ ' versão mostrada à esquerda, mas permite incluir informações'
					+ ' extras no resumo da edição',
				'url': function () {
					df.revertWithComment();
				}
			}
		}
	},

	'warn': {
		'description': 'Usuário: ',

		'Boas-vindas': {
			'bv': {
				'desc': 'Envia uma mensagem de boas-vindas ao novo usuário',
				'subst': 'Bem-vindo',
				'sum': 'Mensagem de boas-vindas a novo usuário'
			},

			'bv-ip': {
				'desc': 'Envia uma mensagem de boas-vindas ao usuário anônimo',
				'subst': 'Bem-vindo IP',
				'sum': 'Mensagem de boas-vindas a usuário anônimo'
			}
		},

		'Avisos 1': {
			'av': {
				'desc': 'Envia um aviso ao usuário',
				'subst': 'Aviso|' + pageName,
				'sum': 'Aviso'
			},

			'av2': {
				'desc': 'Envia um segundo aviso ao usuário',
				'subst': 'Aviso2|' + pageName,
				'sum': 'Aviso 2'
			},

			'bv-av-reg': {
				'desc': 'Envia uma mensagem de boas-vindas e um aviso ao usuário',
				'subst': 'Bv-av-registrado|' + pageName,
				'sum': 'Mensagem de boas-vindas e aviso a usuário registrado'
			},

			'bv-av': {
				'desc': 'Envia uma mensagem de boas-vindas e um aviso ao usuário iniciante',
				'subst': 'Bv-av|' + pageName,
				'sum': 'Mensagem de boas-vindas e aviso a iniciante'
			}
		},

		'Avisos 2': {
			'propaganda': {
				'desc': 'Envia um aviso sobre propagandas ao usuário',
				'subst': 'Propaganda|' + pageName,
				'sum': 'Aviso sobre propaganda'
			},

			'mudança de grafia': {
				'desc': 'Envia um aviso sobre mudança de grafia ao usuário',
				'subst': 'Ortografia|' + pageName,
				'sum': 'Aviso sobre ortografia'
			},

			'assinatura em artigo': {
				'desc': 'Envia um aviso sobre assinatura em artigo ao usuário',
				'subst': 'Aviso-assinatura|' + pageName,
				'sum': 'Aviso sobre assinatura em artigo'
			},

			'cópia': {
				'desc': 'Envia um aviso sobre os direitos de autor ao usuário',
				'subst': 'Aviso-cópia',
				'sum': 'Aviso sobre direitos de autor'
			},

			'data': {
				'desc': 'Envia um aviso sobre datas ao usuário',
				'subst': 'Av-data|' + pageName,
				'sum': 'Aviso sobre datas'
			},

			'cite fonte': {
				'desc': 'Envia um aviso sobre não citar as fontes ao usuário.',
				'subst': 'cite fonte|' + pageName,
				'sum': 'Aviso sobre não citar as fontes'
			},

			'mostrar previsão': {
				'desc': 'Envia um aviso sobre como mostrar previsão.',
				'subst': 'mostrar previsão|' + pageName,
				'sum': 'Aviso sobre como mostrar previsão'
			},

			'Av-interwiki': {
				'desc': 'Envia um aviso sobre a falta de interwiki na página.',
				'subst': 'av-interwiki|' + pageName + '|~~' + '~~',
				'sum': 'Aviso sobre a falta de interwikis'
			},

			'Aviso-categoria': {
				'desc': 'Envia um aviso sobre a falta de categoria na página.',
				'subst': 'av-categoria|' + pageName + '|~~' + '~~',
				'sum': 'Aviso sobre a falta de categoria'
			}
		},

		'Avisos de remoção de marcação de eliminação': {
			'av-nr': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação ao usuário',
				'subst': 'Não remova|' + pageName,
				'sum': 'Aviso sobre remoção da marcação de eliminação'
			},

			'av-nr-er': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação rápida ao usuário',
				'subst': 'Não remova|' + pageName + '|er',
				'sum': 'Aviso sobre remoção da marcação de eliminação rápida'
			},

			'av-nr-esr': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação semirrápida ao usuário',
				'subst': 'Não remova|' + pageName + '|esr',
				'sum': 'Aviso sobre remoção da marcação de eliminação semirrápida'
			},

			'av-nr-matrad': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação por problemas de tradução ao usuário',
				'subst': 'Não remova|' + pageName + '|matrad',
				'sum': 'Aviso sobre remoção da marcação de eliminação por problemas de tradução'
			},

			'av-nr-pe': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação por consenso ao usuário',
				'subst': 'Não remova|' + pageName + '|pe',
				'sum': 'Aviso sobre remoção da marcação de eliminação por consenso'
			},

			'av-nr-susp': {
				'desc': 'Envia um aviso sobre remoção da marcação de suspeita de violação de direitos autorais ao usuário',
				'subst': 'Não remova|' + pageName + '|suspeito',
				'sum': 'Aviso sobre remoção da marcação de suspeita de violação de direitos autorais'
			},

			'av-nr-vda': {
				'desc': 'Envia um aviso sobre remoção da marcação de eliminação por problemas de direitos autorais ao usuário',
				'subst': 'Não remova|' + pageName + '|vda',
				'sum': 'Aviso sobre remoção da marcação de eliminação por problemas de direitos autorais'
			}
		}
	},

	'Vandalismos': {
		'description': 'Vandalismo: ',

		'avisos': {
			'1': {
				'desc': 'Envia o primeiro aviso sobre vandalismo ao usuário',
				'subst': 'Vandalismo|1|' + pageName,
				'sum': 'Aviso sobre vandalismo'
			},

			'2': {
				'desc': 'Envia o segundo aviso sobre vandalismo ao usuário',
				'subst': 'Vandalismo|2|' + pageName,
				'sum': 'Segundo aviso sobre vandalismo'
			},

			'3': {
				'desc': 'Envia o terceiro aviso sobre vandalismo ao usuário',
				'subst': 'Vandalismo|3|' + pageName,
				'sum': 'Terceiro aviso sobre vandalismo'
			},

			'bloq': {
				'desc': 'Envia a notificação de bloqueio pelo vandalismo ao usuário',
				'subst': 'Vandalismo|b|' + pageName,
				'sum': 'Notificação de bloqueio pelo vandalismo'
			}
		}
	},

	'Spam': {
		'description': 'Spam: ',

		'avisos': {
			'1': {
				'desc': 'Envia o primeiro aviso sobre spam ao usuário',
				'subst': 'Av-Spam|1|' + pageName,
				'sum': 'Aviso sobre spam'
			},

			'2': {
				'desc': 'Envia o segundo aviso sobre spam ao usuário',
				'subst': 'Av-Spam|2|' + pageName,
				'sum': 'Segundo aviso sobre spam'
			},

			'3': {
				'desc': 'Envia o terceiro aviso sobre spam ao usuário',
				'subst': 'Av-Spam|3|' + pageName,
				'sum': 'Terceiro aviso sobre spam'
			},

			'bloq': {
				'desc': 'Envia a notificação de bloqueio por spam ao usuário',
				'subst': 'Av-Spam|b|' + pageName,
				'sum': 'Notificação de bloqueio por spam'
			}
		}
	},

	'Difamação': {
		'description': 'Difamações: ',

		'avisos': {
			'1': {
				'desc': 'Envia o primeiro aviso sobre difamações em biografias de pessoas vivas',
				'subst': 'Av-Bpv|1|' + pageName,
				'sum': 'Aviso sobre adição de difamação '
			},

			'2': {
				'desc': 'Envia o segundo aviso sobre difamações em biografias de pessoas vivas',
				'subst': 'Av-Bpv|2|' + pageName,
				'sum': 'Segundo aviso sobre difamação'
			},

			'3': {
				'desc': 'Envia o terceiro aviso sobre difamações em biografias de pessoas vivas',
				'subst': 'Av-Bpv|3|' + pageName,
				'sum': 'Terceiro aviso sobre difamação'
			},

			'bloq': {
				'desc': 'Envia a notificação de bloqueio por difamações em biografias de pessoas vivas',
				'subst': 'Av-Bpv|b|' + pageName,
				'sum': 'Notificação de bloqueio por difamação'
			}
		}
	},

	'Testes': {
		'description': 'Testes: ',

		'avisos': {
			'1': {
				'desc': 'Envia o primeiro aviso sobre testes ao usuário',
				'subst': 'Av-teste|1|' + pageName,
				'sum': 'Aviso sobre testes'
			},

			'2': {
				'desc': 'Envia o segundo aviso sobre testes ao usuário',
				'subst': 'Av-teste|2|' + pageName,
				'sum': 'Segundo aviso sobre testes'
			},

			'3': {
				'desc': 'Envia o terceiro aviso sobre testes ao usuário',
				'subst': 'Av-teste|3|' + pageName,
				'sum': 'Terceiro aviso sobre testes'
			},

			'bloq': {
				'desc': 'Envia a notificação de bloqueio por testes ao usuário',
				'subst': 'Av-teste|b|' + pageName,
				'sum': 'Notificação de bloqueio por testes'
			}
		}
	},

	'Remoção': {
		'description': 'Remoção: ',

		'avisos': {
			'1': {
				'desc': 'Envia o primeiro aviso sobre remoção de conteúdos ao usuário',
				'subst': 'Av-Remoção|1|' + pageName,
				'sum': 'Aviso sobre remoção de conteúdos'
			},

			'2': {
				'desc': 'Envia o segundo aviso sobre remoção de conteúdos ao usuário',
				'subst': 'Av-Remoção|2|' + pageName,
				'sum': 'Segundo aviso sobre remoção de conteúdos'
			},

			'3': {
				'desc': 'Envia o terceiro aviso sobre remoção de conteúdos ao usuário',
				'subst': 'Av-Remoção|3|' + pageName,
				'sum': 'Terceiro aviso sobre remoção de conteúdos'
			},

			'bloq': {
				'desc': 'Envia a notificação de bloqueio por remoção de conteúdos ao usuário',
				'subst': 'Av-Remoção|b|' + pageName,
				'sum': 'Notificação de bloqueio por remoção de conteúdos'
			}
		}
	}
} );

}( mediaWiki, jQuery, diffTools ) );