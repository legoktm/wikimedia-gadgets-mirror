/*jslint white: true */
/*global mw, $ */
if ( $.inArray( mw.config.get( 'wgPageName' ), [
	'Wikipédia:Esplanada/geral',
	'Wikipédia:Esplanada/propostas'
] ) > -1 ) {
	mw.loader.load( 'ext.gadget.NewVillagePumpCore' );
}