/*jslint white: true */
/*global mw, $ */
if ( $.inArray( mw.config.get( 'wgPageName' ), [
	'Wikimedia:Ágora'
] ) > -1 ) {
	mw.loader.load( 'ext.gadget.NewVillagePumpCore' );
}