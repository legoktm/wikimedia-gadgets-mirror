// script for [[:de:Wikipedia:Persönliche Bekanntschaften]]
// author: [[:de:Benutzer:Euku]]
// second part: [[MediaWiki:Gadget-PB2.js]]
// <nowiki>

window.persBekannt = {
  workPage: "Wikipedia:Persönliche_Bekanntschaften/neue_Anfragen",
  requestPage: "Wikipedia:Pers%C3%B6nliche_Bekanntschaften/neue_Eintr%C3%A4ge",
  userList: "Wikipedia:Pers%C3%B6nliche Bekanntschaften/JS-alle Benutzer",
  bigUserList: "Wikipedia:Pers%C3%B6nliche_Bekanntschaften/Teilnehmerliste",
  newUserList: "Wikipedia:Pers%C3%B6nliche_Bekanntschaften/Neu_dazugekommen",
  versionCheckPage: "User:Euku/PB-version.css",
  PBJSversion: 19
};

// load [[MediaWiki:Gadget-PB2.js]] if necessary
var myArticlePath = mw.config.get('wgArticlePath').replace(/\$1/g, "");
if ((mw.config.get('wgPageName') === persBekannt.workPage || mw.config.get('wgPageName') === decodeURIComponent(persBekannt.bigUserList))
	&& (mw.config.get('wgAction') === 'view' || mw.config.get('wgAction') === 'purge')) {
   importScript('MediaWiki:Gadget-PB2.js');
   // load detailed user list from WMFLabs
   //$.getScript("//tools.wmflabs.org/pb/index.py?p=user-jsonp&name=" + mw.config.get('wgUserName'));
}
// </nowiki>