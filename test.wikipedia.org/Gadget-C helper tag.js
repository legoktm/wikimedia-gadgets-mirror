//<nowiki>
C.tag = {
  dialog: null,
  config: {
    'modal-title': 'Chelper - Ajouter/retirer des bandeaux de maintenance',
    'submit': 'valider la requête',
    'c-tag-checkbox-class': 'c-tag-checkbox',
    'c-tag-radio-class': 'c-tag-radio',
    'notify-sucess': 'Les bandeaux ont bien été ajoutés.',
    'notify-no-reason': 'Vous n\'avez fait aucun changement.',
    'notify-not-a-page': 'Impossible de gérer les bandeaux d\'une page inexistante.',
    'notify-protected': 'Impossible de gérer les bandeaux de cette page, celle-ci est protégé.',
    'single-add-summary': 'Ajout de $1',
    'multi-add-summary': 'Ajout de $1 et $2',
    'template-link-summary': '{{[[Modèle:$1|$1]]}}',
  },
  categories: [
  	{display:"Admissibilité", node:null}, //0
  	{display:"Neutralité", node:null},    //1
  	{display:"Mise en forme", node:null}, //2
  	{display:"Sourçage", node:null}, //3
  ],
  /*
    {category:, display:'', template:'', help:'', reason:'', extra:''},
	Magic words :
		* $(page)
		* $(diff)
		* $(user)
		* $(day) $(month) $(year)
		* $(reason)
		* $(extra)
  */
  templates: [
	{category:2, display:'À APIyer', template:'{{À APIyer}}'},
	{category:2, display:'À dater', template:'{{À dater}}'},
	{category:2, display:'à déjargoniser', template:'{{à déjargoniser}}'},
	{category:2, display:'À délister', template:'{{À délister}}'},
	{category:2, display:'À désacadémiser', template:'{{À désacadémiser}}'},
	{category:2, display:'À désangliciser', template:'{{À désangliciser}}'},
	{category:2, display:'À fusionner', template:'{{À fusionner|$(extra)}}', extra:'Liste des articles à fusionner, séparés par des « | » :'},
	{category:2, display:'À illustrer', template:'{{À illustrer}}'},
	{category:2, display:'À recycler', template:'{{À recycler|date=$(month) $(year)}}'},
	{category:2, display:'À TeXifier', template:'{{À TeXifier}}'},
	{category:3, display:'À vérifier', template:'{{À vérifier}}'},
	{category:3, display:'À vérifier/biographie', template:'{{À vérifier/biographie}}'},
	{category:3, display:'À vérifier/droit', template:'{{À vérifier/droit}}'},
	{category:3, display:'À vérifier/généalogie', template:'{{À vérifier/généalogie}}'},
	{category:3, display:'À vérifier/histoire', template:'{{À vérifier/histoire}}'},
	{category:3, display:'À vérifier/sciences', template:'{{À vérifier/sciences}}'},
	{category:2, display:'À wikifier', template:'{{À wikifier|date=$(month) $(year)}}'},
	{category:0, display:'Admissibilité à vérifier', template:'{{Admissibilité à vérifier|date=$(month) $(year)|motif=$(reason)}}', reason:'Motif :'},
	{category:2, display:'Alerte langue', template:'{{Alerte langue|$(reason)|$(extra)}}', extra:'Nom de la langue, tel qu’utilisé dans l’article relatif à cette langue :', reason:'nom de la langue utilisée ainsi qu’au moins une lettre de cette langue qui pourrait poser des problèmes d’affichage :'},
	{category:1, display:'Anecdotes', template:'{{Anecdotes}}'},
	{category:1, display:'Anthropocentrisme', template:'{{Anthropocentrisme|date=$(month) $(year)}}'},
	{category:2, display:'Article court', template:'{{Article court|$(extra)}}', extra:'Article dans lequel le sujet est plus développé :'},
	{category:2, display:'Article incomplet', template:'{{Article incomplet}}'},
	{category:2, display:'Article incompréhensible', template:'{{Article incompréhensible|date=$(month) $(year)}}'},
	{category:2, display:'Article mal proportionné', template:'{{Article mal proportionné}}'},
	{category:1, display:'Article non neutre', template:'{{Article non neutre}}'},
	{category:2, display:'Av. J.-C.', template:'{{Av. J.-C.}}'},
	{category:2, display:'Avertissement Liste', template:'{{Avertissement Liste|$(reason)|$(extra)}}', extra:'Catégorie liée à la liste (sans le préfixe Catégorie: ; facultatif) :', reason:'Éléments référencés :'},
	{category:2, display:'Avertissement filiations franques', template:'{{Avertissement filiations franques}}'},
	{category:2, display:'Avertissement liste wikidata', template:'{{Avertissement liste wikidata}}'},
	{category:2, display:'Avis cégep', template:'{{Avis cégep}}'},
	{category:2, display:'Confusion', template:'{{Confusion|$(reason)}}', reason:'Lien(s) vers un ou plusieurs autres articles, séparés par des « | » :'},
	{category:3, display:'Conventions bibliographiques', template:'{{Conventions bibliographiques|date=$(month) $(year)}}'},
	{category:2, display:'Copie de site', template:'{{Copie de site|1=$(reason)}}', reason:'Adresse du site copié : '},
	{category:1, display:'Désaccord de neutralité', template:'{{Désaccord de neutralité|date=$(month) $(year)}}'},
	{category:1, display:'Désaccord de pertinence', template:'{{Désaccord de pertinence|date=$(month) $(year)}}'},
	{category:3, display:'Détournement de sources', template:'{{Détournement de sources}}'},
	{category:2, display:'Étymologie manquante', template:'{{Étymologie manquante|date=$(month) $(year)}}'},
	{category:2, display:'Exclamation', template:'{{Exclamation|$(reason)}}', reason:'Message de l\'avertissement :'},
	{category:2, display:'Fusion d\'Historique', template:'{{Fusion d\'Historique|$(reason)|~~~}}', extra:'', reason:'Motif de la fusion :'},
	{category:2, display:'Fusion technique', template:'{{Fusion technique|$(extra)}}', extra:'Liste des articles qui font l\'objet de la fusion, sparés par des « | » :'},
	{category:2, display:'Info ruby', template:'{{Info ruby}}'},
	{category:1, display:'Introduction régionale', template:'{{Introduction régionale|date=$(month) $(year)}}'},
	{category:3, display:'Lien internet incomplet', template:'{{Lien internet incomplet}}'},
	{category:2, display:'Lire d\'abord', template:'{{Lire d\'abord|$(extra)}}', extra:'Liste des articles à lire d\'abord, séparés par des « | » :', reason:''},
	{category:2, display:'Math2fr', template:'{{Math2fr}}'},
	{category:2, display:'Mettre à jour', template:'{{Mettre à jour|$(reason)|date=$(month) $(year)|commentaire=$(extra)}}', extra:'Justification de la présence du bandeau (facultatif) :', reason:'Date de dernière mise à jour (facultatif) :'},
	{category:0, display:'Notoriété art', template:'{{Notoriété art|date=$(month) $(year)}}'},
	{category:3, display:'Pas de liens externes', template:'{{Pas de liens externes}}'},
	{category:2, display:'Plusieurs en cours', template:'{{Plusieurs en cours}}'},
	{category:1, display:'Point de vue interne', template:'{{Point de vue interne|date=$(month) $(year)}}', extra:'', reason:''},
	{category:0, display:'Pour Wikiquote', template:'{{Pour Wikiquote}}'},
	{category:1, display:'POV fork', template:'{{POV fork}}'},
	{category:2, display:'Redirect confusion', template:'{{Redirect confusion|$(reason)|$(extra)}}', extra:'Confusion possible avec l\'article :', reason:'Nom de la redirection :'},
	{category:2, display:'Remix', template:'{{Remix|$(reason)}}', extra:'', reason:'Autre article :'},
	{category:2, display:'Résumé introductif', template:'{{Résumé introductif|date=$(day) $(month) $(year)}}'},
	{category:2, display:'Résumé introductif trop court', template:'{{Résumé introductif trop court|date=$(day) $(month) $(year)|date=$(day) $(month) $(year)}}'},
	{category:2, display:'Résumé introductif trop long', template:'{{Résumé introductif trop long|date=$(day) $(month) $(year)}}'},
	{category:1, display:'Scrutin en cours', template:'{{Scrutin en cours}}'},
	{category:3, display:'Sources obsolètes', template:'{{Sources obsolètes|date=$(month) $(year)}}'},
	{category:2, display:'Style non encyclopédique', template:'{{Style non encyclopédique|date=$(month) $(year)}}'},
	{category:2, display:'Synopsis', template:'{{Synopsis|date=$(month) $(year)}}'},
	{category:2, display:'Titre incorrect', template:'{{Titre incorrect|1=$(reason)}}', reason:'Titre souhaité :'},
	{category:3, display:'Travail inédit', template:'{{Travail inédit|date=$(month) $(year)|Cet article peut contenir}}'},
	{category:2, display:'Trop d\'images', template:'{{Trop d\'images|date=$(month) $(year)}}'},
	{category:2, display:'Trop de citations', template:'{{Trop de citations|date=$(month) $(year)}}'},
	{category:2, display:'Trop de liens', template:'{{Trop de liens|date=$(month) $(year)}}'},
	{category:2, display:'Trop de wikiliens', template:'{{Trop de wikiliens|date=$(month) $(year)}}'},
	{category:2, display:'Trop long', template:'{{Trop long}}'},
	{category:0, display:'Vérifiabilité', template:'{{Vérifiabilité|date=$(month) $(year)}}'},
	{category:0, display:'Vie privée', template:'{{Vie privée|date=$(month) $(year)}}'},
  ],
  categories_container: null,
  alphabetic_container: null,
  current_display_mode: "categories",

  init: function() {
  },
  launch: function() {
  	if(mw.config.get('wgArticleId') === 0) {
    	mw.notify(C.tag.config['notify-not-a-page'], {title:'C-helper', type:'error'});
    	return;
  	}
  	if(mw.config.get('wgRestrictionEdit') == ["sysop"]) {
    	mw.notify(C.tag.config['notify-protected'], {title:'C-helper', type:'error'});
    	return;
  	}
	if(this.dialog === null) {
		this.build_dialog();
	}
	this.dialog.dialog("open");
  },
  "build_dialog": function() {
    this.dialog = $('<div/>', {title:this.config['modal-title']});
    var form = $('<form/>');
    this.dialog.append(form);
    //Radio
    $("<div/>").text("Trier par ")
               .append($("<input/>", {type:"radio",name:"C-tag-display-mode", value:"categories", id:"C-tag-display-mode-categories", class:this.config['c-tag-radio-class'], checked: "checked"}))
               .append($("<label/>", {"for":"C-tag-display-mode-categories"}).text("Catégories"))
               .append($("<input/>", {type:"radio",name:"C-tag-display-mode", value:"alphabetic", id:"C-tag-display-mode-alphabetic", class:this.config['c-tag-radio-class']}))
               .append($("<label/>", {"for":"C-tag-display-mode-alphabetic"}).text("Ordre alphabétique"))
               .appendTo(form);

    //Create sections
    this.alphabetic_container = $("<div/>", {id:"C-tag-alphabetic-container"});
    this.categories_container = $("<div/>", {id:"C-tag-categories-container"});
    for(i=0; i<this.categories.length; i++) {
    	this.categories[i].node = $("<div/>").append($("<h3/>").text(this.categories[i].display)).appendTo(this.categories_container);
    }
    for(i=0; i<this.templates.length; i++) {
		this.templates[i].node = $("<div/>");
		this.templates[i].node.append($("<input/>", {type:"checkbox", id:"C-tag-"+i, tag_id:i, class:this.config['c-tag-checkbox-class']}));
		this.templates[i].node.append($("<label/>", {"for":"C-tag-"+i}).text(this.templates[i].display));
		
		if(this.templates[i].help !== '') {
			this.templates[i].node.append(C.util.construct_help_icon(this.templates[i].help));
		}
		if(this.templates[i].hasOwnProperty('reason')) {
			this.templates[i].node.append($('<span/>', {id:'C-tag-reason-'+i, class:this.config['c-tag-reason-class']})
			                      		.hide()
			                      		.append($('<br/>'))
			                            .append($('<label/>', {"for":'C-tag-reason-input-'+i}).html(this.templates[i].reason))
			                            .append("&nbsp;")
			    						.append($('<input/>', {type:'text', id:'C-tag-reason-input-'+i})));
		}
		if(this.templates[i].hasOwnProperty('extra')) {
			this.templates[i].node.append($('<span/>', {id:'C-tag-extra-'+i, class:this.config['c-tag-extra-class']})
			                      		.hide()
			                      		.append($('<br/>'))
			                            .append($('<label/>', {"for":'C-tag-extra-input'+i}).html(this.templates[i].extra))
			                            .append("&nbsp;")
			    						.append($('<input/>', {type:'text', id:'C-tag-extra-input'+i})));
		}
		
		this.categories[this.templates[i].category].node.append(this.templates[i].node);
    }
    form.append(this.alphabetic_container);
    form.append(this.categories_container);
    
    this.dialog.dialog({
      autoOpen: false,
      height: 400,
      width: 600,
      modal: true,
      buttons: [
        {
          text: C.tag.config['submit'],
          click: function() {
            C.tag.dialog.dialog("close");
            C.tag.validate();
          },
        },
      ],
    dialogClass: 'c-helper-dialog',
    });
    $('.'+this.config['c-tag-radio-class']).change(function() {
    	C.tag.switch_display_mode($(this).val());
    });
	$('.'+this.config['c-tag-checkbox-class']).change(function() {
		if($(this).is(":checked")) {
			$('#C-tag-reason-'+$(this).attr('tag_id')).show();
			$('#C-tag-extra-'+$(this).attr('tag_id')).show();
		}
		else {
			$('#C-tag-reason-'+$(this).attr('tag_id')).hide();
			$('#C-tag-extra-'+$(this).attr('tag_id')).hide();
		}
	});
  },
  switch_display_mode: function(new_mode) {
  	if(this.current_display_mode != new_mode) {
  		if(new_mode == "categories") {
  			$("#C-tag-categories-container").show();
  			for(i=0; i<this.templates.length; i++) {
				this.categories[this.templates[i].category].node.append(this.templates[i].node);
			}
			$("#C-tag-alphabetic-container").hide();
  		}
  		else {
			$("#C-tag-alphabetic-container").show();
  			for(i=0; i<this.templates.length; i++) {
				this.alphabetic_container.append(this.templates[i].node);
			}
  			$("#C-tag-categories-container").hide();
  		}
  		this.current_display_mode = new_mode;
  	}
  },
  validate: function() {
  	var to_prepend = "";
  	var summary_array = [];
  	for(i=0; i<this.templates.length; i++) {
  		if($('#C-tag-'+i).is(':checked')) {
  			tmp = this.templates[i].template.replace(/\$\(day\)/g, "13")
  			                             .replace(/\$\(month\)/g, "December")
  			                             .replace(/\$\(year\)/g, "2015")
  			                             .replace(/\$\(page\)/g, mw.config.get('wgPageName'))
  			                             .replace(/\$\(diff\)/g, mw.config.get('wgRevisionId'))
  			                             .replace(/\$\(user\)/g, mw.config.get('wgUserName'));
  			if($('#C-tag-reason-input-'+i).length > 0) {
  				tmp = tmp.replace(/\$\(reason\)/g, $('#C-tag-reason-input-'+i).val());
  			}
  			if($('#C-tag-extra-input'+i).length > 0)
  				tmp = tmp.replace(/\$\(extra\)/g, $('#C-tag-extra-input'+i).val());
  				
  			to_prepend += tmp+'\n';
  			summary_array.push(this.templates[i].display);
  		}
  	}
  	if(summary_array.length > 0) {
  		if(summary_array.length == 1) {
  			summary = this.config['single-add-summary'].replace(/\$1/g, this.config['template-link-summary'].replace(/\$1/g, summary_array[0]));
  		}
  		else {
  			summary = "";
  			while(summary_array.length > 2)
  				summary += this.config['template-link-summary'].replace(/\$1/g, summary_array.shift()) + ", ";
  			summary += this.config['template-link-summary'].replace(/\$1/g, summary_array.shift());
  			summary = this.config['multi-add-summary'].replace(/\$1/g, summary).replace(/\$2/g, this.config['template-link-summary'].replace(/\$1/g, summary_array[0]));
  		}
  		C.util.prepend(null, to_prepend, summary, function() {
  			mw.notify(C.tag.config['notify-sucess'], {title:'C-helper', type:'info'});
	    	C.util.reload();
  		});
  	}
  },
};

C.modules.tag.callback = C.tag;
//</nowiki>