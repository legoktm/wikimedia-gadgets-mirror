//* by [[User:Ianezz]] ([[Commons:Village_pump/Archive/2009Sep#CSS_placement_of_categories]]), based on [[:wikinews:Help:User_style#Moving_categories_to_top]]
 
function catsattop() {
  if (wgCanonicalNamespace=="Special") return;
  var cats = document.getElementById('catlinks');
  var bc = document.getElementById('bodyContent');
  bc.insertBefore(cats, bc.childNodes[0]);
}
 
hookEvent ('load', catsattop);