/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Imported from version XXXX as of DATE from [[SCRIPT_SOURCE]]
 * SHORT_DESCRIPTION, see [[SCRIPT_HOME_PAGE]]
 */
 // Version 2: 50 most revisions always, if available

// Page title
var gPageName = mw.config.get( 'wgPageName' );

// Gives us the most current revision ID for the page
var gCurRevisionId = mw.config.get( 'wgCurRevisionId' );

// Action: view/query/history
var gAction = mw.config.get( 'wgAction' );

// User name
var gUserName = mw.config.get( 'wgUserName' );

// The URL of the page, relative to DOCUMENT_ROOT
var gScript = mw.config.get( 'wgScript' );

// Revision ID of right revision on diff page
var gRightRevID = mw.config.get( 'wgRevisionId' );

// Revision ID of left revision on diff page
var gLeftRevID = mw.util.getParamValue( 'oldid' );

// Get value of diff param in URL
var gDiff = mw.util.getParamValue( 'diff' );

// Page ID
var gPageID = mw.config.get( 'wgArticleID' );

// Server
var gServer = mw.config.get( 'wgServer' );

// Function called when a tick on the slider is clicked
// Params: v1 - Left revision ID; v2 - Right revision ID
function refresh( v1, v2 ) {
    var $url = gServer + gScript + '?title=' + gPageName + '&diff=' + v2 + '&oldid=' + v1;
    location.href = $url;
}


// Formating date in JS
function formatDate( rawDate ) {
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
	var f = new Date( rawDate );
	var fDate = f.getUTCDate();
	var fMonth = f.getUTCMonth();
	var fYear = f.getUTCFullYear();
	var fHours = ( '0' + f.getUTCHours()).slice(-2);
	var fMinutes = ( '0' + f.getUTCMinutes()).slice(-2);
	return ( fHours + ':' + fMinutes + ', ' + fDate + ' ' + months[fMonth] + ' ' + fYear ).toString();
}


// Setting the tick marks on the slider
// Params: element - jQuery slider; revs - revisions data from API
function setSliderTicks( element, revs ) {
    var $slider =  $( element );
    var max =  $slider.slider( "option", "max" );
    var min =  $slider.slider( "option", "min" );
    var spacing =  100 / ( max - min );
    for ( var i = 0; i <= max-min ; i++ ) {
    	var html = '<b>' + formatDate( revs[i].timestamp ) + '</b><br>';
    	html += mw.html.escape( revs[i].user ) + '<br>';
    	if( revs[i].comment !== '' ) {
    		html += '<br><i>' + mw.html.escape( revs[i].parsedcomment ) + '</i>';
    	}
        $('<div class="ui-slider-tick-mark" title="<center>' +  html +'</center>"></div>')
        .css( {
        	'left' : ( spacing * i - 0.75 ) + '%',
        	'width' : spacing + '%',
        	'background' : i % 2 === 0 ? 'white' : 'black'
        } )
        .tipsy( {
        	gravity: 's',
        	html: true,
        	fade: true,
        } )
        .appendTo( $slider );
     }
}

// Adding the initial barebones slider
// Params: revs - revisions data from API; vals - initial positions for the slider handles
function addSlider( revs, vals ) {
	numberRevs = revs.length;
	var $slider = $( '<div class="range-slider"></div>' )
					.slider({
						range: true,
						min: 1,
						max: numberRevs,
						step: 1,
						animate: "fast",
						values: vals,
						create: function( event, ui ) {
							setSliderTicks( event.target, revs );
						},
						stop: function( event, ui ) {
							var v1 = revs[ui.values[0]-1].revid;
							var v2 = revs[ui.values[1]-1].revid;
							refresh( v1, v2 );
						},
						slide: function( event, ui ) {

						}
					});
	$( '.ui-slider-handle:first' ).attr( 'title', 'Huh' ).tipsy({ gravity: 's', html: true, fade: true });
	$html = $( '<td colspan="4" style="text-align:center;" class="slider"></td>' ).append( $slider );
	$html2 = $( '<tr>' ).append( $html );
	$element = $( '.diff > tbody > tr' ).eq(0).after( $html2 );
}

// Function to find the initial positions of the slider handles
// Params: revs - revisions data from API
function findInitialValues( revs ) {
	var v1, v2, f1 = false, f2 = false, i;
	if ( gDiff == 'prev' ) {
		for ( i = 0; i < revs.length; i++) {
			if( revs[i].revid == gRightRevID ) {
				v2 = i+1;
				v1 = i;
				f1 = true;
				f2 = true;
				break;
			}
		}
	} else {
		for( i = 0; i < revs.length; i++ ) {
			if( revs[i].revid == gLeftRevID ) {
				v1 = i+1;
				f1 = true;
			}
		}
		for ( i = 0; i < revs.length; i++) {
			if( revs[i].revid == gRightRevID ) {
				v2 = i+1;
				f2 = true;
			}
		}
	}
	if( f1 === false || f2 === false ) {
		return false;
	}
	return [v1, v2];
}

// Driver function
mw.loader.using( ['jquery.ui.slider', 'jquery.ui.tooltip', 'jquery.tipsy'], function () {
	$( document ).ready( function() {
		$.ajax( {
			url: mw.util.wikiScript( 'api' ),
			data: {
				action: 'query',
				prop: 	'revisions',
				format: 'json',
				rvprop: 'ids|timestamp|user|parsedcomment',
				titles: gPageName,
				formatversion:	2,
				rvstartid: gCurRevisionId,
				"continue": "",
				rvlimit: "50"
			},
			success: function( data ) {
				revs = data.query.pages[0].revisions;
				if ( !revs ) {
					return;
				}
				revs.reverse();
				vals = findInitialValues( revs );
				if( vals !== false ) {
					addSlider( revs, vals );
				}
			}
		} );
	} );
} );