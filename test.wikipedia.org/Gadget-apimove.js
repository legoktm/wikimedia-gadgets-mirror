/* API-submitting redirect-free mover, version [0.0.1]
Originally from: http://test.wikipedia.org/wiki/MediaWiki:Gadget-apimove.js

Quick and dirty. Can be fixed many ways:
* proper dom
* better message
* more options
* free coupon with first purchase
*/

if(wgCanonicalSpecialPageName == 'Movepage') $(noRedirMoveGen)
function noRedirMoveGen() {
  var frm = document.getElementById('movepage');
  var sub = wgServer + wgScriptPath + '/api.php';
  //sub = 'http://www.cs.tut.fi/cgi-bin/run/~jkorpela/echo.cgi';
  var newfrm = '<form method="post" action="' + sub + '" id="movepage2"><input type="hidden" name="from" value="">'
  + '<input type="hidden" name="to" value=""><input type="hidden" name="token" value="">'
  + '<input type="hidden" name="reason" value=""><input type="hidden" name="movetalk" value="1">'
  + '<input type="hidden" name="noredirect" value="1"><input type="hidden" name="action" value="move">'
  + '<input type="button" name="subby" onclick="noRedirMove();" value="Move page without generating redirects"></form>';
  frm.parentNode.innerHTML += newfrm;
}

function noRedirMove() {
  var reallymove = confirm('This will attempt to move the page by submitting to the API.\nThis allows the redirect creation to be supressed. \nSeveral options are not available with this method, such as moving subpages, fixing redirects, or watching/unwatching. \nThe success or error message will also be returned from the API.');
  if(!reallymove) return
  var frm = document.getElementById('movepage');
  var frm2 = document.getElementById('movepage2');
  frm2.from.value = frm.wpOldTitle.value;
  frm2.to.value = frm.wpNewTitle.value;
  frm2.token.value = frm.wpEditToken.value;
  frm2.reason.value = frm.wpReason.value;
  if(!frm.wpMovetalk || frm.wpMovetalk.checked == false) frm2.movetalk.parentNode.removeChild(frm2.movetalk)
  frm2.subby.disabled = 'disabled';
  frm2.submit();
}