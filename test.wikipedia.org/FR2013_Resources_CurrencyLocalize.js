<script>
function currencyLocalize(currency, amount, language){
    // Do some basic number formatting - digit separators etc
    if (window.Intl && typeof window.Intl === "object") {
        if (Geo.country) {
            var locale = language + '-' + Geo.country;
        } else {
            var locale = language;
        }
        var formatter = new Intl.NumberFormat(locale);
    } else {
        // boo, bad browser! let's just have a thing that throws it back unformatted for now
        var formatter = {};
        formatter.format = function(number) {
            return number.toString();
        };
    }
    if (isNaN(amount) || amount == "") {
        // it's probably the "other" string or box
        var fmAmount = amount;
    } else {
        var fmAmount = formatter.format(amount);
    }
    // End number formatting

    var currencies = {
        "USD" : "$\t",
        "EUR" : {
            "en" : "€\t",
            "cy" : "€\t",
            "ga" : "€\t",
            "mt" : "€\t",
            "nl" : "€ \t",
            "lv" : "€ \t",
            "tr" : "€ \t",
            "default" : "\t €"
        },
        "AED" : " د.إ \t",
        "ANG" : "ƒ\t",
        "ARS" : "$\t",
        "AUD" : "$\t",
        "BAM" : "\t KM",
        "BBD" : "Bcs$\t",
        "BDT" : "৳\t",
        "BGN" : "лв\t",
        "BHD" : "د.ب\t",
        "BMD" : "BD$\t",
        "BOB" : "$b\t",
        "BRL" : "R$\t",
        "BZD" : "BZ$\t",
        "CAD" : {
            "fr" : "$\t",
            "default" : "$\t"
        },
        "CHF" : "Fr. \t",
        "CLP" : "$\t",
        "CNY" : "\t ¥",
        "COP" : "$\t",
        "CRC" : "\t ₡",
        "CZK" : "\t Kč",
        "DKK" : "\t kr.",
        "DOP" : "RD$\t",
        "DZD" : "د.ج\t",
        "EEK" : "\t kr",
        "EGP" : {
            "en" : "E£\t",
            "default" : "\t جنيه"
        },
        "FJD" : "FJ$\t",
        "GBP" : "£\t",
        "GTQ" : "Q\t",
        "HKD" : "HK$\t",
        "HNL" : "L\t",
        "HRK" : "\t kn",
        "HUF" : "\t Ft",
        "IDR" : "Rp \t",
        "ILS" : {
            "he" : "\t&thinsp;₪",
            "yi" : "\t&thinsp;₪",
            "ar" : "\t&thinsp;₪",
            "default" : "₪&thinsp;\t"
        },
        "INR" : "Rs. \t",
        "ISK" : "\t kr",
        "JMD" : "J$ \t",
        "JOD" : "دينار\t",
        "JPY" : "¥\t",
        "KES" : "\t KSh",
        "KRW" : "₩\t",
        "KWD" : "د.ك \t",
        "KZT" : "〒 \t", // TODO: don't use the JP postal code symbol once KZT works in unicode
        "LBP" : "LBP \t",
        "LKR" : "\t Rs.",
        "LTL" : "\t Lt",
        "LVL" : "\t Ls",
        "MAD" : "د.م.\t",
        "MKD" : "\t ден",
        "MOP" : "MOP$\t",
        "MUR" : "\t Rs",
        "MXN" : "$\t",
        "MVR" : "Rf. \t",
        "MYR" : "RM\t",
        "NIO" : "C$\t",
        "NOK" : "\t kr",
        "NZD" : "$\t",
        "OMR" : "ر.ع\t",
        "PAB" : "\t B/.",
        "PEN" : "S/. \t",
        "PHP" : "₱\t",
        "PKR" : "Rs \t",
        "PLN" : "\t zł",
        "PYG" : "\t ₲",
        "QAR" : "ر.ق\t",
        "RON" : "\t lei",
        "RUB" : "\t руб",
        "SAR" : "﷼\t",
        "SCR" : "SR \t",
        "SEK" : "\t kr",
        "SGD" : "S$ \t",
        "SVC" : "\t ₡",
        "THB" : {
            "th" : "\t บาท",
            "default" : "\t ฿"
        },
        "TND" : "\t د.ت",
        "TRY" : "₺\t",
        "TTD" : "TT$\t",
        "TWD" : "NT$\t",
        "TZS" : "\t/=",
        "UAH" : "₴\t",
        "UYU" : "$U \t",
        "UZS" : "\t сўм",
        "VEF" : "Bs.F. \t",
        "VND" : "\t₫",
        "VUV" : "VT\t",
        "XAF" : "FCFA\t",
        "XCD" : "EC$\t",
        "XOF" : "CFA \t",
        "XPF" : "\t F",
        "ZAR" : "R \t"
    };
    if(currencies[currency] == null){
        return currency + "&nbsp;" + fmAmount;
    }
    if( currencies[currency] instanceof Object ){ // not a string
        if(currencies[currency][language] != null){
            return currencies[currency][language].replace("\t", fmAmount);
        }
        return currencies[currency]["default"].replace("\t", fmAmount);
    }
    return currencies[currency].replace("\t", fmAmount);
}
</script>