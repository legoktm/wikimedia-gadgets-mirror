//from http://www.json.org/json.js and [[en:User:Zocky/AutoComplete.js]]
 
String.prototype.parseJSON = function () {
    try {
        return !(/[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(
                this.replace(/"(\\.|[^"\\])*"/g, ''))) &&
            eval('(' + this + ')');
    } catch (e) {
        return false;
    }
};
 
function ac$query(cb,what) 
{
  if (ac$query.arguments.length>1 && ac$x) 
  {
    var i=2;
    var url="/w/query.php?format=json&what="+what;
    while (i < ac$query.arguments.length)
    {
      url=url+"&"+ac$query.arguments[i] + "=" + ac$query.arguments[i+1];
      i+=2;
    }
 
    ac$x.onreadystatechange=function() {
      if (ac$x.readyState==4)
        return ac$x.status==200 
        ? cb(ac$x.responseText.parseJSON()) 
        : cb(false);
    };
    ac$x.open("GET",url,true);
    ac$x.setRequestHeader('Accept','text/*');
    ac$x.send(null);
  }
  else return false;
}
 
// helper functions
function ac$$e(id)   { return document.getElementById(id); }
function ac$$c(name,id) { var el=document.createElement(name); el.id=id; return el };
function ac$$ia(node,newnode) { return node.parentNode.insertBefore(newnode,node.nextSibling); }
function ac$$uc(s) { return s.substring(0,1).toUpperCase()+s.substring(1) };
 
function ac$inputKeyDown(e)
{
  if (e.which==40 && ac$b.style.visibility=="visible")
  {
    ac$b.focus();
//    ac$b.selectedIndex=0;
//    ac$s.value=ac$b.options[0].value;
  }
  else
  {
    try {clearTimeout(ac$t)} catch (e) {};
    ac$t=setTimeout("ac$timeOut()",500)
  }
}
 
function ac$boxKeyPress(e)
{
  if (e.which==13)
  {
    ac$s.value=ac$b.options[ac$b.selectedIndex].value;
    ac$b.style.visibility="hidden";
    ac$b.selectedIndex=-1;
    ac$s.focus();
  }
  else if (e.which==27)
  {
    ac$b.style.visibility="hidden";
    ac$b.selectedIndex=-1;
    ac$s.focus();
  }
  else if (e.which)
  { 
    ac$s.value+=String.fromCharCode(e.which);
    ac$s.focus();
    ac$b.selectedIndex=-1;
    ac$s.selectionStart=ac$s.selectionEnd=ac$s.value.length;
    try {clearTimeout(ac$t)} catch (e) {};
    ac$t=setTimeout("ac$timeOut()",500)
  }
  e.preventDefault();
}
 
function ac$boxClick(e)
{
    ac$s.value=ac$b.options[ac$b.selectedIndex].value;
    ac$b.style.visibility="hidden";
    ac$b.selectedIndex=-1;
    ac$s.focus();
}
 
function ac$blur(e)
{
  if (e.target==ac$b)
  {
    if (ac$b.style.visibility=="hidden") ac$b.blur()  
  }
  else if (e.target!=ac$s && e.target!=document && e.target!=window)
  {
    ac$b.style.visibility="hidden";
    ac$b.selectedIndex=-1;
  }
}
 
 
 
function ac$timeOut()
{
  if (ac$s.value.length>0)
  {
    try {ac$x.abort()} catch(e){};
    cb=function(obj) 
    {
       ac$b.innerHTML='';
       for (var i in obj.meta.namespaces)
       {
         try
         {
           var ns=obj.meta.namespaces[i]['*'];
           ac$n[ns]=i;
           if (
             ac$s.value.toUpperCase() == ns.substring 
                                       (0,ac$s.value.length).toUpperCase()
           )
           {
             ac$b.innerHTML+='<option style="font-weight:bold">'
             + ns
             + ':</option>';
           }
         } catch(e) {}
       }
 
       for (var i in obj.pages)
       {
         if (
           ac$s.value.toUpperCase() == obj.pages[i].title.substring 
                                       (0,ac$s.value.length).toUpperCase()
         )
           ac$b.innerHTML+= ( 
             obj.pages[i].redirect!="" 
             ? '<option>'
             : '<option style="color:silver">'
           )
           + obj.pages[i].title
           + '</option>';
       }
//       ac$b.size= ac$b.options.length<8 ? ac$b.options.length : 8;
         ac$b.options.length || (ac$b.innerHTML='<option value="'+ac$s.value+'" style="color:silver">(nie znaleziono)</option>');
       ac$b.style.visibility="visible";
    };
 
    var ns=ac$s.value.match(/^.*?(?=:)/);
    if (ns in ac$n)
    {
       var apns = ac$n[ns];
       var apfrom = ac$s.value.replace(/^.*?:/,'');
    }
    else
    {
       var apns=0;
       var apfrom=ac$s.value;
    }
 
    ac$query(cb,"allpages|namespaces","apfrom",ac$$uc(apfrom.replace(/ /g,'_')),"aplimit","50","apnamespace",apns);
  }
  else
  {
    ac$b.style.visibility="hidden";
  }
}
 
// initialization
 
var ac$s;
var ac$b;
var ac$t;
var ac$n=[];
var ac$x = window.XMLHttpRequest ? new XMLHttpRequest()
         : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP")
         : false;
 
function ac$init()
{
  ac$s=ac$$e("searchInput");
  if(ac$s)
  {
    ac$b=ac$$c('select','autocompletebox');
    ac$b.size=8;
    ac$b.style.visibility="hidden";
    ac$$ia(ac$s,ac$b);
 
    ac$s.setAttribute('autocomplete','off');
    ac$s.addEventListener('keydown',ac$inputKeyDown,false);
    ac$b.addEventListener('keypress',ac$boxKeyPress,false);
    ac$b.addEventListener('click',ac$boxClick,false);
    document.addEventListener('focus',ac$blur,false);
  }
}
 
$(ac$init);