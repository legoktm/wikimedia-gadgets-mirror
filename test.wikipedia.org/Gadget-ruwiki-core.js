// Загрузка скриптов через систему подгаджетов
mw.loader.using( 'mediawiki.util', function () {
	var namespaceNumber = mw.config.get( 'wgNamespaceNumber' );

	// Скрипты для служебных страниц
	if ( namespaceNumber === -1 ) {
		var specialGadgets = [
			'Abusefilter',
			'Block',
			'Log',
			'Movepage',
			'Newpages',
			'Search',
			'Upload'
		];
		var canonicalSpecialPageName = mw.config.get( 'wgCanonicalSpecialPageName' );
		if ( specialGadgets.indexOf( canonicalSpecialPageName ) > -1 ) {
			mw.loader.load( 'ext.gadget.ruwiki-common-special-' + canonicalSpecialPageName.toLowerCase() );
		}
		return;
	}

	// Скрипты для действий
	var action = mw.config.get( 'wgAction' );
	var actionGadgets = {
		'edit': [ 'ext.gadget.common-action-edit', 'ext.gadget.wikificator', 'ext.gadget.summaryButtons' ]
	};
	actionGadgets[ 'submit' ] = actionGadgets[ 'edit' ];
	actionGadgets[ 'parsermigration-edit' ] = actionGadgets[ 'edit' ]; // Удалить после исчезновения ссылки «Редактировать с инструментом переезда на новый парсер» в сайдбаре

	if ( actionGadgets[ action ] ) {
		mw.loader.load( actionGadgets[ action ] );
	}

	// Скрипты для пространств
	var namespaceGadgets = {
		6: [ 'ext.gadget.common-namespace-file' ]
	};

	if ( namespaceGadgets[ namespaceNumber ] ) {
		mw.loader.load( namespaceGadgets[ namespaceNumber ] );
	}
	
} );