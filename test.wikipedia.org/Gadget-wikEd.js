/**
 * Editor com funções avançadas (Firefox 1.5 ou superior, Mozilla 1.3 ou superior,
 * SeaMonkey, WebKit, Google Chrome, ou Safari)
 * @source: [[w:en:User:Cacycle/wikEd.js]]
 * @see: [[Wikipédia:Software/Scripts/wikEd]], [[w:en:User:Cacycle/wikEd]]
 * @author: [[w:en:User:Cacycle]]
 */
( function( mw, $ ) {
'use strict';

window.wikEd = {};
window.wikEd.config = {};
window.wikEd.config.regExTypoFix = true;
window.wikEd.config.regExTypoFixURL = '//pt.wikipedia.org/w/index.php?title=Project:AutoWikiBrowser/Typos&action=raw';

if ( $.client.profile().name !== 'msie'
	&& mw.config.get( 'wgCanonicalSpecialPageName' ) !== 'Upload'
) {
	mw.loader.load( '//en.wikipedia.org/w/index.php?title=User:Cacycle/wikEd.js&action=raw&ctype=text/javascript' );
}

}( mediaWiki, $ ) );