$( function () {
	if ( $( '#wikilabels-home' ).length ) {
		// TODO: Migrate from this Temporary hack
		// Wiki-Labels [[File:User:EpochFail/WikiLabels.js]]
		mw.loader.load( '//labels.wmflabs.org/gadget/loader.js' );

		// TODO: Remove the code above, move the full code to [[MediaWiki:Gadget-wikilabels.js]],
		// and load it from there with the following line
		// mw.loader.load( 'ext.gadget.wikilabels' );
	}
} );