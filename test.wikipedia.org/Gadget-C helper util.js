C.util = {
	config: {
		'time-before-refresh': 1000,
		'notify-network-issue': 'Problème de réseau, rechargez la page et réessayez.',
	  	help_icon_src: '//upload.wikimedia.org/wikipedia/commons/a/ab/PL_Wiki_CWNJ_ikona.svg',
	  	help_icon_width: 12,
	  	help_icon_class: 'C-help-icon',
	},
	reload: function() {
		setTimeout(function() {
			location.reload();
		}, this.config['time-before-refresh']);
	},
	'new_section': function(page, section_title, section_content, summary, callback) {
		if(page === null) {
			page = mw.config.get('wgPageName');
		}
		$.ajax({
			url: mw.util.wikiScript('api'),
			data: {
				action: 'query',
				meta: 'tokens',
				format: 'json',
				type: 'csrf',
			},
			dataType: 'json'
		}).then(function(data) {
			$.ajax({
				url: mw.util.wikiScript('api'),
				method: 'POST',
				data: {
					action: 'edit',
					title: page,
					summary: C.param.summary+summary,
					text: section_content,
					section: 'new',
					sectiontitle: section_title,
					tags:'C-helper',
					token: data.query.tokens.csrftoken,
					format: 'json',
				},
				dataType: 'json',
				success: function(data) {
					callback();
				},
				error: function(data) {
    				mw.notify(C.util.config['notify-network-issue'], {title:'C-helper', type:'error'});
				},
			});
		});
	},
	prepend: function(page, content, summary, callback) {
		this.edit(page, content, "prepend", summary, callback);
	},
	append: function(page, content, summary, callback) {
		this.edit(page, content, "append", summary, callback);
	},
	replace: function(page, content, summary, callback) {
		this.edit(page, content, "replace", summary, callback);
	},
	edit: function(page, content, method, summary, callback) {
		if(page === null) {
			page = mw.config.get('wgPageName');
		}
		$.ajax({
			url: mw.util.wikiScript('api'),
			data: {
				action: 'query',
				meta: 'tokens',
				format: 'json',
				type: 'csrf',
			},
			dataType: 'json'
		}).then(function(data) {
			data = {
				action: 'edit',
				title: page,
				summary: C.param.summary+summary,
				tags:'C-helper',
				token: data.query.tokens.csrftoken,
				format: 'json',
			};
			if(method == "append")
				data.appendtext = content;
			else if(method == "prepend")
				data.prependtext = content;
			else
				data.text = content;
			$.ajax({
				url: mw.util.wikiScript('api'),
				method: 'POST',
				data: data,
				dataType: 'json',
				success: function(data) {
					callback();
				},
				error: function(data) {
    				mw.notify(C.util.config['notify-network-issue'], {title:'C-helper', type:'error'});
				},
			});
		});
	},
	'construct_help_icon': function(title) {
		return $('<span/>').append("&nbsp;").append($('<img/>', {src: this.config.help_icon_src, width: this.config.help_icon_width, title: title}).tooltip({position: {my: "left top", at: "right+8 top-12"}}));
	},
	'parse_wikicode': function(string) {
		var reg = new RegExp("\\[\\[([^\\[\\]]+)\\|([^\\[\\]]+)\\]\\]");
		do {
			prev = string;
			string = string.replace(reg, '<a href="'+mw.config.get('wgServer')+mw.config.get('wgArticlePath')+'">$2</a>');
		} while(string != prev);
		reg = new RegExp("\\[\\[([^\\[\\]]+)\\]\\]");
		do {
			prev = string;
			string = string.replace(reg, '<a href="'+mw.config.get('wgServer')+mw.config.get('wgArticlePath')+'">$1</a>');
		} while(string != prev);
		return string;
	}
};