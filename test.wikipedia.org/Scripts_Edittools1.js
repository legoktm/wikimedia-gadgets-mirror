//Commons-style Edittools, see http://www.mediawiki.org/wiki/User_talk:Alex_Smotrov/edittools.js <nowiki>

var charinsert = {
'Standard': '[+]  [[+]]  [[+|+]]  {{+}}  – —  “+” ‘+’ «+» ‹+› „+“ ‚+‘  … ~ | ° &nbsp;  ≈ ≠ ≤ ≥ ± − × ÷ ← → ² ³ ½ · § \n [[Category:+]]  [[:Image:+]]  +<includeonly>  +<noinclude>  #REDIRECT[[+]]  {{DEFAULTSORT:+}}',
'Latin': 'ÁáĆćÉéÍíÓóŚśÚúÝýǾǿ  ÀàÈèÌìÒòÙù  ÂâĈĉÊêĜĝĤĥÎîĴĵÔôŝŜÛû  ÄäËëÏïÖöÜüÿ  ÃãÑñÕõ  Å å  Ç ç  ČčŠšŭ  Ł ł  ŐőŰű  Ø ø  ĀāĒēĪīŌōŪū  ß  ÆæŒœ  ÐðÞþ|',
'Greek': 'ΑΆΒΓΔΕΈΖΗΉΘΙΊΚΛΜΝΞΟΌΠΡΣΤΥΎΦΧΨΩΏ  αάβγδεέζηήθιίκλμνξοόπρσςτυύφχψωώ', 
'IPA': 'ʈɖɟɡɢʡʔ  ɸʃʒɕʑʂʐʝɣʁʕʜʢɦ  ɱɳɲŋɴ  ʋɹɻɰ  ʙʀɾɽ  ɫɬɮɺɭʎʟ  ɥʍɧ  ɓɗʄɠʛ  ʘǀǃǂǁ  ɨʉɯ  ɪʏʊ  ɘɵɤ  ɚ  ɛɜɝɞʌɔ  ɐɶɑɒ  ʰʷʲˠˤⁿˡ  ˈˌːˑ',
'Arabic': 'ابتثجحخدذرزسشصضطظعغفقكلمنهوي  ﺍﺑﺗﺛﺟﺣﺧﺩﺫﺭﺯﺳﺷﺻﺿﻃﻇﻋﻏﻓﻗﻛﻟﻣﻧﻫﻭﻳ\
  ﺍﺒﺘﺜﺠﺤﺨﺪﺬﺮﺰﺴﺸﺼﻀﻄﻈﻌﻐﻔﻘﻜﻠﻤﻨﻬﻮﻴ  ﺎﺐﺖﺚﺞﺢﺦﺪﺬﺮﺰﺲﺶﺺﺾﻂﻆﻊﻎﻒﻖﻚﻞﻢﻦﻪﻮﻲ\
  ء- ّ- ْ- ً- ِ- آأإةؤئى  پچژگﭪڠ۰۱۲۳٤٥٦٧۸۹',
'Cyrillic': 'АБВГДЂЕЁЖЗЅИЙЈКЛЉМНЊОПРСТЋУФХЦЧЏШЩЪЫЬЭЮЯ  абвгдђеёжзѕийјклљмнњопрстћуфхцчџшщъыьэюя',
'Catalan': 'ÁáÀàÇçÉéÈèËëÍíÏïÓóÒòÖöÚúÙù',
'Czech': 'ÁáČčĎďÉéĚěÍíŇňÓóŘřŠšŤťÚúŮůÝýŽž',
'Devanagari': 'ँ ं ः अ आ इ ई उ ऊ ऋ ऌ ऍ ऎ ए ऐ ऑ ऒ ओ औ क क़ ख ख़ ग ग़ घ ङ च छ ज ज़ झ ञ ट ठ ड ड़ द ढ ढ़ ण त थ ध न ऩ प फ फ़ ब भ म य य़ र ऱ ल ळ ऴ व श ष स ह ़ ऽ ा ि ॊ ो ौ ् ी ु ू ृ ॄ ॅ ॆ े ै ॉ ॐ ॑ ॒ ॓ ॔ ॠ ॡ ॢ ॣ । ॥ ॰',
'Esperanto': 'ĈĉĜĝĤĥĴĵŜŝŬŭ',
'Estonian': 'ČčŠšŽžÕõÄäÖöÜü',
'French': 'ÀàÂâÇçÉéÈèÊêËëÎîÏïÔôŒœÙùÛûÜüŸÿ',
'German': 'ÄäÖöÜüß',
'Hawaiian': 'ĀāĒēĪīŌōŪūʻ',
'Hebrew': 'אבגדהוזחטיכךלמםנןסעפףצץקרשת־״׳',
'Hungarian': 'ŐőŰű',
'Icelandic': 'ÁáÐðÉéÍíÓóÚúÝýÞþÆæÖö',
'Italian': 'ÁáÀàÉéÈèÍíÌìÓóÒòÚúÙù',
'Latvian': 'ĀāČčĒēĢģĪīĶķĻļŅņŠšŪūŽž',
'Lithuanian': 'ĄąČčĘęĖėĮįŠšŲųŪūŽž',
'Maltese': 'ĊċĠġĦħŻż',
'Old English': 'ĀāÆæǢǣǼǽĊċÐðĒēĠġĪīŌōŪūǷƿȲȳÞþȜȝ',
'Pinyin': 'ÁáÀàǍǎĀāÉéÈèĚěĒēÍíÌìǏǐĪīÓóÒòǑǒŌōÚúÙùÜüǓǔŪūǗǘǛǜǙǚǕǖ',
'Polish': 'ąĄćĆęĘłŁńŃóÓśŚźŹżŻ',
'Portuguese': 'ÁáÀàÂâÃãÇçÉéÊêÍíÓóÔôÕõÚúÜü',
'Romaji': 'ĀāĒēĪīŌōŪū',
'Romanian': 'ĂăÂâÎîŞşŢţ',
'Scandinavian': 'ÀàÉéÅåÆæÄäØøÖö',
'Serbian': 'АаБбВвГгДдЂђЕеЖжЗзИиЈјКкЛлЉљМмНнЊњОоПпРрСсТтЋћУуФфХхЦцЧчЏџШш',
'Spanish': 'ÁáÉéÍíÑñÓóÚúÜü¡¿',
'Turkish': 'ÇçĞğİıÖöŞşÜüÂâÎîÛû',
'Vietnamese': 'ÀàẢảÁáẠạÃãĂăẰằẲẳẴẵẮắẶặÂâẦầẨẩẪẫẤấẬậĐđÈèẺẻẼẽÉéẸẹÊêỀềỂểỄễẾếỆệỈỉĨĩÍíỊịÌìỎỏÓóỌọÒòÕõÔôỒồỔổỖỗỐốỘộƠơỜờỞởỠỡỚớỢợÙùỦủŨũÚúỤụƯưỪừỬửỮữỨứỰựỲỳỶỷỸỹỴỵÝý',
'Welsh': 'ÁáÀàÂâÄäÉéÈèÊêËëÌìÎîÏïÓóÒòÔôÖöÙùÛûẀẁŴŵẄẅÝýỲỳŶŷŸÿ',
'Yiddish': 'א אַ אָ ב בֿ ג ד ה ו וּ װ ױ ז זש ח ט י יִ ײ ײַ כ ך כּ ל ל מ ם נ ן ס ע ע פ פּ פֿ ף צ ץ ק ר ש שׂ תּ ת ׳ ״ ־'
}
var charinsertDivider = '·'


appendCSS('\
.mw-editTools {text-align:center;\
 border:1px solid #aaa;\
 font-size:120%;\
 padding:1px;\
 margin-top:10px;\
}\
.mw-editTools a {\
 color: black !important;\
 background-color: #ccddee;\
 font-weight: bold;\
 font-size: 0.9em;\
 text-decoration: none !important;\
 border: 1px #006699 outset;\
 padding: 0 0.1em 0.1em 0.1em;\
}\
.mw-editTools a:hover {\
 background-color: #bbccdd;\
 border: 1px #F0F0F0 inset;\
}')


createEditTools()


function createEditTools(){

 //find div#mw-editTools
 var box = document.getElementById('wpTextbox1')
 while ((box=box.nextSibling) && box.className!='mw-editTools');
 if (!box) return
 box.title = 'Click on the character or tag to insert it into edit window'

 //append user-defined  sets
 if (window.charinsertCustom)
   for (id in charinsertCustom)
	   if (charinsert[id]) charinsert[id] += ' ' + charinsertCustom[id]
		 else charinsert[id] = charinsertCustom[id]

 //create drop-down select
 var prevSubset = 0, curSubset = 0
 var sel = document.createElement('select'), id
 for (id in charinsert)
   sel.options[sel.options.length] = new Option(id, id)
 sel.style.cssFloat = sel.style.styleFloat = 'left'
 sel.style.marginRight = '5px'
 sel.title = 'Choose character subset'
 sel.onchange = sel.onkeyup = selectSubset
 box.appendChild(sel)
 //create "recall" switch
 if (window.editToolsRecall){
   var recall = document.createElement('span')
   recall.appendChild(document.createTextNode('↕')) //↔
   recall.onclick = function(){ sel.selectedIndex = prevSubset; selectSubset() }
   with (recall.style) { cssFloat = styleFloat = 'left'; marginRight = '5px'; cursor = 'pointer' }
   box.appendChild(recall)
 }
 //show standard set
 selectSubset()
 return

 function selectSubset(){
  //remember previous (for "recall" button)
  prevSubset = curSubset
  curSubset = sel.selectedIndex
  //hide other subsets
	var pp = box.getElementsByTagName('p') 
  for (var i=0; i<pp.length; i++)
    pp[i].style.display = 'none'
  //show/create current subset
  var id = sel.options[curSubset].value
  var p = document.getElementById(id)
  if (!p){
    p = document.createElement('p')
    p.id = id
    if (id == 'Arabic'){ p.style.fontSize = '120%'; p.dir = 'rtl'}
    createTokens(p, charinsert[id])
    box.appendChild(p)
	}
  p.style.display = 'inline'
 } 

} //createEditTools


 
function createTokens(paragraph, str){
 var tokens = str.split(' '), token, i, n
 for (i in tokens) {
   token = tokens[i]
   n = token.indexOf('+')
   if (token == '' || token == '_')
     addText(charinsertDivider + ' ')
   else if (token == '\n')
     paragraph.appendChild(document.createElement('br'))
   else if (token == '___')
     paragraph.appendChild(document.createElement('hr'))
   else if (token.charAt(token.length-1) == ':')  // : at the end means just text
     addBold(token)
   else if (n == 0) // +<tag>  ->   <tag>+</tag>
     addLink(token.substring(1), '</' + token.substring(2), token.substring(1))
   else if (n > 0) // <tag>+</tag>
     addLink(token.substring(0,n), token.substring(n+1))
   else if (token.length > 2 && token.charCodeAt(0) > 127) //a string of insertable characters
     for (var i=0; i < token.length; i++) addLink(token.charAt(i), '')
   else
     addLink(token, '')
  }

 function addLink(tagOpen, tagClose, name){
  var a = document.createElement('a')
  tagOpen = tagOpen.replace(/\./g,' ')
  tagClose = tagClose ? tagClose.replace(/_/g,' ') : ''
  name = name || tagOpen + tagClose
  name = name.replace(/\\n/g,'')
  a.appendChild(document.createTextNode(name))
  a.href = "javascript:insertTags('" + tagOpen + "','" + tagClose + "','')"
  paragraph.appendChild(a)
  addText(' ')
 }       

 function addBold(text){
  var b = document.createElement('b')
  b.appendChild(document.createTextNode(text.replace(/_/g,' ')))
  paragraph.appendChild(b)
  addText(' ')
 }       

 function addText(txt){
  paragraph.appendChild(document.createTextNode(txt))
 }

} //createTokens </nowiki>