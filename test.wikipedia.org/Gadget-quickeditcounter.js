// Original version:
// - QuickEditCounter script by [[:pl:User:ChP94]]
// - Released under the [http://www.gnu.org/licenses/gpl.txt GNU Public License (GPL)]
// Modified by [[:pl:User:Beau]]

var qecGadget = {
	data: null,
	loaded: false,
	version: 4,

	init: function() {
		if (mw.config.get( 'wgNamespaceNumber' ) != 2 && mw.config.get( 'wgNamespaceNumber' ) != 3)
			return;

		if (mw.util.getParamValue('printable') == 'yes')
			return;

		this.username = mw.config.get( 'wgTitle' ).replace(/\/.*$/, '');

		var that = this;

		var request = {
			action:	'query',
			list:	'users',
			usprop:	'editcount|gender',
			format:	'json',
			ususers:	this.username,
			requestid:	new Date().getTime()
		};

		jQuery.getJSON(mw.config.get( 'wgServer' ) + mw.config.get( 'wgScriptPath' ) + '/api.php', request, function(result) {
			if (that.loaded) {
				that.showResults(result);
			}
			else {
				that.data = result;
			}
		});
		jQuery(document).ready(function() {
			that.loaded = true;
			if (that.data) {
				that.showResults(that.data);
				that.data = null;
			}
		});
	},
	showResults: function(data) {
		data = data.query.users[0];
		if (!data || data.name != this.username || data.invalid != null)
			return;

		var firstHeading;
		var headers = document.getElementsByTagName("h1");

		for(i=0; i<headers.length; i++) {
			var header = headers[i];
			if(header.className == "firstHeading" || header.id == "firstHeading" || header.className == "pagetitle") {
				firstHeading = header; break;
			}
		}

		if(!firstHeading)
			firstHeading = document.getElementById("section-0");

		if(!firstHeading)
			return;

		var html = data.gender == "female" ? 'Ta użytkowniczka' : 'Ten użytkownik';
		var lang = 'pl';
		var wiki = 'wikipedia';

		var m;
		if (m = mw.config.get( 'wgServer' ).match(/^http:\/\/(.+?).([^.]+).org$/)) {
			lang = m[1];
			wiki = m[2];
		}
		else if (m = mw.config.get( 'wgScriptPath' ).match(/\/(.+?)\/(.+?)\//)) {
			lang = m[2];
			wiki = m[1];
		}

		html += ' ma łącznie <a href="http://toolserver.org/~soxred93/count/index.php?name=' + encodeURIComponent(this.username) + '&wiki=' + encodeURIComponent(wiki) + '&lang=' + encodeURIComponent(lang) + '">' + data.editcount + '</a> edycji.';

		var div = document.createElement("div");
		div.style.cssText = "font-size:8pt;line-height:1em";
		div.className = 'plainlinks';
		div.innerHTML = html;

		if (skin == 'modern') {
			div.style.marginLeft = "10px";
			div.style.display = "inline-block";
		}

		firstHeading.appendChild(div);
	}
};

qecGadget.init();
window.qecGadget = qecGadget;