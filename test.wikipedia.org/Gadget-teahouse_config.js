
//For deployment on test.wikipedia.org

mw.util.teahouse.init({
	basePage: 'Wikipedia:Teestube/Fragen',
	i18n: {
		"teahouse-button-text": "Stelle deine Frage",
		"teahouse-button-title": "Lerne die Community kennen",
		"teahouse-dialog-title": "Stelle deine Frage",
		"teahouse-dialog-description-top": "Stelle deine Frage an die Wikipedia Community! Auf $1 kannst du dir die Fragen anderer Benutzer ansehen.",
		"teahouse-dialog-label-summary": "Deine Frage",
		"teahouse-dialog-label-text": "Genauere Beschreibung (Kann später bearbeitet werden)",
		"teahouse-dialog-label-similar": "Ähnliche, bereits gestellte Fragen",
		"teahouse-dialog-btn-ok": "Veröffentlichen",
		"teahouse-dialog-btn-cancel": "Abbrechen",
		"teahouse-dialog-licence-html": "Veröffentlicht unter der <a target=\"_blank\" href=\"//de.wikipedia.org/wiki/Wikipedia:Lizenzbestimmungen_Commons_Attribution-ShareAlike_3.0_Unported\">„Creative Commons Attribution/Share Alike“</a> Lizenz",
		"teahouse-dialog-anon-ip-hint-html": "Deine IP Adresse wird zusammen mit der Frage veröffentlicht",
		"teahouse-dialog-url-hint-html": "Du verwendest eine URL in deiner Frage. Unter Umständen kann sie daher nicht gespeichert werden",
		"teahouse-dialog-max-length-hint-html": "Es sind maximal $1 Zeichen erlaubt",
		"teahouse-dialog-error-process": "Entschuldige, es ist ein Fehler bei der Verarbeitung deiner Anfrage aufgetreten",
		"teahouse-dialog-error-details": "Fehlerdetails",
		"teahouse-dialog-msg-title-save": "Frage veröffentlicht",
		"teahouse-dialog-msg-text-save": "Deine Frage wurde unter $1 veröffentlicht. Möchtest du die komplette Liste der Fragen sehen?",
		"teahouse-dialog-msg-btn-yes": "Ja",
		"teahouse-dialog-msg-btn-no": "Nein",
		"teahouse-notifications-badge-title": "Es gibt Reaktionen auf deine Fragen",
		"teahouse-notifications-popup-title": "Reaktionen auf deine Fragen",
		"teahouse-board-added-by-teahouse": "Hinzugefügt durch Teahouse-Gadget"
	}
});