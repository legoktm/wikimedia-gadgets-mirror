/**
 * Add a "Bot" link to the personal portlet menu.
 * Dependencies: mediawiki.util, mediawiki.Title, mediawiki.Uri
 *
 * @source mediawiki.org/wiki/Snippets/MySandbox
 * @version 3
 */
( function ( mw, $ ) {
 
        $( document ).ready( function () {
                var conf, title, url;
 
                // Customize/Translate this to your needs
                conf = {
                        botName: 'RileyBot',
                        portletLabel: 'Bot',
                        portletTooltip: 'See your bot\'s contribs',
                        editintroPagename: 'Template:User_sandbox',
                        preloadPagename: 'Template:User_sandbox/preload'
                };
                // Don't alter the code below
 
                // Use Special:MyPage (as opposed to mw.user.getName()) so that it
                // works for logged-out users as well.
                title = new mw.Title( 'Special:Contributions/' + conf.botName );
 
                url = new mw.Uri( title.getUrl() );
                url.extend({
                        action: 'edit',
                        redlink: 1,
                        editintro: new mw.Title( conf.editintroPagename ),
                        preload: new mw.Title( conf.preloadPagename )
                });
 
                mw.util.addPortletLink(
                        'p-personal',
                        url,
                        conf.portletLabel,
                        'pt-Bot',
                        conf.portletTooltip,
                        null,
                        '#pt-preferences'
                );
        });
}( mediaWiki, jQuery ) );