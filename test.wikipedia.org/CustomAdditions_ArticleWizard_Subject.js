mw.loader.using(['mediawiki.api', 'oojs-ui'], function () {
	var api=new mw.Api();
	// Create an input 
	var textInput = new OO.ui.TextInputWidget( { 
	  	placeholder: 'Type the new article name and press Enter to search',
	  	maxLength: 50,
	  	autofocus: true,
	  	icon:'search',
	  	align: 'center'
	} );
	
	// Replace the markup-provided search box
	$('#CustomAdditions-ArticleWizard-Subject-SearchDiv').empty();
	$('#CustomAdditions-ArticleWizard-Subject-SearchDiv').append(textInput.$element);
	
	var label1 = new OO.ui.LabelWidget( {
  		label: ''
	} );
	$('#CustomAdditions-ArticleWizard-Subject-SearchDiv').append(label1.$element);
	
	// Create the results list element
	var resultsList = $('<ol></ol>');
	
	// Put the element into DOM
	$('#CustomAdditions-ArticleWizard-Subject-SearchDiv').append(resultsList);
	
	textInput.on('enter', function(){
		// Empty the results list
		resultsList.filter('ol').empty();
		// Put note 'searching'
		label1.setLabel('Searching...');
		// Get the results
		api.get({
			action: 'query',
			list: 'search',
			srsearch: textInput.value,
			srlimit: 10
		}).then(function(data){
			// Show the results
			label1.setLabel(data.query.search.length + ' result(s) found.');
			$.each(data.query.search, function(index,value){
				var text = '<li><a href="'+wgServer+'/wiki/'+value.title+'">'+value.title+'</a> - '+value.snippet+'</li>';
				resultsList.filter('ol').append($(text));
			});
		});
	});
});