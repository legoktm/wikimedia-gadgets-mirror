// The Wikipedia Adventure Mission 1
 
( function ( window, document, $, mw, gt ) {
 
//automatic api:edit function to send yourself messages
function sendMessage( targetPage, msgPage, linkTo ) {
	var api = new mw.Api();
	api.get( {
		'action' : 'query',
		'titles' : msgPage,
		'prop'   : 'revisions|info',
		'intoken' : 'edit',
		'rvprop' : 'content',
		'indexpageids' : 1
	} ).done( function (result) {
		result = result.query;
		var page = result.pages[result.pageids[0]];
		var text = page.revisions[0]['*'];
		api.post( {
			'action' : 'edit',
			'title' : targetPage,
			'appendtext' : "\n" + text,
			'summary' : 'Ný skilaboð (líkt eftir sjálfvirkt af [[Wikipedia:Wikipedia ævintýrið|Wikipedia ævintýrinu]])',
			'token' : page.edittoken
		} ).done( function () {
			window.location.href = linkTo;
		} );
	} );
}
 
// Fail gracefully post-save but not postedit
var postEditButtons = [];
if ( mw.config.get( 'wgAction' ) === 'view' && !gt.isPostEdit() ) {
        postEditButtons.push( {
                name: 'Smelltu hér til að fara til baka og gera breytingu',
                onclick: function() {
                        window.location.href = new mw.Uri().extend( { action: 'edit' } ).toString();
                }
        } );
}
 
// Fail gracefully post-save but not postedit for visual editor
var postEditButtonsVisual = [];
if ( mw.config.get( 'wgAction' ) === 'view' && !gt.isPostEdit() ) {
        postEditButtonsVisual.push( {
                name: 'Til baka',
                onclick: function() {
                        window.location.href = window.location.href +
"&veaction=edit";
                }
        } );
}
 
gt.defineTour( {
        name: 'twa1is',
		shouldLog: true,
        steps: [ {
                //1
                title: 'Velkomin/n á wikipedia!',
                description: '<br><div align="left">[[File:TWA_guide_left_top.png|link=]]</div>Wikipedia er frjálst alfræðirit sem  <b>allir geta breytt</b>.  Ég er hér til að fara með þig í gagnvirka ferð yfir heiminn okkar.<br><br>Ferðin hefur 7 sendiferðir, hver þeirra hefur sínar egin uppákomur og þarfast ákveðnrar kunnáttu. Allar sendiferðirnar eru hannaðar til að gera þig að góðum þáttakanda á Wikipedia.<br><br>',
                onShow: gt.parseDescription,
                overlay: true,
                closeOnClickOutside: false,
	            buttons: [ {
                        name: 'Undirbúningur fyrir ferðina',
                        action: 'next',
                } ],
                allowAutomaticOkay: false	
 
 
        },  {
                //2
                title: 'Það sem þú þarft að vita áður en þú ferð',
                description: '<br><b>Ekki smella á [x]</b><br> Þetta box er geimbúningurinn þinn: ef þú lokar honum án þess að klára sendiferðina þá ferðu frá ævintýrinu og þarft að gera sendiferðina frá upphafi.<br><br><b>Sjálfvirk skilaboð</b><br> Þegar þú leikur þennan leik, þá munt þú senda skilaboð á þína persónulegu wikipedia síðu, í hvert skipti sem þú sérð  <big><b>*</b></big> í bláa takkanum.<br><br>',
                onShow: gt.parseDescription,
                overlay: true,
                closeOnClickOutside: false,
		        buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=1'          
                } , {
                        name: 'Komdu með mér...',
                        action: 'next',
                         } ],
                allowAutomaticOkay: false		
 
        },  {
                //3
                title: 'Afhverju Wikipedia?',
                description: '<br><div align="right">[[File:TWA_guide_right_top.png|link=]]</div>Við höfum æðislegt markmið.  <br><br><b>Ímyndaðu þér veröld þar sem hver einasta manneskja á plánetunni er gefin summa mannlegar þekkingar.</b><br><br>Það ótrúlegasta er að...<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=2'          
                } , {
                         name: 'Það er að gerast',
                         action: 'next',
                         } ],
                allowAutomaticOkay: false				
 
        },  {
                //4
                title: 'Það er að gerast',
                description: '<br><div align="right">[[File:TWA_guide_right_top.png|link=]]</div>Wikipedia er skoðuð meira en 66 sinnum á mínótu. Við erum tíunda stærsta vefsíða Íslands. Við erum aðeins 11 ára gömul!<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=3'          
                } , {
                        name: 'Hver skrifar þessa wikipediu?',
                        action: 'next',
                         } ],
	            allowAutomaticOkay: false
        },  {
                //5
                title: 'Hver skrifar þessa wikipediu?',
                description: '<br><div align="right">[[File:TWA_guide_right_top.png|link=]]</div>Þú gerir það :) Það eru rétt yfir 40 þúsund notendur. Það sem er mikilvægast af öllu er að þú þarft ekki að vera sérfræðingur til að leggja þitt af mörkum. Næstum allir af þáttakendum okkar eru sjálfboðaliðar.<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=4'          
                } , {
                        name: 'Afhverju breytir fólk?',
                        action: 'next',
                         } ],
		        allowAutomaticOkay: false
        },  {
 
                //6
                title: 'Uppgvötvaðu þitt hlutverk',
                description: '<br><div align="left">[[File:TWA_guide_left_top.png|link=]]</div>Það ótrúlega við wikipediu er að þú færð að uppgvötva þína eigin leið og hlutverk. Áhrif hverrar manneskju vegur að mörkum. Þitt hlutverk getur breytt heiminum.<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=5'          
                } , {
                        name: 'Ertu tilbúin/n?',
                        action: 'next',
                         } ],
                allowAutomaticOkay: false,
},  {
                //7
                title: 'Skráðu þig inn eða búðu til aðgang',
                description: '<br><div align="left">[[File:TWA_guide_left_top.png|link=]]</div>Að búa til aðgang gefur þér marga kosti. Sláðu til.<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=6'  
                } , {
                	name: 'Ég er skráð/ur inn',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=8'
 
                } , {
                	name: 'Ég þarf að skrá mig inn',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:UserLogin' ) + '?tour=twa1&step=7'
 
                } , {
                	name: 'Skráðu þig!',
                    action: 'externalLink',
                    url: mw.config.get('wgServer') + mw.config.get('wgScriptPath') + '/index.php?title=Special:UserLogin&returnto=Wikipedia:TWA/1/Start&returntoquery=tour%3Dtwa1%26step%3D8%26showGettingStarted%3Dfalse&type=signup'
 
                } ],
                allowAutomaticOkay: false,
                shouldSkip: function () {
                return mw.config.get( 'wgUserId' )  !== null;
                }
 
} , {
                //8
                title: 'Segðu halló við Wikipedia',
                description: '<br>  Byrjum á því að kynna okkur fyrir samfélaginu.<br><br>Það mun aðeins taka nokkrar sekóndur að hlaða inn heiminum - ljósið ferðast svo hratt.<br><br>(Fyrir afganginn af ferðinni þarft þú að vera skráð/ur inn.)<br><br>',
                overlay: true,
                onShow: gt.parseDescription,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=6'          
                } , {
                         name: 'Halló heimur*',
                         onclick: function()  {  if(!mw.config.get('wgUserName')){  alert( "Vinsamlegast skráðu þig inn." );   return;   } sendMessage( 'User talk:' + mw.config.get( 'wgUserName' ), 'Wikipedia:TWA/Welcome' , mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=9'); } 
                } ],	
                allowAutomaticOkay: false
 
} , {
                //9
                title: 'Notendasíðan þín',
                description: '<br><div align="right">[[File:TWA_guide_right_top.png|link=]]</div>Notendasíðan þín er staður til að segja öðrum þáttakendum um sjálfan þig. Þú getur deilt upplýsingum um bakgrunn þinn, áhugamál og því sem þú vilt leggja af mörkum til verkefnisins. Deildu eins miklu eða litlu eins og þú vilt...<br><br><i>Mundu að þetta er mjög opinber notendasíða, svo haltu persónulegum upplýsingum leyndum.</i><br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Start' ) + '?tour=twa1&step=8'          
                } , {
                        name: 'Hvernig lítur góð notendasíða út?',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Wikipedia:TWA/1/Bio' ) + '?tour=twa1&step=10'
                } ],
		allowAutomaticOkay: false
 
} , {
                //10
                title: 'Ögraðu sjálfum þér...Hér fyrir neðan',
                description: 'Vísbending: þú lærir eins mikið á því að gera það vitlaust eins og að gera það rétt. Þú getur alltaf reynt aftur!',
                attachTo:'#contentSub',
                position: 'bottom',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                allowAutomaticOkay: false, 
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=9'          
                } ],
 
} , {
                //11
                title: 'Þú átt að gera!',
                description: '<br>Það að stofna notendasíðu á wikipedia er eins einfalt og að breyta henni.<br><br>Smelltu á <b>Skapa Frumkóða</b> eða <b>Breyta Frumkóða</b> hérna fyrir ofan.<br><br>(Þetta ævintýri notar alltaf frumkóða ritilinn).<br><br>',
                attachTo: '#ca-edit',
                position: 'bottom',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                allowAutomaticOkay: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=9'          
                } ],
                shouldSkip: function() {
                        return gt.hasQuery( { action: 'edit' } );
                }
 
}, {
                //12
                title: 'Breytingarviðmótið',
                description: '<br><div align="right">[[File:TWA_guide_right_top.png|link=]]</div>Hérna gerast galdrarnir.<br><br>Sláðu inn í stóra texta kassann uppi til vinstri: notendanafnið þitt, borg eða land, menntun, færni og áhugamál. Hvað ertu spennt/ur fyrir að <i>gera</i> hérna? Deildu eins miklu eða litlu eins og þú villt, en gerðu í það minnsta <b>EINA</b> breytingu. <br><br>Ef þú hefur nú þegar notendasíðu, gerðu að minnsta kosti <b>EINA</b> umbót á henni.<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                attachTo: '#wpTextbox1', 
                position: 'bottomRight',
                closeOnClickOutside: false,
	            buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=11'          
                } , {
                        name: 'Skrifaði það',
                        action: 'next'
               } ],
 
} , {
                //13
                title: 'Breytingarágrip og vista',
                description: '<br>Þetta lítur mjög vel út! Áður en þú smellir, skildu eftir stutt skilaboð um þær breytingar sem þú hefur gert, sem kallast breytingarágrip. Þetta hjálpar öðrum að fylgja þínum breytingum.<br><br>Segjum að þú hefur "Kynni sjálfan mig".<br><br>Nú þarft þú aðeins að vista það. Vistun gerir breytinguna opinbera og aðgengilega öllum. Smelltu á VISTA þegar þú ert tilbúin/n.<br><br>',
                onShow: gt.parseDescription,
                overlay: false,
                attachTo:  '#wpSave', 
                position: 'bottomRight',
                closeOnClickOutside: false,
                allowAutomaticOkay: false,
                shouldSkip: function() {
                        return gt.isPostEdit();
                },
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=12&action=edit'          
                } ],
                buttons: postEditButtons
 
} , {
                //14
                title: 'Til hamingju!',
                description: 'Þú VANNST þér inn NÝTT TÓL:  <b>Merki þáttakandans</b><center>[[File:TWA badge 1.png|250px|link=]]</center><br>Þú ert þáttakandi á Wikipedu! Hvernig finnst þér um það? Það er flott að þú hefur kynnt þig.<br>',
                overlay: false,
                onShow: gt.parseDescription,
                closeOnClickOutside: false,
                allowAutomaticOkay: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=13&action=edit'          
                } , {
                        name: 'Gerðu það enþá betra*',
                        onclick: function()  {  if(!mw.config.get('wgUserName')){  alert( "Please login." );   return;   } sendMessage( 'User:' + mw.config.get( 'wgUserName' ), 'Wikipedia:TWA/Badge/1template2' , mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=15'); } 
                } ],
 
} , {
                //15
                title: 'Enþá betra',
                description: '<br>Förum til baka og gerum nokkrar breytingar á textanum. Smelltu á <b>Breyta Frumkóða</b><br><br>',
                overlay: false,
                attachTo: '#ca-edit',
                position: 'bottom',
                onShow: gt.parseDescription,
                closeOnClickOutside: false,
                allowAutomaticOkay: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=14'          
                } ],
                shouldSkip: function() {
                        return gt.hasQuery( { action: 'edit' } );
                }
 
} , {
                //16
                title: 'Bæta feitletrun við',
                description: '<br><div align="right">[[File:TWA guide right top.png|link=]]</div>Í texta boxinu, yfirstrikaðu notendanafnið þitt (eða einhveja lykilsetningu) með músinni þinni.<br><br>Smelltu síðan á þennan takka(athuga hvort attachto sé rétt með Firefox). <br><br>Breytingar stikan auðveldar vinnslu á Wikipediu, því það bætir stílbrögðum við fyrir þig.',
                attachTo: '#mw-editbutton-bold',
                position: 'top',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=15'          
                } , {
                	    name: 'Feitletrað',
                        action: 'next'
                } ],
 
} , {
                //17
                title: 'Bæta skáletrun við',
                description: '<br><div align="right">[[File:TWA guide right top.png|link=]]</div>Yfirstrikaðu núna yfir áhugamálin þín.<br><br>Smelltu síðan á þennan takka til að gera hann skáletraðan. (athuga attachTo með FF)<br><br>',
                attachTo: '#mw-editbutton-italic', 
                position: 'top',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                allowAutomaticOkay: false	,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=16&action=edit'          
                } , {
                	    name: 'Skáletrað',
                        action: 'next'
                } ],
 
} , {
                //18
                title: 'Bæta við wikitenglum',
                description: '<br><div align="right">[[File:TWA guide right top.png|link=]]</div>Þú getur tengt við aðrar síður á Wikipedia. Þetta hjálpar við að "stækka vefinn" og kemur í veg fyrir að þú náir einhverjum árangri á meðan þú flakkar á milli síðna ;)<br><br> Yfirstrikaðu borgina eða landið sem þú ert frá. <br><br>Smelltu síðan á þenntan takka.<br><br>Að lokum, BÆTTU tenglinum við.<br><br>',
                attachTo: '#mw-editbutton-link', 
                position: 'top',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=17&action=edit'          
                } , {
                	    name: 'Wikitengt',
                        action: 'next'
                } ],
 
} , {
                //19
                title: 'Breytingarágrip og vista',
                description: '<br>Þú "Bætti við feitletrun, skáletrun og wikitenglum". Smelltu bara á VISTA og breytingarnar þínar verða aðgengilegar öllum.<br><br>',
                attachTo: '#wpSave',
                position: 'bottomRight',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                allowAutomaticOkay: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=18&action=edit'          
                } ],
                shouldSkip: function() {
                        return gt.isPostEdit();
                },
                buttons: postEditButtons
 
} , {
                //20
                title: 'Þú gerðir það :)',
                description: 'Þú VANNST þér inn NÝTT TÓL:  <b>Stílsniðs merki</b><center>[[File:TWA badge 2.png|250px|link=]]</center><br>Þú ert að læra hratt. Þú ert æðisleg/ur. Við erum réttsvo að byrja en þú hefur grunn verkfærin til að fara í ævintýri. Taktu eftir kunnátu mælinum neðst á síðunni eftir því sem þú öðlast frekari styrk.<br>',
                overlay: true,
                onShow: gt.parseDescription,
                closeOnClickOutside: false,
                buttons: [ {
                        name: '<big>←</big>',
                        action: 'externalLink',
                        url: mw.util.getUrl( 'Special:MyPage' ) + '?tour=twa1&step=19&action=edit'          
                } , {
                        name: 'Hvað gerist næst??*',
                        onclick: function()  {  if(!mw.config.get('wgUserName')){  alert( "Vinsamlegast skráðu þig inn." );   return;   } sendMessage( 'User:' + mw.config.get( 'wgUserName' ), 'Wikipedia:TWA/Badge/2template2' , mw.util.getUrl( 'Wikipedia:TWA/1/End' ) + '?tour=twa1&step=21'); } 
                } ],
                allowAutomaticOkay: false
 
} , {
                //21
                title: 'Sendiferð 1 lokið!',
                description: '<br>[[File:Carl Czerny - Duo Concertante - 1. Allegro (short).ogg]]<br><b>Ferðin að sendiferð 2...</b>',
                onShow: gt.parseDescription,
                overlay: false,
                closeOnClickOutside: false,
                buttons: [ {
                	    name: 'Til hamingju ég!',
                        action: 'end'      
                } ],
 
 
}]
 
} );
 
} (window, document, jQuery, mediaWiki, mediaWiki.guidedTour ) ) ;