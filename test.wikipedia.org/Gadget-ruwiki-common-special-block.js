function runAsEarlyAsPossible( callback, $testElement, func ) {
	func = func || $;
	$testElement = $testElement || $( '#footer' );

	if ( $testElement.length ) {
		callback();
	} else {
		func( callback );
	}
}

mw.hook( 'wikipage.content' ).add( function () {
console.log((-window.lastTime + (window.lastTime = $.now())) + ' ms — MediaWiki:Gadget-ruwiki-common-special-block.js function run.');

// Подсветка изменившихся элементов
$.fn.highlight = function () {
	$( this ).addClass( 'highlighted' );
	setTimeout( function () {
		$( '.highlighted' ).removeClass( 'highlighted' );
	}, 1000 );
};

// Получить элемент ввода MediaWiki / установить его значение
function elem( name, value ) {
	var $el = $( '[name="wp' + name + '"]' );
	if ( value !== undefined ) {
		// Устанавливаем значение
		if ( $el.prop( 'tagName' ) === 'SELECT' ) {
			var $option = $el.find( 'option[value*="' + value + '"]' );
			if ( $option.prop( 'selected' ) !== !!value ) {
				$option.prop( 'selected', true ).end().change();
				$el.highlight();
			}
		} else if ( $el.attr( 'type' ) === 'checkbox' ) {
			if ( $el.prop( 'checked' ) !== !!value ) {
				$el.prop( 'checked', value );
				$el.highlight();
			}
		} else {  // type = text
			if ( /-other$/.test( name ) ) {
				// Устанавливаем значение «родительского» выпадающего списка — «other»
				elem( name.replace( /-other$/ , ''), 'other' );
			}
			if ( $el.val() !== value ) {
				$el.val( value );
				$el.highlight();
			}
		}
	}
	return $el;
}

elem( 'Reason' ).change( function () {
	if ( /Unacc/.test( this.value ) ) {
		elem( 'Expiry' , 'infinite' );
		if ( /Unacc/.test( this.value ) ) {
			elem( 'CreateAccount', false );
			elem( 'DisableEmail', false );
			elem( 'AutoBlock', false );
		}
	} else if ( /Abusing/.test( this.value ) ) {
		// Если значение не установлено
		if ( elem( 'Expiry' ).val() === 'other' && !elem( 'Expiry-other' ).val() ) {
			elem( 'Expiry-other', '5 years' );
		}
		elem( 'CreateAccount', true );
		elem( 'DisableEmail', true );
		elem( 'HardBlock', true );
	} else if ( /Vandalism/.test( this.value ) ||
		/«общая» учётная запись/i.test( this.value ) )
	{
		elem( 'Expiry', 'infinite' );
	}
} );

// https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver
// Если изменение состояния эмуляции селекта в OOjs UI будет само инициировать событие change
// на настоящем селекте, этот кусок можно будет удалить.
var observe = function () {
	if ( typeof MutationObserver === 'function' ) {
		var observer = new MutationObserver( function ( mutations ) {
			mutations.forEach( function ( mutation ) {
				if ( mutation.addedNodes.length ) {
					elem( 'Reason' ).change();
				}
			} );
		} );
		observer.observe(
			$( '#mw-input-wpReason-select .oo-ui-dropdownWidget-handle .oo-ui-labelElement-label' )[ 0 ],
			{ childList: true }
		);
	}
};

runAsEarlyAsPossible( observe, $( '#mw-input-wpReason-select .oo-ui-dropdownWidget-handle .oo-ui-labelElement-label' ), function () {
	setTimeout( observe, 2000 );
} );

}() );
console.log((-window.lastTime + (window.lastTime = $.now())) + ' ms — MediaWiki:Gadget-ruwiki-common-special-block.js loaded.');