/**
 * Highlights your username in historic
 *
 * @author [[w:en:User:Gray King]]
 * @author [[w:pt:User:!Silent]]
 * @source [[w:en:User:Gary King/highlight my username in history.js]]
 * @update 23/Fev/2014
*/
/* global mediaWiki, jQuery */

( function( mw, $ ) {
'use strict';

function highlightsUsername() {
	$( '#pagehistory .mw-userlink' ).each( function() {
		if ( $( this ).text() === mw.config.get( 'wgUserName' ) ) {
			$( this ).addClass( 'highlightsUsername' );
		}
	} );
}

if ( mw.config.get( 'wgAction' ) === 'history' ) {
	$( highlightsUsername );
}

}( mediaWiki, jQuery ) );