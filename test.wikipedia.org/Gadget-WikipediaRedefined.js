/* The JS part of WikipediaRedefined
CC-BY-SA Thomas netAction Schmidt */


if (!!('ontouchstart' in window)) $('body').addClass('wprtouch');


$(function() {
if (mediaWiki.config.get("skin")!="simple") {
  $('<div id="WikipediaRedefinedMessage">Um Wikipedia Redefined zu nutzen bitte <a href="//test.wikipedia.org/wiki/Special:Preferences#mw-prefsection-rendering">Benutzeroberfläche „Einfach“ einschalten</a>!').prependTo('#bodyContent');
} else {
// Transform UL lists to SELECT inputs
$('#p-lang ul, #p-sisterprojects ul, #p-personal ul').each(function(){
  var list=$(this),
  select=$(document.createElement('select')).insertBefore($(this).hide());
  $('>li a', this).each(function(){
    var target=$(this).attr('target'),
    option=$(document.createElement('option'))
     .appendTo(select)
     .val(this.href)
     .html($(this).html());
  });
  /* move headline h5 to first element of select box */
  $(document.createElement('option'))
       .prependTo(select)
       .html(list.parent().parent().find('h5').html())
       .attr('disabled','disabled');
  /* if headline visible then select corresponding option, else select successor */
  if (list.parent().parent().find('h5').css('display')=='inline')
    select.find('option[disabled="disabled"]').attr('selected','selected');
    else select.find('option[disabled!="disabled"]').first().attr('selected','selected');

  list.parent().parent().find('h5').hide();
  list.parent().parent().addClass('dropDown');
  list.remove();

  select.change(function () { window.location.href=$(this).val(); });
  select.focus(function () { $(this).find('option[disabled="disabled"]').attr('selected','selected'); });
}); // each
}}); // ready