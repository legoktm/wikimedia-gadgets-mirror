window.C = {};

C.param = {
	summary: '[[C-helper]] : ',
};

C.modules = {
  'si':{
    id: 'C-si',
    title: 'Demander une SI',
    label: 'SI',
  },
  'tag':{
    id: 'C-tag',
    title: 'Apposer un bandeau de maintenance',
    label: 'Bandeau',
  },
  'dph':{
    id: 'c-dph',
    title: 'Demander la purge de certaines versions d\'historiques',
    label: 'Masquage',
  },
  'purge':{
    id: 'c-purge',
    title: 'Forcer le rafraichissement de cette page',
    label: 'Purge',
  },
  'message':{
    id: 'C-message',
    title: 'Laisser un message sur la pdd de l\'utilisateur',
    label: 'Message',
  },
};

C.tab = {
  'page':['tag', 'si', 'dph', 'purge'],
  'user':['message', 'si', 'dph', 'purge'],
  'diff':['message', 'tag', 'si', 'dph', 'purge'],
};

$(function() {
	
	if($('.skin-monobook').length)
		ul = $('.pBody ul');
	else {
		caption = $('<div/>', {id: 'C-captions', class: 'vectorMenu'});
		h3 = $('<h3/>');
		h3.html('<span>Č</span><a href="#"></a>');
		ul = $('<ul/>');
		menu = $('<div/>', {class: 'menu'}).append(ul);
		caption.append(h3);
		caption.append(menu);
		$("#p-cactions").after(caption);
	}
	
	var tab = 'page';
	if($(".diff").length)
		tab = 'diff';
	else if(mw.config.get('wgNamespaceNumber') == 2 || mw.config.get('wgNamespaceNumber') == 3)
		tab = 'user';
	
	$.each(C.tab[tab], function(key, item) {
	  a = $('<a/>', {href: '#', title: C.modules[item].title}).text(C.modules[item].label);
	  li = $('<li/>', {id: C.modules[item].id}).append(a);
	  ul.append(li);
	  $("#"+C.modules[item].id).click(function(e){
	    e.preventDefault();
	    id = $(e.currentTarget).attr("id");
	    $.each(C.modules, function(key, item){
	      if(item.id == id) {
	        item.callback.launch();
	      }
	    });
	  });
	  console.log("Module '"+item+"' loaded");
	});
});