mw.hook( 'wikipage.content' ).add( function() {
	var api = new mw.Api();
	
	var apiRequest = api.get( {
		'action':'query',
		'titles': mw.config.get('wgPageName'),
		'prop': 'categories',
		'clprop': 'sortkey|hidden',
		'cllimit': 50,
		'indexpageids': true,
		'formatversion': 2
	} );
	var apiRequest2 = api.loadMessagesIfMissing( [ 'pagecategories', 'colon-separator' ] );

	$.when( apiRequest, apiRequest2 ).done( function( data ) {
		var categories = [],
			response1Data = data[0],
			response2Data = data[1];
		
		if( !response1Data.query.pages[0].categories ) {
			return;
		}
		function categoryHtmlGenerator(element, index, array) {
			if( element.hidden ) {
				return;
			}
			var title = new mw.Title( element.title, mw.config.get( 'wgNamespaceIds' ).file );
  			categories.push(
  				$( '<a>' ).attr( {
  					'href': mw.util.getUrl( element.title )
  				} ).append(
  					document.createTextNode( title.getMainText() ) 
  				).get( 0 )
  			);
  			if( index+1 < array.length ) {
  				categories.push( ', ' );
  			}
		}
		response1Data.query.pages[0].categories.forEach( categoryHtmlGenerator );

		if( categories.length === 0 ) {
			return;
		}
		categories.unshift( document.createTextNode(
			mw.message( 'pagecategories' ).escaped() + mw.message( 'colon-separator' ).escaped() + ' '
		) );
		
		$('.printfooter')
			.after( $( '<div>' )
				.addClass( 'catlinks' )
				.attr( 'id', 'catlinks' )
				.data( 'mw', 'interface' )
				.append( $( '<div>' )
						.attr( 'id', 'mw-normal-catlink' )
						.append( categories )
				)
			);
	} );
} );