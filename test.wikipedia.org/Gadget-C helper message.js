//<nowiki>
C.message = {
  dialog: null,
  config: {
    'modal-title': 'Chelper - Informer un utilisateur',
    'submit': 'valider la requête',
    'c-message-checkbox-class': 'c-message-checkbox',
    'c-message-radio-class': 'c-message-radio',
    'notify-sucess': 'Les messages ont bien été ajoutés.',
    'notify-no-reason': 'Vous n\'avez fait aucun changement.',
    'notify-not-a-page': 'Impossible d\'ajouter un message à un utilisateur depuis cette page.',
    'page-input-help': 'De nombreux modèles font référence à la page où le problème à eu lieu ; C-helper n\'a pas réussi à la déterminer automatiquement, veuillez donc la préciser manuellement si nécessaire.',
    'subst-checkbox-help': 'Cette case doit rester coché dans la plupart des cas.',
    'single-add-summary': '$1',
    'multi-add-summary': '$1 et $2',
    'template-link-summary': '$1',
  },
  categories: [
  	{display:"Souhaiter la bienvenue", node:null}, //0
  	{display:"Maladresses", node:null},    //1
  	{display:"Modèles vandalisme", node:null},    //2
  	{display:"Suggestion", node:null},    //3
  	{display:"Liens externes", node:null},    //4
  	{display:"Copyvio", node:null},    //5
  	{display:"Annonce de Suppression Immédiate", node:null},    //6
  	{display:"Aide", node:null},    //7
  ],
  /*
    {category:, display:'', template:'', help:'', reason:'', page:'', diff:''},
	Magic words :
		* $(page)
		* $(diff)
		* $(user)
		* $(day) $(month) $(year)
		* $(reason)
		* $(extra)
  */
  templates: [
	{category:0, display:'Bienvenue nouveau', template:'Bienvenue nouveau|$(user)|sign=~~~~|message=$(extra)', extra:'Ajouter un message personnel (facultatif) :', help:''},
	{category:0, display:'Bienvenue IP', template:'Bienvenue IP', help:''},
	{category:0, display:'Bienvenue IP méritante', template:'Bienvenue IP méritante|$(user)|sign=~~~~|message=$(extra)', extra:'Ajouter un message personnel (facultatif) :', help:''},
	{category:1, display:'Test 0', template:'Test 0|$(page)|raison=$(extra)|user=$(user)', extra:'Type de maladresse (facultatif) :', help:''},
	{category:1, display:'Ajout POV', template:'Ajout POV|$(page)|user=$(user)', help:''},
	{category:1, display:'Non-encyclo', template:'Non-encyclo|user=$(user)', help:''},
	{category:1, display:'Retrait injustifié', template:'Retrait injustifié|$(page)|user=$(user)', help:''},
	{category:1, display:'Ortho', template:'Ortho|$(page)|user=$(user)', help:''},
	{category:1, display:'Signature', template:'Signature', help:''},
	{category:1, display:'Traduction automatique', template:'Traduction automatique|$(page)|user=$(user)', help:''},
	{category:1, display:'No ref wiki', template:'No ref wiki|$(page)|user=$(user)', help:''},
	{category:1, display:'PU brouillon', template:'PU brouillon', help:''},
	{category:1, display:'Compte publicitaire', template:'Compte publicitaire', help:''},
	{category:2, display:'Test 1', template:'Test 1|$(page)', help:''},
	{category:2, display:'Test 2', template:'Test 2', help:''},
	{category:2, display:'Test 3', template:'Test 3', help:''},
	{category:2, display:'Test 4', template:'Test 4', help:''},
	{category:2, display:'Faux décès', template:'Faux décès|$(page)', help:''},
	{category:2, display:'Vandalisme cracra', template:'Vandalisme cracra', help:''},
	{category:3, display:'Faut sourcer', template:'Faut sourcer|$(page)|user=$(user)', help:''},
	{category:3, display:'Motivation modif', template:'Motivation modif|$(page)|user=$(user)', help:''},
	{category:4, display:'Bienvenue spammeur', template:'Bienvenue spammeur|$(page)|user=$(user)', help:''},
	{category:4, display:'LE HC', template:'LE HC|$(page)|user=$(user)', help:''},
	{category:4, display:'LE dans texte', template:'LE dans texte|$(page)|user=$(user)', help:''},
	{category:4, display:'Bienvenue spammeur 2', template:'Bienvenue spammeur 2|$(page)', help:''},
	{category:4, display:'Bienvenue spammeur 3', template:'Bienvenue spammeur 3|$(page)', help:''},
	{category:5, display:'Bienvenue Copyvio 1', template:'Bienvenue Copyvio 1|$(page)|$(extra)', extra:'Site copié (facultatif) :', help:''},
	{category:5, display:'Copieurlight', template:'Copieurlight|$(page)|$(extra)', extra:'Site copié (facultatif) :', help:''},
	{category:5, display:'Bienvenue Copyvio 3', template:'Bienvenue Copyvio 3|$(page)|$(extra)', extra:'Site copié (facultatif) :', help:''},
	{category:6, display:'BSI CAA', template:'BSI CAA|$(page)', help:''},
	{category:6, display:'BSI BàS', template:'BSI BàS|$(page)|user=$(user)', help:''},
	{category:6, display:'BSI promo', template:'BSI promo|$(page)|user=$(user)', help:''},
	{category:6, display:'BSI canular', template:'BSI canular|$(page)|user=$(user)', help:''},
	{category:6, display:'BSI doublon', template:'BSI doublon|$(page)|$(extra)|user=$(user)', extra:'Titre de l\'article en double :', help:''},
	{category:6, display:'Bienvenue SI 2', template:'Bienvenue SI 2', help:''},
	{category:6, display:'Bienvenue SI 3', template:'Bienvenue SI 3', help:''},
	{category:7, display:'Aide sources', template:'Aide sources|user=$(user)', help:''},
	{category:7, display:'Aide images', template:'Aide images|user=$(user)', help:''},
	{category:7, display:'Aide wikification', template:'Aide wikification|user=$(user)', help:''},
	{category:7, display:'Aide liens', template:'Aide liens', help:''},
  ],
  categories_container: null,
  alphabetic_container: null,
  current_display_mode: "categories",

  init: function() {
  },
  launch: function() {
  	if(mw.config.get('wgArticleId') === 0) {
    	mw.notify(C.message.config['notify-not-a-page'], {title:'C-helper', type:'error'});
    	return;
  	}
	if(this.dialog === null) {
		this.build_dialog();
	}
	this.dialog.dialog("open");
  },
  "build_dialog": function() {
    this.dialog = $('<div/>', {title:this.config['modal-title']});
    var form = $('<form/>');
    this.dialog.append(form);
    
    // Try to determine the page name to display in the template, if it's ambigeous, ask a manual input
    if(mw.config.get('wgNamespaceNumber') == 2 || mw.config.get('wgNamespaceNumber') == 3) {
    	$("<div/>")
               .append($("<label/>", {"for":"C-message-input-page"}).text("Article modifié :"))
               .append($("<input/>", {type:"text", name:"C-message-input-page", id:"C-message-input-page"}))
               .append(C.util.construct_help_icon(this.config['page-input-help']))
               .appendTo(form);
    }
    else {
    	$("<div/>")
               .append($("<input/>", {type:"hidden", name:"C-message-input-page", id:"C-message-input-page", value:mw.config.get('wgPageName')}))
               .appendTo(form);
    }
	$("<div/>")
           .append($("<input/>", {type:"checkbox", checked: "checked", name:"C-message-checkbox-subst", id:"C-message-checkbox-subst"}))
           .append($("<label/>", {"for":"C-message-checkbox-subst"}).text("Subsrer les modèles"))
           .append(C.util.construct_help_icon(this.config['subst-checkbox-help']))
           .appendTo(form);

    //Create sections
    this.alphabetic_container = $("<div/>", {id:"C-message-alphabetic-container"});
    this.categories_container = $("<div/>", {id:"C-message-categories-container"});
    for(i=0; i<this.categories.length; i++) {
    	this.categories[i].node = $("<div/>").append($("<h3/>").text(this.categories[i].display)).appendTo(this.categories_container);
    }
    for(i=0; i<this.templates.length; i++) {
		this.templates[i].node = $("<div/>");
		this.templates[i].node.append($("<input/>", {type:"checkbox", id:"C-message-"+i, message_id:i, class:this.config['c-message-checkbox-class']}));
		this.templates[i].node.append($("<label/>", {"for":"C-message-"+i}).text(this.templates[i].display));
		
		if(this.templates[i].help !== '') {
			this.templates[i].node.append(C.util.construct_help_icon(this.templates[i].help));
		}
		if(this.templates[i].hasOwnProperty('extra')) {
			this.templates[i].node.append($('<span/>', {id:'C-message-extra-'+i, class:this.config['c-message-extra-class']})
			                      		.hide()
			                      		.append($('<br/>'))
			                            .append($('<label/>', {"for":'C-message-extra-input-'+i}).html(this.templates[i].extra))
			                            .append("&nbsp;")
			    						.append($('<input/>', {type:'text', id:'C-message-extra-input-'+i})));
		}
		this.categories[this.templates[i].category].node.append(this.templates[i].node);
    }
    form.append(this.alphabetic_container);
    form.append(this.categories_container);
    
    this.dialog.dialog({
      autoOpen: false,
      height: 400,
      width: 600,
      modal: true,
      buttons: [
        {
          text: C.message.config['submit'],
          click: function() {
            C.message.dialog.dialog("close");
            C.message.validate();
          },
        },
      ],
    dialogClass: 'c-helper-dialog',
    });

	$('.'+this.config['c-message-checkbox-class']).change(function() {
		if($(this).is(":checked")) {
			$('#C-message-extra-'+$(this).attr('message_id')).show();
		}
		else {
			$('#C-message-extra-'+$(this).attr('message_id')).hide();
		}
	});
  },

  validate: function() {
  	var to_append = "";
  	var summary_array = [];
  	for(i=0; i<this.templates.length; i++) {
  		if($('#C-message-'+i).is(':checked')) {
  			tmp = this.templates[i].template.replace(/\$\(day\)/g, "{{subst:CURRENTDAY}}")
  			                             .replace(/\$\(month\)/g, "{{subst:CURRENTMONTHNAME}}")
  			                             .replace(/\$\(year\)/g, "{{subst:CURRENTYEAR}}")
  			                             .replace(/\$\(page\)/g, $('#C-message-input-page').val())
  			                             .replace(/\$\(diff\)/g, mw.config.get('wgRevisionId'))
  			                             .replace(/\$\(user\)/g, mw.config.get('wgUserName'));
  			if($('#C-message-extra-input-'+i).length > 0)
  				tmp = tmp.replace(/\$\(extra\)/g, $('#C-message-extra-input-'+i).val());
  			subst = "";
  			if($('#C-message-checkbox-subst').is(':checked'))
  				subst = "subst:";
            
  			to_append += '\n\n{{'+subst+tmp+'}}';
  			summary_array.push(this.templates[i].display);
  		}
  	}
  	if(summary_array.length > 0) {
  		if(summary_array.length == 1) {
  			summary = this.config['single-add-summary'].replace(/\$1/g, this.config['template-link-summary'].replace(/\$1/g, summary_array[0]));
  		}
  		else {
  			summary = "";
  			while(summary_array.length > 2)
  				summary += this.config['template-link-summary'].replace(/\$1/g, summary_array.shift()) + ", ";
  			summary += this.config['template-link-summary'].replace(/\$1/g, summary_array.shift());
  			summary = this.config['multi-add-summary'].replace(/\$1/g, summary).replace(/\$2/g, this.config['template-link-summary'].replace(/\$1/g, summary_array[0]));
  		}
  		
  		var target_user_name = "";
  		if($(".diff").length)
  			target_user_name = $(".diff-ntitle .mw-userlink").text();
  		else
  			target_user_name = mw.config.get('wgRelevantUserName');
  		
  		C.util.append("Discussion utilisateur:"+target_user_name, to_append, summary, function() {
  			mw.notify(C.message.config['notify-sucess'], {title:'C-helper', type:'info'});
  		});
  	}
  },
};

C.modules.message.callback = C.message;
//</nowiki>