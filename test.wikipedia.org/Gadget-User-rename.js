// Add "Rename User" link
jQuery(document).ready( function( $ ) {
	if ( $.inArray( mw.config.get('wgNamespaceNumber'), [2, 3] ) ) {
		mw.util.addPortletLink( 'p-cactions', wgScript + '?title=Special:RenameUser&user=' + encodeURIComponent( wgTitle.split('/')[0] ),
				'Rename User', 'ca-rights', 'Rename this user');
	}
} );