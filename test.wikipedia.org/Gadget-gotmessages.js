/* First, we hide the usermessage boxes before documents load.
We don't want to do this in CSS, because if the user has JS 
disabled or broken, the default message box should show instead.
Note: replace with appendCSS call, if bug#13232 is ever fixed.
*/

var umstyle = document.createElement('style');
umstyle.type = 'text/css';
umstyle.rel = 'stylesheet';
if (umstyle.styleSheet) umstyle.styleSheet.cssText = '.usermessage {display:none;}'
else umstyle.appendChild(document.createTextNode('.usermessage {display:none;}'))
document.getElementsByTagName('head')[0].appendChild(umstyle);

/* If the page contains a usermessage div, style the pt-mytalk. */

$(gotNewMessages);
function gotNewMessages() {
  var um = getElementsByClassName(document.getElementById('bodyContent'),'div','usermessage');
  if(um.length == 0) return
  var links = um[0].getElementsByTagName('a');
  for(var i=0;i<links.length;i++) {
    if(links[i].href.indexOf('diff=cur') != -1) {
      var penultimate = links[i].href;
    }
  }
  var mytalk = document.getElementById('pt-mytalk');
  if(!mytalk) return
  
  mytalk.style.fontWeight = 'bold';
  var img = '//commons.wikimedia.org/w/thumb.php?f=Emblem-important.svg&w=13';
  if(penultimate) {
    /* add a linked icon if the message contains a diff=cur */
    var plink = document.createElement('a');
    plink.setAttribute('href',penultimate);
    plink.setAttribute('title','diff to last change');
    var pimg = document.createElement('img');
    pimg.setAttribute('src',img);
    pimg.style.width = '13px';
    pimg.style.height = '13px';
    pimg.style.marginLeft = '5px';
    plink.appendChild(pimg);
    plink.appendChild(document.createTextNode(' '));
    mytalk.appendChild(plink);
  } else {
    mytalk.style.background = 'transparent url("' + img + '") no-repeat center right';
    mytalk.style.paddingRight = '16px';
  }
}