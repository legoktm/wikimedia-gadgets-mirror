// Original code written by [[User:Ilmari Karonen]]
// Rewritten & extended by [[User:DieBuche]]. Botdetection and encoding fixer by [[User:Lupo]]
// Validation and further development [[User:Rillke]], 2011-2012
//
// Ajax-based replacement for [[MediaWiki:Quick-delete-code.js]]
//
// Invoke automated jsHint-validation on save: A feature on Wikimedia Commons
// Interested? See [[:commons:MediaWiki:JSValidator.js]] or [[:commons:Help:JSValidator]].
//
// TODO: Fix problems with moves of videos
// TODO: Delete talk
//<nowiki>

/*global jQuery:false, mediaWiki:false */
/*jshint curly:false, laxbreak:true, scripturl:true, onecase:true, */ 

(function($, mw, undefined) {

'use strict';

// Guard against multiple inclusions
if (typeof window.AjaxQuickDelete === 'object') return;

var nsNumber = mw.config.get('wgNamespaceNumber'),
    pageName = mw.config.get('wgPageName'),
    canonicalNs = mw.config.get('wgCanonicalNamespace'),
    AQD;

// A bunch of helper functions	
function firstItem(o) { for (var i in o) { if (o.hasOwnProperty(i)) { return o[i]; } } }

// Create the AjaxQuickDelete-singleton (object literal)
AQD = window.AjaxQuickDelete = {
   /**
    ** Set up the AjaxQuickDelete object and add the toolbox link.  Called via $(document).ready() during page loading.
    **/
   install: function() {
      // Disallow performing operations on empty or special pages
      AQD.doNothing = (0 === mw.config.get('wgArticleId') || nsNumber < 0);

      if (AQD.doNothing) return;

      // Check edit restrictions and do not install anything if protected
      if (mw.config.get('wgRestrictionEdit') && mw.config.get('wgRestrictionEdit').length) {
            if ($.inArray(mw.config.get('wgRestrictionEdit')[0], mw.config.get('wgUserGroups')) === -1) {
                  return;
            }
      }

      // wait for document.readyState
      $(function() {
         // Trigger a jQuery event for other scripts that like to know
         // when this script has loaded all translations and is ready to install
         $(document).triggerHandler('scriptLoaded', ['AjaxQuickDelete']);

         // Set up toolbox link
         if (nsNumber === 14) {
            // In categories the discuss-category link
            // mw.util.addPortletLink('p-tb', 'javascript:AjaxQuickDelete.discussCategory();', AQD.i18n.toolboxLinkDiscuss, 't-ajaxquickdiscusscat');
         } else {
            // On other pages, the nominate-for-deletion link
            mw.util.addPortletLink('p-tb', 'javascript:AjaxQuickDelete.nominateForDeletion();', AQD.i18n.toolboxLinkDelete, 't-ajaxquickdelete');
         }

         // Extra buttons
         if ("1" === mw.user.options.get('gadget-QuickDelete')) {
            // Wait until the user's js was loaded and executed
            mw.loader.using(['ext.gadget.QuickDelete', 'user'], function() {
               AQD.doInsertTagButtons();
            });
         }
      });
   },
   
   /**
    ** Ensure that all variables are in a good state
    ** You must call this method before doing anything!
    ** TODO: Never override pageName, always clean task queue
    **/
   initialize: function() {
      pageName = mw.config.get('wgPageName');
      this.tasks = [];
      this.destination = undefined;
      this.details = undefined;
   },

   insertTagOnPage: function(tag, img_summary, talk_tag, talk_summary, prompt_text, page) {
      this.initialize();

      this.pageName = (page === undefined) ? pageName.replace(/_/g, ' ') : page.replace(/_/g, ' ');
      this.tag = tag + '\n';
      this.img_summary = img_summary;

      // first schedule some API queries to fetch the info we need...

      // get token
      this.addTask('findCreator');

      this.addTask('prependTemplate');

      // Cave: insertTagOnPage is inserted as javascript link and therefore talk_tag can be "undefined"/string
      if (talk_tag && talk_tag !== "undefined") {
         this.talk_tag = talk_tag.replace('%FILE%', this.pageName);
         this.talk_summary = talk_summary.replace('%FILE%', '[[:' + this.pageName + ']]');

         this.usersNeeded = true;
         this.addTask('notifyUploaders');
      }
      this.addTask('reloadPage');

      if (tag.indexOf("%PARAMETER%") !== -1) {
         this.prompt([{
            message: '',
            prefill: '',
            returnvalue: 'reason',
            cleanUp: true,
            noEmpty: true,
            minLength: 1
         }], prompt_text || this.i18n.reasonForDeletion);
      } else {
         this.nextTask();
      }
   },

   discussCategory: function() {
      // reset task list in case an earlier error left it non-empty
      this.initialize(); 

      this.pageName = pageName.replace(/_/g, ' ');
      this.startDate = new Date();
      this.tag = '{' + '{subst:cfd}}';
      this.img_summary = 'This category needs discussion';
      this.talk_tag = '{' + '{subst:cdw|' + pageName + '}}';
      this.talk_summary = "[[:" + pageName + "]] needs discussion";
      this.subpage_summary = 'Starting category discussion';

      // set up some page names we'll need later
      this.requestPage = 'Commons:Categories for discussion/' + this.formatDate("YYYY/MM/") + pageName;
      this.dailyLogPage = 'Commons:Categories for discussion/' + this.formatDate("YYYY/MM");

      // first schedule some API queries to fetch the info we need...
      this.addTask('findCreator');
      
      // ...then schedule the actual edits
      this.addTask('notifyUploaders');
      this.addTask('prependTemplate');
      this.addTask('createRequestSubpage');
      this.addTask('listRequestSubpage');

      // finally reload the page to show the deletion tag
      this.addTask('reloadPage');

      var lazyLoadNode = this.createLazyLoadNode(this.i18n.moreInformation, 'MediaWiki:Gadget-AjaxQuickDelete.js/DiscussCategoryInfo', '#AjaxQuickDeleteCatInfo');

      this.prompt([{
         message: '',
         prefill: '',
         returnvalue: 'reason',
         cleanUp: true,
         appendNode: lazyLoadNode,
         noEmpty: true,
         parseReason: true
      }], this.i18n.reasonForDiscussion);

   },
   nominateForDeletion: function(page) {
      var o = this;
   
      // reset task list in case an earlier error left it non-empty
      this.initialize(); 

      mw.loader.using(['jquery.byteLength', 'jquery.ui.dialog'], function() {
         o.pageName = (page === undefined) ? pageName.replace(/_/g, ' ') : page.replace(/_/g, ' ');
         o.startDate = new Date();

         // set up some page names we'll need later
         var requestPage = o.pageName;
         
         // MediaWiki has an ugly limit of 255 bytes per title, excluding the namespace
         while ($.byteLength(requestPage) + $.byteLength(o.requestPagePrefix.replace(/^.+?\:/, '')) >= 255) {
            requestPage = $.trim(requestPage.slice(0, requestPage.length-1));
         }
         o.requestPage = o.requestPagePrefix + requestPage;
         o.dailyLogPage = o.requestPagePrefix + 'Log/' + o.formatDate("YYYY MON DAY");

         o.tag = "{{Cancella|arg=%TOPIC%}}";
         
         switch (nsNumber) {
            // On MediaWiki pages, wrap inside comments (for css and js)
            case 8:
               o.tag = '/*' + o.tag + '*/';
               break;
            // On templates and creator/institution-templates: Wrap inside <noinclude>s.
            case 10:
            case 100:
            case 106:
               o.tag = '<noinclude>' + o.tag + '</noinclude>';
               break;
         }
         o.img_summary = 'Nomine per la cancellatione';
         o.talk_tag = '{' + '{subst:idw|' + requestPage + '}}';
         o.talk_summary = "[[:" + o.pageName + "]] è stato nominato per la cancellatione";
         o.subpage_summary = 'Avvio di richiesta di cancellazione';

         
         // first schedule some API queries to fetch the info we need...
         o.addTask('findCreator');

         // ...then schedule the actual edits
         o.addTask('prependTemplate');
         o.addTask('createRequestSubpage');
         o.addTask('listRequestSubpage');
         o.addTask('notifyUploaders');

         // finally reload the page to show the deletion tag
         o.addTask('reloadPage');

         var lazyLoadNode = o.createLazyLoadNode(o.i18n.moreInformation, 'MediaWiki:Gadget-AjaxQuickDelete.js/DeleteInfo', '#AjaxQuickDeleteDeleteInfo');
         o.prevDRNode = $('<ul>').attr('id', 'AjaxDeletePrevRequests');
         o.secureCall('checkForFormerDR');
         var toAppend = $('<div>').append($('<div>').attr('class', 'ajaxDeleteLazyLoad').css({'max-height': Math.max(Math.round($(window).height()/2)-250, 100), 'min-height': 0, overflow: 'auto'}).append(o.prevDRNode), '<br/>', lazyLoadNode);

         o.prompt([{
            message: o.i18n.specifyGeneralTopic,
            prefill: '',
            returnvalue: 'topic',
            noEmpty: true
         }, {
            message: o.i18n.reasonForDeletion,
            prefill: o.reason || '',
            returnvalue: 'reason',
            cleanUp: true,
            noEmpty: true,
            parseReason: true,
            appendNode: toAppend
         }], o.i18n.toolboxLinkDelete);
      });
   },
   
   // Check whether there was a deletion request for the same title in the past
   checkForFormerDR: function() {
      // Don't search for "kept" when nominating talk pages
      if (nsNumber % 2 === 0) {
         this.talkPage = mw.config.get('wgFormattedNamespaces')[nsNumber+1] + ':' + this.pageName.replace(canonicalNs+':', '');
         this.queryAPI({
            action: 'query',
            prop: 'templates',
            titles: this.talkPage,
            tltemplates: 'Template:Kept',
            tllimit: 1
         }, 'formerDRTalk');
      }
      this.queryAPI({
         action: 'query',
         list: 'backlinks',
         bltitle: this.pageName,
         blnamespace: 4,
         blfilterredir: 'nonredirects',
         bllimit: 500
      }, 'formerDRRequestpage');
   },
   formerDRTalk: function(r) {
      var pgs = r.query.pages;
      $.each(pgs, function(id, pg) {
         if ($.isArray(pg.templates)) {
            $('<li>').append($('<a>', { text: AQD.i18n.keptAfterDR, href: mw.util.getUrl(AQD.talkPage) })).prependTo(AQD.prevDRNode);
         } else if (undefined === pg.missing) {
             $('<li>').append($('<a>', { text: AQD.i18n.hasTalkpage, href: mw.util.getUrl(AQD.talkPage) })).appendTo(AQD.prevDRNode);
         }
      });
   },
   formerDRRequestpage: function(r) {
      var bls = r.query.backlinks;
      var _addItem = function(t, m, bl) {
         $('<li>').append($('<a>', { text: t.replace('%PAGE%', bl.title), href: mw.util.getUrl(bl.title) }))[m](AQD.prevDRNode);
      };
      $.each(bls, function(i, bl) {
         if (this.requestPage === bl.title) {
            _addItem(AQD.i18n.mentionedInDR, 'prependTo', bl);
         } else if (/^Wikipedia:Pagine da cancellare\/[^L]/.test(bl.title)) { 
            _addItem(AQD.i18n.mentionedInDR, 'appendTo', bl);
         } else if (/^Wikipedia:Bar\//.test(bl.title)) {
            _addItem(AQD.i18n.mentionedInForum, 'appendTo', bl);
         }
      });
   },
   
   renderNode: function($node, remotecontent, selector) {
      if (selector) selector = ' ' + selector;
      
      $node.load(mw.config.get('wgScript') + '?' + $.param({
         'action': 'render',
         'title': remotecontent,
         'uselang': mw.config.get('wgUserLanguage')
      }) + (selector || ''), function() {
         $node.find('a').each(function(i, el) {
            var $el = $(el);
            $el.attr('href', $el.attr('href').replace('MediaWiki:Anoneditwarning', mw.config.get('wgPageName')));
         });
      });
      return $node;
   },

   createLazyLoadNode: function(label, page, selector) {
      return $('<div>', {
         style: 'min-height:40px;'
      }).append($('<a>', {
         'href': '#',
         'text': label
      }).click(function(e) {
         e.preventDefault();
         var $content = $(this).parent().find('.ajaxDeleteLazyLoad');
         var $contentInner = $content.find('.ajax-quick-delete-loading');
         if ($contentInner.length) {
            // first time invoked, do the XHR to load the content
            AQD.renderNode($content, $contentInner.data('aqdPage'), selector);
         }
         $content.toggle('fast');
      }), $('<div>', {
         'class': 'ajaxDeleteLazyLoad',
         'style': 'display:none;'
      }).append($('<span>', {
         'class': 'ajax-quick-delete-loading',
         'text': this.i18n.loading
      }).data('aqdPage', page)));
   },
   removeProgress: function() {
      this.showProgress();
      return this.nextTask();
   },

   /**
    ** Edit the current page to add the specified tag.  Assumes that the page hasn't
    ** been tagged yet; if it is, a duplicate tag will be added.
    **/
   prependTemplate: function() {
      var page = {
         title: this.pageName,
         text: this.tag,
         editType: 'prependtext'
      };
      
      if (window.AjaxDeleteWatchFile) page.watchlist = 'watch';

      this.showProgress(this.i18n.addingAnyTemplate);
      this.savePage(page, this.img_summary, 'nextTask');
   },

   /**
    ** Create the DR subpage (or append a new request to an existing subpage).
    ** The request page will always be watchlisted.
    **/
   createRequestSubpage: function() {
      this.templateAdded = true; // we've got this far; if something fails, user can follow instructions on template to finish
      var page = {};
      page.title = this.requestPage;
      page.text = "\n=== [[:" + this.pageName + "]] ===\n" + 
         "{{Cancellazione/proposta|" + this.pageName + "}}\n<noinclude>{{DEFAULTSORT:" + this.pageName + "}}" + 
         "[[Categoria:Cancellazioni del " + this.formatDate('DAY MON YYYY') + "]]</noinclude>\n" +
         this.reason + " ~~" + "~~\n";
      page.watchlist = 'watch';
      page.editType = 'appendtext';

      this.showProgress(this.i18n.creatingNomination);

      this.savePage(page, this.subpage_summary, 'nextTask');
   },

   /**
    ** Transclude the nomination page onto today's DR log page, creating it if necessary.
    ** The log page will never be watchlisted (unless the user is already watching it).
    **/
   listRequestSubpage: function() {
      var page = {};
      page.title = this.dailyLogPage;

      page.text = "\n{{" + this.requestPage + "}}\n";
      page.watchlist = 'nochange';
      page.editType = 'appendtext';

      this.showProgress(this.i18n.listingNomination);

      this.savePage(page, "Elencare [[" + this.requestPage + "]]", 'nextTask');
   },

   /**
    ** Notify any uploaders/creators of this page using {{idw}}.
    **/
   notifyUploaders: function() {
      this.uploadersToNotify = 0;
      for (var user in this.uploaders) {
         if (this.uploaders.hasOwnProperty(user)) {
            if (user === mw.config.get('wgUserName')) continue; // notifying yourself is pointless
            var page = {};
            page.title = this.userTalkPrefix + user;
            page.text = "\n" + this.talk_tag + " ~~" + "~~\n";
            page.editType = 'appendtext';
            page.redirect = true;
            if (window.AjaxDeleteWatchUserTalk) page.watchlist = 'watch';
            this.savePage(page, this.talk_summary, 'uploaderNotified');

            this.showProgress(this.i18n.notifyingUploader.replace('%USER%', user));

            this.uploadersToNotify++;
         }
      }
      if (this.uploadersToNotify === 0) this.nextTask();
   },

   uploaderNotified: function() {
      this.uploadersToNotify--;
      if (this.uploadersToNotify === 0) this.nextTask();
   },

   /**
    ** Compile a list of uploaders to notify.  Users who have only reverted the file to an
    ** earlier version will not be notified.
    ** DONE: notify creator of non-file pages
    **/
   findCreator: function() {
      var query;
      if (nsNumber === 6) {
         query = {
            action: 'query',
            prop: 'imageinfo|revisions|info',
            rvprop: 'content|timestamp|user',
            rvdir: 'newer',
            rvlimit: 1,
            intoken: 'edit',
            iiprop: 'user|sha1|comment',
            iilimit: 50,
            titles: this.pageName
         };

      } else {
         query = {
            action: 'query',
            prop: 'info|revisions',
            rvprop: 'user|timestamp',
            rvlimit: 1,
            rvdir: 'newer',
            intoken: 'edit',
            titles: this.pageName
         };
      }
      this.showProgress(this.i18n.preparingToEdit);
      this.queryAPI(query, 'findCreatorCB');
   },
   findCreatorCB: function(result) {
      this.uploaders = {};
      var pages = result.query.pages,
         pg = firstItem(pages),
         rv;

      // The edittoken only changes between sessions
      this.edittoken = pg.edittoken;
      
      if (!pg.revisions) {
         this.disableReport = true;
         throw new Error('The page you are attempting to add a tag to was deleted or moved. Unable to retrieve the content.');
      }
      rv = pg.revisions[0];

      //First handle non-file pages
      if (nsNumber !== 6 || !pg.imageinfo) {

         this.pageCreator = rv.user;
         this.starttimestamp = pg.starttimestamp;
         this.timestamp = rv.timestamp;

         if (undefined !== this.pageCreator) {
            this.uploaders[this.pageCreator] = true;
         }

      } else {
         var info = pg.imageinfo;

         var content = rv['*'];

         var seenHashes = {};
         for (var i = info.length - 1; i >= 0; i--) { // iterate in reverse order
            var iii = info[i];
            if (iii.sha1 && seenHashes[iii.sha1]) continue; // skip reverts
            seenHashes[iii.sha1] = true;

            this.uploaders[iii.user] = true;
         }
      }

      this.nextTask();
   },


   /**
    ** Pseudo-Modal JS windows.
    **/
   prompt: function(questions, title, width) {
      var o = this;
      var dlgButtons = {};
      dlgButtons[this.i18n.submitButtonLabel] = function() {
         $.each(questions, function(i, v) {
            var response = $('#AjaxQuestion' + i).val();
            if (v.type === 'checkbox') response = $('#AjaxQuestion' + i)[0].checked;
            if (v.cleanUp) {
               if (v.returnvalue === 'reason') response = AQD.cleanReason(response);
               if (v.returnvalue === 'destination') response = AQD.cleanFileName(response);
            }
            AQD[v.returnvalue] = response;
            if (v.returnvalue === 'topic' && AQD.tag) {
               AQD.tag = AQD.tag.replace('%TOPIC%', response);
            }
            if (v.returnvalue === 'reason' && AQD.tag) {
               if (AQD.talk_tag) AQD.talk_tag = AQD.talk_tag.replace('%PARAMETER%', response);
               AQD.img_summary = AQD.img_summary.replace('%PARAMETER%', response);
               AQD.img_summary = AQD.img_summary.replace('%PARAMETER-LINKED%', '[[:' + response + ']]');
            }
         });
         $(this).dialog('close');
         AQD.nextTask();
      };
      dlgButtons[this.i18n.cancelButtonLabel] = function() {
         $(this).dialog('close');
      };

      var $submitButton, $cancelButton;
      var $AjaxDeleteContainer = $('<div>', {
         id: 'AjaxDeleteContainer'
      });
      
      var _convertToTextarea = function(e) {
         var $el = $(this),
            $input = $el.data('toConvert'),
            $tarea = $('<textarea>', { id: $input.attr('id'), style: 'height:10em; width:98%; display:none;' });
            
         $el.off();
         $el.fadeOut();
         $input.parent().prepend(
            $tarea
               .data('v', $input.data('v')).data('parserResultNode', $input.data('parserResultNode'))
               .val($input.val()).keyup(_parseReason).on('keyup input', _validateInput));
         $tarea.slideDown();
         $input.remove();
      };
      
      var _parseReason = function(event) {
         var $el = $(this),
            $parserResultNode = $el.data('parserResultNode');

         if (!$parserResultNode) return;
         
         $parserResultNode.css('color', '#877');

         var _gotParsedText = function(r) {
            try {
               $parserResultNode.html(r);
               $parserResultNode.css('color', '#000');
            } catch (ex) {}
         };
         mw.loader.using(['ext.gadget.libAPI'], function() {
            mw.libs.commons.api.parse($el.val(), mw.config.get('wgUserLanguage'), pageName, _gotParsedText);
         });
      };
      
      var _validateInput = function(event) {
         var $el = $(this),
            v = $el.data('v');
            
         if (v.noEmpty) {
            if ($.trim($el.val()).length < (v.minLength || 10)) {
               $submitButton.button('option', 'disabled', true);
            } else {
               $submitButton.button('option', 'disabled', false);
            }
         }
         if (('TEXTAREA' !== $el.prop('nodeName')) && 
            (event.which === 13) && 
            (v.enterToSubmit !== false) && 
            !$submitButton.button('option', 'disabled')
         ) $submitButton.click();
      };

      $.each(questions, function(i, v) {
         v.type = (v.type || 'text');
         if (v.type === 'textarea') {
            $AjaxDeleteContainer.append('<label for="AjaxQuestion' + i + '">' + v.message + '</label>').append('<textarea rows=20 id="AjaxQuestion' + i + '">');
         } else {
            $AjaxDeleteContainer.append('<label for="AjaxQuestion' + i + '">' + v.message + '</label>').append('<input type="' + v.type + '" id="AjaxQuestion' + i + '" style="width:97%;">');
         }

         var curQuestion = $AjaxDeleteContainer.find('#AjaxQuestion' + i);

         if (v.parseReason) {
            var $parserResultNode = $('<div>', {
               id: 'AjaxQuestionParse' + i,
               html: '&nbsp;'
            });
            $AjaxDeleteContainer.append('<br><label for="AjaxQuestionParse' + i + '">' + o.i18n.previewLabel + '</label>').append($parserResultNode);

            curQuestion.data('parserResultNode', $parserResultNode).keyup(_parseReason);
         }
         if (v.type !== 'textarea') $AjaxDeleteContainer.append('<br><br>');
         if (v.appendNode) {
            $AjaxDeleteContainer.append(v.appendNode);
         }
         if ('number' === typeof v.byteLimit) {
            mw.loader.using('jquery.byteLimit', function() {
               curQuestion.byteLimit(v.byteLimit);
            });
         }

         curQuestion.data('v', v);
         curQuestion.on('keyup input', _validateInput);
         
         // SECURITY: prefill could contain evil jsCode. Never use it unescaped!
         // Use .val() or { value: prefill } or '<input value="' + mw.html.escape() + '" ...>
         curQuestion.val(v.prefill);
         if (v.type === 'checkbox') curQuestion.attr('checked', v.prefill).attr('style', 'margin-left: 5px');
      });
      
      if (mw.user.isAnon()) {
         AQD.renderNode($('<div>', { id: 'ajaxDeleteAnonwarning' }), 'MediaWiki:Anoneditwarning').appendTo($AjaxDeleteContainer);
      }

      var $dialog = $('<div></div>').append($AjaxDeleteContainer).dialog({
         width: (width || 600),
         modal: true,
         title: title,
         dialogClass: "wikiEditor-toolbar-dialog",
         close: function() {
            $(this).dialog("destroy");
            $(this).remove();
         },
         buttons: dlgButtons,
			open: function() {
            // Look out for http://bugs.jqueryui.com/ticket/6830 / jQuery UI 1.9
				var $buttons = $(this).parent().find('.ui-dialog-buttonpane button');
				$submitButton = $buttons.eq(0).specialButton('proceed');
				$cancelButton = $buttons.eq(1).specialButton('cancel');
			}
      });

      $.each(questions, function(i, v) {
         var curQuestion = $AjaxDeleteContainer.find('#AjaxQuestion' + i);
         curQuestion.keyup();
         if (v.type === 'text') {
            var $q = curQuestion.wrap('<div style="position:relative;">').parent();
            var $i = $.createIcon('ui-icon-arrow-4-diag').attr('title', 'Expand to textarea');
            $('<span>', { 'class': 'ajaxTextareaConverter' }).append($i).appendTo($q).data('toConvert', curQuestion).click(_convertToTextarea);
         }
      });

      $('#AjaxQuestion0').focus().select();
   },
   cleanReason: function(uncleanReason) {
      // trim whitespace
      uncleanReason = uncleanReason.replace(/^\s*(.+)\s*$/, '$1');
      // remove signature
      uncleanReason = uncleanReason.replace(/(?:\-\-|–|—)? ?~{3,5}$/, '').replace(/^~{3,5} ?/, '');
      return uncleanReason;
   },

   /**
    ** For display of progress messages.
    **/
   showProgress: function(message) {
      if (!message) {
         if (this.progressDialog) this.progressDialog.remove();
         this.progressDialog = 0;
         document.body.style.cursor = 'default';
         return;
      }
      if ($('#feedbackContainer').length) {
         $('#feedbackContainer').html(message);
      } else {
         document.body.style.cursor = 'wait';

         this.progressDialog = $('<div></div>').html('<div id="feedbackContainer">' + (message || this.i18n.preparingToEdit) + '</div>').dialog({
            width: 450,
            height: 90,
            minHeight: 90,
            modal: true,
            resizable: false,
            draggable: false,
            closeOnEscape: false,
            dialogClass: 'ajaxDeleteFeedback',
            open: function() {
               $(this).parent().find('.ui-dialog-titlebar').hide();
            },
            close: function() {
               $(this).dialog("destroy");
               $(this).remove();
            }
         });
      }

   },
   /**
    ** Submit an edited page.
    **/
   savePage: function(page, summary, callback) {
      mw.user.tokens.set('editToken', AQD.edittoken);
      
      var edit = {
         cb: function(r) {
            AQD.secureCall(callback, r);
         },
         // r-result, query, text
         errCb: function(t, r, q) {
            AQD.fail(t);
         },
         title: page.title,
         summary: summary,
         watchlist: (page.watchlist || 'preferences'),
         editType: page.editType,
         text: page.text
      };
      if (page.redirect) edit.redirect = true;
      
      mw.loader.using(['ext.gadget.libAPI'], function() {
         mw.libs.commons.api.editPage(edit);
      });
   },

   /**
    ** Does a MediaWiki API request and passes the result to the supplied callback (method name).
    ** Uses POST requests for everything for simplicity.
    **/
   queryAPI: function(params, callback) {
      mw.loader.using(['ext.gadget.libAPI'], function() {
         mw.libs.commons.api.query(params,
         {
            method: 'POST',
            cache: false,
            cb: function(r) {
               AQD.secureCall(callback, r);
            },
            // r-result, query, text
            errCb: function(t, r, q) {
               AQD.fail(t);
            }
         });
      });
   },
   
   /**
    ** Method to catch errors and report where they occurred
    **/
   secureCall: function (fn) {
      var o = AQD;
      try {
         o.currentTask = arguments[0];
         if ($.isFunction(fn)) {
            return fn.apply(o, Array.prototype.slice.call(arguments, 1)); // arguments is not of type array so we can't just write arguments.slice
         } else if ('string' === typeof fn) {
            return o[fn].apply(o, Array.prototype.slice.call(arguments, 1));
         } else {
            o.fail('This is not a function!');
         }
      } catch (ex) {
         o.fail(ex);
      }
   },

   /**
    ** Simple task queue.  addTask() adds a new task to the queue, nextTask() executes
    ** the next scheduled task.  Tasks are specified as method names to call.
    **/
   tasks: [],
   // list of pending tasks
   currentTask: '',
   // current task, for error reporting
   addTask: function(task) {
      this.tasks.push(task);
   },
   nextTask: function() {
      this.secureCall(this.tasks.shift());
   },
   retryTask: function() {
      this.secureCall(this.currentTask);
   },

   /**
    ** Once we're all done, reload the page.
    **/
   reloadPage: function() {
      this.showProgress();
      if (this.pageName && this.pageName.replace(/ /g, '_') !== pageName) return;
      var encTitle = (this.destination || pageName);
      encTitle = encodeURIComponent(encTitle.replace(/ /g, '_')).replace(/%2F/ig, '/').replace(/%3A/ig, ':');
      location.href = mw.config.get('wgServer') + mw.config.get('wgArticlePath').replace("$1", encTitle);
   },

   /**
    ** Error handler. Throws an alert at the user and give him 
    ** the possibility to retry or autoreport the error-message.
    **/
   fail: function(err) {
      var o = this;
      if (typeof err === 'object') {
         var stErr = err.message + ' \n\n ' + err.name;
         if (err.lineNumber) stErr += ' @line' + err.lineNumber;
         err = stErr;
      }

      var msg = this.i18n.taskFailure[this.currentTask] || this.i18n.genericFailure;

      //TODO: Needs cleanup
      var fix = '';
      if (this.img_summary === 'Nominating for deletion') {
         fix = (this.templateAdded ? this.i18n.completeRequestByHand : this.i18n.addTemplateByHand);
      }

      var dlgButtons = {};
      dlgButtons[this.i18n.retryButtonLabel] = function() {
         $(this).remove();
         o.retryTask();
      };
      if (-1 !== $.inArray(o.currentTask, ['movePage', 'deletePage', 'notifyUploaders']) && (/code 50\d/.test(err) || /missingtitle/.test(err))) {
         dlgButtons[this.i18n.ignoreButtonLabel] = function() {
            $(this).remove();
            o.nextTask();
         };
      }
      if (!this.disableReport) {
         dlgButtons[this.i18n.reportButtonLabel] = function() {
            $('#feedbackContainer').contents().remove();
            $('#feedbackContainer').append($('<img>', {
               src: '/w/skins/common/images/ajax-loader.gif'
            })).css('text-align', 'center');
            var randomId = Math.round(Math.random()*1099511627776);
            var toSend = '\n== Autoreport by AjaxQuickDelete ' + randomId + ' ==\n' + err + '\n++++\n:Task: ' + o.currentTask + '\n:NextTask: ' + o.tasks[0] + '\n:LastTask: ' + o.tasks[o.tasks.length - 1] + 
               '\n:Page: {{Page|1=' + (o.pageName || pageName) + '}}\n:Skin: ' + mw.user.options.get('skin') +
               '\n:[{{fullurl:Special:Contributions|target={{subst:urlencode:{{subst:REVISIONUSER}}}}&offset={{subst:REVISIONTIMESTAMP}}}} Contribs] ' + 
               '[{{fullurl:Special:Log|user={{subst:urlencode:{{subst:REVISIONUSER}}}}&offset={{subst:REVISIONTIMESTAMP}}}} Log] ' +
               'before error';
            $.post(o.apiURL, {
               'action': 'edit',
               'format': 'json',
               'title': 'MediaWiki talk:Gadget-AjaxQuickDelete.js/auto-errors',
               'summary': '[[#Autoreport by AjaxQuickDelete ' + randomId + '|Reporting an AjaxQuickDelete error.]] Random ID=' + randomId,
               'appendtext': toSend,
               'token': (o.edittoken || mw.user.tokens.get('editToken'))
            }, function() {
               o.reloadPage();
            });
         };
      }
      dlgButtons[this.i18n.abortButtonLabel] = function() {
         $(this).remove();
      };
      
      this.disableReport = false;
      this.showProgress();
      this.progressDialog = $('<div>').append($('<div>', {
         id: 'feedbackContainer',
         html: (msg + ' ' + fix + '<br>' + this.i18n.errorDetails + '<br>' + mw.html.escape(err) + '<br>' + (this.tag ? (this.i18n.tagWas + this.tag) : '') + '<br><a href="' + mw.util.getUrl('MediaWiki talk:AjaxQuickDelete.js') + '" >' + this.i18n.errorReport + '</a>')
      })).dialog({
         width: 550,
         modal: true,
         closeOnEscape: false,
         title: this.i18n.errorDlgTitle,
         dialogClass: "ajaxDeleteError",
         buttons: dlgButtons,
         close: function() {
            $(this).dialog("destroy");
            $(this).remove();
         }
      });
   },
   
   /**
    ** Very simple date formatter.  Replaces the substrings "YYYY", "MM" and "DD" in a
    ** given string with the UTC year, month and day numbers respectively.
    ** Also replaces "MON" with the English full month name and "DAY" with the unpadded day.
    **/
   formatDate: function(fmt, date) {
      return mw.libs.commons.formatDate(fmt, date, (mw.libs.commons.api && mw.libs.commons.api.getCurrentDate() || new Date()));
   },

   // Constants
   // DR subpage prefix
   requestPagePrefix: "Wikipedia:Pagine da cancellare/",
   // user talk page prefix
   userTalkPrefix: mw.config.get('wgFormattedNamespaces')[3] + ":",
   // MediaWiki API script URL
   apiURL: mw.util.wikiScript('api'),


   // Translatable strings
   // by [[User:Raoli]]
   i18n: {
      toolboxLinkDelete: "Proponi per la cancellazione",
      toolboxLinkDiscuss: "Metti categoria in discussione",
 
      // GUI reason prompt form
      reasonForDeletion: "Perché il file va cancellato?",
      specifyGeneralTopic: "Specificare un argomento generale!",
      reasonForDiscussion: "Perché la categoria necessita di discussione?",
      moreInformation: "Maggiori informazioni",
      loading: "Sto caricando...",
 
      keptAfterDR: "Questa pagina è stata mantenuta in seguito alla richiesta di cancellazione. Contatta l\'amministratore che l\'ha mantenuta prima di rimetterla in la cancellazione.",
      hasTalkpage: "Esiste la pagina di discussione. Considera di leggertela o di aggiungervi commenti.",
      mentionedInDR: "Considera di leggere il dibattito per la cancellazione –%PAGE%– che collega a questa pagina.",
      mentionedInForum: "In %PAGE%, questa pagina è parte di una discussione.",
 
      // Labels
      previewLabel: "Anteprima:",
      submitButtonLabel: "Convalida",
      cancelButtonLabel: "Annulla",
      abortButtonLabel: "Interrompi",
      reportButtonLabel: "Segnala automaticamente",
      retryButtonLabel: "Riprova",
      ignoreButtonLabel: "Ignora e continua",
 
      // GUI progress messages
      preparingToEdit: "Sto preparando le modifiche alle pagine... ",
      creatingNomination: "Sto creando la pagina di nomina... ",
      listingNomination: "Sto aggiungendo la pagina all\'elenco giornaliero... ",
      addingAnyTemplate: "Sto aggiungendo il template alla " + canonicalNs.toLowerCase() + " pagina... ",
      notifyingUploader: "Sto notificando a %USER%... ",
 
      // Extended version
      toolboxLinkSource: "Nessuna fonte",
      toolboxLinkLicense: "Nessuna licenza",
      toolboxLinkPermission: "Nessuna autorizzazione",
      toolboxLinkCopyvio: "Segnala violazione del copyright",
      reasonForCopyvio: "Perché il file è in violazione del copyright?",
 
      // Errors
      errorDlgTitle: "Errore",
      genericFailure: "Si è verificato un errore durante il tentativo di eseguire l\'azione richiesta.",
      taskFailure: {
         listUploaders: "Si è verificato un errore durante la determinazione del " + (nsNumber === 6 ? " caricante(i) del file" : "creatore di questa pagina") + ".",
         loadPages: "Si è verificato un errore durante la preparazione alla nomina di questa " + canonicalNs.toLowerCase() + " per la cancellazione.",
         prependDeletionTemplate: "Si è verificato un errore durante l\'aggiunta del template {{Cancella}} a questa " + canonicalNs.toLowerCase() + ".",
         createRequestSubpage: "Si è verificato un errore durante la creazione della pagina della richiesta.",
         listRequestSubpage: "Si è verificato un errore durante l\'aggiunta della richiesta di cancellazione al registro giornaliero.",
         notifyUploaders: "Si è verificato un errore durante la notifica al " + (nsNumber === 6 ? " caricante(i) del file" : "creatore di questa pagina") + "."
      },
      addTemplateByHand: "Per mettere in cancellazione questa " + canonicalNs.toLowerCase() + " modifica la pagina aggiungendo il template {{Cancella}} e segui le istruzioni mostrate.",
      completeRequestByHand: "Segui le istruzioni nell\'avviso di cancellazione per completare la richiesta.",
      errorDetails: "Una descrizione dettagliata dell\'errore è mostrata qui sotto:",
      errorReport: "Segnala manualmente l\'errore qui o clicca su <code>Segnala automaticamente</code> per inviare una segnalazione automatica dell\'errore.",
      tagWas: "L\'etichetta da inserire in questa pagina è stata "
   }
};

AQD.install();

}(jQuery, mediaWiki));
// </nowiki>