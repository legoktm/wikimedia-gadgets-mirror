/** MediaWiki:FR2015/Resources/FullScreenLocalize-SymbolVariant.js
  * Do various things to localize full-screen banners
  */

$(document).ready(function() {

    /* Allow overriding country */
    if( mw.util.getParamValue("country") ) {
        Geo.country = mw.util.getParamValue("country");
    }

    var currency = getCurrency(Geo.country);
    var language = mw.config.get('wgUserLanguage');

    /* Links (in smallprint) */
    $('a.localize, .frbanner-footer a').each(function() {
        
        // Add parameters for LandingCheck
        var uri = new mw.Uri( $(this).attr("href") );
        uri.extend({
            country:      Geo.country,
            language:     language,
            uselang:      language,
            utm_medium:   'sitenotice',
            utm_campaign: mw.centralNotice.getDataProperty('campaign'),
            utm_source:   mw.centralNotice.getDataProperty('banner')
        });
        $(this).attr( "href", uri.toString() );

        // Make links open in new tab
        $(this).attr("target", "_blank");
    });

    /* Legal text variants */
    if (Geo.country === 'US') {
        $('.informationsharing-US').show();
        $('.informationsharing-NL').hide();
        $('.informationsharing-nonUS').hide();
    }
    if (Geo.country === 'NL') {
        $('.informationsharing-US').hide();
        $('.informationsharing-NL').show();
        $('.informationsharing-nonUS').hide();
    }

    /* Main amount buttons */
    $('.frbanner-amounts input[id^="field-amount_total"]').each(function(index) {
        var $input = $(this);
        var $label = $('label[for="' + $input.attr('id') + '"]');
        var amount = convertAsk( $input.val(), currency, Geo.country );
        $input.val( amount );
        $label.html( amount );
        // $label.html( currencyLocalize( currency, amount, language) );
    });

    // /* "Other" box. Don't know how to do it for the symbol coming after the number though */
    // $('.frbanner-amounts #amount_other fieldset label').html(
    //     currencyLocalize( currency, '', language )
    // );

    /* Replace class "frbanner-currencysymbol" with currency symbol */
    $('.frbanner-currencysymbol').html(
        currencyLocalize( currency, '', language ).replace(' ', '')
    );

    /* Replace class "frbanner-currencycode" with currency ISO code */
    $('.frbanner-currencycode').html(currency);

    // Find instances of %AVERAGE% in banner text, and replace with correct average for currency & country
    var average   = fundraisingAmounts.pick(fundraisingAmounts.averages, currency, Geo.country);
    var avgString = currencyLocalize(currency, average, language).replace(/\.$/, ''); // strip any period from end for use in running text

    $('#{{{banner}}}').each(function(index){
        var newHtml = $(this).html().replace(/%AVERAGE%/g, '<span style="white-space: nowrap;">' + avgString + '</span>');
        $(this).html(newHtml);
    });

    // Find instances of %MINIMUM% in banner text, and replace with correct minimum for currency & country
    var ifEveryone = fundraisingAmounts.pick(fundraisingAmounts.ifEveryone, currency, Geo.country)[0];
    var ifString   = currencyLocalize(currency, ifEveryone, language).replace(/\.$/, ''); // strip any period from end for use in running text

    $('#{{{banner}}}').each(function(index){
        var newHtml = $(this).html().replace(/%MINIMUM%/g, '<span style="white-space: nowrap;">' + ifString + '</span>');
        $(this).html(newHtml);
    });

});

/* end of MediaWiki:FR2015/Resources/FullScreenLocalize-SymbolVariant.js */