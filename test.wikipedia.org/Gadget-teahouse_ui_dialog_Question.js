
/*!
 * Teahouse user interface QuestionDialog class.
 */

/**
 * Dialog for submitting a question to the Teahouse board.
 *
 * @class
 * @extends OO.ui.ProcessDialog
 *
 * @constructor
 * @param {Object} [config] Configuration options
 */

mw.util.teahouse.registerType( 'mw.util.teahouse.ui.dialog.Question' );

mw.util.teahouse.ui.dialog.Question = function ThUiQuestionDialog( config, teahousecfg ) {
	this._config = teahousecfg;
	mw.util.teahouse.ui.dialog.Question.super.call( this, config );
};
OO.inheritClass( mw.util.teahouse.ui.dialog.Question, OO.ui.ProcessDialog );

mw.util.teahouse.ui.dialog.Question.static.title = mw.message('teahouse-dialog-title').plain();

mw.util.teahouse.ui.dialog.Question.static.actions = [
	{
		action: 'save',
		label: mw.message('teahouse-dialog-btn-ok').plain(),
		flags: 'primary',
		disabled: true
	},
	{
		label: mw.message('teahouse-dialog-btn-cancel').plain(),
		flags: 'safe'
	}
];

mw.util.teahouse.ui.dialog.Question.prototype.initialize = function () {
	mw.util.teahouse.ui.dialog.Question.super.prototype.initialize.apply( this, arguments );

	this.content = new OO.ui.PanelLayout( {
		$: this.$,
		padded: true,
		scrollable: true,
		expanded: false
	} );

	this.tiQuestion = new OO.ui.TextInputWidget({
		classes: [ 'teahouse-inputWidget', 'teahouse-tiQuestion' ]
	});
	this.tiQuestion.on( 'change', this.onTiQuestionChange, [], this );
	var flQuestion = new OO.ui.FieldLayout(
		this.tiQuestion,
		{
			label: mw.message('teahouse-dialog-label-summary').plain(),
			align: 'top'
		}
	);

	this.tiDesc = new OO.ui.TextInputWidget( {
		classes: [ 'teahouse-inputWidget', 'teahouse-tiDesc' ],
		multiline: true
	});
	this.tiDesc.on( 'change', this.onTiDescChange, [], this );

	var flDesc = new OO.ui.FieldLayout(
		this.tiDesc,
		{
			label: mw.message('teahouse-dialog-label-text').plain(),
			align: 'top'
		}
	);

	this.$footerTable = $('<table>').addClass('teahouse-footer-table');

	this.$urlHint = this.addFooterRow( mw.message('teahouse-dialog-url-hint-html').plain(), 'info ');
	this.$urlHint.hide();
	this.$licence = this.addFooterRow( mw.message('teahouse-dialog-licence-html').plain(), 'info ');
	this.$anonIPHint = this.addFooterRow( mw.message('teahouse-dialog-anon-ip-hint-html').plain(), 'info ');

	this.$similarQuestions = $('<div>');

	var anchor =  mw.html.element( 'a', {
		href: mw.util.getUrl( this._config.basePage ),
		title: this._config.basePage,
		target: '_blank'
	}, this._config.basePage );

	this.content.$element.append(
		$('<p>').append( mw.message('teahouse-dialog-description-top', anchor).text() )
	);

	this.content.$element.append( flQuestion.$element );
	this.content.$element.append( this.$similarQuestions );
	this.content.$element.append( flDesc.$element );
	this.content.$element.append( this.$footerTable );

	this.$body.addClass('teahouse-question-dialog-body');
	this.$body.append( this.content.$element );
};

mw.util.teahouse.ui.dialog.Question.prototype.onTiQuestionChange = function( value ) {
	//If there is no question entered or it is way too short to be a question
	//we disable the "save" action
	this.getActions().setAbilities( {
		save: value.length > 3
	});

	if(value !== '' && value.length > 3) {
		if( !this.tiQuestion.isPending() ) {
			this.tiQuestion.pushPending();
		}

		var me = this;
		mw.util.teahouse.board.getSimilarQuestions( value )
			.done( function( items ){
				var $list = $('<ul>').addClass('teahouse-similar-questions-list');
				for( var title in items ) {

					var anchor =  mw.html.element( 'a', {
						href: mw.util.getUrl( title ),
						title: title,
						target: '_blank'
					}, items[title] );

					$list.append( '<li>'+anchor+'</li>' );
				}

				if( $list.children().length > 0 ) {
					var $label = $('<span>').addClass( 'oo-ui-labelElement-label' )
						.append( mw.message('teahouse-dialog-label-similar').plain() );

					me.$similarQuestions.empty();
					me.$similarQuestions.append( $label );
					me.$similarQuestions.append( $list );
				}

				me.tiQuestion.popPending();
				me.getManager().updateWindowSize( me );
			});
			return;
	}

	this.$similarQuestions.empty();
	this.getManager().updateWindowSize( this );
};

mw.util.teahouse.ui.dialog.Question.prototype.onTiDescChange = function( value ) {
	//CREDITS: http://stackoverflow.com/questions/8188645/javascript-regex-to-match-a-url-in-a-field-of-text
	var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/;

	if( value.match( urlPattern ) ) {
		this.$urlHint.show();
	}
	else {
		this.$urlHint.hide();
	}
	this.getManager().updateWindowSize( this );
};

mw.util.teahouse.ui.dialog.Question.prototype.getSetupProcess = function ( openingData ) {
	//Hide the IP saving hint if user is anon.
	if( mw.user.isAnon() === false ) {
		this.$anonIPHint.hide();
	}
	return mw.util.teahouse.ui.dialog.Question.super.prototype.getSetupProcess.call( this, openingData );
};

mw.util.teahouse.ui.dialog.Question.prototype.getTeardownProcess = function ( closingData ) {
	//Reset the fields on close
	this.tiQuestion.setValue( '' );
	this.tiDesc.setValue( '' );

	return mw.util.teahouse.ui.dialog.Question.super.prototype.getTeardownProcess.call( this, closingData );
};

mw.util.teahouse.ui.dialog.Question.prototype.getActionProcess = function ( action ) {
	var me = this;

	var question = {
		title: this.tiQuestion.getValue(),
		text: this.tiDesc.getValue()
	};

	if ( action === 'save' ) {
		return new OO.ui.Process( function() {
			//This is a quick implementation for length restriction
			//TODO: Check when user enters text and give a nice hint. Also
			//disable "publish" button in this case.
			if( question.text.length >= me._config.questionTextMaxLength ) {
				var error = new OO.ui.Error(
					mw.message(
						'teahouse-dialog-max-length-hint-html',
						me._config.questionTextMaxLength
					).plain()
				);

				return $.Deferred().reject([error]).promise();
			}

			return mw.util.teahouse.board.publishQuestion( question )
				.done(function( editdata ) {
					//TODO: add a parameter whether to reset fields or not
					//This parameter will be passed to 'getTeardownProcess'
					me.close( { action: action } )
						.done(function( data ){

							var anchor = mw.html.element('a', {
								href: mw.util.getUrl( editdata.questionpage.title ),
								title: editdata.questionpage.title,
								target: '_blank'
							}, editdata.questionpage.title );

							var msg = mw.message('teahouse-dialog-msg-text-save', anchor ).text();

							mw.util.teahouse.dialog.openMessageDialog({
								title: mw.message('teahouse-dialog-msg-title-save').plain(),
								message: msg
							}, function ( opening ) {
								opening.then( function ( opened ) {
									opened.then( function ( data ) {
										if( data.action === 'yes' ) {
											window.location.href = mw.util.getUrl( me._config.basePage );
										}
									});
								});
							});
						});
				});
		});
	}

	return mw.util.teahouse.ui.dialog.Question.super.prototype.getActionProcess.call( this, action );
};

mw.util.teahouse.ui.dialog.Question.prototype.getBodyHeight = function () {
	return this.content.$element.outerHeight( true );
};
/**
 * Adds a row to the footer area of the dialog and returns it as jQuery object
 * @param string html
 * @param string icon (optional) an icon name
 * @returns {jQuery} The &lt;row&gt; element
 */
mw.util.teahouse.ui.dialog.Question.prototype.addFooterRow = function( html, icon ){
	var $row = $('<tr>');
	this.$footerTable.append( $row );
	var $cell = $('<td>').addClass('teahouse-text-cell').append( html );
	$row.append( $cell );

	if( icon ) {
		var icon = new OO.ui.IconWidget({
			icon: icon
		});
		$row.prepend( $('<td>').append( icon.$element ) );
	}
	else {
		$cell.attr( 'rowspan', 2 );
	}

	return $row;
};