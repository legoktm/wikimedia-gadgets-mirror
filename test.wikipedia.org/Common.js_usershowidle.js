/* User idle status checker, version [0.2.5a] - conceptual
Originally from: //test.wikipedia.org/wiki/MediaWiki:Common.js/usershowidle.js

Method:
* Checks for object with id="user-status-activator" to activate.
** Builds array of images in spans with class="user-status"
** Iterates over array and makes API call for each USERNAME's last contrib (and optionally log entry) timestamp.
*** Each API call that returns indicates the idle time of that user as a selected time-range image and as a mouseover title.

Notes:
* Gives callback functions numerical IDs, so squid/user caching only works on individual lists, not per username.
* Limited to 10 indicators per page.
* Only works in User and User_talk namespaces.
* Should work in other content languages... not tested much though.
* Built to default to a link to the user's Special:Contributions (hard-coded so), so non-js fallback is implicit.

Options:
* showidleDefaultTheme: default image theme if not user selected.
* showidleImages : an array of arrays of images to be shown based on when the user last edited more than x-seconds ago. Descending order.
* usI18n: Translations.

User Instructions and Options:
* A id="user-status-activator" must appear on the page (not necessarily around the spans).
* A class="user-status" is of course required in a parent-span to activate an image.
* You can add " us-nolog" to the class to prevent a user's log entries being checked.
* You can change theme (from among the prelisted defaults) using "us-theme-THEMENAME".
* Image parameters must have a link=Special:Contributions/USERNAME and NO frame or thumb 
** Example: <span class="user-status us-nolog us-theme-pog">[[Image:Question_Mark_in_a_Circle.png|20px|link=Special:Contributions/Splarka]]</span>

ToDo:
* Test with all possibilities, test in other languages.
* Build more themes?
*/


var usI18n = {
  'error'    :'Error: ',
  'noedits'  :'User has no edits.',
  'sinceedit':'User last edited ',
  'sincelog' :'User last performed an action ',
  'sinceago' :' ago.'
}

var showidleDefaultTheme = 'pog';
/* If user has been idle for more than 'gt' seconds, show this image for their status.
   You can have as many images as you like here as long as they're in descending order (and should all be the same resolution).
   Images _can_ be different resolutions/shapes, but will get ugly browser scaling.
*/
var showidleImages = {
  'pog': [
    {'gt': '2592000', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/4/46/Black_pog.svg/20px-Black_pog.svg.png'},
    {'gt':  '604800', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Purple_pog.svg/20px-Purple_pog.svg.png'},
    {'gt':   '86400', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/20px-Red_pog.svg.png'},
    {'gt':   '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Orange_pog.svg/20px-Orange_pog.svg.png'},
    {'gt':    '3600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Blue_pog.svg/20px-Blue_pog.svg.png'},
    {'gt':       '0', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Green_pog.svg/20px-Green_pog.svg.png'},
    {'gt':      '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/1/1a/White_pog.svg/20px-White_pog.svg.png'}
  ],
  'bigpog': [
    {'gt': '2592000', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/4/46/Black_pog.svg/40px-Black_pog.svg.png'},
    {'gt':  '604800', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Purple_pog.svg/40px-Purple_pog.svg.png'},
    {'gt':   '86400', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/40px-Red_pog.svg.png'},
    {'gt':   '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Orange_pog.svg/40px-Orange_pog.svg.png'},
    {'gt':    '3600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/d/d7/Blue_pog.svg/40px-Blue_pog.svg.png'},
    {'gt':       '0', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Green_pog.svg/40px-Green_pog.svg.png'},
    {'gt':      '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/1/1a/White_pog.svg/40px-White_pog.svg.png'}
  ],
  'kbounce': [
    {'gt': '2592000', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Nuvola_apps_kbouncefx.png/40px-Nuvola_apps_kbouncefx.png'},
    {'gt':  '604800', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Nuvola_apps_kbounce.png/40px-Nuvola_apps_kbounce.png'},
    {'gt':   '86400', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Nuvola_apps_kbouncey.png/40px-Nuvola_apps_kbouncey.png'},
    {'gt':   '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/6/63/Nuvola_apps_kbounceb.png/40px-Nuvola_apps_kbounceb.png'},
    {'gt':    '3600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Nuvola_apps_kbouncec.png/40px-Nuvola_apps_kbouncec.png'},
    {'gt':       '0', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/7/79/Nuvola_apps_kbouncev.png/40px-Nuvola_apps_kbouncev.png'},
    {'gt':      '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Nuvola_apps_kbouncegr.png/40px-Nuvola_apps_kbouncegr.png'}
  ],
  'planet': [
    {'gt':'31557600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/0/06/Neptune.jpg/40px-Neptune.jpg'},
    {'gt': '2592000', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/6/61/Saturn.jpg/40px-Saturn.jpg'},
    {'gt':  '604800', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Uranus_Voyager_2.jpg/40px-Uranus_Voyager_2.jpg'},
    {'gt':   '86400', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/e/e2/Jupiter.jpg/40px-Jupiter.jpg'},
    {'gt':   '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/b/b8/2005-1103mars-full.jpg/40px-2005-1103mars-full.jpg'},
    {'gt':    '3600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Full_Moon_Luc_Viatour.jpg/40px-Full_Moon_Luc_Viatour.jpg'},
    {'gt':       '0', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/9/97/The_Earth_seen_from_Apollo_17.jpg/40px-The_Earth_seen_from_Apollo_17.jpg'},
    {'gt':      '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/5/5f/HubbleDeepField.800px.jpg/40px-HubbleDeepField.800px.jpg'}
  ],
  'trafficdot': [
    {'gt': '604800', 'img':'//upload.wikimedia.org/wikipedia/commons/0/04/Trafikklys_raudt.png'},
    {'gt':  '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/4/4c/Trafikklys_gult.png'},
    {'gt':      '0', 'img':'//upload.wikimedia.org/wikipedia/commons/d/da/Trafikklys_gr%C3%B8nt.png'},
    {'gt':     '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/9/99/Black_square.jpg/42px-Black_square.jpg'}
  ],
  'trafficbar': [
    {'gt':  '86400', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/4/45/Traffic_lights_red.svg/20px-Traffic_lights_red.svg.png'},
    {'gt':  '21600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Traffic_lights_red-yellow.svg/20px-Traffic_lights_red-yellow.svg.png'},
    {'gt':   '3600', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/1/19/Traffic_lights_yellow.svg/20px-Traffic_lights_yellow.svg.png'},
    {'gt':      '0', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/4/4f/Traffic_lights_green.svg/20px-Traffic_lights_green.svg.png'},
    {'gt':     '-1', 'img':'//upload.wikimedia.org/wikipedia/commons/thumb/1/1c/Ampel.svg/20px-Ampel.svg.png'}
  ]
}

function userShowidle() {
  var div = document.getElementById('user-status-activator');
  if(!div) return
  var docobj = document.getElementById('bodyContent') || document.getElementById('content') || document.getElementById('mw-content') || document.body
  var span = getElementsByClassName(docobj,'span','user-status');
  if(span.length == 0) return

  for(var i=0;i<span.length;i++) {
    if(i >= 10) return;  //sane limit?
    var a = span[i].getElementsByTagName('a');
    if(a.length == 0) continue
    var img = a[0].getElementsByTagName('img');
    if(img.length == 0) continue
    img[0].setAttribute('id','user-status-image-' + i);
    var thc = span[i].className.match(/us-theme-[a-zA-Z0-9-_]*/);
    if(thc) img[0].setAttribute('class',thc);
    var user = a[0].getAttribute('href',2);
    user = user.split('/');
    user = user[user.length-1];
    user = user.replace(/_/,'%20'); // &ucuser= accepts underscores, &leuser= doesn't

    if(span[i].className.indexOf('us-nolog') == -1) {
      var url = wgServer + wgScriptPath + '/api.php?action=query&maxage=900&smaxage=900&format=json&callback=usiCB&list=usercontribs|logevents&uclimit=1&ucprop=timestamp&ucuser=' + user + '&lelimit=1&leprop=timestamp|user&leuser=' + user + '&requestid=' + i;
    } else {
      var url = wgServer + wgScriptPath + '/api.php?action=query&maxage=900&smaxage=900&format=json&callback=usiCB&list=usercontribs&uclimit=1&ucprop=timestamp&ucuser=' + user + '&requestid=' + i;
    }
    importScriptURI(url);
  }
}
if(wgNamespaceNumber == 2 || wgNamespaceNumber == 3) $(userShowidle)

function usiCB(obj) {
  var docobj = document.getElementById('bodyContent') || document.getElementById('content') || document.getElementById('mw-content') || document.body
  var num = obj['requestid'];
  var img = document.getElementById('user-status-image-' + num);
  if(!obj['query'] || !obj['query']['usercontribs']) {
    var error = (obj['error'] && obj['error']['info']) ? obj['error']['info'] : 'Unknown API Error'
    img.setAttribute('title',usI18n.error + error);
    return;
  }
  var uc = obj['query']['usercontribs'];
  var ts = (uc.length > 0) ? uc[0]['timestamp'] : 0
  var sincemsg = usI18n.sinceedit;
  if(obj['query']['logevents']) {
    //optional log events checking
    var le = obj['query']['logevents'];
    var lts = (le.length > 0) ? le[0]['timestamp'] : 0
    if(lts > ts) {
      ts = lts;
      sincemsg = usI18n.sincelog;
    }
  }
  var now = new Date();
  var tsd = new Date();
  tsd.setISO8601(ts);
  var timesince = Math.floor((now - tsd)/1000);
  if(timesince == '') timesince = -1;

  var theme = showidleDefaultTheme;
  var thc = img.className.match(/us-theme-[a-zA-Z0-9_]*/);
  if(thc) { 
    thc = thc[0].split('-',3);
    thc = thc[thc.length-1];
    if(showidleImages[thc]) theme = thc
  }

  for(var i=0;i<showidleImages[theme].length;i++) {
    if(timesince >= parseInt(showidleImages[theme][i]['gt'])) {
      img.setAttribute('src',showidleImages[theme][i]['img']);
      img.setAttribute('class','user-status-image');
      if(timesince > -1) {
        img.setAttribute('title',sincemsg + duration(timesince) + usI18n.sinceago);
      } else {
        img.setAttribute('title',usI18n.noedits);
      }
      break;
    }
  }
}

//ISO 8601 date module by Paul Sowden, licensed under AFL.
Date.prototype.setISO8601 = function(string) {
  if(!string) return
  var regexp = '([0-9]{4})(-([0-9]{2})(-([0-9]{2})(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\.([0-9]+))?)?(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?';
  var d = string.match(new RegExp(regexp));
  if(d.length < 1) return
  var offset = 0;
  var date = new Date(d[1], 0, 1);
  if(d[3]) date.setMonth(d[3] - 1)
  if(d[5]) date.setDate(d[5])
  if(d[7]) date.setHours(d[7])
  if(d[8]) date.setMinutes(d[8])
  if(d[10]) date.setSeconds(d[10])
  if(d[12]) date.setMilliseconds(Number('0.' + d[12]) * 1000)
  if(d[14]) {
    offset = (Number(d[16]) * 60) + Number(d[17]);
    offset *= ((d[15] == '-') ? 1 : -1);
  }
  offset -= date.getTimezoneOffset();
  time = (Number(date) + (offset * 60 * 1000));
  this.setTime(Number(time));
}

function duration(input,depth) {
  var num = input;
  var out = '';
  var s = num % 60; num = Math.floor(num / 60);
  var m = num % 60; num = Math.floor(num / 60);
  var h = num % 24; num = Math.floor(num / 24);
  var d = num % 7;  num = Math.floor(num / 7);
  var w = num % 52; num = Math.floor(num / 52);
  var y = num
  if(y > 0) out += y + 'yrs '
  if(y + w > 0) out += w + 'wks '
  if(y + w + d > 0) out += d + 'days '
  if(y + w + d + h > 0) out += h + 'hrs '
  if(y + w + d + h + m > 0) out += m + 'mins '
  out += s + 'secs';  
  if(depth && depth < out.split(' ').length) {
    out = out.split(' ').slice(0,depth).join(' ');
  }
  return out;
}