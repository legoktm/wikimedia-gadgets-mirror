/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Teahouse "Ask a question" feature, used by the Wikimedia Foundation's Teahouse project, see [[Wikipedia:Teahouse/Questions]]
 */
//<nowiki>
(function($,mw) {
	mw.teahouse = {
		'questionform' : ' \
		<div class="wp-teahouse-question-form"> \
			<p>Write out your question in the textbox below.</p>\
			<p><label for="wp-th-question-title">Short summary of your question: </label> \
			<input id="wp-th-question-title" type="text" size="90" /></p>\
			<textarea rows="10" cols="20" id="wp-th-question-text"></textarea> \
			<p>\
			<span class="wp-th-sign-hint">On Wikipedia, you should sign all of your non-article posts by ending them with four tildes (~~~~)</span> \
			<a href="#" id="wp-th-question-ask">Ask my question</a> \
			</p> \
			<p>\
			<span class="wp-th-sign-req"><small>(Note: this question submission form will not work without them.)</small></span>\
			</p> \
		</div> \
		',

		addQuestion : function( title, text ) {
			var wikitext = '==' + title + "==\n" + text + "\n";

			$('.wp-teahouse-question-form').hide();

			$('.wp-teahouse-ask')
				.find('.selflink')
				.empty()
				.addClass('mw-ajax-loader');

			var api = new mw.Api();

			api.get( {
				'action' : 'query',
				'titles' : 'Wikipedia:Teahouse/Questions',
				'prop'   : 'revisions|info',
				'intoken' : 'edit',
				'rvprop' : 'content',
				'indexpageids' : 1
			}).done(
				function(result) {
					result = result.query;
					var page = result.pages[result.pageids[0]];
					var oldText = page.revisions[0]['*'];
					var newText = oldText.replace( /^==/m, wikitext+"==" );

					api.post(
						{
							'action' : 'edit',
							'title' : 'Wikipedia:Teahouse/Questions',
							'text' : newText,
							'summary' : '/'+'* '+title+' *'+'/ new section',
							'token' : page.edittoken
						}).done(
							function() {location.reload();}
						);
				});
		}
	};

	$(function() {
		mw.loader.using( ['jquery.ui.button', 'mediawiki.api'], function() {
			if ( !$('.wp-teahouse-ask').length ) {
				return;
			}

			var $form = $(mw.teahouse.questionform);
			$('.wp-teahouse-ask').after($form);

			// Prevent flash
			$form.css( 'left', '-10000px' );

			// Set up position
			setTimeout( function() {
				var $trigger = $('.wp-teahouse-ask');
				var pos = $trigger.position();
				var hCenter = ( $trigger.parent().width() / 2 );
				$form.css( 'top', pos.top + $trigger.height() + 'px' );
				$form.css( 'left', (hCenter - ($form.width()) / 2) + 'px' );
				$form.hide();
			}, 0);

			$form.find('#wp-th-question-ask')
				.button({
					disabled : true
				})
				.click( function(e) {
					e.preventDefault();

					var title = $form.find('#wp-th-question-title').val();
					var text = $form.find('#wp-th-question-text').val();

					if ( title && /~~~~\s*$/.test(text) ) {
						mw.teahouse.addQuestion( title, text );
					}
				})
				.end()
				.find('#wp-th-question-text')
					.keypress( function(e) {
						var $textbox = $(this);
						setTimeout( function() {
							if ( (/~~~~\s*$/).test($textbox.val()) ) {
								$form.find('#wp-th-question-ask')
									.button( 'option','disabled', false );
							} else {
								$form.find('#wp-th-question-ask')
									.button( 'option','disabled', true );
							}
						}, 0 );
					} );

			$('.wp-teahouse-ask').click(function(e) {
				$form.toggle('fast');
				e.cancelBubble = true; // for IE
				if (e.stopPropagation) {
					e.stopPropagation();
					e.preventDefault();
				}
			});

			$(document).click( function(e) {
				var $target = $(e.target);
				if ( ! $target.is('.wp-teahouse-question-form *') &&
					! $target.is('.wp-teahouse-ask *')
				) {
					$('.wp-teahouse-question-form').fadeOut();
				}
			} );

			$(document).keydown( function(e) {
				if ( e.keyCode == 27 ) {// ESC
					$('.wp-teahouse-question-form').fadeOut();
				}
			});
		} );
	} );
} )(jQuery,mediaWiki);

if ( mw.config.get("wgPageName") == 'Wikipedia:Teahouse/Questions' ) {
(function($,mw) {
 
	$(function() {
		mw.loader.using( ['jquery.ui.button', 'mediawiki.api'], function() {
 
		function addResponse( section, headline, text ) {
			var wikitext = '\n\n:' + text;
 
			$('.wp-teahouse-respond-form').hide();
 
			$('.wp-teahouse-respond')
				.find('.selflink')
				.empty()
				.addClass('mw-ajax-loader');
 
			var api = new mw.Api();
 
			api.get( {
				'action' : 'query',
				'titles' : 'Wikipedia:Teahouse/Questions',
				'prop'   : 'revisions|info',
				'intoken' : 'edit',
				'rvprop' : 'content',
				'indexpageids' : 1
			}).done(
				function(result) {
					result = result.query;
					var page = result.pages[result.pageids[0]];
 
					api.post(
						{
							'action' : 'edit',
							'section' : section,
							'title' : 'Wikipedia:Teahouse/Questions',
							'appendtext' : wikitext,
							'summary' : '/* ' + headline + ' */' + ' response',
							'token' : page.edittoken
						}).done (
							function() {location.reload();}
						);
				});
		}
 
			var headers = $('h2:gt(0)').find('.mw-editsection:first'); 
			var k;
			var header;        
			headers.each(function(k) {
 
				$(this).prepend("<span style='font-size: 2em;color:#3fb6ff;text-shadow: #444 0.1em 0.1em 0.1em;'>«</span>&nbsp;&nbsp;&nbsp;<a style='font-weight:bold;cursor:pointer; color:#7d7b75;' href='#' class='wp-teahouse-respond' id='wp-teahouse-respond-" + k + "'>Join this discussion</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
 
				var formCode = 	'<div class="wp-teahouse-respond-form" id="wp-teahouse-respond-form-' + k + '"><p>You can type your response below.</p><textarea rows="10" cols="20" id="wp-th-respond-text-' + k + '"></textarea><p><span class="wp-th-sign-hint">On Wikipedia, you should sign all of your posts by ending them with four tildes (~~~~)</span><a href="#" class="wp-th-respond" id="wp-th-respond-' + k + '">Add my response</a></p></div>';
 
				var rLink = $('#wp-teahouse-respond-' + k);
				rLink.after(formCode); 
 
				var rForm = $('#wp-teahouse-respond-form-' + k);
				var rText = $('#wp-th-respond-text-' + k);
				var rButton = $('#wp-th-respond-' + k);
				var headline = $(this).parents('h2').find('span.mw-headline').html();
				headline = headline.replace(/<span class="mw-headline-number">[\d]*<\/span> /, '');
 
				// Prevent flash
				rForm.css( 'left', '-10000px' );
 
				// Set up position
				setTimeout( function() {
					var pos = rLink.position();
					var hCenter = ( $(window).width() / 2 );
					rForm.css( 'top', pos.top + 20 + 'px' );
					rForm.css( 'left', (hCenter - (rForm.width()) /2) + 'px' );
					rForm.hide();
				}, 0);
 
				rButton.button({disabled : true}).click( function(e) {
					e.preventDefault();
 
					var text = rText.val();
						addResponse( k+1, headline, text );
				}).end()
 
				rText.keypress( function(e) {
						var $textbox = $(this);
						setTimeout( function() {
							if ( (/~~~~\s*$/).test($textbox.val()) ) {
								rButton.button( 'option','disabled', false );
							} else {
								rButton.button( 'option','disabled', true );
							}
						}, 0 );
					} );
 
				rLink.click(function(e) {
					rForm.toggle('fast');
					e.cancelBubble = true; // for IE
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
				});
 
			$(document).click( function(e) {
				var $target = $(e.target);
				if ( ! $target.is('.wp-teahouse-respond-form *') &&
					! $target.is('.wp-teahouse-respond *')
				) {
					$('.wp-teahouse-respond-form').fadeOut();
				}
			} );
 
				$(document).keydown( function(e) {
					if ( e.keyCode == 27 ) {// ESC
						rForm.fadeOut();
					}
				});   //after loop
			}); 
		});
	} );
} )(jQuery,mediaWiki);
}
//</nowiki>
console.log('hi');