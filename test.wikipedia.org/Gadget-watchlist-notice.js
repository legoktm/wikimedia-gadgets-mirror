/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Shows notices to registered users on top of their watchlist
 */
 
/* global mw */
if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Watchlist' ) {
	mw.loader.load( 'ext.gadget.watchlist-notice-core' );
}