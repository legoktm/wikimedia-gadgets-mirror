/* Font/size selector test gadget for textarea [Version 0.0.2]

To do:
 * more fonts? custom input possibilties via prompt or monobook.js?
 * cookie system to remember defaults
 * test in IE
*/

if(wgAction == 'edit' || wgAction == 'submit') {
  var tweakedTextarea = 'wpTextbox1';
} else if(wgCanonicalSpecialPageName == 'Upload') {
  var tweakedTextarea = 'wpUploadDescription';
}
if(window.tweakedTextarea) $(tweakTextarea);

function tweakTextarea() {
  var ttsizes = ['50%','60%','70%','80%','90%','100%','125%','150%','175%','200%'];
  var ttfonts = [
    '* generic defaults','monospace','serif','sans-serif','cursive','fantasy',
    '* other (may not work for you)','"Times New Roman",serif','"MS Georgia",serif','"Bitstream Cyberbit",serif','"Ryumin Light-KL",serif',
    '"MS Trebuchet",sans-serif','"MS Verdana",sans-serif','"MS Arial",sans-serif','"Helvetica",sans-serif','"MS Tahoma",sans-serif','"Arial Hebrew",sans-serif','"Helvetica Cyrillic",sans-serif',
    '"Courier",monospace','"MS Courier New",monospace','"Osaka Monospaced",monospace'];
  var ed = document.getElementById('editform');
  if(!ed) return;
  var ttform = document.createElement('div');
    ttform.appendChild(document.createTextNode('Change font and size:'));
    var ftsel = document.createElement('select');
      ftsel.setAttribute('onchange','tweakTextareaApply()');
      ftsel.setAttribute('id','fontselector');
      for(var i=0;i<ttfonts.length;i++) {
        var op = document.createElement('option');
        var nam = (ttfonts[i].indexOf(',') != -1) ? ttfonts[i].substr(0,ttfonts[i].indexOf(',')) : ttfonts[i].replace(/^\*/,'')
        op.setAttribute('value',ttfonts[i]);
        if(ttfonts[i] == 'monospace') op.setAttribute('selected','selected')
        if(ttfonts[i].indexOf('*') == 0) op.setAttribute('disabled','disabled')
        op.appendChild(document.createTextNode(nam));
        ftsel.appendChild(op);
      }
    ttform.appendChild(ftsel);
    var fssel = document.createElement('select');
      fssel.setAttribute('onchange','tweakTextareaApply()');
      fssel.setAttribute('id','sizeselector');
      for(var i=0;i<ttsizes.length;i++) {
        var op = document.createElement('option');
        var nam = ttsizes[i];
        op.setAttribute('value',ttsizes[i]);
        if(ttsizes[i] == '100%') op.setAttribute('selected','selected')
        op.appendChild(document.createTextNode(nam));
        fssel.appendChild(op);
      }
    ttform.appendChild(fssel);
  ed.parentNode.insertBefore(ttform,ed);
}

function tweakTextareaApply() {
  var txt = document.getElementById(tweakedTextarea);
  if(!txt) return;
  var ftsel=document.getElementById('fontselector');
  txt.style.fontFamily = ftsel[ftsel.selectedIndex].value;
  var fssel=document.getElementById('sizeselector');
  txt.style.fontSize = fssel[fssel.selectedIndex].value;
}