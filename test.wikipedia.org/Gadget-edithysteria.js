// Original version:
// - EditHysteria script by [[:pl:User:ChP94]]
// - Released under the [http://www.gnu.org/licenses/gpl.txt GNU Public License (GPL)]
// Modified by [[:pl:User:Beau]]

var editHysteriaGadget = {
	data: null,
	loaded: false,
	version: 3,

	init: function() {
		if (mw.config.get( 'wgNamespaceNumber' ) < 0)
			return

		if (mw.util.getParamValue('printable') == 'yes')
			return

		var that = this;

		var request = {
			action:	'query',
			prop:	'revisions',
			rvprop:	'user|ids',
			format:	'json',
			titles:	mw.config.get( 'wgPageName' ),
			rvlimit:	50,
			requestid:	new Date().getTime()
		};
		jQuery.getJSON(mw.config.get( 'wgServer' ) + mw.config.get( 'wgScriptPath' ) + '/api.php', request, function(result) {
			if (that.loaded) {
				that.showResults(result);
			}
			else {
				that.data = result;
			}
		});
		jQuery(document).ready(function() {
			that.loaded = true;
			if (that.data) {
				that.showResults(that.data);
				that.data = null;
			}
		});
	},
	userlink: function(name) {
		var str = '<a href="' + mw.util.getUrl('User:' + name) + '">' + name + '</a> ' +
		  '(<a href="' + mw.util.getUrl('Special:Contributions/' + name) + '">wkład</a>, <a href="' + mw.util.getUrl('User talk:' + name) + '">dyskusja</a>';

		if ( $.inArray( 'sysop', mw.config.get( 'wgUserGroups' ) ) != -1 ) {
			str += ', <a href="' + mw.util.getUrl('Special:Blockip/' + name) + '">zablokuj</a>';
		}

		str += ')';

		return str;
	},
	showResults: function(data) {
		var page = data.query.pages[wgArticleId];
		var firstHeading;
		var headers = document.getElementsByTagName("h1");

		for(i=0; i<headers.length; i++) {
			var header = headers[i];
			if(header.className == "firstHeading" || header.id == "firstHeading" || header.className == "pagetitle") {
				firstHeading = header; break;
			}
		}

		if(!firstHeading)
			firstHeading = document.getElementById("section-0");

		if(!firstHeading)
			return;

		var div = document.createElement("div");
		div.style.cssText = "font-size:8pt;line-height:1em";
		div.className = 'plainlinks';
		if (skin == 'modern')
		{
			div.style.marginLeft = "10px";
			div.style.display = "inline-block";
		}

		firstHeading.appendChild(div);

		var html = '';
		if (! page) {
			div.innerHTML = html + "Strona nie istnieje.";
			div.style.color = "red";
			return;
		}

		var topuser = page.revisions[0].user;
		var ldiff;
		var luser;
		var count = 1;

		for (var i = 1; i < page.revisions.length; i++) {
			ldiff = page.revisions[i].revid;
			luser = page.revisions[i].user;
			if (luser != topuser)
				break;
			count++;
		}

		if (ldiff) {
			html += 'Ostatnio <a href="'+ mw.util.getUrl(wgPageName) + '?diff=cur&oldid=' + ldiff + '"/>edytowany</a>' +
				' przez ' + this.userlink(topuser) + ' (' + count + 'x)';

			if(luser != "" && luser != topuser) {
				html += ', a wcześniej przez  ' + this.userlink(luser);
			}
			html += '.';
			div.innerHTML = html;
		}
		else if (topuser != "") {
			div.innerHTML = html + 'To jest jedyna wersja tego artykułu utworzona przez ' + this.userlink(topuser) + '.';
		}
		if (skin == 'modern') {
			var links = div.getElementsByTagName('a');
			for (var i = 0; i < links.length; i++) {
				links[i].style.color = 'white';
				links[i].style.textDecoration = 'underline';
			}
		}
	}
};

editHysteriaGadget.init();