// Add "User rights" link
jQuery(document).ready( function( $ ) {
	if ( $.inArray( mw.config.get('wgNamespaceNumber'), [2, 3] ) ) {
		mw.util.addPortletLink( 'p-cactions', wgScript + '?title=Special:UserRights&user=' + encodeURIComponent( wgTitle.split('/')[0] ),
				'ব্যবহারকারী অধিকার', 'ca-rights', 'ব্যবহারকারীর অধিকার পরিবর্তন করুন');
	}
} );