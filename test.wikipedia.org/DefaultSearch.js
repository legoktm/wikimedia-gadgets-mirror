//
//change default from Go to Search
//

$j(document).ready(
  function ()
  {
      if (typeof GoButton != 'undefined') return;
      // Classic skin has no IDs on the buttons, just names! And CologneBlue and Classic have
      // two search forms.
      var $searchBtn = $('#searchform #mw-searchButton, #searchform2 #mw-searchButton, #searchform input[name=fulltext], #searchform2 input[name=fulltext]');
      var $goBtn = $('#searchform #searchGoButton, #searchform2 #searchGoButton2, #searchform input[name=go], #searchform2 input[name=go]');
      var $textfield = $('#searchform #searchInput, #searchform2 #searchInput2, #searchform input[name=searchInput], #searchform2 input[name=searchInput2]');

      $textfield.keydown(function (evt) {
            var e = evt || window.event;
            var key = (typeof (e.keyCode) != 'undefined' ? e.keyCode : e.which);
            if (key == 13) { // Enter
              $searchBtn.click ();
              return false;
            }
          });
          
      // Indicate default visually. Changing the style of a button works also in IE. (However,
      // Safari seems to ignore this).
      $goBtn.css('fontWeight', "normal");
      $searchBtn.css('fontWeight', "bold");
  });