<script>
var fundraisingAmounts = fundraisingAmounts || {};

fundraisingAmounts.pick = function(source, currency, country) {
    /*
     * Select the correct amount or array of amounts from object in "source"
     * In future may extend to allow a language parameter too
     * 
     * @param {object} source   - the amounts object e.g. fundraisingAmounts.asks, fundraisingAmounts.averages
     * @param {string} currency - ISO code of currency
     * @param {string} country  - ISO code of country (optional)
     * @return {array/number}   - depending on source
     */

    if ( source[currency]["default"] ) { // we need to go deeper
        if ( source[currency][country] !== undefined ) {
            return source[currency][country];
        } else {
            return source[currency]["default"];
        }
    } else {
        return source[currency];
    }
};

function convertAsk(amount, currency, country) {
    /*
     * Given an amount in USD, find an "equivalent" local ask amount for the given currency and country
     * @TODO: namespace this
     * 
     * @param {number} amount   - USD amount
     * @param {string} currency - ISO code of currency
     * @param {string} country  - ISO code of country
     * @return {number}         - local amount
     */
    var usdbase = [3, 5, 10, 20, 30, 50, 100];
    var usdamount = parseInt(amount, 10);
    if(isNaN(usdamount)){
        return 0;
    }
 
    var index = $.inArray(usdamount, usdbase);
    if (index == -1) {
        // the amount is not in the USD ask array, find a near neighbor
        index = 0;
        while (usdbase[index+1] < usdamount && index < usdbase.length + 1) {
            index++;
        }
    }

    var array = fundraisingAmounts.pick(fundraisingAmounts.asks, currency, country);
    if (array === undefined) {
        // in case we don't have amounts for this currency
        return usdamount;
    } else {
        return array[index];
    }
}

function getAverage(currency, country, language) {
    /*
     * Return a formatted string with the "average" donation amount in the given currency, country and language
     * @TODO: just move this into banner code
     * 
     * @param {string} currency - ISO code of currency
     * @param {string} country  - ISO code of country
     * @param {string} country  - ISO code of language     
     * @return {string}         - formatted amount 
     */
    var amount = fundraisingAmounts.pick(fundraisingAmounts.averages, currency, country);
    return currencyLocalize(currency, amount, language);
}

function getMinimumString(currency, country, language) {
    /*
     * Return a formatted string with the "if everyone" amount in the given currency, country and language
     * @TODO: just move this into banner code
     * 
     * @param {string} currency - ISO code of currency
     * @param {string} country  - ISO code of country
     * @param {string} country  - ISO code of language     
     * @return {string}         - formatted amount 
     */
    var amount = fundraisingAmounts.pick(fundraisingAmounts.ifEveryone, currency, country)[0];
    return currencyLocalize(currency, amount, language);
}
</script>