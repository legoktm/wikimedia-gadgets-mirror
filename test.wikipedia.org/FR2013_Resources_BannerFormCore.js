<script>
/**
  * Core code for forms in banners.
  * Handles showing the correct payment methods, validating input, and sending on to payments wiki.
  * (Note that localizing amount options is done elsewhere, in BannerFormAmountOptions.js)
  *
  * Dependencies:
  *     MediaWiki:FR2013/Resources/CurrencyMinimums.js
  *     MediaWiki:FR2013/Resources/Country2Currency.js
  *     MediaWiki:FR2013/Resources/PaymentOutages.js
  *     MediaWiki:FR2015/Resources/MethodLocalize.js
  *
  */

function checkPaymentOutages() {
    /* Check against the scheduled payment method outages below
     * and hide the relevant button for any which are ongoing.
     */
    {{MediaWiki:FR2013/Resources/PaymentOutages.js}}
    var now = new Date();

    for (var i = outages.length - 1; i >= 0; i--) {
        if ( now > outages[i]['start'] && now < outages[i]['end'] ) {
            if (outages[i]['country'] === undefined || outages[i]['country'] == Geo.country) {
                $('.paymentmethod-' + outages[i]['method']).hide();
            }
        }
    }
}

function validateForm(form) {
    /* Check the form, especially the amount submitted 
     * Return the amount if valid, otherwise return false
     */

    var error = true;
    var amount = null;

    if (form.amount) {
        // If there are some amount radio buttons, then look for the checked one
        for (var i = 0; i < form.amount.length; i++) {
            if (form.amount[i].checked) {
                amount = form.amount[i].value;
            }
        }
    }

    // Check the "other" amount box
    if (form.amountGiven.value != '') {
        var otherAmount = form.amountGiven.value;
        otherAmount = otherAmount.replace(/[,.](\d)$/, ':$10');
        otherAmount = otherAmount.replace(/[,.](\d)(\d)$/, ':$1$2');
        otherAmount = otherAmount.replace(/[$£€¥,.]/g, '');
        otherAmount = otherAmount.replace(/:/, '.');
        form.amountGiven.value = otherAmount;
        amount = otherAmount;
    }

    // Check amount is a real number
    error = ( amount == null || isNaN(amount) || amount.value <= 0 );

    // Check amount is at least the minimum
    var currency = form.currency_code.value;
    if (amount < getMinimum(currency) || error) {
        alert('{{{validation-error-minimum|{{int:fr2013-dropdown-smallamount-error}}}}}'.replace('$1', getMinimum(currency) + ' ' + currency));
        error = true;
    }

    if ( error ) {
        return false;
    } else {
        return amount;
    }

}

function redirectPayment(paymentMethod, paymentSubMethod, skipValidation) {

    if ( skipValidation || validateForm(document.paypalcontribution) ) {

        if (typeof paymentSubMethod == 'undefined') {
            paymentSubMethod = '';
        }
        var form = document.paypalcontribution; // we should really change this some day
        var paymentsURL = 'https://payments.wikimedia.org/index.php/Special:GatewayFormChooser';

        form.action = paymentsURL;

        if ( form.language.value === 'pt' && Geo.country === 'BR' ) {
            form.language.value = 'pt-br';
        }

        // WorldPay override for cc
        if( paymentMethod === 'cc-wp' ) {
            paymentMethod = 'cc';
            form.payment_method.value = 'cc';
            form.gateway.value = 'worldpay';
            form.ffname.value = 'worldpay';
        }
        
        // Adyen override for cc
        if( paymentMethod === 'cc-adyen' ) {
            paymentMethod = 'cc';
            form.payment_method.value = 'cc';
            form.gateway.value = 'adyen';
        }

        var frequency = $("input[name='frequency']:checked").val();
        if( frequency !== 'monthly' ){
            frequency = 'onetime';
            form.recurring.value = 'false';
        } else {
            form.recurring.value = 'true';
        }

        var mixins = mw.centralNotice.getDataProperty( 'mixins' );

        if ( mixins && mixins.bannerHistoryLogger ) {
            form.bannerhistlog.value = mw.centralNotice.bannerHistoryLogger.id;
        }

        form.utm_key.value = mw.centralNotice.bannerData.cookieCount || 0;
        form.payment_method.value = paymentMethod;
        form.payment_submethod.value = paymentSubMethod;
        
        var full_dotted_payment_method = paymentMethod;
        if ( form.recurring.value == 'true' ) {
            full_dotted_payment_method = 'r' + full_dotted_payment_method;
        }
        if ( paymentSubMethod ) {
            full_dotted_payment_method = form.payment_method.value + '.' + paymentSubMethod;
        }
        form.utm_source.value += '.no-LP.' + full_dotted_payment_method;

        $('.frbanner-form button').attr('disabled', 'disabled'); // Disable to prevent double submission

        if ( mixins && mixins.bannerHistoryLogger ) {
            mw.centralNotice.bannerHistoryLogger.ensureLogSent().always(function() {
                form.submit();
            });
        } else {
            form.submit();
        }
        
    }
}

function toggleMonthly( monthly ){
    if( monthly.type === 'checkbox' ){
        monthly = monthly.checked;
    }
    if (monthly) {
        $('#{{{banner}}}-form').addClass('form-monthly');
    } else {
        $('#{{{banner}}}-form').removeClass('form-monthly');
    }
}


$(document).ready( function() {

    // Allow overriding the geolocation for testing different countries
    if(mw.util.getParamValue('country') !== null) {
        Geo.country = mw.util.getParamValue('country');
    }

    var currency = getCurrency(Geo.country);
    var language = mw.config.get('wgUserLanguage');

    // Display correct payment methods
    {{MediaWiki:FR2015/Resources/MethodLocalize.js}}
    checkPaymentOutages();

    // If there are no recurring payment methods available, disable the "monthly" option
    if ( !$('#{{{banner}}}-form *[class^="paymentmethod-"]:not(.no-monthly)').length ) {
        $('#frequency_monthly').prop('disabled', 'disabled');
    }

    // Set the form fields
    $("input[name='country']").val(Geo.country);
    $("input[name='currency_code']").val(currency);
    $("input[name='language']").val(language);
    $("input[name='return_to']").val( encodeURI('Special:LandingCheck?basic=true&landing_page=Thank_You&uselang=' + mw.config.get('wgUserLanguage')) );

    // Handle pressing Enter on "Other" field
    $(document).on('keydown', 'input[name="amountGiven"]', function(e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            $('.paymentmethod-options button, .frbanner-submit button').filter(':visible').first().click();
            return false;
        }
    });

});
</script>