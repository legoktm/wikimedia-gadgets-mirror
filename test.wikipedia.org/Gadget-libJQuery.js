/**
 * [[:commons:MediaWiki:libJQuery.js]]
 * Commons-jQuery additions that can be useful to Commons and other Wikimedia Wikis
 *
 * Invoke automated jsHint-validation on save: A feature on WikimediaCommons
 * Interested? See [[:commons:MediaWiki:JSValidator.js]], [[:commons:Help:JSValidator.js]].
 *
 * @rev 1 (2012-07-26)
 * @author Rillke, 2012
 *
 * <nowiki>
 */
 
/*global jQuery:false, mediaWiki:false*/
/*jshint smarttabs: true*/
 
( function ( $, mw ) {
"use strict";

// Extending jQuery-object
// You can call the new methods like this: $.method()
$.extend({
	/**
	 * Create 
	 *
	 * @example
	 *      $.createIcon('ui-icon-alert').appendTo( mw.util.$content );
	 *
	 * @param {String} iconClass jQuery UI icon class name.
	 *
	 * @context {jQuery}
	 * @return {Object} jQuery-wrapped DOM-object: The icon that has been created
	 */
	createIcon: function (iconClass) {
		return $('<span>', { 'class': 'ui-icon ' + iconClass + ' jquery-inline-icon', text: ' ' });
	},
	/**
	 * Create a notify-area (area with a background and an icon dependent on the state passed-in)
	 * More about possible arguments: Look at http://jqueryui.com/ and subpages
	 *
	 * @example
	 *      var $area = $.createNotifyArea( $('<span>').text('Hello World'), 'ui-icon-alert', 'ui-state-error' );
	 *      $area.appendTo( mw.util.$content );
	 *
	 * @param {String|DOM-object|jQuery-wrapped DOM-object} textNode Message-string, HTML or DOM-node that will be used in the area.
	 * @param {String} icon jQuery UI icon class name.
	 * @param {String} state class(es) that indicate(s) the state of the area. E.g. ui-state-highlight, ui-state-disabled, ui-state-error, ...
	 *
	 * @context {jQuery}
	 * @return {Object} jQuery-wrapped DOM-object: The area that has been created
	 */
	createNotifyArea: function(textNode, icon, state) {
		return $('<div>', { 'class': 'ui-widget' }).append(
			$('<div>', { 'class': state + ' ui-corner-all', style: 'margin-top:20px; padding:0.7em;' }).append($('<p>').append(
				$.createIcon(icon).css('margin-right', '.3em'), textNode
			))
		);
	}
});

// i18n and configuration for the buttons
mw.messages.set({
	'libjq-cancel-title':  "Close this dialog [Esc]",
	'libjq-proceed-title': "Proceed [Enter] in single-line text fields",
	'libjq-report-title':  "Reporting errors helps improving the application"
});
var buttonConfig = {
	proceed: {
		'icon': 'ui-icon-circle-check',
		'class': 'ui-button-green',
		'title': 'libjq-proceed-title'
	},
	cancel: {
		'icon': 'ui-icon-circle-close',
		'class': 'ui-button-red',
		'title': 'libjq-cancel-title'
	},
	report: {
		'icon': 'ui-icon-circle-check',
		'class': '',
		'title': 'libjq-report-title'
	}
};

// Extending jQuery's prototype
// You can call the new methods like this: $('selector').method()
$.extend($.fn, {
	/**
	 * Style a button like a proceed, cancel, ... button
	 *
	 * @example
	 *      $('#MySelector').children('button').specialButton('proceed').hide()
	 *
	 * @param {String} which name of the special-button type
	 *
	 * @context {jQuery-wrapped DOM-object}
	 * @return {jQuery-wrapped DOM-object} returns the same jQuery-wrapped DOM-object so you can perform further actions on it
	 */
	specialButton: function(which) {
		return (function($btn, cfg) {
			return $btn.button({ 
				icons: { 
					primary: cfg.icon
				} 
			}).addClass(cfg['class']).attr('title', mw.message(cfg.title).parse());
		}(this, buttonConfig[which]));
	}
});

}( jQuery, mediaWiki ));
 
// </nowiki>