
/*!
 * Teahouse user interface MessageDialog class.
 */

/**
 * Dialog displaying a message to a user
 *
 * @class
 * @extends OO.ui.MessageDialog
 *
 * @constructor
 * @param {Object} [config] Configuration options
 */

mw.util.teahouse.registerType( 'mw.util.teahouse.ui.dialog.Message' );

mw.util.teahouse.ui.dialog.Message = function TeahouseUiMessageDialog( config ) {
	mw.util.teahouse.ui.dialog.Message.super.call( this, config );
};
OO.inheritClass( mw.util.teahouse.ui.dialog.Message, OO.ui.MessageDialog );

mw.util.teahouse.ui.dialog.Message.static.actions = [
	{ action: 'yes', label: mw.message('teahouse-dialog-msg-btn-yes').plain() },
	{ action: 'no', label: mw.message('teahouse-dialog-msg-btn-no').plain() }
];
/**
 * @inheritdoc
 */
mw.util.teahouse.ui.dialog.Message.prototype.initialize = function () {
	// Parent method
	mw.util.teahouse.ui.dialog.Message.super.prototype.initialize.call( this );

	//Create new panel
	this.$message = $('<div>').addClass( 'oo-ui-messageDialog-message' );

	this.text.$element.append( this.$message );
};

/**
 * @inheritdoc
 */
mw.util.teahouse.ui.dialog.Message.prototype.getSetupProcess = function ( data ) {
	data = data || {};

	// Parent method
	return mw.util.teahouse.ui.dialog.Message.super.prototype.getSetupProcess.call( this, data )
		.next( function () {

			//We hide the base class' OO.ui.LabelWidget
			this.message.$element.hide();

			this.$message.empty();
			this.$message.append(data.message);
		}, this );
};