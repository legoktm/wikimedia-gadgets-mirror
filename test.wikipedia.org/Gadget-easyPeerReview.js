/*jslint browser: true, devel: true, undef: true, eqeqeq: true, newcap: true, immed: true, maxerr: 80 */
/*global addPortletLink, jsMsg, addOnloadHook, Bawolff, escapeQuotesHTML, _hasAttribute, window */
/*members AsyncQueue, EasyReview_comment, EasyReview_copyright, 
    EasyReview_copyright_comment, EasyReview_news, EasyReview_news_comment, 
    EasyReview_npov, EasyReview_npov_comment, EasyReview_revid, 
    EasyReview_sg, EasyReview_sg_comment, EasyReview_sources, 
    EasyReview_sources_comment, Request, action, add, comment, confirm, 
    content, createElement, delaySend, edit, exec, failReview, getAttribute, 
    getElementById, getElementsByName, getElementsByTagName, getPage, 
    indexOf, innerHTML, lastIndex, length, level, mwapi, next, page, 
    postReview, previousSibling, prop, redraw, replace, revid, review, 
    rvprop, section, selectChange, sight, src, start, style, submit, 
    substring, summary, titles, toString, value, visibility review_i18n,
    review_tab, review_tab_tooltip, isFail, name, message, doWeirdGoogleHack,
    newid, LazyVar, pass, fail, not_reviewed, form_comment, form_comment_overall,
    form_rev_numb, form_submit, form_copyright, form_copyright_page, form_news_page,
    form_news, form_sources_page, form_sources, form_npov, form_npov_page,
    form_sg, form_sg_page, unknownError, softEditError, pub_edit_summary,
    review_comment, review_header_pass, review_header_fail
*/
//So that this doesn't interfere with var names of anything else 
if (typeof Bawolff === "undefined") Bawolff = {};

// Quick hack - IF THIS BREAKS REVERT
jsMsg2 = function(arg) {
 if ( $( '#mw-js-notice2' ).length == 0 ) {
  $( '#siteNotice' ).after( $( '<div id="mw-js-notice2" ></div>' ) );
 }
 $( '#mw-js-notice2' ).empty();
 $( '#mw-js-notice2' ).html( arg );
}

Bawolff.sight_status = 3; // Ugly error checking code, but I don't really know whats going on.

if (!Bawolff.review_i18n) {
 //this has issue of not allowing partial translation.
 //These are treated as raw HTML. don't give yourself an XSS! (basically avoid >, <, and " unless its part of html)
 //Anything that ends in _page should be in url form (Aka [[project:ét b]] would be 'project:%C3%A9t_b' not 'project:ét b' )
 Bawolff.review_i18n = {
  review_tab: 'Review',
  review_tab_tooltip: 'Do a peer review on this page',
  pass: 'Pass',
  fail: 'Fail',
  not_reviewed: 'Not reviewed',
  form_comment: 'Comment: ',
  form_comment_overall: 'Comments by reviewer: ',
  form_rev_numb: 'Reviewing revision ',
  form_submit: 'Submit Review',
  form_copyright: 'Copyright',
  form_copyright_page: 'Wikinews:Copyright',
  form_news_page: 'Wikinews:Content_guide',
  form_news: 'Newsworthiness',
  form_sources_page: 'Wikinews:Cite_your_sources',
  form_sources: 'Verifiability',
  form_sg: 'Style',
  form_sg_page: 'Wikinews:Style_guide',
  form_npov_page: 'Wikinews:Neutral_point_of_view',
  form_npov: 'NPOV',
  form_read_talk: 'I have read all the important information on the talk page, such as previous review comments (<a href="' + mw.config.get('wgServer') + mw.config.get('wgArticlePath').replace('$1', 'Talk:' + encodeURIComponent(mw.config.get('wgTitle').replace(/\s/g, '_'))) + '" target="_blank">Open talk page in a new window</a>)',
  read_talk: 'You cannot review the article until you have read the talk page and checked the appropriate box. (blame BRS)',
  tasksError: 'Could not add article flag to article due to error: "$1". Peer review template will still be added to talk, but make your checks.',
  unknownError: 'Easy review script has uncaught exception [tell Bawolff]: ',
  softEditError: "Something bad happened (recoverable edit error, like a captcha or something. However we couldn't figure out how to recover.)",
  errorNoPub: "Could not add \{\{publish\}\} to article. Due to error: '$1'. Continuing with rest of review process.",
  errorFetchFailureSuccess: "Could not retrieve page source from server when trying to replace review with publish in successful review",
  errorFetchFailFail: "Could not retrieve page source from server when trying to replace review with tasks in failed review",
  ErrorEditConflictCancel: "User prematurely cancelled due to edit conflict",
  warningNoArticleID: 'Warning (Please leave user:Bawolff a note): Could not figure out revision id of new published version of article. Falling back to using revision id of penultimate revision. you may have to sight the latest version of this article manually.',
  pub_edit_summary: "Publish. (Using [[MediaWiki:Gadget-easyPeerReview.js|easy peer review]])",
  review_comment:  "Submitted using easy peer review - MediaWiki:Gadget-easyPeerReview.js", //comment in src.
  review_header_pass: "Review of revision $1 [Passed]",
  review_header_fail: "Review of revision $1 [Not ready]",
  review_revid: 'revid', //argument name for revid in peer reviewed template 
  review_copyright: 'copyright',
  review_newsworthy: 'newsworthy',
  review_verifiable: 'verifiable',
  review_npov: 'npov',
  review_style: 'style',
  review_reviewer: 'reviewer',
  review_comments_field: 'comments',
  review_time: 'time',
  review_peer_reviewed: 'peer_reviewed', //peer review template name.
  tasks: 'tasks', //translating the call to {{tl|tasks}}
  tasks_copyvio: 'copyvio',
  tasks_news: 'news',
  tasks_src: 'src',
  tasks_npov: 'npov',
  tasks_mos: 'mos',
  tasks_rereview: 're-review',
  publish: 'Publish', //name of publish template.
  editSummary_sight: "Publishing article after successful peer review (Using [[MediaWiki:Gadget-easyPeerReview.js]]): ",
  editSummary_failReview: "Not ready when reviewed: article needs improvement(s). Add article flag. (Using [[MediaWiki:Gadget-easyPeerReview.js|easy peer review]]",
  editConflict: "*********[edit conflict]********** \nSomeone has edited this page since you have reviewed it. Do you want to review this page (including the latest edits) anyways?",
  done: 'Done.',
  commentPageExists: 'The comments page already exists and is not using Liquid threads. \n\nPress ok to overwrite any pre-existing non-LiquidThreads comments, press cancel to keep as is. Any pre-existing LiquidThreads comments will not be overwritten',
  success: '"$1" has been successfully reviewed. $2', //$1 = article name, $2= make lead box
  success_ml: 'Make this story a lead article', //$1 = article name
  success_ml_url: mw.config.get('wgArticlePath').replace('$1', 'Wikinews:Make_lead'),
  fail_review: '<b>Done</b> "$1" has been reviewed (With the article <b>failing</b> the review process)', //$1=article name.
  trigger_id: 'review', //look for this id in pages to mean that the article is ready for review.
  trigger_func: function () {}, //leave alone unless you know what you're doing. return true to signify this page is eligable to be reviewed.
  regex_sources: /\*\{\{(?:[sS]ource(-pr|-science)?|[aA]pasource|[pP]apersource|[oO]rsource|[sS]ourceReg)\|[^}]*\}\}/gm,
  prePubTransform: function (page) {
        //called before adding publish to article
        page = page.replace(/\{\{[dD]ate\|[^}]*\}\}/, "\{\{date|\{\{subst:#time:F j, Y}}}}");
        page = page.replace(/\{\{(?:[bB]reaking|[uU]rgent) review(?:\|[^}]*)?\}\}/g, "\{\{breaking}}"); //rm {{tl|breaking review}} add breaking
        page = page.replace(/\{\{(?:[rR]eview|[bB]reaking ?review|[uU]rgent review|[uU]rgent|[uU]rgent-review|[qQ]uick review|[rR]eady|[uU]nder review)(?:\|[^}]*)?\}\}(?:\n)?/g, ""); //rm {{tl|review}}
        page = page.replace(/\{\{[dD]evelop(?:ing)?(?:\|[^}]*)?\}\}(?:\n)?/g, ""); //rm {{tl|develop}}
        return page;
  },
  stripReviewForFail: function (page, tasks_template) {
        //if review fails, remove review tag. add tasks
        page = page.replace(/\{\{(?:[rR]eview|[bB]reaking review|[uU]rgent review|[qQ]uick review|[rR]eady|[uU]nder review)(?:\|[^}]*)?\}\}/, tasks_template); //replace {{tl|review}} with tasks
        page = page.replace(/\{\{(?:[rR]eview|[bB]reaking review|[uU]rgent review|[qQ]uick review|[rR]eady|[uU]nder review)(?:\|[^}]*)?\}\}(?:\n)?/g, ""); //if multiple review templates
        return page;
  }

 } 
}

Bawolff.review = function() {
//this is initilization function. called onload
//pre-req: already checked in main namespace, user is in group editor, and action is view
//check if {{tl|review}} is on page
    if (document.getElementById(Bawolff.review_i18n.trigger_id) || Bawolff.review_i18n.trigger_func()) {
        mw.util.addPortletLink("p-cactions", "javascript:Bawolff.review.start();void%200;", Bawolff.review_i18n.review_tab, "ca-peerReview", Bawolff.review_i18n.review_tab_tooltip);
    }
}
Bawolff.review.doWeirdGoogleHack = false; // FIXME: this is crap.

if (mw.config.get('wgDBname') === 'enwikinews') {
 Bawolff.review.doWWC = true;
}
Bawolff.review.start = function () {
    if (mw.config.get('wgDBname') === 'enwikinews') {
        mw.loader.load( '/w/index.php?title=' + 'User:Bawolff/mwapilib.js' + '&action=raw&ctype=text/javascript' ); //needed when pressing submit
        if (Bawolff.review.doWWC) mw.loader.load( '/w/index.php?title=' + 'User:Bawolff/mwapilib2.js' + '&action=raw&ctype=text/javascript' );
    } else {
        mw.loader.load('//en.wikinews.org/w/index.php?title=User%3ABawolff%2Fmwapilib.js&action=raw&ctype=text/javascript&scrver=2');
    }

    var talk = document.getElementById('ca-talk');
    var readTalk = '';
    if (talk && talk.className.indexOf('new') === -1) {
        readTalk = '<br/><input type="checkbox" id="read-talk-box"/> <label for="read-talk-box">' + Bawolff.review_i18n.form_read_talk + '</label>';
    }

    //This draws the dialog. called on click
    var container = document.createElement("div");
    container.innerHTML ='<form id="easyReviewForm" action="javascript:void 0" onsubmit="Bawolff.review.submit(); return false;"><table class="metadata plainlinks ambox ambox-notice EasyReview" id="EasyReview-mainAmbox" style="line-height: 1.2"><tr><td class="ambox-image"><a href="/wiki/File:Pictogram_voting_question-blue.svg" class="image" title="Pictogram_voting_question-blue.svg"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/9/90/Pictogram_voting_question-blue.svg/60px-Pictogram_voting_question-blue.svg.png" width="60" height="62" border="0" /></a></td><td>' + Bawolff.review_i18n.form_rev_numb + mw.config.get('wgCurRevisionId') + '<ul style="line-height:1.6;"><li><b><a href="/wiki/' + Bawolff.review_i18n.form_copyright_page + '" title="' + decodeURIComponent(Bawolff.review_i18n.form_copyright_page).replace(/_/g, ' ') + '">' + Bawolff.review_i18n.form_copyright + '</a>:</b> <img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-copyright-image" width="10" height="10" border="0" /><select name="EasyReview-copyright" onchange="Bawolff.review.selectChange()"><option value="pass">' + Bawolff.review_i18n.pass + '</option><option value="fail" >' + Bawolff.review_i18n.fail + '</option><option value="n/a" selected="selected">' + Bawolff.review_i18n.not_reviewed + '</option></select> <label for="EasyReview-copyright-comment" style="visibility:hidden">' + Bawolff.review_i18n.form_comment + '</label><input id="EasyReview-copyright-comment" name="EasyReview-copyright-comment" type="text" length="30" style="visibility: hidden"/></li><li><b><a href="/wiki/' + Bawolff.review_i18n.form_news_page + '" title="' + decodeURIComponent(Bawolff.review_i18n.form_news_page).replace(/_/g, ' ') + '">' + Bawolff.review_i18n.form_news + '</a>:</b> <img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-news-image" width="10" height="10" border="0" /><select name="EasyReview-news" onchange="Bawolff.review.selectChange()"><option value="pass">' + Bawolff.review_i18n.pass + '</option><option value="fail" >' + Bawolff.review_i18n.fail + '</option><option value="n/a" selected="selected">' + Bawolff.review_i18n.not_reviewed + '</option></select> <label for="EasyReview-news-comment" style="visibility:hidden">' + Bawolff.review_i18n.form_comment + '</label><input id="EasyReview-news-comment" name="EasyReview-news-comment" type="text" length="30" style="visibility: hidden"/></li><li><b><a href="/wiki/' + Bawolff.review_i18n.form_sources_page + '" title="' + decodeURIComponent(Bawolff.review_i18n.form_sources_page).replace(/_/g, ' ') + '">' + Bawolff.review_i18n.form_sources + '</a>:</b> <img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-sources-image" width="10" height="10" border="0" /><select name="EasyReview-sources" onchange="Bawolff.review.selectChange()"><option value="pass">' + Bawolff.review_i18n.pass + '</option><option value="fail" >' + Bawolff.review_i18n.fail + '</option><option value="n/a" selected="selected">' + Bawolff.review_i18n.not_reviewed + '</option></select> <label for="EasyReview-sources-comment" style="visibility:hidden">' + Bawolff.review_i18n.form_comment + '</label><input id="EasyReview-sources-comment" name="EasyReview-sources-comment" type="text" length="30" style="visibility: hidden"/></li><li><b><a href="/wiki/' + Bawolff.review_i18n.form_npov_page + '" title="' + decodeURIComponent(Bawolff.review_i18n.form_npov_page).replace(/_/g, ' ') + '">' + Bawolff.review_i18n.form_npov + '</a>:</b> <img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-npov-image" width="10" height="10" border="0" /><select name="EasyReview-npov" onchange="Bawolff.review.selectChange()"><option value="pass">' + Bawolff.review_i18n.pass + '</option><option value="fail" >' + Bawolff.review_i18n.fail + '</option><option value="n/a" selected="selected">' + Bawolff.review_i18n.not_reviewed + '</option></select> <label for="EasyReview-npov-comment" style="visibility:hidden">' + Bawolff.review_i18n.form_comment + '</label><input id="EasyReview-npov-comment" name="EasyReview-npov-comment" type="text" length="30" style="visibility: hidden"/></li><li><b><a href="/wiki/' + Bawolff.review_i18n.form_sg_page + '" title="' + decodeURIComponent(Bawolff.review_i18n.form_sg_page).replace(/_/g, ' ') + '">' + Bawolff.review_i18n.form_sg + '</a>:</b> <img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-sg-image" width="10" height="10" border="0" /><select name="EasyReview-sg" onchange="Bawolff.review.selectChange()"><option value="pass">' + Bawolff.review_i18n.pass + '</option><option value="fail" >' + Bawolff.review_i18n.fail + '</option><option value="n/a" selected="selected">' + Bawolff.review_i18n.not_reviewed + '</option></select> <label for="EasyReview-sg-comment" style="visibility:hidden">' + Bawolff.review_i18n.form_comment + '</label><input id="EasyReview-sg-comment" name="EasyReview-sg-comment" type="text" length="30" style="visibility: hidden"/></li></ul><p><b><label for="EasyReview-commentBox">' + Bawolff.review_i18n.form_comment + '</label></b> <textarea row="3" id="EasyReview-commentBox" name="EasyReview-commentBox"></textarea>' + readTalk + '</p><input type="submit" value="'+ Bawolff.review_i18n.form_submit + '"/> <input type="submit" value="Cancel" onclick="jsMsg2(\'\');return false"/></td></tr></table></form>';

/*<img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png" id="EasyReview-sg-image" width="10" height="10" border="0" /><select name="EasyReview-sg" onchange="Bawolff.review.selectChange()"><option value="pass">Pass</option><option value="fail" selected="selected">Fail</option></select> <label for="EasyReview-sg-comment" style="visibility:hidden">Comment: </label><input id="EasyReview-sg-comment" name="EasyReview-sg-comment" type="text" length="30" style="visibility: hidden"/>
*/
/*//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Art%C3%ADculo_bueno.svg/10px-Art%C3%ADculo_bueno.svg.png
//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png
*/
jsMsg2(container);
Bawolff.review.selectChange();

}
Bawolff.review.selectChange = function () {
//basically a redraw operation. something change, so redraw box
    Bawolff.review.redraw("sg", document.getElementsByName("EasyReview-sg")[0].value === "pass", document.getElementsByName("EasyReview-sg")[0].value !== "fail");
    Bawolff.review.redraw("npov", document.getElementsByName("EasyReview-npov")[0].value === "pass", document.getElementsByName("EasyReview-npov")[0].value !== "fail");
    Bawolff.review.redraw("sources", document.getElementsByName("EasyReview-sources")[0].value === "pass", document.getElementsByName("EasyReview-sources")[0].value !== "fail");
    Bawolff.review.redraw("news", document.getElementsByName("EasyReview-news")[0].value === "pass", document.getElementsByName("EasyReview-news")[0].value !== "fail");
    Bawolff.review.redraw("copyright", document.getElementsByName("EasyReview-copyright")[0].value === "pass", document.getElementsByName("EasyReview-copyright")[0].value !== "fail");
}
Bawolff.review.redraw = function (prefix, state, hidden) {
    var base = "EasyReview-" + prefix;
    if (state) {
        document.getElementById(base + "-image").src = "//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Art%C3%ADculo_bueno.svg/10px-Art%C3%ADculo_bueno.svg.png";
    } else {
        document.getElementById(base + "-image").src = "//upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Red_x.svg/10px-Red_x.svg.png";
    }
    if (hidden) {
        document.getElementById(base + "-comment").previousSibling.style.visibility = "hidden";
        document.getElementById(base + "-comment").style.visibility = "hidden";
    } else {
        document.getElementById(base + "-comment").previousSibling.style.visibility = "visible";
        document.getElementById(base + "-comment").style.visibility = "visible";
    }
}

Bawolff.review.isFail = function ( value ) {
 //should this area be flagged in {{tl|tasks}}.
 if (value === 'pass' || value === 'n/a') {
  return true;
 }
 return false;
}
Bawolff.review.submit = function () {
var readTalk = document.getElementById('read-talk-box');
if (readTalk && !readTalk.checked ) {
    alert( Bawolff.review_i18n.read_talk );
    return false;
}
try {
//form submit handler
    var text;
    var review = {
        "EasyReview_revid": mw.config.get('wgCurRevisionId'),
        "EasyReview_sg" :  Bawolff.review.isFail(document.getElementsByName("EasyReview-sg")[0].value),
        "EasyReview_sg_comment" : document.getElementsByName("EasyReview-sg")[0].value !== 'fail' ? document.getElementsByName("EasyReview-sg")[0].value : document.getElementById("EasyReview-sg-comment").value,

        "EasyReview_npov" :  Bawolff.review.isFail(document.getElementsByName("EasyReview-npov")[0].value),
        "EasyReview_npov_comment" : document.getElementsByName("EasyReview-npov")[0].value !== 'fail' ? document.getElementsByName("EasyReview-npov")[0].value : document.getElementById("EasyReview-npov-comment").value,

        "EasyReview_sources" :  Bawolff.review.isFail(document.getElementsByName("EasyReview-sources")[0].value),
        "EasyReview_sources_comment" : document.getElementsByName("EasyReview-sources")[0].value !== 'fail' ? document.getElementsByName("EasyReview-sources")[0].value : document.getElementById("EasyReview-sources-comment").value,

        "EasyReview_news" :  Bawolff.review.isFail(document.getElementsByName("EasyReview-news")[0].value),
        "EasyReview_news_comment" : document.getElementsByName("EasyReview-news")[0].value !== 'fail' ? document.getElementsByName("EasyReview-news")[0].value : document.getElementById("EasyReview-news-comment").value,

        "EasyReview_copyright" :  Bawolff.review.isFail(document.getElementsByName("EasyReview-copyright")[0].value),
        "EasyReview_copyright_comment" : document.getElementsByName("EasyReview-copyright")[0].value  !== 'fail' ? document.getElementsByName("EasyReview-copyright")[0].value : document.getElementById("EasyReview-copyright-comment").value,

        "EasyReview_comment" : document.getElementById("EasyReview-commentBox").value
    }

    // True -> pass
    /* No longer needed (I think)
    review.EasyReview_sg_comment = (review.EasyReview_sg ? 'pass' : review.EasyReview_sg_comment );
    review.EasyReview_npov_comment = (review.EasyReview_npov ? 'pass' : review.EasyReview_npov_comment );
    review.EasyReview_sources_comment = (review.EasyReview_sources ? 'pass' : review.EasyReview_sources_comment );
    review.EasyReview_news_comment = (review.EasyReview_news ? 'pass' : review.EasyReview_news_comment );
    review.EasyReview_copyright_comment = (review.EasyReview_copyright ? 'pass' : review.EasyReview_copyright_comment );
    */

    //pass review if all subsections are passed
    var passed = review.EasyReview_sg_comment === 'pass' && review.EasyReview_npov_comment === 'pass' && review.EasyReview_sources_comment === 'pass' && review.EasyReview_news_comment === 'pass' && review.EasyReview_copyright_comment === 'pass';

    //func for publishing article
    if (passed) {
    Bawolff.review.postReview(review);
    } else {
    Bawolff.review.failReview(review);
    }
 
} catch(err) {
 alert(Bawolff.review_i18n.unknownError + err.name + ': ' + err.message);
}
    return false;

}

Bawolff.review.failReview = function(r) {



    /*var cont = confirm('review failed. continue posting review?'); //for debuging
     *if (!cont) {throw new Error("User bailed [failed rev]");}*/

    //post {{tl|tasks|copyvio|news|src|npov|mos|re-review}} on page, peer review on talk.

    //First post peer review template on talk

    //generate content to add to talk page
    var reviewText = "<!-- " + Bawolff.review_i18n.review_comment + " --> \{\{" + Bawolff.review_i18n.review_peer_reviewed + "|" + Bawolff.review_i18n.review_revid + "=" + r.EasyReview_revid + "|" + Bawolff.review_i18n.review_copyright + "=" + r.EasyReview_copyright_comment + "|" + Bawolff.review_i18n.review_newsworthy + "=" + r.EasyReview_news_comment + "|" + Bawolff.review_i18n.review_verifiable + "=" + r.EasyReview_sources_comment + "|" + Bawolff.review_i18n.review_npov + "=" + r.EasyReview_npov_comment + "|" + Bawolff.review_i18n.review_style + "=" + r.EasyReview_sg_comment + "|" + Bawolff.review_i18n.review_reviewer + "=" + mw.config.get('wgUserName') + "|" + Bawolff.review_i18n.review_comments_field + "=" + r.EasyReview_comment + "|" + Bawolff.review_i18n.review_time + "=~~" + "~~" + "~}}";
    var reviewHeader = Bawolff.review_i18n.review_header_fail.replace(/\$1/, r.EasyReview_revid); 

    //callback to ajax that inserts review template on talk
    var talk_rev_cb = function (success) {
        if (success) {
            if (!Bawolff.review.isError()) {
                jsMsg2('<p><a class="image" href="/wiki/File:X_mark.svg"><img height="17" width="15" src="//upload.wikimedia.org/wikipedia/commons/thumb/a/a2/X_mark.svg/15px-X_mark.svg.png" alt="X mark.svg"/></a> ' + Bawolff.review_i18n.fail_review.replace(/\$1/g, mw.html.escape(mw.config.get('wgTitle')))  + '</p>');
            location = '#mw-js-message';
            }
        }
        else {
            throw new Error(Bawolff.review_i18n.softEditError);
        }

    }

    Bawolff.mwapi.edit({content: reviewText, page: mw.config.get('wgFormattedNamespaces')[1] + ":" + mw.config.get('wgTitle'), summary: reviewHeader, section: "new"}, talk_rev_cb);
    /***END posting peer review template to talk page ****/

    /***Start replace {{tl|review}} with {{tl|tasks}}****/

    //generate tasks tag. ({{tl|tasks|copyvio|news|src|npov|mos|re-review}})
    var tasks_template = "\{\{" + Bawolff.review_i18n.tasks;
    //for each one, if failed (if false) add the tag
    tasks_template += (r.EasyReview_copyright ? "" : "|" + Bawolff.review_i18n.tasks_copyvio);
    tasks_template += (r.EasyReview_news ? "" : "|" + Bawolff.review_i18n.tasks_news);
    tasks_template += (r.EasyReview_sources ? "" : "|" + Bawolff.review_i18n.tasks_src);
    tasks_template += (r.EasyReview_npov ? "" : "|" + Bawolff.review_i18n.tasks_npov);
    tasks_template += (r.EasyReview_sg ? "" : "|" + Bawolff.review_i18n.tasks_mos);
    tasks_template += "|" + Bawolff.review_i18n.tasks_rereview + "}}";

    //Get the page
    var final_article_text = ""; //string that contains page that will be posted to server (Pay attention to variable scope)
    var apiOps = new Bawolff.mwapi.AsyncQueue; //Container to chain api requests

    var getPage_cb = function (res) {
        //callback to do work on the current page (aka put article flag)
        var page = res[mw.config.get('wgTitle')]; //wgPageName is url escaped
        if (page === undefined) { //if not in main ns
            page = res[mw.config.get('wgCanonicalNamespace') + ":" + mw.config.get('wgTitle')];
        }
        if (page === undefined) { //badness happened
            throw new Error(Bawolff.review_i18n.errorFetchFailFail);
        }
        page = Bawolff.review_i18n.stripReviewForFail(page, tasks_template);

        if (page.indexOf(tasks_template) === -1) {
            page = tasks_template + page; // if the above regexs fail for some reason, add tasks to top.
        }
        final_article_text = page;
    }

    //add Obtain and modify page to queue of api operations
    Bawolff.mwapi.getPage(mw.config.get('wgPageName'), getPage_cb,  apiOps);

    /****end obtain and modify page***/
    /***start save page to server****/

    //this is really ugly.
    //wrap in function so it uses the variable in the outer function's scope (final_article_text) at time of execution

    var postChanges = function () {

        var edit_cb = function(success, resp) {
            if (!success) {
                var APIerror = resp.getElementsByTagName("error");
                var err = "Unknown Error (not API)"; //default
                if (APIerror.length !== 0) {
                    err = APIerror[0].getAttribute("info");
                }
                alert(Bawolff.review_i18n.taskError.replace(/\$1/, err));
            }

            apiOps.next(); //go to next action

        }

        Bawolff.mwapi.edit({content: final_article_text, page: mw.config.get('wgPageName'), summary: Bawolff.review_i18n.editSummary_failReview}, edit_cb);
    }

    apiOps.add(postChanges); //add to ops queue

    /******End incredibly ugly add {{tl|tasks}} to page, code*******/

    try {
        apiOps.start(); //start editing
    } catch (e) {
        alert(Bawolff.review_i18n.unknownError + e.message)
    }


}

Bawolff.review.postReview = function(r) {
    //argument: review object
    //called from form handler if review was SUCCESSFUL


   /***** DO WEIRD GOOGLE NEWS HACK ********/
   //FIXME: this is stupid
   //create a redirect with a numb in it.

if (Bawolff.review.doWeirdGoogleHack) {
    var googleOps = new Bawolff.mwapi.AsyncQueue;


    var google_cb = function(success, resp) {
        //alert("We reached the callback");
        //this is a callback for after done editing.
        if (!success) {
            var APIerror = resp.getElementsByTagName("error");
            if (APIerror.length !== 0) {
                var err = APIerror[0].getAttribute("info");
            }
        
            alert("Could not make a redirect for google news. Contact Bawolff. Error: "+ err + ". Continuing with rest of review process");
        }
        var editTags = resp.getElementsByTagName('edit');
        if (editTags && editTags[0] && (+editTags[0].getAttribute('newrevid')) > 2) {
            //so we sight the page.
            var GoogleRedirId = editTags[0].getAttribute('newrevid');
    Bawolff.mwapi.sight({revid: GoogleRedirId, level: '1', comment: "Making redirect for google with a really long number, as those google people like long numbers.(Using [[MediaWiki:Gadget-easyPeerReview.js]]) "});
        } else if (editTags && editTags[0] && _hasAttribute(editTags[0], 'nochange')) {
            alert('It appears this article already has a redirect for google. Please double check that [[Article/' + mw.config.get('wgArticleId') + "/" + mw.config.get('wgPageName') +']] is sighted properly');
        }
        else {
            var error = (editTags ? (editTags[0] ? "newrevid=" + editTags[0].getAttribute('newrevid') : "0th edit tag is false") : "Error getting edit elms");
            alert('Warning (Please leave user:Bawolff a note): Could not figure out revision id of google news redirect to article. Falling back to dying in a hole. you may have to sight the google redirect manually.\n--------------\nTechnical info: ' + error);
        }
    }

    Bawolff.mwapi.edit({content: "#Redirect[[" + mw.config.get('wgPageName') + "]]\n\n{\{Google News}}", page: "Article/" + mw.config.get('wgArticleId') + "/" + mw.config.get('wgPageName'), summary: "Add a redirect for google (Using [[MediaWiki:Gadget-easyPeerReview.js]])", minor: "true"}, google_cb);



}
/**********END GOOGLE NEWS HACK *******/

    /*var cont = confirm('review passed. continue posting review?'); //for debuging
    if (!cont) {throw new Error("User bailed [passed rev]");}*/

   // alert(reviewText); //make it do stuff later.
/**************
*Post changes to server.
*prereq: assume user has ability to edit page and to sight it
*first edit current page
**<nowiki>{{review}} -> {{publish}}</nowiki> (in correct position)
**sight that revision
**get rev number of published version, and update review template
**Post a new section named "Review" with the peer review template.
**************/



    var apiOps = new Bawolff.mwapi.AsyncQueue; //list of apiops we have to do.

    //Check if rev we are looking at is most current.
    var actual_rev;

    //Make a callback function that
    //checks if theres been an edit
    //and if so prompts the user as to wether to proceed.
 
    var checkEditConfilict_cb = function (res) {

        //Get the revid from the api query
        actual_rev = res.getElementsByTagName('rev')[0].getAttribute('revid');

        //if its different from what we're looking at, prompt if we want to continue.
        if (actual_rev != r.EasyReview_revid) {
            var keep_going = window.confirm(Bawolff.review_i18n.editConflict);

            if (!keep_going) {throw new Error(Bawolff.review_i18n.ErrorEditConflictCancel);}
            //fixme: should be some soft error thats less ugly
            //perhaps subclass of error thats recognized by mwapilib

        }
    }

    var checkEditConflict = new Bawolff.mwapi.Request({action:"query", prop: "revisions", titles: mw.config.get('wgPageName'), rvprop: "ids"});
    checkEditConflict.delaySend(apiOps, checkEditConfilict_cb);

    /***End checking for edit conflict ****/
    /***Start editng of article (add pub tag, rm review)*****/

    //Get the page
    var final_article_text = ""; //string that contains page that will be posted to server
    var getPage_cb = function (res) {
        //callback to do work on the current page (aka publish it)
        var page = res[mw.config.get('wgTitle')]; //wgPageName is url escaped
        if (page === undefined) { //if not in main ns
            page = res[mw.config.get('wgCanonicalNamespace') + ":" + mw.config.get('wgTitle')];
        }
        if (page === undefined) { //badness happened
            throw new Error(Bawolff.review_i18n.errorFetchFailureSuccess);
        }

        page = Bawolff.review_i18n.prePubTransform(page); //remove developing. fix date, etc

        //Add {{tl|publish}} (code slightly inelegant)

        var src = Bawolff.review_i18n.regex_sources;
        var endOfSrc = page.length;
        while (src.exec(page) !== null) {endOfSrc = src.lastIndex;}
        page = page.substring(0, endOfSrc) + "\n\n{\{" + Bawolff.review_i18n.publish + "}}" + page.substring(endOfSrc, page.length);
        
        //end insertion of publish template.
        final_article_text = page; //push to outside scope.
        if (final_article_text.length < 10) {
            //if result less than length of pub tag, wine.
            throw new Error(Bawolff.review_i18n.errorNoPub.replace(/\$1/, 'Regex Error'));
        }
        /***WWC code ***/
        
        if (Bawolff.review.doWWC) {
         Bawolff.review.countThoseChars(page);
        }
        if (mw.config.get('wgDBname') === 'enwikinews') {
         try {
            api().setDefaultSummary('Creating initial comment page from [[Wikinews:Commentary pages on news events/body|template]] (commentary header + useliquidthreads) via [[mediawiki:Gadget-easyPeerReview.js|EzPR]]').checkPageExists(mw.config.get('wgFormattedNamespaces')[102] + ':' + mw.config.get('wgPageName')).lift(function (exists) { return !exists ? true : confirm( Bawolff.review_i18n.commentPageExists ); }).abortIfFalse().getPage('Wikinews:Commentary pages on news events/body').replace(/[<>]\/?includeonly[<>]|<noinclude>.*?<\/noinclude>/g, '').savePage(mw.config.get('wgFormattedNamespaces')[102] + ':' + mw.config.get('wgPageName')).exec();
         } catch (e) {
           alert('Easy Peer Review experienced some issue when creating opinion page. Please tell [[user:Bawolff]] (include browser and technical details).\n--------\nTechnical details: ' + e.message); //just in case, should not happen
         }
        }




    }

    //add Obtain and modify page to queue of api operations
    Bawolff.mwapi.getPage(mw.config.get('wgPageName'), getPage_cb,  apiOps);

    /****end obtain and modify page***/
    /***start save page to server****/

    //this is really ugly.
    //wrap in function so it uses the global variable at time of execution

    var postChanges = function () {
       

        var edit_cb = function(success, resp) {
            if (!success) {
                var APIerror = resp.getElementsByTagName("error");
                if (APIerror.length !== 0) {
                    var err = APIerror[0].getAttribute("info");
                }
            
                alert(Bawolff.review_i18n.errorNoPub.replace(/\$1/, err));
            }
            var editTags = resp.getElementsByTagName('edit');
            if (editTags && editTags[0] && editTags[0].getAttribute('newrevid') > 2) {
                //so we sight the published revision
                Bawolff.review.newid = editTags[0].getAttribute('newrevid');
            } else {
                alert(Bawolff.review_i18n.warningNoArticleID);
            }
            apiOps.next(); //go to next action

        }

        Bawolff.mwapi.edit({content: final_article_text, page: mw.config.get('wgPageName'), summary: Bawolff.review_i18n.pub_edit_summary}, edit_cb);
    }

    apiOps.add(postChanges); //add to ops queue

    /******End incredibly ugly publish page code*******/
    //Sight the apropriate revision.

     try {
         Bawolff.sight_status = 4;
         Bawolff.mwapi.sight({revid: new Bawolff.LazyVar('Bawolff.review.newid'), level: '1', comment: Bawolff.review_i18n.editSummary_sight + r.EasyReview_comment },
             (function (res) { Bawolff.sight_status = 0; if (!res) {alert("Error sighting article"); Bawolff.sight_status = 5}}),
             undefined, apiOps);
     } catch (e) {
         alert('Something bad happened when trying to sight the article. Please tell [[user:Bawolff]]. You may have to sight this revision manually\n\n----------------\nDetails: revid: ' + Bawolff.review.newid + '; Error: ' + e.name + ': ' + e.message);
     }

    /******Start put review template on talk page*****/

    //generate template

    var reviewText = "<!-- " + Bawolff.review_i18n.review_comment + " --> \{\{" + Bawolff.review_i18n.review_peer_reviewed + "|" + Bawolff.review_i18n.review_revid + "=" + r.EasyReview_revid + "|" + Bawolff.review_i18n.review_copyright + "=" + r.EasyReview_copyright_comment + "|" + Bawolff.review_i18n.review_newsworthy + "=" + r.EasyReview_news_comment + "|" + Bawolff.review_i18n.review_verifiable + "=" + r.EasyReview_sources_comment + "|" + Bawolff.review_i18n.review_npov + "=" + r.EasyReview_npov_comment + "|" + Bawolff.review_i18n.review_style + "=" + r.EasyReview_sg_comment + "|" + Bawolff.review_i18n.review_reviewer + "=" + mw.config.get('wgUserName') + "|" + Bawolff.review_i18n.review_comments_field + "=" + r.EasyReview_comment + "|" + Bawolff.review_i18n.review_time + "=~~" + "~~" + "~}}";
    var reviewHeader = Bawolff.review_i18n.review_header_pass.replace(/\$1/, r.EasyReview_revid); 

    var talk_rev_cb = function (success) {
        if (success) {
            var ua = navigator.userAgent.toLowerCase();
            var sMesg = function () {
                jsMsg2('<p><a href="/wiki/File:Yes_check.svg" class="image" title="Yes check.svg"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Yes_check.svg/16px-Yes_check.svg.png" width="16" height="16" /></a> <b>' + Bawolff.review_i18n.done + '</b> ' + Bawolff.review_i18n.success.replace(/\$2/, ' <form action="' + Bawolff.review_i18n.success_ml_url + '" method="GET"><button type="submit" value="' + mw.html.escape(mw.config.get('wgTitle')) + '" name="use-page">' + (ua.indexOf('msie') === -1 ? Bawolff.review_i18n.success_ml : mw.html.escape(mw.config.get('wgTitle')) ) + '</button></form></p>').replace(/\$1/, '<i>' + mw.html.escape(mw.config.get('wgTitle')) + '</i>'));
                location = '#mw-js-message';
            }

            if ( Bawolff.sight_status !== 0 ) {
                if ( Bawolff.sight_status === 4 ) {
                     // maybe people navigate away before its done?
                     jsMsg2('<img src="//upload.wikimedia.org/wikipedia/commons/2/21/Throbber_allbackgrounds_stroopwafel.gif"> We\'re almost ready to review your article, I just need to finish my tea break. Please stand by...');
                     window.setTimeout( (function () {
                             if ( Bawolff.sight_status === 4 ) {
                                 window.setTimeout( arguments.callee, 250 );
                             } else {
                                 sMesg();
                             }
                         }), 250 );
                } else {
                    alert("For some reason, sighting the article failed. You'll need to manually sight.\n\n---------------\nSight error code: " + Bawolff.sight_status + ". (Please tell Bawolff this part)");
                }
            }
            else if (!Bawolff.review.isError()) {
                sMesg();
            }
        }
        else {
            throw new Error(Bawolff.review_i18n.softEditError);
        }

    }

    Bawolff.mwapi.edit({content: reviewText, page: mw.config.get('wgFormattedNamespaces')[1] + ":" + mw.config.get('wgTitle'), summary: reviewHeader, section: "new"}, talk_rev_cb, undefined, apiOps);

   /****End talk page editing ***/
   /****Start making edits!*****/
    try {
        apiOps.start(); //start editing
    } catch (e) {
        alert(Bawolff.review_i18n.unknownError + e.message)
    }

}

if ((mw.config.get('wgAction') === "view") && (mw.config.get('wgNamespaceNumber') === 0) && ((mw.config.get('wgUserGroups').toString().indexOf("editor") > -1) || (mw.config.get('wgUserGroups').toString().indexOf("facilitator") > -1))) {
    $(Bawolff.review);
}


Bawolff.review.isError = function () {
 //mw-js-message-mwapi-error
 var msg = document.getElementById('mw-js-message');
 return msg && (msg.className.indexOf('mw-js-message-mwapi-error') !== -1)
}

/**** WWC code ***/

Bawolff.review.countThoseChars = function (page) {
try {
//precond page =page contents.
api(page).push().lift(function (p) { return p.length;}).swap().
replace(/\<[^>]*\>/g, '').
replace(/^\s{0,4}(={1,6})([\s\S]*)\1\s*$/mg, "$2").
replace(/\'{2,5}/g, '').
replace(/\{\{[dD]ate\|[^\}]*\}\}/g, '').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/(\{\{[^\|}]*)\|(\|[^\}=]*\|)?[^\|\}=]*=([^\|}]*)((?:\|[^\}]*)?\}\})/g, '$1$2$4 $3 ').
replace(/\{\{[^\|}]*\}\}/g, '').
replace(/\{\{[^\|}]*\|([^\}]*)\}\}/g, ' $1 ').
replace(/\[http(?:s)?:\/\/\S*\s([^\]]*)]/g, ' $1 ').
replace(/http(?:s)?:\/\/\S*/g, '').
replace(/\[\[[iIfF][mMiI][aAlL][gGeE][eE]?\:[^\]]*\|([^\]]*)\]\]/g, ' $1 ').
replace(/\[\[[cC]ategory\:([^\]]*)\]\]/g, ' $1 ').
replace(/\[\[[^\|\]]*\|([^\]]*)\]\]/g, '$1').
replace(/\[\[([^\]]*)\]\]/g, '$1').
replace(/\s{2,}/g, ' ').
lift2(Bawolff.review.convertToPoints).exec();
} catch (e) {
//jsMsg(document.createTextNode('WWC error [step 1] [tell bawolff]:' + e.message));

alert('WWC error [step 1] [tell bawolff]:' + e.message);
}
}

Bawolff.review.convertToPoints = function (count, raw) {
try {
 count = count.length;
 var log = '\n# '
 var cats = document.getElementById('catlinks').getElementsByTagName('a');
 var OR, user, points, catName, userCat, inContest;
 for (var i = 0; i < cats.length; i++) {
 catName = cats[i].title
  if (catName.substring(catName.length - 12, catName.length) === "(Wikinewsie)") {
   user = catName.substring(9, catName.length - 13);
   userCat = catName;
  }
  if (catName === "Category:Original reporting") {
   OR = true;
  }
  if (catName === "Category:Writing Contests/May 2010") {
   inContest = true;
  }
 }
 var detailed;
 if (!userCat || !inContest) return; //not a contest entry
 log += '[[:' + userCat + '|' + user + ']] submitted: [[' + mw.config.get('wgTitle') + "]] for '''"; 
 if (count < 1200) {
  detailed = '1 pt [as short] ';
  points = 1;
 } else {
  detailed = '3 pts [as synth] ';
  points = 3;
 }
 if ((count - 1200)/700 > 0) {
  detailed += '+' + 2*Math.floor((count - 1200)/700) + 'pts [per extra length] ';
  points += 2*Math.floor((count - 1200)/700);
 }
 if (OR) {
  detailed += '+6 pts [as OR] ';
  points += 6;
 }
 if (document.getElementById('broadcast-report')) {
  detailed += '+4 pts [as Broadcast] ';
  points += 4;
 }

 log += points + "''' points. <small>(Normalized/Raw chars:" + count + '/' + raw + '; ' + detailed + ')</small>';

 api('Wikinews:May 2010 writing contest/log').setDefaultSummary('Appending to contest log [via EzPR]; ' + points + ' points for ' + user).getPage().lift(function (p) { return p + log}).savePage().exec();
} catch (e) {

alert('WWC error [step 2] [tell bawolff]:' + e.message);

}
}