// <pre>
/* ======================================================================== *\
	Adds a link that will allow one to show/hide sidebar
 
	version:		1.4.0
	copyright:		(C) 2006-2007 Maciej Jaros (pl:User:Nux, en:User:EcceNux)
	licence:		GNU General Public License v2,
				http://opensource.org/licenses/gpl-license.php
\* ======================================================================== */
 
//
// Cookie + default hidding management
//
if (window.hideSidebarByDefault)
{
	window.hideSidebarByDefault = (document.cookie.indexOf("js_hideSidebarByDefault=0")==-1);
}
else
{
	window.hideSidebarByDefault = (document.cookie.indexOf("js_hideSidebarByDefault=1")!=-1);
}
 
//
// Fast CSS init (faster because not run on load)
//
if (window.hideSidebarByDefault)
{
	document.write('<style type="text/css">'
		+'#content {margin-left: 0px; z-index: 100}'
		+'#p-logo {display: none;}'
		+'#p-cactions {left: 0px;}'
		+'#p-cactions a:hover {position:relative !important; z-index:101 !important;}'
		+'#p-search {position:relative; z-index:0;}'
		+'</style>'
	);
}
 
function initHideSidebar()
{
	var el = document.getElementById('content');
	var elNew = document.createElement('span');
	elNew.onclick = hideSidebar;
	el.style.position = 'relative';
	elNew.style.position = 'absolute'; // for IE.ver<7
	elNew.id = 'hideSidebarElement';
	elNew.style.cssText= 'cursor:pointer; color:#696; font-weight:bold; font-size:20px; left:0px; bottom:0px; padding:2px; z-index:2; position:fixed;';
 
	el.insertBefore(elNew, el.firstChild);
 
	//
	// mange hidding by deafault
	if (window.hideSidebarByDefault)
	{
		// for IE
		var el = document.getElementById('content');
		el.style.marginLeft = '0px';
		el.style.zIndex = '100'
 
		elNew.innerHTML = '»';
		elNew.sbShown = false;
	}
	else
	{
		elNew.innerHTML = '«';
		elNew.sbShown = true;
	}
 
	if (window.userInitHideSidebar != null)
	{
		userInitHideSidebar();
	}
}
$(initHideSidebar);
 
function hideSidebar()
{
	if (this.sbShown)
	//
	// hidding
	//
	{
		var el = document.getElementById('content');
		el.style.marginLeft = '0px';
		el.style.zIndex = '100'
 
		var el = document.getElementById('p-logo');
		el.style.display = 'none';
 
		var el = document.getElementById('p-cactions');
		el.style.left = '0px';
 
		this.innerHTML = '»';
		this.sbShown = false;
 
		document.cookie='js_hideSidebarByDefault=1; path=/';
	}
	//
	// showing
	//
	else
	{
		var el = document.getElementById('content');
		el.style.marginLeft = '12.2em';
		el.style.zIndex = '2'
 
		var el = document.getElementById('p-logo');
		el.style.display = 'block';
 
		var el = document.getElementById('p-cactions');
		el.style.left = '11.5em';
 
		this.innerHTML = '«';
		this.sbShown = true;
 
		document.cookie='js_hideSidebarByDefault=0; path=/';
	}
}
 
// </pre>