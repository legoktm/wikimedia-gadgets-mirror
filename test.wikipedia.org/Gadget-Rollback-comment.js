function addExtRbLink() {
    var rbnode = [], diffnode, index = {}, gebcn = document.getElementsByClassName
        ? function(a, b, c) { return a.getElementsByClassName(c) }
        : getElementsByClassName;
    if (typeof rollbackLinksDisable == 'object' && rollbackLinksDisable instanceof Array)
        for (var i = 0; i < rollbackLinksDisable.length; i++)
            index[rollbackLinksDisable[i]] = true;
    if (!('user' in index) && wgCanonicalSpecialPageName == "Contributions" ||
        !('recent' in index) && wgCanonicalSpecialPageName == "Recentchanges" ||
        !('watchlist' in index) && wgCanonicalSpecialPageName == "Watchlist")
        rbnode = gebcn(document.getElementById("bodyContent"), "span", "mw-rollback-link");
    else if (!('history' in index) && wgAction == "history")
        rbnode = gebcn(document.getElementById("pagehistory"), "span", "mw-rollback-link");
    else if (!('diff' in index) && (diffnode = document.getElementById("mw-diff-ntitle2")))
        rbnode = gebcn(diffnode, "span", "mw-rollback-link");
    for (var i = 0, len = rbnode.length; i < len; i++)
        addExtendedRollbackLink(rbnode[i]);
};
 
function confirmRollback() {
    var url = this.href;
    var user = url.match(/[?&]from=([^&]*)/);
    if (!user) return;
    user = decodeURIComponent(user[1].replace(/\+/g, " "));
    var summary = prompt("추가할 편집 요약을 입력하세요\n\n$user 는 편집이 되돌려질 사용자 이름으로 치환됩니다.",
                         rollbackSummaryDefault);
    if (summary == undefined)
        return false;
    else if (summary == "")
        return true;
    this.href += "&summary=" + '[[Special:Contributions/$2|$2]]([[User talk:$2|토론]])의 편집을 전부 되돌림: '.replace(/\$2/g, user) + encodeURIComponent(summary.replace(/\$user/g, user));
    return true;
};

function rollbackAsBot() {
    this.href += "&bot=1";
    return true;
}
 
function addExtendedRollbackLink(rbnode) {
    var rblink = rbnode.getElementsByTagName("a")[0];
    var alink = rblink.cloneNode(true);
    alink.className = "";
    alink.firstChild.nodeValue = "되돌리기 (+편집요약)";
    alink.onclick = confirmRollback;
    rbnode.insertBefore(alink, rblink.nextSibling);
    rbnode.insertBefore(document.createTextNode(" | "), alink);
    if (userIsInGroup('sysop'))
    {
        var blink = rblink.cloneNode(true);
        blink.className = "";
        blink.firstChild.nodeValue = "되돌리기 (+봇표시)";
        blink.onclick = rollbackAsBot;
        rbnode.insertBefore(blink, alink.nextSibling);
        rbnode.insertBefore(document.createTextNode(" | "), blink);
    }
};
if (typeof rollbackLinksDisable == 'undefined')
    rollbackLinksDisable = [];
if (typeof rollbackSummaryDefault == 'undefined')
    rollbackSummaryDefault = "";

$(addExtRbLink);

function userIsInGroup (group)
{
  if (wgUserGroups) {
    if (!group || group.length == 0) group = '*';
    return wgUserGroups.join (' ').indexOf (group) >= 0;
  }
  return false;
}