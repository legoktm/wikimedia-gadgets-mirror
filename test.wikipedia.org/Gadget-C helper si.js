C.si = {
  dialog: null,
  config: {
  	'request-page': 'User:0x010C/Wikipédia:Demande de suppression immédiate',
  	'deletion-template': '{{Suppression Immédiate|$2 - $3|~'+'~~}}\n',
    'modal-title': 'Chelper - Choix du critère de Supression Imédiate',
    'avert-label': 'Avertir le créateur de l\'article',
    'c-si-extra-class': 'c-si-extra',
    'c-si-radio-class': 'c-si-radio',
    'submit': 'valider la requête',
    'criteria-general-title': 'Critères généraux',
    'criteria-article-title': 'Critères spécifiques aux articles',
    'criteria-redirect-title': 'Critères spécifiques aux redirections',
    'criteria-file-title': 'Critères spécifiques aux fichiers',
    'criteria-category-title': 'Critères spécifiques aux catégories',
    'criteria-user-title': 'Critères spécifiques aux pages utilisateurs',
    'criteria-template-title': 'Critères spécifiques aux modèles',
    'criteria-portal-title': 'Critères spécifiques aux portails',
    'notify-sucess': 'La demande de Supression immédiate a bien été transmise.',
    'notify-no-reason': 'Vous n\'avez sélectionné aucun critère de supression.',
    'notify-not-a-page': 'Impossible de demander la Supression Immédiate d\'une page inexistante.',
    'notify-protected': 'Impossible de demander automatiquement la Supression Immédiate de cette page, celle-ci est protégé.',
    'request-title': '{{a|$1}}',
    'request-content': "* '''Demandé''' par ~~"+"~~\n* '''Motif''' : $2 - $3",
    'request-summary': 'Suppression immédiate demandée pour [[$1]] en raison du critère $2',
  },
  criteria: {
    general: [
      {code: 'G1', text: 'Page manifestement aberrante', help: 'Bac à sable Bac à sable '},
      {code: 'G2', text: '« Bac à sable »', help: 'Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable Bac à sable '},
      {code: 'G3', text: 'Vandalisme ou canular manifeste ', help: ''},
      {code: 'G4', text: 'Recréation d\'une page supprimée par décision PàS', help: '', extra: 'Lien vers la PàS', extra_default: '[[Discussion:'+mw.config.get('wgTitle')+'/Suppression]]'},
      {code: 'G5', text: 'Page créée par un contributeur banni ou bloqué', help: '', extra: 'Nom du contributeur problématique', extra_default: ''},
    ],
    article: [
      {code: 'A1', text: 'Article sans sujet clair', help: ''},
      {code: 'A2', text: 'Article en langue étrangère', help: ''},
      {code: 'A3', text: 'Article sans contenu', help: ''},
      {code: 'A5', text: 'Article transféré à un autre wiki', help: ''},
      {code: 'A10', text: 'Article récemment créé qui doublonne un article existant', help: '', extra: 'Lien vers l\'article antérieur', extra_default: ''},
      {code: 'A11', text: 'Article ne répondant pas aux critères d\'admissibilité', help: ''},
      {code: 'A12', text: 'Travail inédit manifeste', help: ''},
    ],
    redirect: [
      {code: 'R1', text: 'Redirection inappropriée, peu pertinente, voire incongrue', help: ''},
      {code: 'R2', text: 'Redirection renvoyant de l\'espace encyclopédique vers un autre espace de nom', help: ''},
      {code: 'R3', text: 'Graphie improbable', help: ''},
    ],
    file: [
      {code: 'F1', text: 'Fichier redondant', help: '', extra: 'Lien vers le fichier antérieur', extra_default: ''},
      {code: 'F2', text: 'Fichier image corrompu ou vide', help: ''},
      {code: 'F3', text: 'Licence inappropriée', help: ''},
      {code: 'F4', text: 'Pas d\'information sur la licence', help: ''},
      {code: 'F5', text: 'Image orpheline non libre de droit', help: ''},
      {code: 'F6', text: 'Motif justificatif absent, dans le cas d\'une utilisation non libre de droit', help: ''},
      {code: 'F7', text: 'Revendication injustifiée du fair use', help: ''},
      {code: 'F8', text: 'Image disponible à l\'identique dans Commons', help: '', extra: 'Lien vers le fichier sur commons', extra_default: ''},
      {code: 'F9', text: 'Violation manifeste de copyright', help: '', extra: 'Lien vers le fichier source copié', extra_default: ''},
      {code: 'F10', text: 'Fichier inutile sans média', help: ''},
      {code: 'F11', text: 'Aucune preuve d\'autorisation', help: ''},
    ],
    category: [
      {code: 'C1', text: 'Catégorie vide depuis au moins quatre jours', help: ''},
      {code: 'C2', text: 'Renommage ou fusion', help: ''},
      {code: 'C7', text: 'Renommage demandé par le créateur de la catégorie', help: ''},
    ],
    user: [
      {code: 'U1', text: 'Demande de l\'utilisateur', help: ''},
      {code: 'U2', text: 'Utilisateur inexistant', help: ''},
    ],
    template: [
      {code: 'M2', text: 'Modèle donnant une fausse image des règles', help: ''},
      {code: 'M3', text: 'Modèle sans intérêt ou doublonnant des modèles existants', help: ''},
    ],
    portal: [
      {code: 'P1', text: 'Portail qui serait supprimé s\'il s’agissait d\'un article', help: ''},
      {code: 'P2', text: 'Portail quasi-vide', help: ''},
    ],
  },
  init: function() {
  },
  launch: function() {
  	if(mw.config.get('wgArticleId') === 0) {
    	mw.notify(C.si.config['notify-not-a-page'], {title:'C-helper', type:'error'});
    	return;
  	}
  	if(mw.config.get('wgRestrictionEdit') == ["sysop"]) {
    	mw.notify(C.si.config['notify-protected'], {title:'C-helper', type:'error'});
    	return;
  	}
	if(this.dialog === null) {
		this['build-dialog']();
	}
	this.dialog.dialog("open");
  },
  'build-dialog': function() {
    this.dialog = $('<div/>', {title:C.si.config['modal-title']});
    var form = $('<form/>');
    this.dialog.append(form);
    form.append($('<input/>', {type:'checkbox', checked:'checked',id:'C-si-avert'})).append($('<label/>', {"for":"C-si-avert"}).html(C.si.config['avert-label']));
    
    
    var title = [];
    var namespace = [];
    
    if($("#redirectsub").length > 0) {
        title.push(this.config['criteria-redirect-title']);
        namespace.push(this.criteria.redirect);
    }
    
    switch(mw.config.get('wgNamespaceNumber')) {
      case 0:
        title.push(this.config['criteria-article-title']);
        namespace.push(this.criteria.article);
        break;
      case 6:
        title.push(this.config['criteria-file-title']);
        namespace.push(this.criteria.file);
        break;
      case 14:
        title.push(this.config['criteria-category-title']);
        namespace.push(this.criteria.category);
        break;
      case 2:
      case 3:
        title.push(this.config['criteria-user-title']);
        namespace.push(this.criteria.user);
        break;
      case 10:
        title.push(this.config['criteria-template-title']);
        namespace.push(this.criteria.template);
        break;
      case 100:
      case 101:
      case 102:
      case 103:
        title.push(this.config['criteria-portal-title']);
        namespace.push(this.criteria.portal);
        break;
    }
    
    title.push(this.config['criteria-general-title']);
    namespace.push(this.criteria.general);
    
    for (var i = 0; i < namespace.length; ++i) {
    	form.append($('<hr/>'));
    	form.append($('<h3/>').text(title[i]));
    	for (var j = 0; j < namespace[i].length; ++j) {
    		form.append($('<input/>', {type:'radio', id:'C-si-'+namespace[i][j].code, class:this.config['c-si-radio-class'], name:'C-si-criteria', explanation: namespace[i][j].text, code: namespace[i][j].code}))
    		    .append($('<label/>', {"for":'C-si-'+namespace[i][j].code}).text(namespace[i][j].code + ' : ' + namespace[i][j].text));
    		if(namespace[i][j].help !== '') {
    			form.append(C.util.construct_help_icon(namespace[i][j].help));
    		}
    		form.append($('<br/>'));
    		if(namespace[i][j].hasOwnProperty('extra')) {
    			extra_default = '';
    			if(namespace[i][j].hasOwnProperty('extra_default'))
    				extra_default = namespace[i][j].extra_default;
    			form.append($('<span/>', {id:'C-si-extra-'+namespace[i][j].code, class:this.config['c-si-extra-class']}).hide()
    			    .append($('<label/>', {"for":'C-si-extra-input-'+namespace[i][j].code}).html(namespace[i][j].extra))
    			    .append("&nbsp;")
    			    .append($('<input/>', {type:'text', id:'C-si-extra-input-'+namespace[i][j].code, value:extra_default}))
    			    .append($('<br/>')));
    		}
    	}
    }
    
    this.dialog.dialog({
      autoOpen: false,
      height: 400,
      width: 600,
      modal: true,
      buttons: [
        {
          text: C.si.config['submit'],
          click: function() {
            C.si.dialog.dialog("close");
            C.si.validate();
          },
        },
      ],
    dialogClass: 'c-helper-dialog',
    });
    
	$('.'+this.config['c-si-radio-class']).change(function() {
	        $('.'+C.si.config['c-si-extra-class']).hide();
	        $('#C-si-extra-'+$(this).attr('code')).show();
	});
  },
  validate: function() {
    var selected = $('input[name=C-si-criteria]:checked');
    if(selected.length === 0) {
    	mw.notify(C.si.config['notify-no-reason'], {title:'C-helper', type:'error'});
    	return;
    }
    page = mw.config.get('wgPageName').replace('_', ' ');
    reason = selected.attr('explanation');
    code = selected.attr('code');
    extra = $('#C-si-extra-input-'+code);
    if(extra.length > 0) {
    	console.log("abs");
    	if(extra.val() !== '') {
    		reason += ' ('+extra.val()+')';
    		console.log("trf");
    	}
    }
	C.util.prepend(
		null,
		C.si.config['deletion-template'].replace('$1', page).replace('$2', code).replace('$3', reason),
		C.si.config['request-summary'].replace('$1', page).replace('$2', code).replace('$3', reason),
    	function() {
	    	C.util.new_section(
	    		C.si.config['request-page'],
	    		C.si.config['request-title'].replace('$1', page).replace('$2', code).replace('$3', reason),
	    		C.si.config['request-content'].replace('$1', page).replace('$2', code).replace('$3', reason),
	    		C.si.config['request-summary'].replace('$1', page).replace('$2', code).replace('$3', reason),
	    		function() {
	    			mw.notify(C.si.config['notify-sucess'], {title:'C-helper', type:'info'});
	    			C.util.reload();
	    		}
	    	);
    	}
    );
  },
};

C.modules.si.callback = C.si;