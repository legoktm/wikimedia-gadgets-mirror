/* TEST BY [[User:Waihorace]] - For [[User:Waihorace/zhwpmp]] (New design of Chinese Wikipedia Homepage)
If removing this page is necessary, contact via [[User talk:Waihorace]] */

$(function() {
$( "#dialog-modal" ).dialog({
autoOpen: false,
modal: true,
show: {
effect: "blind",
duration: 100
},
hide: {
effect: "explode",
duration: 100
}
});
$( "#opener" ).click(function() {
$( "#dialog-modal" ).dialog( "open" );
});
});