window.Sharer = {};

Sharer.makeLink = function(url,label,title){
	var li = $('<li/>');
	var a = $('<a/>');
	a.attr('href',url);
	a.attr('title',title);
	a.attr('rel','nofollow');
	a.css('font-size','90%');
	a.text(label);
	li.append(a);
	return li;
}

Sharer.run = function(){
	if(mw.config.get('wgUserLanguage') == 'fa' && mw.config.get('wgNamespaceNumber') == 0)
	{
		var p_sharer = $('<div/>').attr('class','portal').attr('id','p-sharer');
		var h3 = $('<h3/>').text('Share').appendTo(p_sharer);
		var div = $('<div/>').attr('class','body').appendTo(p_sharer);
		var ul = $('<ul/>').appendTo(div);
		p_sharer.insertAfter($('#p-tb'));
		var container = $('#p-sharer div ul');
		var title = encodeURIComponent(mw.config.get('wgPageName'));
		var li = Sharer.makeLink('https://plus.google.com/share?hl=fa&url=http://test.wikipedia.org/wiki/' + title,'Google+','Share in Google+');
		container.append(li);
		var li = Sharer.makeLink('https://twitter.com/intent/tweet?original_referer=&related=test&via=test&url=http://test.wikipedia.org/wiki/' + title,'twitter','share in twitter');
		container.append(li);
		var li = Sharer.makeLink('https://www.facebook.com/sharer/sharer.php?u=http://test.wikipedia.org/wiki/' + title + '&t=' + title,'facebook','share in facebook');
		container.append(li);
		var li = Sharer.makeLink('whatsapp://send?text=http://test.wikipedia.org/wiki/' + title + '&t=' + title,'whatsapp','share in whatsapp');
		container.append(li);
	}
};

$(Sharer.run);