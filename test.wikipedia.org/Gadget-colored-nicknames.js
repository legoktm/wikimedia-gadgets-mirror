
// Author: [[pl:User:Beau]]

if (!cn$replacement)
	var cn$replacement = {};

if (!cn$class)
	var cn$class = {};

var coloredNicknamesGadget = {
	queries: 2,
	loaded: false,
	cache: {},
	version: 1,
	userLink: /(?:Wiki(?:pedysta|skryba|reporter)|U.+ytkownik|User)/,
	contribLink: /Specjalna:Wk/,

	init: function() {
		if (mw.config.get( 'wgNamespaceNumber' ) > -1 && mw.config.get( 'wgAction' ) != 'history')
			return;

		var that = this;

		var request = {
			action:	'query',
			list:	'allusers',
			augroup:	'sysop',
			aulimit:	'max',
			maxage:	43200,
			smaxage:	600,
			format:	'json'
		};
		jQuery.getJSON(mw.config.get( 'wgServer' ) + mw.config.get( 'wgScriptPath' ) + '/api.php', request, function(result) {
			that.addToList(result, 'nick_admin');
		});

		var request = {
			action:	'query',
			list:	'allusers',
			augroup:	'bot',
			aulimit:	'max',
			maxage:	43200,
			smaxage:	600,
			format:	'json'
		};
		jQuery.getJSON(mw.config.get( 'wgServer' ) + mw.config.get( 'wgScriptPath' ) + '/api.php', request, function(result) {
			that.addToList(result, 'nick_bot');
		});

		jQuery(document).ready(function() {
			that.loaded = true;
			if (that.queries == 0)
				that.doColor();
			}
		);
	},
	addToList: function(data, type) {
		for (id in data.query.allusers) {
			var nick = data.query.allusers[id].name;
			if (this.list[nick]) {
				this.list[nick].push(type);
			}
			else {
				this.list[nick] = new Array(type);
			}
		}
		this.queries--;
		if (this.queries == 0 && this.loaded)
			this.doColor();
	},
	getUserClass: function(nick) {
		var userClass = this.cache[nick];

		if (userClass) {
			return userClass;
		}
		userClass = new Array();

		var nc = cn$class[nick];
		if (nc)
			userClass.push(nc);

		if (this.list[nick]) {
			userClass = userClass.concat(this.list[nick]);
		}

		if (nick.match(/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/)) {
			userClass.push('nick_ip');
			if (this.isDynamic(nick)) {
				userClass.push('nick_dynamic_ip');
			}
		}
		this.cache[nick] = userClass;

		return userClass;
	},
	isDynamic: function(ip) {
		return false;
	},
	doColor: function() {
		this.queries = -1;

		if ((typeof dynamicIpsGadget) == 'object') {
			this.isDynamic = function(ip) { return dynamicIpsGadget.isDynamic(ip); }
		}

		var links = document.getElementsByTagName('a');

		for (var i = 0; i < links.length; i++)
		{
			var link = links[i];
			if (! link.href.match(this.userLink) && ! link.href.match(this.contribLink))
				continue;

			var replacement = cn$replacement[link.text];
			if (replacement)
				link.innerHTML = replacement;

			var userClass = this.getUserClass(link.text);
			if (userClass.length)
				link.className += ' ' + userClass.join(' ');
		}

		this.cache = {};
	}
};

coloredNicknamesGadget.list = {
	'Andre Engels':	['nick_steward'],
	'Avraham':	['nick_steward'],
	'Barras':	['nick_steward'],
	'Bastique':	['nick_steward'],
	'Bsadowski1':	['nick_steward'],
	'Darkoneko':	['nick_steward'],
	'DerHexer':	['nick_steward'],
	'Dferg':	['nick_steward'],
	'Dungodung':	['nick_steward'],
	'Eptalon':	['nick_steward'],
	'Erwin':	['nick_steward'],
	'Fr33kman':	['nick_steward'],
	'Guillom':	['nick_steward'],
	'J.delanoy':	['nick_steward'],
	'Jafeluv':	['nick_steward'],
	'Jusjih':	['nick_steward'],
	'Jyothis':	['nick_steward'],
	'Laaknor':	['nick_steward'],
	'Leinad':	['nick_steward'],
	'M7':	['nick_steward'],
	'Magister Mathematicae':	['nick_steward'],
	'Manuelt15':	['nick_steward'],
	'Mardetanha':	['nick_steward'],
	'Matanya':	['nick_steward'],
	'Mav':	['nick_steward'],
	'Melos':	['nick_steward'],
	'Mentifisto':	['nick_steward'],
	'Mercy':	['nick_steward'],
	'Millosh':	['nick_steward'],
	'Nick1915':	['nick_steward'],
	'Pathoschild':	['nick_steward'],
	'PeterSymonds':	['nick_steward'],
	'Ruslik0':	['nick_steward'],
	'Shanel':	['nick_steward'],
	'Shizhao':	['nick_steward'],
	'Wpedzich':	['nick_steward'],
	'Wutsje':	['nick_steward']
};

coloredNicknamesGadget.init();