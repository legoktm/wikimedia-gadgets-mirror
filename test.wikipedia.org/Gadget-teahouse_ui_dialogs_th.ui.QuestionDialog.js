
/*!
 * Teahouse user interface QuestionDialog class.
 */

/**
 * Dialog for submitting a question to the Teahouse board.
 *
 * @class
 * @extends OO.ui.ProcessDialog
 *
 * @constructor
 * @param {Object} [config] Configuration options
 */

mw.teahouse.util.registerType( 'th.ui.QuestionDialog' );

th.ui.QuestionDialog = function ThUiQuestionDialog( config, teahousecfg ) {
	this._config = teahousecfg;
	th.ui.QuestionDialog.super.call( this, config );
};
OO.inheritClass( th.ui.QuestionDialog, OO.ui.ProcessDialog );

th.ui.QuestionDialog.static.title = mw.message('th-dialog-title').plain();

th.ui.QuestionDialog.static.actions = [
	{
		action: 'save',
		label: mw.message('th-dialog-btn-ok').plain(),
		flags: 'primary',
		disabled: true
	},
	{
		label: mw.message('th-dialog-btn-cancel').plain(),
		flags: 'safe'
	}
];

th.ui.QuestionDialog.prototype.initialize = function () {
	th.ui.QuestionDialog.super.prototype.initialize.apply( this, arguments );

	this.content = new OO.ui.PanelLayout( {
		$: this.$,
		padded: true,
		scrollable: true,
		expanded: false
	} );

	this.tiQuestion = new OO.ui.TextInputWidget({
		classes: [ 'th-inputWidget', 'th-tiQuestion' ]
	});
	this.tiQuestion.on( 'change', this.onTiQuestionChange, [], this );
	var flQuestion = new OO.ui.FieldLayout(
		this.tiQuestion,
		{
			label: mw.message('th-dialog-label-summary').plain(),
			align: 'top'
		}
	);

	this.tiDesc = new OO.ui.TextInputWidget( {
		classes: [ 'th-inputWidget', 'th-tiDesc' ],
		multiline: true
	});

	var flDesc = new OO.ui.FieldLayout(
		this.tiDesc,
		{
			label: mw.message('th-dialog-label-text').plain(),
			align: 'top'
		}
	);

	this.$footerTable = $('<table>').addClass('th-footer-table');

	this.$licence = this.addFooterRow( mw.message('th-dialog-licence-html').plain(), 'info ');
	this.$anonIPHint = this.addFooterRow( mw.message('th-dialog-anon-ip-hint-html').plain(), 'info ');

	this.$similarQuestions = $('<div>');

	var anchor =  mw.html.element( 'a', {
		href: mw.util.getUrl( this._config.basePage ),
		title: this._config.basePage,
		target: '_blank'
	}, this._config.basePage );

	this.content.$element.append(
		$('<p>').append( mw.message('th-dialog-description-top', anchor).text() )
	);

	this.content.$element.append( flQuestion.$element );
	this.content.$element.append( this.$similarQuestions );
	this.content.$element.append( flDesc.$element );
	this.content.$element.append( this.$footerTable );

	this.$body.addClass('th-question-dialog-body');
	this.$body.append( this.content.$element );
};

th.ui.QuestionDialog.prototype.onTiQuestionChange = function( value ) {
	//If there is no question entered or it is way too short to be a question
	//we disable the "save" action
	this.getActions().setAbilities( {
		save: value.length > 3
	});

	if(value !== '' && value.length > 3) {
		if( !this.tiQuestion.isPending() ) {
			this.tiQuestion.pushPending();
		}

		var me = this;
		mw.teahouse.board.getSimilarQuestions( value )
			.done( function( items ){
				var $list = $('<ul>').addClass('th-similar-questions-list');
				for( var title in items ) {

					var anchor =  mw.html.element( 'a', {
						href: mw.util.getUrl( title ),
						title: title,
						target: '_blank'
					}, items[title] );

					$list.append( '<li>'+anchor+'</li>' );
				}

				if( $list.children().length > 0 ) {
					var $label = $('<span>').addClass( 'oo-ui-labelElement-label' )
						.append( mw.message('th-dialog-label-similar').plain() );
					me.$similarQuestions.append( $label );
					me.$similarQuestions.append( $list );
				}

				me.tiQuestion.popPending();
				me.getManager().updateWindowSize( me );
			});
			return;
	}

	this.$similarQuestions.empty();
	this.getManager().updateWindowSize( this );
};

th.ui.QuestionDialog.prototype.getSetupProcess = function ( openingData ) {
	//Hide the IP saving hint if user is anon.
	if( mw.user.isAnon() === false ) {
		this.$anonIPHint.hide();
	}
	return th.ui.QuestionDialog.super.prototype.getSetupProcess.call( this, openingData );
};

th.ui.QuestionDialog.prototype.getTeardownProcess = function ( closingData ) {
	//Reset the fields on close
	this.tiQuestion.setValue( '' );
	this.tiDesc.setValue( '' );

	return th.ui.QuestionDialog.super.prototype.getTeardownProcess.call( this, closingData );
};

th.ui.QuestionDialog.prototype.getActionProcess = function ( action ) {
	var me = this;

	var question = {
		title: this.tiQuestion.getValue(),
		text: this.tiDesc.getValue()
	};

	if ( action === 'save' ) {
		return new OO.ui.Process( function() {
			return mw.teahouse.board.publishQuestion( question )
				.done(function( editdata ) {
					//TODO: add a parameter whether to reset fields or not
					//This parameter will be passed to 'getTeardownProcess'
					me.close( { action: action } )
						.done(function( data ){

							var anchor = mw.html.element('a', {
								href: mw.util.getUrl( editdata.questionpage.title ),
								title: editdata.questionpage.title,
								target: '_blank'
							}, editdata.questionpage.title );

							var msg = mw.message('th-dialog-msg-text-save', anchor ).text();

							mw.teahouse.dialog.openMessageDialog({
								title: mw.message('th-dialog-msg-title-save').plain(),
								message: msg
							}, function ( opening ) {
								opening.then( function ( opened ) {
									opened.then( function ( data ) {
										if( data.action === 'yes' ) {
											window.location.href = mw.util.getUrl( me._config.basePage );
										}
									});
								});
							});
						});
				});
		});
	}

	return th.ui.QuestionDialog.super.prototype.getActionProcess.call( this, action );
};

th.ui.QuestionDialog.prototype.getBodyHeight = function () {
	return this.content.$element.outerHeight( true );
};
/**
 * Adds a row to the footer area of the dialog and returns it as jQuery object
 * @param string html
 * @param string icon (optional) an icon name
 * @returns {jQuery} The &lt;row&gt; element
 */
th.ui.QuestionDialog.prototype.addFooterRow = function( html, icon ){
	var $row = $('<tr>');
	this.$footerTable.append( $row );
	var $cell = $('<td>').addClass('th-text-cell').append( html );
	$row.append( $cell );

	if( icon ) {
		var icon = new OO.ui.IconWidget({
			icon: icon
		});
		$row.prepend( $('<td>').append( icon.$element ) );
	}
	else {
		$cell.attr( 'rowspan', 2 );
	}

	return $row;
};