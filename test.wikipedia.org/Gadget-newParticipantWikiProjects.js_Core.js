/*
 * Facilitates the inclusion of the a new participant to a WikiProject
 *
 * @author [[pt:User:!Silent]]
 * @date 16/abr/2013
 */
/*jslint browser: true, white: true, devel: true, continue: true, regexp: true, plusplus: true */
/*global mediaWiki, jQuery */

( function( mw, window, $ ) {
'use strict';

var	api = new mw.Api(),
	newParticipant_wikiProjects = {};

newParticipant_wikiProjects.run = function( $prompt, fields ) {
	var	$inputs = $( '#newParticipant_wikiProjects-dialog input[type="text"]' );

	$inputs.each( function( i ) {
		if ( $( this ).val() === '' && fields[ i ].search( /\[optional\]/ ) === -1 ) {
			$( this ).addClass( 'newParticipant_wikiProjects-fill-field' );
		} else {
			$( this ).removeClass( 'newParticipant_wikiProjects-fill-field' );
		}
	} );

	if ( $inputs.hasClass( 'newParticipant_wikiProjects-fill-field' ) ) {
		return;
	}

	mw.notify( 'Obtendo o conteúdo da página.' );

	api.getCurrentPageText().done( function( actualText ) {
		var	info = [];

		$inputs.each( function() {
			info.push( $( this ).val() );
		} );

		mw.notify( 'Editando a página...' );

		api.editPage( {
			watchlist: 'preferences',
			minor: true,
			text: actualText
				.split( '|-\n|}' )
				.join(
					'\n|-\n| {{Usuário2|'
					+ mw.config.get( 'wgUserName' ) + '}}\n| '
					+ info.join( '\n| ' )
					+ '\n|-\n|}'
				),
			done: {
				success: function() {
					location.href = mw.util.getUrl() + '?npwp=success';
				},
				apiError: function( error ) {
					mw.notify(
						$.parseHTML(
							'Não foi possível editar a página.<br />A API retornou o código de erro "'
							+ error.code + '": ' + error.info + '.<br />Se o problema persistir,'
							+ ' favor informar no <a href="' + mw.util.getUrl( 'WP:Café dos Programadores' ) + '">Café dos programadores</a>'
						)
					);
				},
				unknownError: function() {
					mw.notify( 'Não foi possível editar a página devido a um erro desconhecido da API.' );
				}
			}
		} ).fail( function() {
			mw.notify( 'Não foi possível fazer uma requisição com o servidor. Verifique sua conexão com a internet.' );
		} );
	} );

	$prompt.dialog( 'close' );
};

newParticipant_wikiProjects.prompt = function() {
	var	i,
		fields = $( '#newParticipant_wikiProjects-fields' ).text().replace( /\n/g, '' ).split( /\d=/g ),
		$prompt = $( '<div id="newParticipant_wikiProjects-dialog" class="ui-widget"></div>' );

	fields.shift();

	for ( i = 0; i < fields.length; i++ ) {
		$prompt.append(
			fields[ i ].replace( '[optional]', '' ).replace(/\s+$/, '')
				+ ': <input type="text" id="newParticipant_wikiProjects-field-' + i + '" /><br />'
		);
	}

	$prompt.dialog( {
		title: 'Preencha os campos abaixo',
		width: 'auto',
		height: 'auto',
		modal: true,
		open: function() {
			$( '.ui-dialog-titlebar-close' ).hide();
		},
		close: function() {
			$( '#newParticipant_wikiProjects-dialog' )
				.dialog( 'destroy' )
				.remove();
		},
		buttons: {
			'OK': function() {
				newParticipant_wikiProjects.run( $prompt, fields );
			},
			'Cancelar': function() {
				$prompt.dialog( 'close' );
			}
		}
	} );
};

newParticipant_wikiProjects.init = function() {
	if ( !$( '#newParticipant_wikiProjects-fields' ).length
		&& !$( '#newParticipant_wikiProjects-prompt' ).length
	) {
		return;
	}

	if ( mw.util.getParamValue( 'npwp' ) === 'success' ) {
		mw.notify( 'Seu nome foi inserido na lista de participantes com sucesso.' );
		window.history.pushState( {}, 'npwp', mw.util.getUrl() );
	}

	$( '#newParticipant_wikiProjects-openDialog' ).click( function() {
		newParticipant_wikiProjects.prompt();
	} );
};

newParticipant_wikiProjects.init();

}( mediaWiki, window, jQuery ) );