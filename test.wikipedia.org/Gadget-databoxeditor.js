/*
======================================================
                      ATTENTION
======================================================
Ce script est en test. Il ne sert actuellement qu'à
démontrer les possibilités techniques existante pour
éditer les données présentes dans les infobox, provenant
de Wikidata.
Ce qu'il permet actuellement :
* éditer des propriétés utilisant d'autres items (avec une recherche intégré)
  directement depuis l'infobox les utilisant sur Wikipédia.
Si vous cherchez un outil abouti, je vous conseil :
https://ru.wikipedia.org/w/index.php?title=MediaWiki:WEF AllEditors.js
*/

/*
Q = {
  code:"Q123582",
  content:{}
};

P = {
  code:"P403",
  name:"",
  DT:"",
  C:[
    {
      GUID:"",
      V:{}
    }
  ]
};
*/


  /**************************************************
  * Requête pour récupérer le nom de la propriéré P
  * Extraire le nom
  * Si pas en cache
  ** Requête pour récupérer les données de l'entité
  ** Mettre en cache le résultat
  * Extraire :
  ** Le GUID
  ** Le Datatype
  * En fonction du datatype (switch) :
  ** Récupérer les valeurs
  ** Afficher le formulaire adéquat
  **************************************************/
//GUID = E.claims[P][0].id;

Q = {};
P = {};

Q.code = $("#t-wikibase a").attr("href").split("Q")[1];




/*
 * On change les liens + qui mènent à Wikidata
 */

function databoxeditor_changelinks() {
	WDlinks = $('.infobox_v3 img[src="//upload.wikimedia.org/wikipedia/commons/thumb/7/73/Blue_pencil.svg/10px-Blue_pencil.svg.png"]').parent();
	$.each(WDlinks, function(key, WDlink){
	  $(WDlink).attr("href", "javascript:databoxeditor_click(\""+$(WDlink).attr("href").split("#")[1]+"\");");
	});
}
databoxeditor_changelinks();

/*
 * On prépare la dialogbox
 */

var dialog;

$("body").append('<form id="databoxeditor-form"></form>');
databoxeditor_form = $("#databoxeditor-form");
dialog = databoxeditor_form.dialog({
  autoOpen: false,
  height: 250,
  width: 700,
  modal: true,
  title:'DataboxEditor',
  buttons: {
    "Modifier": function() {
      databoxeditor_validate();
      dialog.dialog( "close" );
    },
    "Annuler": function() {
      dialog.dialog( "close" );
    }
  },
  open: function() {
    databoxeditor_form.empty();
    databoxeditor_form.append('<center><img style="margin-top:5px;" src="//upload.wikimedia.org/wikipedia/commons/b/b4/Loading_Animation.gif" alt="" title="Wait"/></center>');
  }
});

/*
 * Lorsque l'utilisateur clic sur l'un des liens
 */

function databoxeditor_click(p) {
  P.code = p;
  get_propriety_name();
}

function get_propriety_name(){
  $.ajax( {
      'url': 'https://test.wikidata.org/w/api.php',
      'data': {
        'action':'wbgetentities',
        'ids':P.code,
        'format': 'json',
        'origin': 'https://test.wikipedia.org'
      },
      'xhrFields': {
          'withCredentials': true
      },
      'dataType': 'json',
      'success': function( data ) {
          P.name = data.entities[P.code].labels.fr.value;
          dialog.dialog("option", "title", P.name);
          dialog.dialog( "open" );
          P.DT = data.entities[P.code].datatype;
          get_entity();
      }
  } );
}

function get_entity(){
  $.ajax( {
      'url': 'https://test.wikidata.org/w/api.php',
      'data': {
        'action':'wbgetclaims',
        'entity':'Q'+Q.code,
          'format': 'json',
          'origin': 'https://test.wikipedia.org'
      },
      'xhrFields': {
          'withCredentials': true
      },
      'dataType': 'json',
      'success': function( data ) {
          Q.content = data;
          P.C = [];
          $.each(Q.content.claims[P.code], function(key, claim){
            P.C[key] = {};
            P.C[key].GUID = claim.id;
            P.C[key].V = claim.mainsnak.datavalue.value;
          });
          display_dialog();
      }
  } );
}

function display_dialog(){
  databoxeditor_form.empty();
  switch(P.DT){
    case "string":
      
      break;
    case "wikibase-item":
      $.each(P.C, function(key, claim) {
        $.ajax( {
            'url': 'https://test.wikidata.org/w/api.php',
            'data': {
              'action':'wbgetentities',
              'ids':'Q'+claim.V["numeric-id"],
              'languages':'fr',
              'props':'labels',
              'format': 'json',
              'origin': 'https://test.wikipedia.org'
            },
            'xhrFields': {
                'withCredentials': true
            },
            'dataType': 'json',
            'success': function( data ) {
               var entityid = "Q"+claim.V["numeric-id"];
               if(data.entities[entityid].labels === undefined)
                  displayed = entityid;
                else
                  displayed = data.entities[entityid].labels.en.value;
                databoxeditor_form.prepend('<fieldset><center><input style="margin-left:5px;width:64%;" type="text" id="databoxeditor_wikibase_item'+key+'" class="'+key+'" value="'+displayed+'"/><input type="text" disabled="disabled" id="databoxeditor_value'+key+'" value="'+entityid+'" style="width:30%;"/></center></fieldset>');
                addAutocomplete('#databoxeditor_wikibase_item'+key, '#databoxeditor_value'+key);
            }
        } );
      });
      break;
    case "time":
      
      break;
    case "monolingualtext":
      
      break;
    case "url":
      
      break;
    case "commonsMedia":
      
      break;
    case "globecoordinate":
      
      break;
  }
}

function addAutocomplete(idInput, idDisplay){
  $( idInput ).autocomplete({
    minLength: 0,
    source: function(request, response) {
      $.ajax( {
        'url': 'https://test.wikidata.org/w/api.php',
        'data': {
          'action':'wbsearchentities',
          'search':request.term,
          'language':'fr',
          'limit':'8',
          'format': 'json',
          'origin': 'https://test.wikipedia.org'
        },
        'xhrFields': {
          'withCredentials': true
        },
        'dataType': 'json',
        'success': function( data ) {
          list = [];
          $.each(data.search, function(key, value){
            list.push({
              label:value.label,
              id:value.id,
              desc:value.description
            });
          });
          response(list);
        }
      });
    },
    /*focus: function( event, ui ) {
           $( "#hey" ).val( ui.item.label );
           return false;
         },*/
    select: function( event, ui ) {
      $(idInput).val(ui.item.label);
      $(idDisplay).attr("value", ui.item.id);

      return false;
    }
  });
  $( idInput ).autocomplete().data("uiAutocomplete")._renderItem = function( ul, item ) {
    return $( "<li>" ).append( "<a><b>" + item.label + "</b><br>" + item.desc + "</a>" ).appendTo( ul );
  };
}

function databoxeditor_validate() {
  P.V = {};
  switch(P.DT){
    case "string":
      
      break;
    case "wikibase-item":
      $.each(P.C, function(key, claim) {
        P.C[key].V = '{"entity-type":"item","numeric-id":'+$("#databoxeditor_value"+key).attr("value").split("Q")[1]+'}';
        console.log(P.C[key]);
      });
      break;
    case "time":
      
      break;
    case "monolingualtext":
      
      break;
    case "url":
      
      break;
    case "commonsMedia":
      
      break;
    case "globecoordinate":
      
      break;
  }
  save_entity();
}


function save_entity(){
  $.ajax( {
    url: 'https://test.wikidata.org/w/api.php',
    data: {
      action: 'query',
      meta: 'tokens',
      format: 'json',
      origin: 'https://test.wikipedia.org'
    },
    xhrFields: {
      withCredentials: true
    },
    dataType: 'json'
  }).then( function ( data ) {
    $.ajax( {
      url: 'https://test.wikidata.org/w/api.php',
      method: 'POST',
      data: {
        action: 'wbsetclaimvalue',
        claim:    P.C[0].GUID,
        snaktype: 'value',
        value:    P.C[0].V,
        summary: 'via [[:fr:User:0x010C/script/DataboxEditor.js|DataboxEditor]] from frwiki',
        format: 'json',
        origin:   'https://test.wikipedia.org',
        token: data.query.tokens.csrftoken
      },
      xhrFields: {
        withCredentials: true
      },
      dataType: 'json',
      success:  function( data ) {
      	console.log(data);
        mw.notify("Saved");
        P.C.shift();
        if(P.C.length > 0)
          save_entity();
        else
          document.location.href = mw.config.get('wgServer')+mw.config.get('wgArticlePath').replace("$1", mw.config.get('wgPageName'))+"?action=purge";
      }
    });
  });
}