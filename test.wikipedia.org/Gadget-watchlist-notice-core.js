/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 */
/** Add dismiss buttons to watchlist-message
 * Allows multiple dismiss buttons on [[MediaWiki:Watchlist-details]] with bumpable cookie IDs.
 * Note: HTML is backwards compatible with old version, new version ignores old syntax, except for dismissed IDs.
 * @author: [[:en:User:Ruud Koot]]
 * @author: [[:en:User:MZMcBride]]
 */
/*jslint white: true, regexp: true */
/*global jQuery, mediaWiki */
( function ( mw, $ ) {
'use strict';

function dismissWatchlistMessage( event ) {
  var $message = $( this ).closest( '.watchlist-message' );
  var cid = $( this ).data( 'watchlistMessage' ).cid;
  $message.hide();

  var e = new Date();
  e.setTime( e.getTime() + (4*7*24*60*60*1000) );
  document.cookie = 'hidewatchlistmessage-' + cid + '=yes; expires=' + e.toGMTString() + '; path=/';
  
  event.preventDefault();
}

function addDismissButton() {
  var watchItems = $( 'div.watchlist-message' );
  if ( watchItems.length === 0) {
  	watchItems = $( 'li.watchlist-message' );
  }
  if ( watchItems.length === 0) {
  	return;
  }
  for ( var i = 0; i < watchItems.length; i++ ) {
    var watchlistCookieID = parseInt( watchItems[i].className.replace( /.*cookie\-ID\_(\d*).*/ig, '$1' ) );
    if ( isNaN( watchlistCookieID ) ) {
    	continue;
    }
    if ( document.cookie.indexOf( 'hidewatchlistmessage-' + watchlistCookieID + '=yes' ) != -1 ) {
      watchItems[i].style.display = 'none';
      continue;
    }
    var Button     = document.createElement( 'span' );
    var ButtonLink = document.createElement( 'a' );
    var ButtonText = document.createTextNode( 'dismiss' );

    ButtonLink.className = 'dismissButton';
    ButtonLink.setAttribute( 'href', '#' );
    ButtonLink.setAttribute( 'title', 'Hide this message' );
    ButtonLink.appendChild( ButtonText );
    $( ButtonLink ).data( 'watchlistMessage', {
    	index: i,
    	cid: watchlistCookieID
    } );
    $( ButtonLink ).click( dismissWatchlistMessage );

    Button.appendChild( document.createTextNode(' [' ) );
    Button.appendChild( ButtonLink );
    Button.appendChild( document.createTextNode( ']' ) );
    watchItems[i].appendChild( Button );
  }
  
  $( '#watchlist-message' ).show();
}

$( addDismissButton );

}( mediaWiki, jQuery ) );