/*
* Tutoriel interactif : L'interface
* Plan et textes sur [[Projet:Tutoriels#L’interface]]
*
* Voir [[:mw:Extension:GuidedTour]] pour plus d'informations
*
* Auteur : [[User:0x010C]]
* [[Catégorie:Guided tour]]
*/

( function ( window, document, $, mw, gt ) {
	var tour;

	tour = new gt.TourBuilder( {
		name: 'historique',
		shouldLog: false
	} );

	// 1
	tour.firstStep( {
		name: 'bienvenue',
		title: 'L\'historique',
		description: '',
		overlay: true,
		closeOnClickOutside: false,
	} )
	.next( function() {
		window.location.href = mw.util.getUrl( 'Test1' );
	} )
	.transition( function() {
		if ( mw.config.get( 'wgPageName' ) === 'Test1' )
			return 'onglet';
	} );
	
	// 2
	tour.step( {
		name: 'onglet',
		title: 'Ouvrir l\'historique d\'une page',
		description: '',
		attachTo: '#ca-history',
		position: 'bottomLeft',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( function() {
		gt.setTourCookie( 'historique', 'bienvenue' );
		window.location.href = mw.util.getUrl( 'Aide:Tutoriels/Historique' );
	} )
	.transition( function() {
		if ( mw.config.get( 'wgAction' ) === 'history' )
			return 'presentation';
	} );
	
	// 3
	tour.step( {
		name: 'presentation',
		title: 'Qu\'est-ce que l\'historique ?',
		description: '',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( function() {
		gt.setTourCookie( 'historique', 'bienvenue' );
		window.location.href = mw.util.getUrl( 'Test1' );
	} )
	.next( 'ligne' );
	
	// 4
	tour.step( {
		name: 'ligne',
		title: 'Une ligne d\'historique',
		description: '',
		attachTo: '#pagehistory li:nth-child(2)',
		position: 'top',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'presentation' )
	.next( 'lienversion' );
	
	// 5
	tour.step( {
		name: 'lienversion',
		title: 'Chaque version est consultable',
		description: '',
		attachTo: '#pagehistory li:nth-child(2) .mw-changeslist-date',
		position: 'topRight',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'ligne' )
	.next( 'liendiff' );
	
	// 6
	tour.step( {
		name: 'liendiff',
		title: 'Voir les différences entre deux versions',
		description: '',
		attachTo: '#pagehistory li:nth-child(2) .mw-history-histlinks a:nth-child(2)',
		position: 'right',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'lienversion' )
	.transition( function() {
		if ( mw.config.get( 'wgAction' ) === 'view' && $( '.diff' ).length ) {
			return 'diff';
		}
	} );
	
	// 7
	tour.step( {
		name: 'diff',
		title: 'Historique : différence entre versions',
		description: '',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( function() {
		gt.setTourCookie( 'historique', 'liendiff' );
		window.location.href = mw.util.getUrl( 'Test1', { action: 'history' } );
	} )
	.next( 'annuler' );
	
	// 8
	tour.step( {
		name: 'annuler',
		title: 'Annuler une modification',
		description: '',
		attachTo: '.mw-diff-undo',
		position: 'bottomLeft',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'diff' )
	.next( function() {
		window.location.href = mw.util.getUrl( 'Test1', { action: 'history' } );
	} )
	.transition( function() {
		if ( mw.config.get( 'wgAction' ) === 'history' )
			return 'blame';
	} );
	
	// 9
	tour.step( {
		name: 'blame',
		title: 'Rechercher l\'auteur d\'un passage de l\'article',
		description: '',
		attachTo: '.mw-history-legend a:nth-child(2)',
		position: 'bottom',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'liendiff' )
	.next( function() {
		window.location.href = mw.util.getUrl( 'Aide:Tutoriels/Historique' );
	} )
	.transition( function() {
		if ( mw.config.get( 'wgPageName' ) === 'Aide:Tutoriels/Historique' )
			return 'fin';
	} );
	

	// 10	
	tour.step( {
		name: 'fin',
		title: 'Fin',
		description: '',
		autoFocus: true,
		overlay: true,
		closeOnClickOutside: false,
		buttons: [ {
			action: 'end'
		} ],
		allowAutomaticOkay: false,
	} );

	window.tour = tour;
	
	//Ajout d'un bouton en bas à droite pour permettre de réafficher une étape accidentellement fermé
	mw.loader.using( [ 'oojs-ui' ], function () {
		var reloadButton = new OO.ui.ButtonWidget( {
			label: 'Réafficher l\'étape courante',
			icon: 'redo',
			iconTitle: 'Réafficher'
		} );
		reloadButton.on( 'click', function() {
			gt.launchTourFromUserState();
		} );
		var container = $( '<div>' );
		container.append( reloadButton.$element[ 0 ] );
		container.css( 'position', 'fixed' ).css( 'bottom', '0px' ).css( 'right', '0px' ).css( 'z-index', '3000' ).appendTo( 'body' );
	} );

} ( window, document, jQuery, mediaWiki, mediaWiki.guidedTour ) );