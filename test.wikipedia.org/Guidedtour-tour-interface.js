/*
* Guidedtour #2 du WikiMOOC
* Interface des articles
*
* Voir [[:mw:Extension:GuidedTour]] pour plus d'informations
*
* Auteur : [[User:0x010C]]
* [[Catégorie:Guided tour]]
*/
//<nowiki>

( function ( window, document, $, mw, gt ) {
	var tour;
	// Prepare some hack to place correctly some boxes on step 2
	if ( mw.config.get( 'wgPageName' ) == 'Fanfreluche' ) {
		$( 'h1' ).html( 'Framboise<span id="gt-interface-titre"></span>' );
	}

	tour = new gt.TourBuilder( {
		name: 'interface',
		shouldLog: false
	} );

	// 1
	tour.firstStep( {
		name: 'bienvenue',
		title: 'Bienvenue !',
		description: new mw.Title( 'Page for bots 1' ),
		overlay: true,
		closeOnClickOutside: false,
	} )
	.next( function() {
		window.location.href = mw.util.getUrl( 'Fanfreluche' );
	} )
	.transition( function() {
		if ( mw.config.get( 'wgPageName' ) == 'Fanfreluche' )
			return 'titre';
	} );
	
	// 2
	tour.step( {
		name: 'titre',
		title: 'Le titre de la page',
		description: new mw.Title( 'Page for bots 2' ),
		attachTo: '#gt-interface-titre',
		position: 'right',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'bienvenue' )
	.next( 'onglets' );
	
	// 3
	tour.step( {
		name: 'onglets',
		title: 'Une interface constante',
		description: new mw.Title( 'Page for bots 3' ),
		attachTo: '#ca-nstab-main',
		position: 'bottomRight',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'titre' )
	.next( 'discussion' );
	
	// 4
	tour.step( {
		name: 'discussion',
		title: 'Articles et discussions',
		description: new mw.Title( 'Page for bots 4' ),
		attachTo: '#ca-talk',
		position: 'bottomRight',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'onglets' )
	.transition( function() {
		if ( mw.config.get( 'wgPageName' ) === 'Talk:Fanfreluche' ) {
			return 'revenirarticle';
		}
	} );
	
	// 5
	tour.step( {
		name: 'revenirarticle',
		title: 'Articles et discussions',
		description: new mw.Title( 'Page for bots 5' ),
		attachTo: '#ca-nstab-main',
		position: 'bottomRight',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.transition( function() {
		if ( mw.config.get( 'wgPageName' ) === 'Fanfreluche' ) {
			return 'modifier';
		}
	} );
	
	// 6
	tour.step( {
		name: 'modifier',
		title: 'Modifier la page',
		description: new mw.Title( 'Page for bots 6' ),
		attachTo: '#ca-ve-edit',
		position: 'bottom',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'discussion' )
	.next( 'historique' );
	
	// 7
	tour.step( {
		name: 'historique',
		title: 'L\historique',
		description: new mw.Title( 'Page for bots 7' ),
		attachTo: '#ca-history',
		position: 'bottom',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'modifier' )
	.next( 'lds' );
	
	// 8
	tour.step( {
		name: 'lds',
		title: 'Suivre une page',
		description: new mw.Title( 'Page for bots 8' ),
		attachTo: '#ca-watch',
		position: 'bottom',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'historique' )
	.next( 'interlangues' );
	
	// 9
	tour.step( {
		name: 'interlangues',
		title: 'Pour les polyglottes',
		description: new mw.Title( 'Page for bots 9' ),
		attachTo: '#p-lang-label',
		position: 'rightTop',
		autoFocus: true,
		closeOnClickOutside: false,
	} )
	.back( 'lds' )
	.next( function() {
		window.location.href = mw.util.getUrl( 'Wiktionnaire:Tutoriel/Interface' );
	} )
	.transition( function() {
		if(mw.config.get( 'wgPageName' ) === 'Wiktionnaire:Tutoriel/Interface' )
			return 'fin';
	} );

	// 10	
	tour.step( {
		name: 'fin',
		title: 'Fin',
		description: new mw.Title( 'Page for bots 10' ),
		autoFocus: true,
		overlay: true,
		closeOnClickOutside: false,
		buttons: [ {
			action: 'end'
		} ],
		allowAutomaticOkay: false,
	} );

	window.tour = tour;
	
	//Ajout d'un bouton en bas à droite pour permettre de réafficher une étape accidentellement fermé
	mw.loader.using( [ 'oojs-ui' ], function () {
		var reloadButton = new OO.ui.ButtonWidget( {
			label: 'Réafficher l\'étape courante',
			icon: 'redo',
			iconTitle: 'Réafficher'
		} );
		reloadButton.on( 'click', function() {
			gt.launchTourFromUserState();
		} );
		var container = $( '<div>' );
		container.append( reloadButton.$element[ 0 ] );
		container.css( 'position', 'fixed' ).css( 'bottom', '0px' ).css( 'right', '0px' ).css( 'z-index', '3000' ).appendTo( 'body' );
	} );

} ( window, document, jQuery, mediaWiki, mediaWiki.guidedTour ) );

//</nowiki>