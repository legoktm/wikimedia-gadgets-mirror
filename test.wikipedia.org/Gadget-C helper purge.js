C.purge = {
  init: function() {
  },
  launch: function() {
  	if(mw.config.get('wgNamespaceNumber') >= 0) {
  		window.location = mw.util.getUrl(null, {action: 'purge'});
  	}
  },
};

C.modules.purge.callback = C.purge;