$( function() {
	'use strict';
	var
		iwlinks = $( 'div.interProject' ),
		pproject,
		wb = $( '#p-wikibase-otherprojects' ),
		wbul;
	if ( !iwlinks.length ) {
		return;
	}
	if ( wb.length ) {
		wbul = wb.find( 'ul' );
		if ( !wbul.length ) {
			return;
		}
		iwlinks.each( function( i, e ) {
			var
				t = $( e ),
				a = t.find( 'a' );
			if ( t.hasClass( 'commons' ) && !wb.find( '.wb-otherproject-commons' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wiktionary' ) && !wb.find( '.wb-otherproject-wiktionary' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikinews' ) && !wb.find( '.wb-otherproject-wikinews' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikisource' ) && !wb.find( '.wb-otherproject-wikisource' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikibooks' ) && !wb.find( '.wb-otherproject-wikibooks' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikiquote' ) && !wb.find( '.wb-otherproject-wikiquote' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikiversity' ) && !wb.find( '.wb-otherproject-wikiversity' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikidata' ) && !wb.find( '.wb-otherproject-wikidata' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikivoyage' ) && !wb.find( '.wb-otherproject-wikivoyage' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'wikispecies' ) && !wb.find( '.wb-otherproject-species' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
			else if ( t.hasClass( 'meta' ) && !wb.find( '.wb-otherproject-meta' ).length ) {
				a.unwrap().wrap( '<li class="' + t.attr( 'class' ) + '"></li>' ).parent().appendTo( wbul );
			}
		} );
	} else {
		pproject = $( '#p-tb' ).clone().attr( 'id', 'p-project' ).insertAfter( '#p-tb' );
		if ( pproject.attr( 'aria-labelledby' ) ) {
			pproject.attr( 'aria-labelledby', 'p-project-label' );
		}
		pproject.find( 'h3' ).attr( 'id', 'p-project-label' ).text( 'På andra projekt' );
		pproject.find( 'ul' ).addClass( 'interProject' ).empty().append( iwlinks );
		iwlinks.replaceWith( function() {
			return $( '<li></li>' ).addClass( $(this).attr( 'class' ) ).append( $(this).contents() );
		} );
	}
} );