/**
 * Button to send a block notification
 * Adds a button to notify the user after a block
 *
 * @author [[pt:User:!Silent]]
 * @date 13/apr/2012
 * @updated 08/oct/2017
 */
/* global jQuery, mediaWiki */
/* jshint laxbreak:true */

( function ( mw, $ ) {
'use strict';

mw.messages.set( {
	// General
	'bbn-buttonText': 'Enviar uma notificação de bloqueio',
	'bbn-checkingBlockRegister': 'Consultando os registros de bloqueio do usuário...',
	'bbn-editUserDiscussion': 'Editando a página de discussão do usuário...',
	'bbn-sectionTitle': 'Notificação de bloqueio',
	'bbn-summarySufix': ', usando um [[MediaWiki:Gadget-blockNotificationsButton.js|gadget]]',
	'bbn-success': 'Mensagem de bloqueio enviada com sucesso (<a href="$1#Notifica.C3.A7.C3.A3o_de_bloqueio">Abrir</a>).',
	'bbn-successBlock': 'Bloqueio bem sucedido',

	// Errors
	'bbn-apiError': 'Erro: a API retornou o código de erro "$1": $2',
	'bbn-unknownError': 'Erro: resultado desconhecido da API.',
	'bbn-requestFail': 'Erro: a requisão falhou.',
} );

/**
 * @class bbn
 */
var bbn = {};

/**
 * Messages
 * @param {string} name Name of the message
 * @param {string|number} [$N] Dynamic parameters to the message (i.e. the values for $1, $2, etc)
 * @see [[mw:ResourceLoader/Default_modules#mediaWiki.message]]
 * @return {string}
 */
bbn.message = function ( /*name[, $1[, $2[, ... ]]]*/ ) {
	return mw.message.apply( this, arguments ).plain();
};

/**
 * Translate to the portuguese the duration of the blocks
 * @param {string} duration The duration of block
 * @return {string}
 */
bbn.translateDurationPT = function ( duration ) {
	var translation;

	duration = duration.split( ' ' );

	if ( duration[ 0 ] === 'infinite' ) {
		return 'tempo indeterminado';
	}

	translation = {
		'second': 'segundo',
		'minute': 'minuto',
		'hour': 'hora',
		'day': 'dia',
		'week': 'semana',
		'month': 'mês',
		'year': 'ano'
	};

	if ( duration[ 0 ] !== '1' ) {
		translation.month = 'meses';
	}

	return duration[ 0 ] + ' '
		+ translation[ duration[ 1 ].replace( /s$/, '' ) ]
		+ ( ( translation.month === 'meses' && duration[ 1 ].indexOf( 'month' ) === -1 ) ? 's' : '' );
};

/**
 * Send the notify
 * @return {undefined}
 */
bbn.sendNotify = function () {
	var logevents,
		userNameBlocked = $( '#mw-content-text' ).find( 'a' ).html();

	mw.notify( bbn.message( 'bbn-checkingBlockRegister' ) );

	$.getJSON( mw.util.wikiScript( 'api' ), {
		action: 'query',
		list: 'logevents',
		format: 'json',
		leprop: 'title|user|timestamp|details|comment',
		lelimit: '1',
		leuser: mw.config.get( 'wgUserName' ),
		letitle: 'User:' + userNameBlocked
	} ).done( function ( data ) {
		logevents = data.query.logevents[ 0 ];

		mw.notify( bbn.message( 'bbn-editUserDiscussion' ) );

		( new mw.Api() ).editPage( {
			title: 'User talk:' + userNameBlocked,
			section: 'new',
			watchlist: 'preferences',
			sectiontitle: bbn.message( 'bbn-sectionTitle' ),
			text: '{{subst:Bloqueado'
				+ ( ( logevents.params.flags.indexOf( 'nousertalk' ) === -1 ) ? '-disc' : '' )
				+ '|1=' + bbn.translateDurationPT( logevents.params.duration )
				+ '|2=' + logevents.comment
				+ '.}' + '} ~~' + '~~',
			summary: bbn.message( 'bbn-sectionTitle' ) + bbn.message( 'bbn-summarySufix' ),
			done: {
				success: function () {
					mw.notify( $.parseHTML( bbn.message( 'bbn-success', mw.util.getUrl( 'User talk:' + userNameBlocked ) ) ) );
				},
				apiError: function ( data ) {
					mw.notify( bbn.message( 'bbn-apiError', data.code, data.info ) );
					$( '#bbn-sendMsg' ).attr( 'disabled', 'false' );
				},
				unknownError: function () {
					mw.notify( bbn.message( 'bbn-unknownError' ) );
					$( '#bbn-sendMsg' ).attr( 'disabled', 'false' );
				}
			}
		} ).fail( function () {
			mw.notify( bbn.message( 'bbn-requestFail' ) );
		} );
	} );
};

/**
 * Run the gadget
 * @return {undefined}
 */
bbn.run = function () {
	if ( !$( '.mw-htmlform-submit' ).length ) {
		$( '#mw-content-text' ).append(
			$( '<input type="button" class="mw-ui-button mw-ui-progressive" id="bbn-sendMsg" value="' + bbn.message( 'bbn-buttonText' ) + '" />' ).on( 'click', function () {
				bbn.sendNotify();
				$( this ).attr( 'disabled', 'true' );
			} )
		);
	}
};

mw.loader.using( 'ext.gadget.mediawiki.api.ptwiki', function() {
if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Block' ) {
	$( bbn.run );
}
});

}( mediaWiki, jQuery ) );

// [[Categoria:!Código-fonte de scripts|Botão de notificação de bloqueio]]