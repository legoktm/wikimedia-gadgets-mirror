/**
 * RefToolbar
 *
 * Adds tools for citing references to the edit toolbar.
 * See [[Wikipedia:RefToolbar]] for further documentation. One of
 * three possible versions will load (Reftoolbar 2.0b, Reftoolbar 2.0a,
 * or Reftoolbar 1.0) depending on the user preferences (the
 * usebetatoolbar and usebetatoolbar-cgd parameters).
 *
 * @see: [[Wikipedia:RefToolbar]]
 * @see: [[MediaWiki:RefToolbar.js]]
 * @see: [[MediaWiki:RefToolbarConfig.js]]
 * @see: [[MediaWiki:RefToolbarLegacy.js]]
 * @see: [[MediaWiki:RefToolbarMessages-en.js]]
 * @see: [[MediaWiki:RefToolbarMessages-de.js]]
 * @see: [[MediaWiki:RefToolbarNoDialogs.js]]
 * @see: [[MediaWiki:Gadget-refToolbarBase.js]]
 * @author: [[User:Mr.Z-man]]
 * @author: [[User:Kaldari]]
 */
/*jshint browser: true, camelcase: true, curly: true, eqeqeq: true */
/*global jQuery, mediaWiki, importScript */
( function ( mw, $ ) {
'use strict';
function initializeRefTools() {
	if ( window.refToolbarInstalled || $( '#wpTextbox1[readonly]' ).length ){
		return;
	}
	if ( mw.user.options.get( 'usebetatoolbar' ) ) {
		// Enhanced editing toolbar is on. Going to load RefToolbar 2.0a or 2.0b.
		if ( mw.user.options.get( 'usebetatoolbar-cgd' ) ) {
			// Dialogs are on. Loading 2.0b. (standard)
			// TODO:
			// * Explicitly declare global variables from [[MediaWiki:RefToolbar.js]] using window.*
			// * Move [[MediaWiki:RefToolbar.js]] to [[MediaWiki:Gadget-refToolbarDialogs.js]]
			// * Create the module 'ext.gadget.refToolbarDialogs' depending on 'ext.gadget.refToolbarBase' and 'ext.wikiEditor'
			// * Replace the code below by mw.loader.load( 'ext.gadget.refToolbarDialogs' );
			mw.loader.using( [ 'ext.gadget.refToolbarBase', 'ext.wikiEditor' ], function () {
				importScript( 'MediaWiki:RefToolbar.js' );
			} );
		} else {
			// Dialogs are off. Loading 2.0a.
			mw.loader.using( 'ext.wikiEditor', function () {
				importScript( 'MediaWiki:RefToolbarNoDialogs.js' );
			} );
		}
	} else if ( mw.user.options.get( 'showtoolbar' ) ) {
		// Enhanced editing toolbar is off. Loading RefToolbar 1.0. (legacy)
		importScript( 'MediaWiki:RefToolbarLegacy.js' );
	} else {
		return;
	}
	window.refToolbarInstalled = true;
}

if ( $.inArray( mw.config.get( 'wgAction' ), [ 'edit', 'submit' ] ) !== -1 ) {
	$( initializeRefTools );
}

}( mediaWiki, jQuery ) );