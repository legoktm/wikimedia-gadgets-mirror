/*jshint curly:false */
/*global jQuery:false, mediaWiki:false, importStylesheet:false*/

(function($, mw) {
	'use strict';
	var slideDuration = (mw.config.get('skin') === 'vector') ? 150 : 0;

	var launch = function() {

		$('div.collapsibleheader').show();

		$('div.collapsibletemplate.collapsed div.body').hide();

		$('table.collapsible.collapsed > tbody > tr:not(:first-child)').toggleClass('hidden');

		$('div.collapsibletemplate div.body').removeClass('show-on-commons');

		function toggleTemplate($element) {
			if ($element.is('tr')) {
				$element
					.parent().parent()
					.toggleClass('collapsed');

				$element.nextAll('tr')
					.toggleClass('hidden');

				// support old browsers without first-child support
				$element.toggleClass('collapsibleCollapsedTH');
			} else {
				$element
					.parent()
					.toggleClass('expanded')
					.toggleClass('collapsed')
					.find('div.body')
					.slideToggle(slideDuration);
			}
		}
		var $headings = $('div.collapsibletemplate > div.collapsibleheader, table.collapsible > tbody > tr:first-child');

		// support old browsers without first-child support
		$('table.collapsible > tbody > tr:first-child th').addClass('collapsibleTH');

		$headings.mousedown(function(e) {
			var $t = $(e.target);
			if ($t.is('a') || $t.parents('a').length !== 0) {
				return true;
			} else {
				toggleTemplate($(this));
				return false;
			}
		});
	};
	mw.hook( 'wikipage.content' ).add( launch );
}(jQuery, mediaWiki));