(function () {
	var wgLangPrefs = mw.config.get('wgLangPrefs');
	var russiaLangs = 
	[   "av",   "ba",  "bxr",  "ce",  "crh", "cv",  "kbd",  "koi",  "krc",  
		"kv",  "lbe",  "lez",  "mdf",  "mhr",  "mrj",  "myv",  "os",  
		"pnt",  "sah",  "tt",  "tyv",  "udm",  "vep",  "xal",  "yi"
  	];
	if(! $.isArray(wgLangPrefs) ) {
		wgLangPrefs = Array();
	}
	for (i=0; i < russiaLangs.length ;i++) {
		if ($.inArray(russiaLangs[i], wgLangPrefs) == -1) {
			wgLangPrefs.push(russiaLangs[i]);
		}
	}
	mw.config.set('wgLangPrefs', wgLangPrefs);
}());