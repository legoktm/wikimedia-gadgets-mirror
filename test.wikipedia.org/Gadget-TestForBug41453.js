// See https://bugzilla.wikimedia.org/show_bug.cgi?id=41453
mw.loader.using( [ 'mediawiki.util', 'jquery.placeholder', 'jquery.spinner' ],
function(){
    console.warn( 'Ready callback was executed.', [].slice.call(arguments) );
}, function(){
    console.warn( 'Error callback was executed.', [].slice.call(arguments) );
} );
console.warn( 'mw.loader.using was executed. What about the callbacks?' );
mw.loader.load( 'ext.gadget.TheBug41453' );
console.warn( 'And what about mw.loader.loader? Did it load the other module?' );