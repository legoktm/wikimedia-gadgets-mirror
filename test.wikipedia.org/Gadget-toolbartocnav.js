(function () {
    function setSelectionRange(input, selectionStart, selectionEnd) {
        if (input.createTextRange) {
            var range = input.createTextRange();
            range.collapse(true);
            range.moveEnd('character', selectionEnd);
            range.moveStart('character', selectionStart);
            range.select();
        }
        else if (input.setSelectionRange) {
            input.focus();
            input.setSelectionRange(selectionStart, selectionEnd);
        }
    }
    function handleClicks(textArea, $tocPopup, $link, resultIndex) {
        $link.click(function () {
            setSelectionRange(textArea, resultIndex, resultIndex);
            $tocPopup.remove();
            $(document.body).off('click.tocnav-popup');
            return false;
        });
    }
    function showToc(context) {
        var $editbox = $('#wpTextbox1');
        if (!$editbox.length)
            return;
        var textArea = $editbox[0];
        var text = $editbox.val();
        var $tocPopup = $('<div class="toc tocnav-popup">');
        var levels = [];
        var currLevel = 0;
        var $currTocEntry = $tocPopup;
        levels.push($currTocEntry);
        var result;
        var captionRe = /^(=+)([^=]*)/mg;
        while ((result = captionRe.exec(text)) !== null) {
            var level = result[1].length;
            var $entry = $('<li>');
            $entry.addClass('toclevel-' + level);
            var $link = $('<a>');
            $link.attr('href', '#');
            handleClicks(textArea, $tocPopup, $link, result.index);
            $entry.append($link);
            $link.text(result[2].trim());
            if (level > currLevel) {
                var $newLevel = $('<ol>');
                levels.push($newLevel);
                $currTocEntry.append($newLevel);
                $currTocEntry = $newLevel;
                currLevel = level;
            }
            else if (level < currLevel) {
                levels.pop();
                $currTocEntry = levels[levels.length - 1];
                currLevel = level;
            }
            $currTocEntry.append($entry);
        }
        var $toolbarButton = context.modules.toolbar.$toolbar.find('.tool-button[rel="tocnav"]');
        if (!$toolbarButton)
            $toolbarButton = context.modules.toolbar.$toolbar;
        var buttonPos = $toolbarButton.position();
        $tocPopup.css('left', buttonPos.left);
        $tocPopup.css('top', buttonPos.top);
        $toolbarButton.after($tocPopup);
        $(document.body).on('click.tocnav-popup', function () {
            $tocPopup.remove();
            $(document.body).off('click.tocnav-popup');
        });
        $tocPopup.click(function (event) {
            event.stopPropagation();
        });
        return false;
    }
    function init() {
        var $editbox = $('#wpTextbox1');
        if (!$editbox.length)
            return;
        $editbox.wikiEditor('addToToolbar', {
            'section': 'advanced',
            'groups': {
                'nav': {
                    label: 'Navigation'
                }
            }
        });
        $editbox.wikiEditor('addToToolbar', {
            'section': 'advanced',
            'group': 'nav',
            'tools': {
                'tocnav': {
                    label: 'TOC',
                    type: 'button',
                    icon: '//upload.wikimedia.org/wikipedia/commons/a/a8/Toolbaricon_definition_list.png',
                    action: {
                        type: 'callback',
                        execute: showToc
                    }
                }
            }
        });
    }
    $(init);
})();