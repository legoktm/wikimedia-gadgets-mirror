$( '.watchlist-msg' ).each( function() {
 	if ( !this.id ) {
		return;
	}
	var hideId = 'hide- ' + this.id;

	if ( $.cookie( hideId ) ) {
		$( this ).remove();
	} else {
		$('<span title="спрятать это сообщение на неделю"\
			style="float:right; margin-left:0.7em; cursor:pointer; color:#04A"\
			>[x]</span>')
			.click( function() {
				$.cookie( hideId, 'y', { expires: 7, path: '/' } );
				$( this ).parent().remove();
			})
			.prependTo( this );
	}
} );