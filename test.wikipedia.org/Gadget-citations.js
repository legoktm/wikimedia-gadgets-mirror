/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Imported from version 1.0 as of 17:18, 9 April 2011 (UTC) from [[User:Smith609/citations.js]]
 * Adds a "Citation bot" link to the toolbox, and a button beneath edit fields, see [[User:Smith609/citations]]
 */

// @Version 1.0 - Adds an "Expand citations" link to the toolbox, and a button beneath edit fields.
// @Version 1.1 - Modify gadget to use jQuery and Citation Bot's new gadgetapi on Tool Labs

function addCBToToolbox() {
	if ( mw.config.get( 'wgCanonicalNamespace' ) !== 'Special' ) {
		var pTb = document.getElementById("p-tb");
		if ( !pTb ) {
			return;
		}
		var escPageName = encodeURIComponent( mw.config.get( 'wgPageName' ));
		mw.util.addPortletLink("p-tb", "//tools.wmflabs.org/citations-dev/doibot.php?edit=toolbar&slow=1&user="+encodeURIComponent( mw.config.get( 'wgUserName' ))+"&page="+escPageName, 'Expand citations', '', "Expand citations and fix common formatting errors");
	}
}


// Add a citation bot button next to the "Show changes" button
function addCitationButton() {
	if ( document.getElementById( 'wpDiff' ) ) {
		var diff = document.getElementById( 'wpDiff' );
		var cbButton = document.createElement( 'input' );
		var newline = document.createTextNode( "\n" );
		cbButton.value = 'Citations';
		cbButton.type = 'button';
		cbButton.id = 'wpCitations';
		cbButton.onclick = function () {
			$citationsButton = $( '#wpCitations' ).clone();
			$.ajax( {
				type: 'POST',
				url: 'https://tools.wmflabs.org/citations-dev/gadgetapi.php',
				data: {
					text: $( '#wpTextbox1' ).val(),
					summary: $( '#wpSummary' ).val()
				},
				beforeSend: function() {
					$( '#wpCitations' ).replaceWith( $spinnerspan );
				},
				success: function( data ) {
					expandedText = data.expandedtext;
					editSummary = data.editsummary;
					$( '#loader' ).replaceWith( $citationsButton );
					loadDiffWindow( expandedText, editSummary );
				},
				error: function( data ) {
					$( '#loader' ).replaceWith( $citationsButton );
					alert( 'Citations request failed' );
				}
			} );
		};
		diff.parentNode.insertBefore( cbButton, diff.nextSibling );
		diff.parentNode.insertBefore( newline, cbButton );
	}
}


function loadDiffWindow( expandedText, editSummary ) {
	// Populate text box with received expanded text
	$( '#wpTextbox1' ).val( expandedText );
	// Populate edit summary textbox with received edit summary
	$( '#wpSummary' ).val( editSummary );
	// Click 'Show preview' button
	$( '#wpDiff' ).click();
}

var $spinnerspan;

// Add a button to allow expanding citations in the text in the edit textbox
$( document ).ready( function() {
	$spinnerspan = $( '<span id="loader"></span>');
	if ( document.images ) {
		// Loading gif when Citations request is processing
		var spinner = new Image();
		spinner.src = "//upload.wikimedia.org/wikipedia/commons/4/42/Loading.gif";
		$spinnerspan.append( spinner );
	}
	addCitationButton();
} );

// Add a toolbox button that runs doibot.php on the most recent saved revision of the page
if ( $.inArray( mw.config.get( 'skin' ), ['monobook', 'modern', 'vector'] ) !== -1 ) {
  $( addCBToToolbox );
}