try { // containerize gadget, to protect other gadgets if this one goes wrong

//Stolen from  (en) wikibooks Written by [[User:Mike.lifeguard]]. - http://en.wikibooks.org/wiki/MediaWiki:Gadget-CleanDeleteReasons.js
//If this acts up on wikinews, feel free to complain to [[user:Bawolff]].
function deletemods(){
	var wpReason = document.getElementById("wpReason");
	if (!wpReason) return;
	var regexp = /(content was|page was empty|content before blanking was|page was empty)/i;
	if (regexp.test(wpReason.value)){
		wpReason.value = "";
	}
}
if (mw.config.get('wgAction') == "delete") $(deletemods);

} catch (e) { // containerize gadget, to protect other gadgets if this one goes wrong
  // ignore
}