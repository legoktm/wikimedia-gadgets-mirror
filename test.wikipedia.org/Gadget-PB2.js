// script for [[:de:Wikipedia:Persönliche Bekanntschaften]] (2nd part)
// author: [[:de:Benutzer:Euku]]
// <nowiki>

// use global variables to avoid problems with ' and "
var pbRequest = {
  textToBeAdded: '',
  comment: ''
};
// localization
var msg = { };
var logVar = "";
var myArticlePath = mw.config.get('wgArticlePath').replace(/\$1/g, "");

if (mw.config.get('wgUserLanguage') == "de" || mw.config.get('wgUserLanguage').indexOf("de-") === 0) {
   // Deutsch (de) und Dialekte (de-at, ...)
   msg = {
	 commentHint: 'Vermeide Details, die die Identität offenlegen könnten.',
	 editCommentAddMe: 'füge mich selbst hinzu',
	 errorMsgSaveParse: "Es ist ein Fehler aufgetreten! Möglicherweise bringt es etwas den Vorgang zu wiederholen. Ein Grund könnte ein Bearbeitungskonflikt sein.<br />Wenn du den Fehler berichten willst, gib bitte auch folgende Informationen an:<br /><pre>Browser: " + navigator.userAgent + "<br />Skript-Version: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'Ein Fehler ist aufgetreten. Benutzerliste konnte nicht geladen werden. Melde es bitte <a href="' + myArticlePath + '/Benutzer:Euku">Benutzer:Euku</a>!',
	 gadgetOutdated: 'Das <a href="' + myArticlePath + 'Wikipedia:Gadgets">Gadget</a>, was du gerade benutzt, wurde vor kurzer Zeit <a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften/Versionslog">aktualisiert</a>. In deinem <a href="' + myArticlePath + 'Browsercache">Browsercache</a> befindet sich allerdings noch die ältere Version. Dies könnte zu Problemen führen. Es wird empfohlen, dass du jetzt die Seite neu lädst und deinen Browsercache umgehst:<br /> <b>Mozilla/Firefox/Safari:</b> Strg + Shift + R (Mactaste + R bei Macintosh) drücken; <b>Konqueror:</b> F5 drücken; <b>Opera:</b> Cache unter Extras ? Einstellungen leeren; <b>Internet Explorer:</b> Strg + F5',
	 gadgetStopped: 'Das <a href="' + myArticlePath + 'Wikipedia:Gadgets">Gadget</a>, was du gerade benutzt, wurde temporär deaktiviert. Wahrscheinlich hat dies technische Ursachen. Du kannst dich über den Grund auf der <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften">Diskussionsseite des Projekts</a> informieren. Versuche es einige Zeit später nochmal!',
	 msgAlreadyIn: 'Du bist bereits in der Liste eingetragen.',
	 msgNoBrackets: "Im Kommentar darf weder '{{' noch '}}' vorkommen.",
	 msgPickSomeone: 'Du musst jemanden auswählen, den du bestätigen willst!',
	 pleaseWait: 'bitte warten ... ',
	 putCommentInHere: 'Hier auf Wunsch Kommentar eingeben',
	 receivedVerifications: 'erhaltene Bestätigungen',
	 retry: 'erneut versuchen',
	 save: 'Speichern',
	 selectAUserInDropdown: 'Bitte auswählen',
	 showUserPage: 'Benutzerseite anzeigen',
	 tabPurge: 'Servercache umgehen (purge)',
	 tabPurgeHint: 'Seite neuladen und Servercache umgehen',
	 tabUnwatchThis: 'diese Seite nicht beobachten',
	 tabUnwatchThisHint: 'diese Seite nicht beobachten',
	 tabWatchNewUsers: 'Liste mit neuen Teilnehmern beobachten',
	 tabWatchNewUsersHint: 'Liste mit neuen Teilnehmern beobachten',
	 tabWatchThis: 'diese Seite beobachten',
	 tabWatchThisHint: 'diese Seite beobachten',
	 userPage: 'Benutzerseite',
	 verificationComment: 'Kommentar zur Bestätigung',
	 waitForThreeVerifications: 'Du hast noch keine drei verschiedenen Bestätigungen erhalten und darfst somit noch nicht selber Bestätigungen vergeben. Warte bitte bis dich jemand bestätigt!',
	 whoToVerify: 'Wen möchtest du bestätigen?',
	 youAreNotInList: '<div align="center">Du bist <b>noch nicht</b> als Teilnehmer bei <i>WP:PB</i> eingetragen und damit nicht bestätigt. Damit darfst noch nicht selber bestätigen und kannst nicht bestätigt werden. Benutze den unteren Link, um dich einzutragen und warte bis du 3 Bestätigungen bekommst.<br><b><a href="javascript:AddMeToThisList();">ich möchte in die Liste der Teilnehmer aufgenommen werden</a>',
	 yourReceivedVerifications: 'deine erhaltenen und vergebenen Bestätigungen',
	 yourUsername: 'Dein Benutzername',
//   not necessary for German
	 pageDescription: '',
	 newRequestsHeadline: 'Anfragen',
	 newRequestsDescription: '',
	 wantsToTakePart: 'möchte in das System eingetragen werden.',
	 Xverifies: 'bestätigt',
	 YwasVerified: '[[:de:Wikipedia:Persönliche Bekanntschaften|gesehen]] zu haben.'
   };
} else if (mw.config.get('wgUserLanguage') == "cs" || mw.config.get('wgUserLanguage') == "sk") {
   // Tschechisch und Slowakisch
   msg = {
	 commentHint: 'Vyvaruj se údajům, které by mohly prozradit tvou identitu.',
	 editCommentAddMe: 'zanést do seznamu účastníků',
	 errorMsgSaveParse: "Došlo k chybě! Pomoci by mohlo provést ještě jednou. Jednou příčinou by mohl být editační konflikt.<br />Pokud bys chtěl chybu ohlásit, uveď prosím i následující informaci:<br /><pre>Browser: " + navigator.userAgent + "<br />Skript-Version: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'Došlo k chybě. Seznam účastníků nemohl být otevřen. Oznam to uživteli <a href="' + myArticlePath + '/Benutzer:Euku">Benutzer:Euku</a>!',
	 gadgetOutdated: '<a href="' + myArticlePath + 'Wikipedia:Gadgets">Udělátko</a>, které užíváš, bylo před krátkou dobou <a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften/Versionslog">aktualizováno</a>. <a href="' + myArticlePath + 'Browsercache">Cache tvého prohlížeče</a> ale stále obsahuje starou verzi. To by mohlo způsobit problémy. Doporučujeme stránku znovu otevřít a vyprázdnit cache:<br /> <b>Mozilla/Firefox/Safari:</b> Strg + Shift + R (Mac + R u Macintoshe); <b>Konqueror:</b> F5; <b>Opera:</b> Cache je pod Extras ? vyprázdnit nastavení; <b>Internet Explorer:</b> Strg + F5',
	 gadgetStopped: '<a href="' + myArticlePath + 'Wikipedia:Gadgets">Gadget</a>, které právě užíváš, bylo dočasně deaktivováno, což má pravděpodobně technické příčiny. Informovat se můžeš na diskusní stránce projektu <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften"></a>. Nebo se pokus o štěstí trochu později!',
	 msgAlreadyIn: 'Již se nacházíš v seznamu účastníků.',
	 msgNoBrackets: "Komentář nesmí obsahovat ani '{{' ani '}}'.",
	 msgPickSomeone: 'Musíš vybrat osobu, kterou chceš potvrdit!',
	 pleaseWait: 'okamžik prosím ... ',
	 putCommentInHere: 'Zde je možno udat komentář',
	 receivedVerifications: 'obdržená potvrzení',
	 retry: 'pokus se znovu',
	 save: 'uložit',
	 selectAUserInDropdown: 'prosím vyber wikipedistu',
	 showUserPage: 'ukaž uživatelskou stránku',
	 tabPurge: 'vyprázdni cache serveru (purge)',
	 tabPurgeHint: 'otevři stranu ještě jednou a vyprázdni cache serveru',
	 tabUnwatchThis: 'tuto stránku nesledovat',
	 tabUnwatchThisHint: 'tuto stránku nesledovat',
	 tabWatchNewUsers: 'sledovat seznam nových účastníků',
	 tabWatchNewUsersHint: 'sledovat seznam nových účastníků',
	 tabWatchThis: 'tuto stránku sledovat',
	 tabWatchThisHint: 'tuto stránku sledovat',
	 userPage: 'uživatelská stánka',
	 verificationComment: 'komentář k potvrzení',
	 waitForThreeVerifications: 'Stále jsi ještě nebyl potvrzen minimálně třemi účastníky a nemůžeš proto potvrzovat jiné. Musíš počkat až budeš třikrát potvrzen.',
	 whoToVerify: 'Koho chceš potvrdit?',
	 youAreNotInList: '<div align="center"><b>Nejsi ještě</b> zaznamenán jako účastník projektu <i>WP:PB</i>. Nemůžeš tedy zatím ani povrzovat jiné ani být jimi povrzen. Klikni na následující odkaz k registraci a čekej, až tě minimálně tři účastníci potvrdí.<br><b><a href="javascript:AddMeToThisList();">chci se registrovat jako účastník projektu</a>',
	 yourReceivedVerifications: 'účastníci, které jsi potvrdil a potvrzení, která jsi obdržel',
	 yourUsername: 'tvé uživatelské jméno',
	 pageDescription: 'Tato stránka je uživatelským rozhraním projektu <a href="' + myArticlePath + ':cs:Wikipedie:Osobní známosti">Osobní známosti</a> a databáze, která je jeho součástí. Zde je možno se do projektu přihlásit a potvrzovat jiné účastníky. Níže uvedené žádosti jsou vyřizovány robotem několikrát za hodinu. K vyřízení jiných problému (smazání, přejmenování, chyby, technické problémy užívej diskusní stránku.',
	 newRequestsHeadline: 'Žádosti',
	 newRequestsDescription: 'Následující žádosti budou do databáze vloženy během několika minut',
	 wantsToTakePart: 'chci se přihlásit do projektu.',
	 Xverifies: 'potvrzuje',
	 YwasVerified: 'že četl stránku <a href="' + myArticlePath + 'Wikipedia:Osobní známosti">Osobní známosti</a>.'
   };
} else if (mw.config.get('wgUserLanguage') == "fr") {
   // Französisch
   msg = {
	 commentHint: 'Évitez les détails qui peuvent révéler votre identité.',
	 editCommentAddMe: 'Ajoutez-moi',
	 errorMsgSaveParse: "Une erreur est survenue ! Il est possible de la corriger en répétant le processus. La raison peut être un conflit de modification. <br />Si vous voulez rapporter l\'erreur, donnez les informations suivantes :<br /><pre>Browser: " + navigator.userAgent + "<br />Skript-Version: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'Une erreur est survenue. La liste d\'utilisateurs ne peut pas être chargée. Dites-le à <a href="' + myArticlePath + '/Benutzer:Euku">Benutzer:Euku</a> s\'il-vous-plaît !',
	 gadgetOutdated: '<a href="' + myArticlePath + 'Wikipedia:Gadgets">Gadget</a> Ce gadget a récemment été mis à jour. Vous êtes en train de l\'utiliser. <a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften/Versionslog"></a>. Mais dans le <a href="' + myArticlePath + 'Browsercache">cache de votre navigateur</a> se trouve une version plus vielle. Cela provoque des problèmes. Rechargez la page et purgez le cache de votre navigateur :<br /> <b>Mozilla/Firefox/Safari :</b> Ctrl + Shift + R (Mactaste + R  si vous avez un Macintosh) ; <b>Konqueror:</b> F5; <b>Opera :</b> Cache sous accessoires ? Videz vos paramètres ; <b>Internet Explorer:</b> Ctrl + F5',
	 gadgetStopped: 'Ce <a href="' + myArticlePath + 'Wikipedia:Gadgets">gadget</a> que vous êtes en train d\'utiliser a été temporairement désactivé. Probablement à cause de problèmes techniques. Vous pouvez vous informer sur les raisons plus précises sur <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften">la page de discussion du projet</a>. Attendez un peu et réessayez plus tard !',
	 msgAlreadyIn: 'Vous avez déjà été ajouté dans la liste.',
	 msgNoBrackets: "Veuillez ne pas utiliser '{{' ni '}}' dans le commentaire.",
	 msgPickSomeone: 'Vous devez choisir quelqu\'un à confirmer !',
	 pleaseWait: 'Veuillez patienter...',
	 putCommentInHere: 'Commentez ici la demande',
	 receivedVerifications: 'confirmations reçues',
	 retry: 'tenter de nouveau',
	 save: 'sauvegarder',
	 selectAUserInDropdown: 'Merci de choisir',
	 showUserPage: 'Montre la page utilisateur',
	 tabPurge: 'Purge le cache du serveur',
	 tabPurgeHint: 'Recharge la page et purge le cache du serveur',
	 tabUnwatchThis: 'ne pas suivre cette page',
	 tabUnwatchThisHint: 'ne pas suivre cette page',
	 tabWatchNewUsers: 'suivre la liste de nouveaux participants',
	 tabWatchNewUsersHint: 'suivre la liste de nouveaux participants',
	 tabWatchThis: 'suivre cette page',
	 tabWatchThisHint: 'suivre cette page',
	 userPage: 'Page d\'utilisateur',
	 verificationComment: 'commentaire sur la confirmation',
	 waitForThreeVerifications: 'Vous n\'avez pas encore reçu trois confirmations différentes : par conséquent, vous ne pouvez pas donner de confirmations. Veuillez attendre les confirmations.',
	 whoToVerify: 'Qui voulez-vous vérifier ?',
	 youAreNotInList: '<div align="center">Vous n\'êtes <b>pas encore</b> inscrit sur<i>WP:PB/FR</i> et n\'avez pas encore été confirmé par conséquent. Vous n\'avez donc pas le droit de donner des confirmations et les autres ne peuvent pas vous confirmer. Utilisez le lien en bas pour vous inscrire et attendez trois confirmations. <br><b><a href="javascript:AddMeToThisList();">Je veux m\'inscrire dans la liste de participants. </a>',
	 yourReceivedVerifications: 'confirmations données et reçues',
	 yourUsername: 'Votre nom d\'utilisateur',
//   not necessary for German
	 pageDescription: '',
	 newRequestsHeadline: 'Demandes',
	 newRequestsDescription: '',
	 wantsToTakePart: 'Je veux participer.',
	 Xverifies: 'confirmé',
	 YwasVerified: 'avoir [[:de:Wikipedia:Persönliche Bekanntschaften|vu]].'
   };
} else if (mw.config.get('wgUserLanguage') == "it") {
   // Französisch
   msg = {
	 commentHint: 'Evita di inserire informazioni private.',
	 editCommentAddMe: 'füge mich selbst hinzu (aggiungimi a [[WP:PB]])',
	 errorMsgSaveParse: "Si è verificato un errore! Potrebbe risolversi riprovando, quindi riprova. Probabilmente si è trattato di un conflitto di edizione.<br />Se ti va, e noi speriamo di sì, faccelo sapere includendo le seguenti informazioni:<br /><pre>Browser: " + navigator.userAgent + "<br />Versione dello script: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'Si è verificato un errore. Non è stato possibile caricare l\'elenco degli utenti. Per cortesia, comunicalo al più presto a <a href="' + myArticlePath + 'Benutzer:Euku">User:Euku</a>!',
	 gadgetOutdated: 'Un aggiornamento risulta disponibile per questo <a href="//en.wikipedia.org/wiki/Wikipedia:Gadget">gadget</a>. Ma la <a href="//en.wikipedia.org/wiki/Browser_cache">cache del tuo browser</a> ne sta conservando una versione non aggiornata. Ricarica questa pagina per vedere le modifiche:<br /><b>Mozilla/Firefox/Safari:</b> premi Ctrl + Shift + R (per Mac: Command Key + R); <b>Konqueror:</b> F5; <b>Opera:</b> Strumenti → Preferenze; <b>Internet Explorer:</b> Ctrl + F5',
	 gadgetStopped: 'Questo <a href="//en.wikipedia.org/wiki/Wikipedia:Gadget">gadget</a> è stato temporaneamente bloccato, verosimilmente per ragioni tecniche e di manutenzione. Per saperne di più, vedi la <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften">pagina di discussione del progetto</a>. Riprova fra poco!',
	 msgAlreadyIn: 'Sei già nell\'elenco.',
	 msgNoBrackets: "I tuoi commenti non devono contenere '{{' o '}}'.",
	 msgPickSomeone: 'Devi sleezionare qualcuno da verificare!',
	 pleaseWait: 'un attimo di pazienza, prego ... ',
	 putCommentInHere: 'Aggiungi un commento qui, dove richiesto.',
	 receivedVerifications: 'verifiche ricevute',
	 retry: 'riprova',
	 save: 'Salva',
	 selectAUserInDropdown: 'Seleziona',
	 showUserPage: 'mostra la pagina utente',
	 tabPurge: 'ignora la cache (purge)',
	 tabPurgeHint: 'ricarica la pagina e ignora la cache del server',
	 tabUnwatchThis: 'non seguire',
	 tabUnwatchThisHint: 'smetti di seguire questa pagina',
	 tabWatchNewUsers: 'vedi l\'elenco dei nuovi partecipanti',
	 tabWatchNewUsersHint: 'osserva la lista dei nuovi aderenti',
	 tabWatchThis: 'segui questa pagina',
	 tabWatchThisHint: 'segui questa pagina',
	 userPage: 'pagina utente',
	 verificationComment: 'Commento',
	 waitForThreeVerifications: 'Non hai ancora ricevuto tre conferme, perciò non puoi confermare altri. Per cortesia attendi di aver ricevuto prima tre conferme!',
	 whoToVerify: 'Chi vuoi confermare?',
	 youAreNotInList: '<div align="center">Non stai <b>ancora</b> partecipando al <i>WP:PB</i> perciò non puoi confermare altri e nessuno può confermare te. Usa il link qui sotto per partecipare e attendi di aver ricevuto tre conferme.<br><b><a href="javascript:AddMeToThisList();">Voglio partecipare</a>',
	 yourReceivedVerifications: 'le conferme che hai ricevuto e quelle che hai rilasciato',
	 yourUsername: 'Il tuo nome utente',
	 pageDescription: 'Questa pagina è una interfaccia utente per <a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften">Persönliche Bekanntschaften</a> e per il database, che è parte del progetto. Qui tu puoi iscriverti al progetto (gratis) e confermare altri wikipediani (gratis anche questo). Le richieste qui sotto elencate saranno esaudite da un servizievole bot che passa diverse volte ogni ora. Per altre richieste (cancellazione, cambio nome utente, errori, problemi di natura tecnica) usa la pagina di discussione.',
	 newRequestsHeadline: 'Richieste',
	 newRequestsDescription: 'Le seguenti richieste saranno aggiunte al database fra pochi minuti:',
	 wantsToTakePart: 'desidera aderire al progetto WP:PB.',
	 Xverifies: 'conferma di aver conosciuto',
	 YwasVerified: '<a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften">personalmente</a>.'
   };
} else if (mw.config.get('wgUserLanguage') == "nl" || mw.config.get('wgUserLanguage') == "nl-informal") {
   msg = {
	 commentHint: 'Vermijd details die de identiteit kan onthullen.',
	 editCommentAddMe: 'voeg mij toe',
	 errorMsgSaveParse: "Er is een fout opgetreden! Mogelijk is het nodig is om het proces deels te herhalen. Een reden zou een bewerkingsconflict kunnen zijn.<br />Als je de fout wilt melden, vermeld dan ook de volgende informatie:<br /><pre>Browser: " + navigator.userAgent + "<br />Script-versie: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'Er is een fout opgetreden. De gebruikerslijst kon niet geladen worden. Meld het alsjeblieft <a href="' + myArticlePath + '/Benutzer:Euku">Benutzer:Euku</a>!',
	 gadgetOutdated: 'De <a href="' + myArticlePath + 'Wikipedia:Gadgets">gadget</a> dat je nu gebruikt wordt voor korte tijd <a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften/Versionslog">bijgewerkt</a>. In jouw <a href="' + myArticlePath + 'Browsercache">browsercache</a> bevindt zich nog steeds de oudere versie. Dit kan tot problemen leiden. Het wordt aanbevolen dat je nu de pagina opnieuw laadt en je browsercache leegt:<br /> <b>Mozilla/Firefox/Safari:</b> Ctrl + Shift + R (Mactaste + R met Macintosh) klikken; <b>Konqueror:</b> F5 klikken; <b>Opera:</b> Cache onder extra\'s ? instellingen legen; <b>Internet Explorer:</b> Ctrl + F5',
	 gadgetStopped: 'Het <a href="' + myArticlePath + 'Wikipedia:Gadgets">gadget</a> wat je nu gebruikt is tijdelijk gedeactiveerd. Waarschijnlijk heeft dit technische oorzaken. Je kunt je over de reden hiervoor op de <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften">overlegpagina van het project</a> laten informeren. Probeer het enige tijd later nogmaals!',
	 msgAlreadyIn: 'Je bent al ingeschreven op de lijst.',
	 msgNoBrackets: "In de reactie mogen geen '{{' en '}}' voorkomen.",
	 msgPickSomeone: 'Je moet iemand kiezen die je wilt bevestigen!',
	 pleaseWait: 'alsjeblieft even wachten ... ',
	 putCommentInHere: 'hier kun je eventueel een reactie ingeven',
	 receivedVerifications: 'ontvangen bevestigingen',
	 retry: 'nogmaals proberen',
	 save: 'opslaan',
	 selectAUserInDropdown: 'maak een keuze',
	 showUserPage: 'gebruikerspagina tonen',
	 tabPurge: 'servercache legen (purge)',
	 tabPurgeHint: 'pagina herladen en servercache legen',
	 tabUnwatchThis: 'deze pagina niet meer volgen',
	 tabUnwatchThisHint: 'deze pagina niet meer volgen',
	 tabWatchNewUsers: 'lijst met nieuwe deelnemers volgen',
	 tabWatchNewUsersHint: 'lijst met nieuwe deelnemers volgen',
	 tabWatchThis: 'deze pagina volgen',
	 tabWatchThisHint: 'deze pagina volgen',
	 userPage: 'gebruikerspagina',
	 verificationComment: 'Reactie bij bevestiging',
	 waitForThreeVerifications: 'Je hebt nog geen drie bevestigingen ontvangen en het is derhalve niet toegestaan ​​om zelf al bevestigingen geven. Wacht alsjeblieft voordat je iemand bevestigt!',
	 whoToVerify: 'Wie wil je bevestigen?',
	 youAreNotInList: '<div align="center">Je bent <b>nog niet</b> als deelnemer bij <i>WP:PB</i> geregistreerd en daarmee ook niet bevestigt. Tevens mag je nog niet zelf anderen bevestigen en kun je niet bevestigd worden. gebruik de onderstaande link om je aan te melden en wacht totdat je drie bevestigingen gekregen hebt.<br><b><a href="javascript:AddMeToThisList();">ik wil graag op de lijst van deelnemers opgenomen worden</a>',
	 yourReceivedVerifications: 'jouw verkregen en gegeven bevestigingen',
	 yourUsername: 'Jouw gebruikersnaam',
//   not necessary for German
	 pageDescription: '',
	 newRequestsHeadline: 'aanvragen',
	 newRequestsDescription: '',
	 wantsToTakePart: 'moet in het systeem aangemeld worden.',
	 Xverifies: 'bevestigt',
	 YwasVerified: '<a href="' + myArticlePath + ':nl:Wikipedia:Ontmoete Wikimedianen">ontmoet</a> te hebben.'
   };
} else {
   // use English for other people
   msg = {
	 commentHint: 'Avoid private information.',
	 editCommentAddMe: 'füge mich selbst hinzu (add me to [[WP:PB]])',
	 errorMsgSaveParse: "An error occurred! Maybe the problem can be resolved by tring again. An edit conflict could be the reason for that.<br />If you want to report this, please add the following details:<br /><pre>Browser: " + navigator.userAgent + "<br />Skript-Version: " + window.persBekannt.PBJSversion,
	 errorMsgDefault: 'An error occurred. The user list could not be loaded. Please report it to <a href="' + myArticlePath + 'Benutzer:Euku">User:Euku</a>!',
	 gadgetOutdated: 'An update is available for this <a href="//en.wikipedia.org/wiki/Wikipedia:Gadget">gadget</a>. But your <a href="//en.wikipedia.org/wiki/Browser_cache">browser cache</a> stores an older version of this. Please reload this page to bypass your browser\'s cache to see the changes:<br /><b>Mozilla/Firefox/Safari:</b> press Ctrl + Shift + R (for Mac: Command Key + R); <b>Konqueror:</b> F5; <b>Opera:</b> Tools → Preferences; <b>Internet Explorer:</b> Ctrl + F5',
	 gadgetStopped: 'This <a href="//en.wikipedia.org/wiki/Wikipedia:Gadget">gadget</a> was stopped temporary. Probably there are tecnical reasons for this. You can read more information on the <a href="' + myArticlePath + 'Wikipedia_Diskussion:Persönliche_Bekanntschaften">project\'s talk page</a>. Try again later!',
	 msgAlreadyIn: 'You are alredy in this list.',
	 msgNoBrackets: "Your comment must not contain '{{' or '}}'.",
	 msgPickSomeone: 'You must select someone you want to verify!',
	 pleaseWait: 'please wait ... ',
	 putCommentInHere: 'Add a comment here, when required.',
	 receivedVerifications: 'received verifications',
	 retry: 'retry',
	 save: 'Save',
	 selectAUserInDropdown: 'Please select',
	 showUserPage: 'show user page',
	 tabPurge: 'bypass server cache (purge)',
	 tabPurgeHint: 'reload this page and bypass server cache',
	 tabUnwatchThis: 'unwatch this page',
	 tabUnwatchThisHint: 'unwatch this page',
	 tabWatchNewUsers: 'watch new participants\' list',
	 tabWatchNewUsersHint: 'watch new participants\' list',
	 tabWatchThis: 'watch this page',
	 tabWatchThisHint: 'watch this page',
	 userPage: 'user page',
	 verificationComment: 'Comment',
	 waitForThreeVerifications: 'You are not verified by three people yet. Therefore you cannot verify others. Please wait until you until receive three verifications!',
	 whoToVerify: 'Who do you want to verify?',
	 youAreNotInList: '<div align="center">You are not participating in <i>WP:PB</i> <b>yet</b>. So you cannot verify someone else and cannot be verified. Use the link below, to participate and wait until you are verified by three people.<br><b><a href="javascript:AddMeToThisList();">I want to take part in this</a>',
	 yourReceivedVerifications: 'your received verifications and verified users',
	 yourUsername: 'Your user name',
	 pageDescription: 'This page is the user interface for <a href="' + myArticlePath + ':en:Wikipedia:Personal Acquaintances">Personal Acquaintances</a> and the database that is part of the project. Here you can join this project and verify other Wikipedians. The requests below will be executed by a bot several times per hour. For other requests (deletion, renaming, errors, technical problems) use the talk page.',
	 newRequestsHeadline: 'Requests',
	 newRequestsDescription: 'The following requests will be added to the database in a few minutes:',
	 wantsToTakePart: 'wants to join the project WP:PB.',
	 Xverifies: 'verifies to know',
	 YwasVerified: '<a href="' + myArticlePath + 'Wikipedia:Persönliche_Bekanntschaften">personally</a>.'
   };
}

var bigUserList;

function loadFile(file) {
	 xmlHttp = null;
	 if (typeof XMLHttpRequest != 'undefined') { xmlHttp = new XMLHttpRequest(); }
	 if (!xmlHttp) {
		try { xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch(e) {
			try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch(e) { throw 'Could not create XMLHttpRequest. Stop now.'; }
		}
	 }
	 if (xmlHttp) {
		var now = new Date();
		xmlHttp.open('GET', mw.config.get('wgServer') + mw.config.get('wgScript') + '?title='+ file +'&action=raw&ctype=text/javascript&ts=' + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes(), false);
		xmlHttp.send(null);
		return (xmlHttp.responseText);
	 }
	 return false;
 }

// creates a new attribute
// e.g. newAttrib("href", "bla") returns
//     href="bla" as an attribute for the <a>-tag
function newAttrib(name, attrib) {
   var tmpAttrib = document.createAttribute(name);
   tmpAttrib.nodeValue = attrib;
   return tmpAttrib;
}

function showErrorMesage(htmlText) {
   var intro = '<div style="float:center; border: 1px solid black; padding: 8px; background-color:white;"><img height="17" width="20" longdesc="/wiki/Bild:Zeichen_101.svg" alt="Achtung" src="//upload.wikimedia.org/wikipedia/commons/thumb/3/38/Zeichen_101.svg/20px-Zeichen_101.svg.png"/>&nbsp;';
   var reload = ' <a href="javascript:startGUI()">' + msg.retry + '</a>';
   var log = "<br><b>;Einflüsse</b><br>wgServer: " + mw.config.get('wgServer') + ", wgUserName: " + mw.config.get('wgUserName') + ", wgUserLanguage: " + mw.config.get('wgUserLanguage') + ", Browser: " + navigator.userAgent + ", persBekannt.PBJSversion: " + window.persBekannt.PBJSversion + "<br><br><b>;Log</b><br>" + logVar;
   document.getElementById("no-gadget-active").innerHTML = intro + htmlText + log + reload + '</div>';
}

function AmIin() {
   // load user list
   var rawText = loadFile(window.persBekannt.userList);
   if (!rawText) {
	showErrorMesage(msg.errorMsgDefault);
	return;
   }
   // remove "<" and ">" to prevent hacks, split text in to array
   var userNameArray = rawText.replace(/(?:<|>)/g, "ERROR").split('\n');
   for (var i in userNameArray)
	   if (mw.config.get('wgUserName') == userNameArray[i])
		   return true;
   return false;
}

function loadBigUserList() {
   var rawTextBig = loadFile(window.persBekannt.bigUserList);
   var searchRegEx = /\{\{\/DB\-Link\|([^\|]+)\|([^}]*)\}\}/g;
   var searchRes;
   var userListArray = [];
   var i = 0;
   while (searchRes = searchRegEx.exec(rawTextBig)) {
	  userListArray[i] = [searchRes[1], searchRes[2]];
	  i++;
   }
   return userListArray;
}

function AddMeToThisList() {
   if (AmIin()) {
	   showErrorMesage(msg.msgAlreadyIn);
	   return;
   }
   tellUserToWait();

   // concat the string to save
   pbRequest.textToBeAdded = "{{Wikipedia:Persönliche Bekanntschaften/neuer Benutzer|Name=" + mw.config.get('wgUserName') + "|Zeit=~~~~~}}";
   pbRequest.comment = msg.editCommentAddMe;
   addTextAndSavePage();
}

// add text to persBekannt.requestPage and save it
function addTextAndSavePage() {
	 logging("Betrete function addTextAndSavePage()");
	 var saveSucc = false;
	 var trySaveCounter = 1;
	 do {
		logging("Speicherversuch: " + trySaveCounter);
		// GET edit token
		xmlHttp = null; // this object is reused
		pbRequest.editToken = null;
		pbRequest.timestamp = null;
		if (typeof XMLHttpRequest != 'undefined') {
			xmlHttp = new XMLHttpRequest(); 
			logging("new XMLHttpRequest() ausgeführt");
		}
		if (!xmlHttp) {
			logging("xmlHttp: " + xmlHttp);
			try { xmlHttp = new ActiveXObject("Msxml2.XMLHTTP"); 
				  logging("xmlHttp2: " + xmlHttp);
			}
			catch(e) {
				 try { xmlHttp = new ActiveXObject("Microsoft.XMLHTTP"); 
					   logging("xmlHttp3: " + xmlHttp);
				 }
				 catch(e) { 
					logging("Could not create XMLHttpRequest. Stopping now.");
					throw 'Could not create XMLHttpRequest. Stopping now.';
				 }
			}
		}

		// this is only for avoiding the browser to use cache...
		var now = new Date();
		var tstamp = '0' + now.getMonth() + now.getDate() + now.getHours() + now.getMinutes() + now.getSeconds();
		// get meta data
		if (xmlHttp) {
		   logging("hole: " + mw.config.get('wgServer') + mw.config.get('wgScriptPath') + "/api.php?action=query&prop=info|revisions&intoken=edit&format=xml&titles=" + window.persBekannt.requestPage + "&dummyvar=" + tstamp);
		   xmlHttp.open('GET', mw.config.get('wgServer') + mw.config.get('wgScriptPath') + "/api.php?action=query&prop=info|revisions&intoken=edit&format=xml&titles=" + window.persBekannt.requestPage + "&dummyvar=" + tstamp, false);
		   xmlHttp.send(null);
		} else {
		   showErrorMesage(msg.errorMsgSaveParse);
		   return;
		}

		// parse meta data
		logging("Antwort war (encodeURIComponent(xmlHttp.responseText)): " + encodeURIComponent(xmlHttp.responseText.replace(/edittoken=\".*?\+\\/i, 'edittoken="xxx"')));
		var xml = parseXML(xmlHttp.responseText);
		pbRequest.editToken = xml.getElementsByTagName("page")[0].getAttribute("edittoken");
		pbRequest.timestamp = xml.getElementsByTagName("rev")[0].getAttribute("timestamp");

		// POST to an URL
		var params = 
		 "action=edit" +
		 "&title=" + decodeURI(window.persBekannt.requestPage) +
		 "&appendtext=" + encodeURIComponent("\n" + pbRequest.textToBeAdded) +
		 "&summary=" + encodeURIComponent(pbRequest.comment) +
		 "&token=" + encodeURIComponent(pbRequest.editToken) +
		 "&basetimestamp="+ pbRequest.timestamp +
		 "&minor=0";

		logging("hole: " + mw.config.get('wgServer') + mw.config.get('wgScriptPath') + "/api.php?format=xml");
		xmlHttp.open("POST", mw.config.get('wgServer') + mw.config.get('wgScriptPath') + "/api.php?format=xml", false);
		xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xmlHttp.onreadystatechange = function() { if (xmlHttp.readyState !== 4) return;  };
		xmlHttp.send(params);

		logging("Antwort: (encodeURIComponent(xmlHttp.responseText))" + encodeURIComponent(xmlHttp.responseText));
		// parse response
		xml = parseXML(xmlHttp.responseText);

		if (xml.getElementsByTagName("edit").length !== 0 && xml.getElementsByTagName("edit")[0].getAttribute("result") == "Success") {
			// reload current page
			saveSucc = true;
			window.location.href = mw.config.get('wgServer') + myArticlePath + decodeURIComponent(window.persBekannt.workPage) + '?action=purge';
		} else if (xml.getElementsByTagName("error").length !== 0 && xml.getElementsByTagName("error")[0].getAttribute("code") == "editconflict") {
			// edit confict, try again
			logging("Bearbeitungskonflikt...");
			trySaveCounter++;
		} else {
			trySaveCounter++;
		}
	} while (!saveSucc && trySaveCounter <= 5);
	if (trySaveCounter > 5) {
	   showErrorMesage("An unknown error occured, message was: " + xmlHttp.responseText);
	}
}

function parseXML(text) {
	var xml;
	if (window.ActiveXObject) {
	   // for IE
	   xml=new ActiveXObject("Microsoft.XMLDOM");
	   xml.async="false";
	   xml.loadXML(text);
	} else if (document.implementation && document.implementation.createDocument) {
	   // every other browser on this planet
	   xml = new DOMParser().parseFromString(text, "text/xml");
	}
	var doc = xml.documentElement;
	if (doc.tagName === "parserError") {
	showErrorMesage("XML parser error: " + doc.textContent);
		return;
	}
	return xml;
}

function saveACKForm(eventP) {
	 if ((eventP !== null) && (eventP != "klick")) {
		var pressedKey = (eventP.keyCode ? eventP.keyCode : (eventP.which ? eventP.which : eventP.charCode));
		if (pressedKey != 13) // enter was pressed in input-field
		   return;
	 } else if (eventP != "klick") {
		return;
	 }
	 var confirmed = document.getElementById('confirmed').value;
	 var comment = document.getElementById('comment').value;
	 if (confirmed == "null") {
		 showErrorMesage(msg.msgPickSomeone);
		 return;
	 }
	 if (comment.indexOf("}}") > -1 || comment.indexOf("{{") > -1) {
		 alert(msg.msgNoBrackets);
		 return;
	 }
	 tellUserToWait();
	 pbRequest.textToBeAdded = "{{Wikipedia:Persönliche Bekanntschaften/neue Bestätigung|Bestätiger=" + mw.config.get('wgUserName') + "|Bestätigter=" + confirmed + "|Kommentar=" + comment + "|Zeit=~~~~~}}";
	 pbRequest.comment = "Bestätige [[User:" + confirmed + "|" + confirmed + "]]";
	 addTextAndSavePage();
}

function tellUserToWait() {
   // tell the user to wait
   document.getElementById('no-gadget-active').innerHTML = msg.pleaseWait;
   var newElement = document.createElement("img");
	   newElement.setAttributeNode(newAttrib("src", "//upload.wikimedia.org/wikipedia/commons/4/42/Loading.gif"));
	   newElement.setAttributeNode(newAttrib("width", 18));
	   newElement.setAttributeNode(newAttrib("id", "busyImg"));
	   newElement.setAttributeNode(newAttrib("height", 18));
   document.getElementById("no-gadget-active").appendChild(newElement);
}
// Updates the link to the userpage when 'onchage' or 'onkeyup' are fired inby the select field
function updateUserLink() {
   var userToConfirm = document.getElementById("confirmed").value;
   var dbText = '<br />→ <small><a target="_blank" href="//tools.wmflabs.org/pb/?p=user&name=' + encodeURIComponent(userToConfirm) + '">' + msg.receivedVerifications + '</a></small>';

   if (document.getElementById("confirmed").value == "null")
	  document.getElementById("userPageLink").innerHTML = "";
   else
	  document.getElementById("userPageLink").innerHTML = '→ <small><a target="_blank" href="' + myArticlePath + 'Benutzer:' + encodeURIComponent(userToConfirm) + '">' + msg.userPage + '</a></small>' + dbText;
}

function presetFormParamsByUrl(userNameArray) {
	var preset_user    = decodeURIComponent((new RegExp('[?|&]preset_user=([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
	var preset_comment = decodeURIComponent((new RegExp('[?|&]preset_comment=([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
	if (typeof(preset_user) !== 'undefined' && userNameArray.indexOf(preset_user) != -1) { $("#confirmed").val(preset_user); }
	if (typeof(preset_comment) !== 'undefined') { $("#comment").val(preset_comment); }
}

function showFormToACKsomeone() {
   // load big user list
   bigUserList = loadBigUserList();
   for (var i = 0; i < bigUserList.length; i++)
	 if (mw.config.get('wgUserName') == bigUserList[i][0]) {
		 if ("u" == bigUserList[i][1]) {
		   showErrorMesage(msg.waitForThreeVerifications);
		   return;
		 } else
		   break;
	 }

   // load user list
   var rawText = loadFile(window.persBekannt.userList);
   if (!rawText) {
		showErrorMesage(msg.errorMsgDefault);
		return;
   }

   var userNameArray = rawText.split('\n');
   var usersToConfirm = '<option value="null">' + msg.selectAUserInDropdown + '</option><optgroup label="0-9">';
   var currentLetter = "0-9";
   for (var i = 0; i < userNameArray.length; i++) {
		// avoid injection of code
		var tmpUser = userNameArray[i].replace("<", "").replace(">", "")
		var tmpUserQuot = tmpUser.replace(/"/g, '&quot;');
		// new header?
		if ((currentLetter == "0-9") && (tmpUser.charAt(0) == "A")) {
			currentLetter = "A";
			usersToConfirm += '</optgroup><optgroup label="A">';
		}
		else if ((currentLetter != "0-9") && (tmpUser.charAt(0) > currentLetter)) {
			currentLetter = tmpUser.charAt(0);
			usersToConfirm += '</optgroup><optgroup label="' + currentLetter + '">';
		}
		if (tmpUser != mw.config.get('wgUserName'))
		   usersToConfirm += '<option value="' + tmpUserQuot + '" label="' + tmpUserQuot + '">' + tmpUserQuot + '</option>';
   }
   usersToConfirm += '</optgroup>';

   var formHTMLcode = '<table align="center" class="wikitable" id="ACKtable">'+
   '<tbody><tr><td style="padding: 0.5em 1em;">' + msg.yourUsername + '</td><td id="myUserName" style="padding: 0.5em 1em;">' + mw.config.get('wgUserName')+
   '<br />→ <small><a target="_blank" href="//tools.wmflabs.org/pb/?p=user&name=' + encodeURIComponent(mw.config.get('wgUserName')) + '">' + msg.yourReceivedVerifications + '</a></small>'+
   '</td></tr>'+
   '<tr><td style="padding: 0.5em 1em;"><label for="confirmed">' + msg.whoToVerify + '</label></td><td style="padding: 0.5em 1em;" id="userListTd">'+
   ' <select id="confirmed" name="confirmed" onchange="javascript:updateUserLink();" onkeyup="javascript:updateUserLink();" style="width: 100%;">' + msg.showUserPage + '\"">' + usersToConfirm +
   '    </select><br/><div id="userPageLink"></div>' +
   '</td></tr>'+
   '<tr><td style="padding: 0.5em 1em;"><label for="comment">' + msg.verificationComment + '<br/><small>' + msg.commentHint + '</small></td><td style="padding: 0.5em 1em;">'+
   '<input type="text" id="comment" title="' + msg.putCommentInHere + '" value="" maxlength="50" onkeypress="javascript:saveACKForm(event)" style="width: 100%;" />'+
   '</td></tr>'+
   '<tr><td style="padding: 0.5em 1em; text-align: right;" colspan="2"><b><a href="javascript:saveACKForm(\'klick\');">' + msg.save + '</a></b>'+
   '</tbody></table>';
   $("#no-gadget-active").html(formHTMLcode); // write form
   
   presetFormParamsByUrl(userNameArray);
   $("#confirmed").focus(); // focus the select field
}

function buildUpGUIForWorkPage() {
   // modify GUI
   tellUserToWait();
   $('#ca-edit').html('<a title="' + msg.tabPurge + '" href="' + myArticlePath + decodeURIComponent(window.persBekannt.workPage) + '?action=purge"><span>' + msg.tabPurgeHint + '</span></a>');
   $('#ca-unwatch').html('<a title="' + msg.tabUnwatchThis + '" href="' + myArticlePath + decodeURIComponent(window.persBekannt.workPage) + '?action=unwatch"><span>' + msg.tabUnwatchThisHint + '</span></a>');
   $('#ca-watch').html('<a title="' + msg.tabWatchThis + '" href="' + myArticlePath + decodeURIComponent(window.persBekannt.workPage) + '?action=watch"><span>' + msg.tabWatchThisHint + '</span></a>');
   $('#ca-delete').html('');
   $('#ca-move').html('<a target="_blank" title="' + msg.tabWatchNewUsers + '" href="' + myArticlePath + window.persBekannt.newUserList + '?action=watch">' + msg.tabWatchNewUsers + '</a>');
   $('#ca-unprotect').html('');
   try {
	  if (mw.config.get('wgUserLanguage') != "de" && mw.config.get('wgUserLanguage').indexOf("de-") != 0) { // replace the German texts
		 $('#page-description').html(msg.pageDescription);
		 $('#new-requests-headline').html(msg.newRequestsHeadline);
		 $('#new-requests-description').html(msg.newRequestsDescription);
		 el = document.getElementsByTagName('span');
		 for(i = 0; i < el.length; i++) {
			att = el[i].getAttribute("name");
			if (att == null)
			   att = el[i].getAttribute("id");
			if(att == 'wants-to-take-part') {
			   el[i].innerHTML = msg.wantsToTakePart;
			}
			else if (att == 'Xverifies') {
			   el[i].innerHTML = msg.Xverifies;
			}
			else if (att == 'YwasVerified') {
			   el[i].innerHTML = msg.YwasVerified;
			}
		 }
	  }
   } catch(e) {}
   if (isGadgetVersionOutdated()) {
	   return;
   }
   if (!AmIin()) {
	  // replace the warning and remove the edit button
	  $("#no-gadget-active").html(msg.youAreNotInList);
	} else { // we can go on
	  showFormToACKsomeone();
	}
}
function filterBySelection() {
	var showByHomewikiVal = $('input[name=showByHomewiki]').val();
	var showVervUsersVal = $('input[name=showVervAllUsers]').val();
	var showActiveUsersVal = $('input[name=showActiveUsers]').val();
	var classListToShow = 'span.participant'
	    + (showActiveUsersVal == 'activeActiveOnly' ? '.active' : (showActiveUsersVal == 'activeInactiveOnly' ? '.inactive' : ''))
	    + (showVervUsersVal == 'vervOnlyVervVal' ? '.verified' : (showVervUsersVal == 'vervOnlyUnervVal' ? '.unverified' : ''));
	
	$("span.participant").css("display", "none");
	$(classListToShow).css("display", "inline");
}
function buildUpGUIForUserlist() {
   if (isGadgetVersionOutdated()) { return; }
   var table = '<table id="viewConfigTbl" class="wikitable">'+
	'<tr><th colnspan="2" id="headlineLabel">Zeige nur Teilnehmer:</th></tr>'+
	'<tr><td id="withHomeWikiLabel">mit Homewiki:</td><td>';
	
   // count the different wikis:
   var homeWikiDict = {};
   $("span.participant span.homewiki").each(function(){
	   var key = $(this).html();
	   homeWikiDict[key] = typeof(homeWikiDict[key]) !== 'undefined' ? homeWikiDict[key] + 1 : 1;
   });
   for (wiki in homeWikiDict) {
	   table += '<input class="control_element" type="checkbox" name="showByHomewiki" value="showByHomewiki-' + wiki
		 '" id="showByHomewiki-' + wiki + '" checked=""><label for="showByHomewiki-' + wiki + '">' + wiki +' (' + homeWikiDict[wiki] + ')</label>';
   }
   table += '</td></tr>';
   table += '<tr><td id="activityLabel">Teilnehmeraktivität:</td><td>'
		 + '<input class="control_element" type="radio" name="showActiveUsers" value="activeAllVal" id="activeAllVal" checked=""><label for="activeAllVal">zeige alle</label>'
		 + '<input class="control_element" type="radio" name="showActiveUsers" value="activeActiveOnly" id="activeActiveOnly"><label for="activeActiveOnly">nur aktive (' + $("span.participant.active").length + ')</label>'
		 + '<input class="control_element" type="radio" name="showActiveUsers" value="activeInactiveOnly" id="activeInactiveOnly"><label for="activeInactiveOnly">nur inaktive (' + $("span.participant.inactive").length + ')</label></td></tr>';
   table += '<tr><td id="verifiedLabel">bestätigt:</td><td>'
		 + '<input class="control_element" type="radio" checked="" value="vervAllVal" name="showVervAllUsers" id="vervAllVal"><label for="vervAllVal">zeige alle</label>'
		 + '<input class="control_element" type="radio" value="vervOnlyVervVal" name="showVervAllUsers" id="vervOnlyVervVal"> <label for="vervOnlyVervVal">nur bestätigte (' + $("span.participant.verified").length + ')</label>'
		 + '<input class="control_element" type="radio" value="vervOnlyUnvervVal" name="showVervAllUsers" id="vervOnlyUnvervVal"> <label for="vervOnlyUnvervVal">nur unbestätigte (' + $("span.participant.unverified").length + ')</label></td></tr>';
   table += '</table>';
	$("#no-gadget-active").html(table);
	// add interaction
	$('input[name=showByHomewiki], input[name=showActiveUsers], input[name=showVervAllUsers]').change(filterBySelection);
}

function isGadgetVersionOutdated() {
   // check if we are the latest version or an old cached version
   var currentVersion = loadFile(window.persBekannt.versionCheckPage);
   if (currentVersion == "stop") {
	   showErrorMesage('<br /><br />' + msg.gadgetStopped);
	   return true;
   } else if (currentVersion > window.persBekannt.PBJSversion) {
	   showErrorMesage('<br /><br />' + msg.gadgetOutdated);
	   return true;
   }
   return false;
}

function checkIfExtendedUserListWasLoaded() {
	if (window.persBekannt.extendedUserlist && window.persBekannt.extendedUserlist.myUserName && $("td#userListTd").length) {
		if (window.persBekannt.extendedUserlist.myUserName != mw.config.get('wgUserName')) {
			showErrorMesage('User name does not match the requested in "persBekannt.extendedUserlist.myUserName"');
			return;
		}
		// loading was successful
		// add additional GUI elements
		$("td#userListTd").prepend('<table><tbody><tr><td>Filter:</td></tr>' +
		'<tr><td><input type="checkbox" checked="true" name="hideConfirmedUsers" id="hideConfirmedUsers">Verstecke bereits von mir bestätigte Benutzer</td></tr>' +
		'<tr><td><input type="checkbox" checked="true" name="hideInactiveUsers" id="hideInactiveUsers">Verstecke inaktive Benutzer</td></tr>' +
		'</tbody></table>');
		$("input#hideConfirmedUsers").click(function() { filterExtendedUserList() });
		$("input#hideInactiveUsers").click(function() { filterExtendedUserList() });
		filterExtendedUserList();
	} else {
		// we must wait
		setTimeout('checkIfExtendedUserListWasLoaded()', 300);
	}
}

function filterExtendedUserList() {
	$('td#userListTd option').each(function(){
		userName = $(this).val();
		if (userName == "null") { return; } // this is the first element
		excludeThis = $('input#hideConfirmedUsers').attr('checked')=='checked' && $.inArray(userName, window.persBekannt.extendedUserlist.alreadyConfirmedUsers) != -1;
		excludeThis |= $('input#hideInactiveUsers').attr('checked')=='checked' && $.inArray(userName, window.persBekannt.extendedUserlist.alreadyConfirmedUsers) != -1;
		if (excludeThis) {
			if ($(this).is(':selected')) {
				// select the first item
				$('td#userListTd option').val('null');
			}
			$(this).hide();
		} else {
			$(this).show();
		}
	});
}

function logging(msg) {
	var now = new Date();
	logVar += "*<b>'''['''</b>" + now.getMonth() + "/" + now.getDate() + " " + now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + "<b>''']'''</b> " + msg + "<br>";
}

function startGUI() {
	if (mw.config.get('wgPageName') === decodeURIComponent(window.persBekannt.workPage)) {
		buildUpGUIForWorkPage();
	} else if (mw.config.get('wgPageName') === decodeURIComponent(window.persBekannt.bigUserList)) {
		buildUpGUIForUserlist();
	}
}
// Start the script
$(document).ready( startGUI );
// </nowiki>