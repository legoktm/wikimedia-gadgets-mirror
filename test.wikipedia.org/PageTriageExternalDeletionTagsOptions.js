$.pageTriageDeletionTagsOptions.Main.proposeddeletion.tags.prod.tag = 'subst:prod';

$.pageTriageDeletionTagsOptions.Main.proposeddeletion.tags.blpprod.tag = 'subst:blp-prod';

$.pageTriageDeletionTagsOptions.Main.speedydeletioncommon.desc = "Mark this page for speedy deletion only if it fits one of the criteria below. There is no catch-all – if it doesn’t fit, use PROD or AfD.";

// Redefine all speedy-deletion tags in order to add new ones in their correct order.
$.pageTriageDeletionTagsOptions.Main.speedydeletioncommon.tags = {
	"dba1": {
		"label": "No context",
		"tag": "speedy deletion-no context",
		"code": "A1",
		"desc": "Articles lacking sufficient context to identify the subject of the article. (A1)",
		"params": {},
		"anchor": "nocontext",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Empty-warn-NPF"
	},
	"dba2": {
		"label": "Foreign language articles that exist on another Wikimedia project",
		"tag": "speedy deletion-foreign language",
		"code": "A2",
		"desc": "Articles having essentially the same content as an article on another Wikimedia project. (A2)",
		"params": {
			"source": {
				"label": "Please add a URL for that source:",
				"input": "required",
				"type": "text",
				"value": ""
			}
		},
		"anchor": "notenglish",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-foreign-notice-NPF"
	},
	"dba3": {
		"label": "No content",
		"tag": "speedy deletion-no content",
		"code": "A3",
		"desc": "Any article (other than disambiguation pages, redirects, or soft redirects) consisting only of external links, category tags and \"see also\" sections, a rephrasing of the title, attempts to correspond with the person or group named by its title, a question that should have been asked at the help or reference desks, chat-like comments, template tags, and/or images. (A3)",
		"params": {},
		"anchor": "nocontent",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Nocontent-warn-NPF"
	},
	"dba7_person": {
		"label": "Unremarkable person",
		"tag": "db-person",
		"code": "A7",
		"desc": "An article about a real person that does not assert the importance or significance of its subject. If controversial, or if there has been a previous AfD that resulted in the article being kept, the article should be nominated for AfD instead. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_band": {
		"label": "Unremarkable musician(s) or band",
		"tag": "db-band",
		"code": "A7",
		"desc": "Article about a band, singer, musician, or musical ensemble that does not assert the importance or significance of the subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_club": {
		"label": "Unremarkable club",
		"tag": "db-club",
		"code": "A7",
		"desc": "Article about a club that does not assert the importance or significance of the subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_corp": {
		"label": "Unremarkable company or organization",
		"tag": "db-corp",
		"code": "A7",
		"desc": "Article about a company or organization that does not assert the importance or significance of the subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_web": {
		"label": "Unremarkable website or web content",
		"tag": "db-web",
		"code": "A7",
		"desc": "Article about a web site, blog, online forum, webcomic, podcast, or similar web content that does not assert the importance or significance of its subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_animal": {
		"label": "Unremarkable individual animal",
		"tag": "db-animal",
		"code": "A7",
		"desc": "Article about an individual animal (e.g. pet) that does not assert the importance or significance of its subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba7_event": {
		"label": "Unremarkable organized event",
		"tag": "db-event",
		"code": "A7",
		"desc": "Article about an organized event (tour, function, meeting, party, etc.) that does not assert the importance or significance of its subject. (A7)",
		"params": {},
		"anchor": "importance",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-notability-notice-NPF"
	},
	"dba9": {
		"label": "No indication of importance (musical recordings)",
		"tag": "speedy deletion-musical recording",
		"code": "A9",
		"desc": "An article about a musical recording that does not indicate why its subject is important or significant and where the artist's article does not exist (both conditions must be true). (A9)",
		"params": {},
		"anchor": "music",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-a9-notice-NPF"
	},
	"dba10": {
		"label": "Recently created article that duplicates an existing topic",
		"tag": "speedy deletion-duplicate article",
		"code": "A10",
		"desc": "A recently created article with no relevant page history that duplicates an existing English Wikipedia topic, and that does not expand upon, detail or improve information within any existing article(s) on the subject, and where the title is not a plausible redirect. (A10)",
		"params": {
			"article": {
				"label": "Article:",
				"input": "required",
				"type": "text",
				"value": ""
			}
		},
		"anchor": "duplicate",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-a10-notice-NPF"
	},
	"dba11": {
		"label": "Obviously made up by creator, and no claim of significance",
		"tag": "db-hoax",
		"code": "A11",
		"desc": "An article which plainly indicates that the subject was invented/coined/discovered by the article's creator or someone they know personally, and does not credibly indicate why its subject is important or significant. (A11)",
		"params": {},
		"anchor": "",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-vandalism-notice"
	},
	"dbg1": {
		"label": "Patent nonsense",
		"tag": "speedy deletion-nonsense",
		"code": "G1",
		"desc": "A page that is patent nonsense, consisting purely of incoherent text or gibberish with no meaningful content or history. (G1)",
		"params": {},
		"anchor": "nonsense",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-nonsense-notice-NPF"
	},
	"dbg2": {
		"label": "Test pages",
		"tag": "speedy deletion-test page",
		"code": "G2",
		"desc": "A page created to test editing or other Wikipedia functions. (G2)",
		"params": {},
		"anchor": "test",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-test-notice-NPF"
	},
	"dbg3_vandalism": {
		"label": "Pure vandalism",
		"tag": "speedy deletion-vandalism",
		"code": "G3",
		"desc": "Plain pure vandalism including redirects left behind by page move vandalism. (G3)",
		"params": {},
		"anchor": "vandalism",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-vandalism-notice-NPF"
	},
	"dbg3_hoax": {
		"label": "Blatant hoax",
		"tag": "speedy deletion-vandalism",
		"code": "G3",
		"desc": "Blatant and obvious misinformation to the point of vandalism. (G3)",
		"params": {},
		"anchor": "vandalism",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-vandalism-notice-NPF"
	},
	"dbg4": {
		"label": "Recreation of a page that was deleted per a deletion discussion",
		"tag": "speedy deletion-previously deleted",
		"code": "G4",
		"desc": "A sufficiently identical and unimproved copy, having any title, of a page deleted via its most recent deletion discussion. (G4)",
		"params": {
			"1": {
				"label": "Please add a URL for that source.",
				"input": "required",
				"type": "text",
				"value": ""
			}
		},
		"anchor": "repost",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Uw-repost-NPF"
	},
	"dbg5": {
		"label": "Creations by banned or blocked users",
		"tag": "speedy deletion-blocked user",
		"code": "G5",
		"desc": "Pages created by banned or blocked users in violation of their ban or block, and which have no substantial edits by others. (G5)",
		"params": {},
		"anchor": "banned",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-banned-notice-NPF"
	},
	"dbg6": {
		"label": "Unnecessary disambiguation page",
		"tag": "db-disambig",
		"code": "G6",
		"desc": "This only applies for orphaned disambiguation pages which either: (1) disambiguate two or fewer existing Wikipedia pages and whose title ends in \"(disambiguation)\" (i.e., there is a primary topic); or (2) disambiguates no (zero) existing Wikipedia pages, regardless of its title. (G6)",
		"params": {},
		"anchor": "",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-disambig-notice-NPF"
	},
	"dbg7": {
		"label": "Author requests deletion",
		"tag": "speedy deletion-author request",
		"code": "G7",
		"desc": "Pages where the author has requested deletion, either explicitly or by blanking the page. (G7)",
		"params": {},
		"anchor": "blanked",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-author-notice-NPF"
	},
	"dbg10_attack": {
		"label": "Attack pages",
		"tag": "speedy deletion-attack",
		"code": "G10",
		"desc": "Pages that disparage, threaten, intimidate or harass their subject or some other entity, and serve no other purpose. (G10)",
		"params": {},
		"anchor": "attack",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Db-attack-notice-NPF"
	},
	"dbg10_negublp": {
		"label": "Wholly negative, unsourced BLP",
		"tag": "db-negublp",
		"code": "G10",
		"desc": "A biography of a living person that is entirely negative in tone and unsourced, where there is no neutral version in the history to revert to. (G10)",
		"params": {},
		"anchor": "",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "db-negublp-notice"
	},
	"dbg11": {
		"label": "Unambiguous advertising or promotion",
		"tag": "speedy deletion-advertising",
		"code": "G11",
		"desc": "Pages that are exclusively promotional, and would need to be fundamentally rewritten to become encyclopedic. (G11)",
		"params": {},
		"anchor": "spam",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Spam-warn-NPF"
	},
	"dbg12": {
		"label": "Unambiguous copyright infringement",
		"tag": "speedy deletion-copyright violation",
		"code": "G12",
		"desc": "Text pages that contain copyrighted material with no credible assertion of public domain, fair use, or a compatible free license, where there is no non-infringing content on the page worth saving. (G12)",
		"params": {
			"url": {
				"label": "Please add a URL for that source.",
				"input": "required",
				"type": "text",
				"value": ""
			}
		},
		"anchor": "copyvio",
		"talkpagenotiftopictitle": "pagetriage-del-tags-speedy-deletion-nomination-notify-topic-title",
		"talkpagenotiftpl": "Nothanks-sd-NPF"
	}
};