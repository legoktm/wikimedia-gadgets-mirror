/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Shows notices to registered users based on their location
 */
 
/* global mw */
if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Watchlist' ) {
	mw.loader.load( 'ext.gadget.geonotice-core' );
}