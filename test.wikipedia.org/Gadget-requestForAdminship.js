/**
 * Creates and closes requests for adminship in pt.wikipedia
 *
 * @author [[w:pt:user:!Silent]]
 * @see [[MediaWiki:Gadget-requestForAdminship.js/core.js]]
 */

( function ( mw, $, window ) {
'use strict';

if ( mw.config.get( 'wgPageName' ) === 'Wikipédia:Administradores/Pedidos_de_aprovação'
	||  mw.config.get( 'wgPageName' ).indexOf( 'Wikipédia:Administradores/Pedidos_de_aprovação/' ) !== -1
) {
	mw.loader.load( 'ext.gadget.requestForAdminshipCore' );
}


}( mediaWiki, jQuery, window ) );

// [[Categoria:!Código-fonte de scripts|Pedido de administração]]