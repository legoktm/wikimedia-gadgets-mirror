/**
 * Wikimedia Portal Template Previewer.
 * See also [[m:Project portals]].
 *
 * @author Splarka, 2008
 * @author Krinkle, 2012
 * @source meta.wikimedia.org/wiki/MediaWiki:Gadget-wm-portal-preview.js
 */
/*global mediaWiki, jQuery */
/*jslint browser: true */
(function (mw, $) {
	"use strict";
	var pageName = mw.config.get('wgPageName'), action = mw.config.get('wgAction'), link;

	function createPopup(html) {
		var popupWindow = window.open('', 'portal_preview', 'scrollbars=1,height=600,width=800,toolbar=0,menubar=0,location=1,directories=0,status=0');
		popupWindow.document.write(html);
		popupWindow.document.close();
	}

	function previewTextareaContents() {
		var html = $('#wpTextbox1').val();
		createPopup(html);
	}

	function previewWikitextFromApi() {
		$.ajax({
			url: mw.util.wikiScript('api'),
			dataType: 'json',
			data: {
				format: 'json',
				action: 'query',
				prop: 'revisions',
				rvprop: 'content',
				indexpageids: '',
				titles: pageName
			}
		}).always(function (obj) {
			var page, html;

			if (obj.query && obj.query.pageids && obj.query.pages) {
				page = obj.query.pages[obj.query.pageids[0]];
				if (page && page.revisions && page.revisions[0] && page.revisions[0]['*']) {
					html = page.revisions[0]['*'];
					createPopup(html);
					return;
				}
			}

			// Error
			mw.util.jsMessage('Portal preview could not be rendered.');
		});
	}

	function init() {
		// Determine if this page is likely to be a project portal
		// @todo: Maybe use categories in a <!-- [[Category: ..]] --> comment and use wgCategories
		if (
			(pageName.indexOf('Www.') === 0 || pageName.indexOf('Secure.') === 0 || pageName.indexOf('Wikimedia_missing_site') === 0)
				&& pageName.indexOf('_template') > 0
		) {
			if (action === 'edit' || action === 'submit') {
				link = mw.util.addPortletLink(
					'p-cactions',
					'#',
					'Preview HTML',
					'ca-portalpreview',
					'Render contents of the textarea as HTML (Warning: may contain arbitrary javascript)'
				);
				$(link).click(previewTextareaContents);
			} else {
				link = mw.util.addPortletLink(
					'p-cactions',
					'#',
					'Preview HTML',
					'ca-portalpreview',
					'Render contents of this wikipage as HTML (Warning: may contain arbitrary javascript)'
				);
				$(link).click(previewWikitextFromApi);
			}
		}
	}

	$(document).ready(init);

}(mediaWiki, jQuery));