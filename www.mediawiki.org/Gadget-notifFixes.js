$(function () {
  // Replace broken Echo stuff with a temporary work around (bug 41830, bug 41814)
  $('#pt-notifications').replaceWith('<li id="pt-lqtmessages"><a href="/wiki/Special:NewMessages">My new messages</a></li>');
});