/**
 * Warning! Global gadget file!
 * 
 * This gadget adds a clock in the personal toolbar that shows the current time
 * in UTC, and also provides a link to purge the current page.
 *
 * Revision: November 2017
 * Source: https://www.mediawiki.org/wiki/MediaWiki:Gadget-UTCLiveClock.js
 *
 * Installation:
 * 
 * 1. Copy the JS page at https://www.mediawiki.org/wiki/MediaWiki:Gadget-UTCLiveClock.js
 * to the page [[MediaWiki:Gadget-UTCLiveClock.js]] on your wiki.
 * 
 * 2. Copy the CSS page at https://www.mediawiki.org/wiki/MediaWiki:Gadget-UTCLiveClock.css
 * to the page [[MediaWiki:Gadget-UTCLiveClock.css]] on your wiki.
 * 
 * 3. Copy the CSS page at https://www.mediawiki.org/wiki/MediaWiki:Gadget-UTCLiveClock-pagestyles.css
 * to the page [[MediaWiki:Gadget-UTCLiveClock-pagestyles.css]] on your wiki.
 * 
 * 4. Add a description of the gadget to the page [[MediaWiki:Gadget-UTCLiveClock]]
 * on your wiki. You can use https://www.mediawiki.org/wiki/MediaWiki:Gadget-UTCLiveClock
 * as a template.
 * 
 * 5. Add the following code to your wiki's [[MediaWiki:Gadgets-definition]]:
 * 
 *     * UTCLiveClock[ResourceLoader|type=general|dependencies=mediawiki.util,mediawiki.api,mediawiki.notify|peers=UTCLiveClock-pagestyles]|UTCLiveClock.js|UTCLiveClock.css
 *     * UTCLiveClock-pagestyles[hidden|skins=vector,monobook]|UTCLiveClock-pagestyles.css
 * 
 */
/*global mw, $ */
mw.loader.using( ['mediawiki.util', 'mediawiki.api', 'mediawiki.notify'] ).then( function () {

function padWithZeroes( num ) {
	// Pad a number with zeroes. The number must be an integer where
	// 0 <= num < 100.
	return num < 10 ? '0' + num.toString() : num.toString(); 
}

function showTime( $target ) {
	var now = new Date();

	// Set the time.
	var hh = now.getUTCHours();
	var mm = now.getUTCMinutes();
	var ss = now.getUTCSeconds();
	var time = padWithZeroes( hh ) + ':' + padWithZeroes( mm ) + ':' + padWithZeroes( ss );
	$target.text( time );

	// Schedule the next time change.
	// 
	// We schedule the change for 100 ms _after_ the next clock tick. The delay
	// from setTimeout is not precise, and if we aim exactly for the tick, there
	// is a chance that the function will run slightly before it. If this
	// happens, we will display the same time for two seconds in a row - not
	// good. By scheduling 100 ms after the tick, we will always be about 100 ms
	// late, but we are also very likely to display a new time every second.
	var ms = now.getUTCMilliseconds();
	setTimeout( function () {
		showTime( $target );
	}, 1100 - ms );
}

function liveClock() {
	// Set CSS styles. We do this here instead of on the CSS page because some
	// wikis load this page directly, without loading the accompanying CSS.
	mw.util.addCSS( '#utcdate a { font-weight:bolder; font-size:120%; }' );

	// Reset whitespace that was set in the peer CSS gadget; this prevents the
	// effect of the p-personal menu jumping to the left when the JavaScript
	// loads.
	$( '.client-js > body.skin-vector #p-personal ul' ).css( 'margin-right', 'initial' );
	$( '.client-js > body.skin-monobook #p-personal ul' ).css( 'margin-right', 'initial' );

	// Add the portlet link.
	var node = mw.util.addPortletLink(
		'p-personal',
		mw.util.getUrl( null, { action: 'purge' } ),
		'',
		'utcdate'
	);
	if ( !node ) {
		return;
	}

	// Purge the page when the clock is clicked. We have to do this through the
	// API, as purge URLs now make people click through a confirmation screen.
	$( node ).on( 'click', function ( e ) {
		new mw.Api().post( { action: 'purge', titles: mw.config.get( 'wgPageName' ) } ).then( function () {
			location.reload();
		}, function () {
			mw.notify( 'Purge failed', { type: 'error' } );
		} );
		e.preventDefault();
	} );

	// Show the clock.
	showTime( $( node ).find( 'a:first' ) );
}

$( liveClock );
} );