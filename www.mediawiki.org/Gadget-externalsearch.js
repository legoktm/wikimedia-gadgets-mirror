/**
 * Change Special:Search to use a drop-down menu
 *
 *  Description: Change Special:Search to use a drop-down menu, with the default being
 *               the internal MediaWiki engine, to add the custom Google search engine
 *               described at [[Wikimedia technical search]]
 *  Created and maintained by: [[w:en:User:Gracenotes]]
 */
/*global mw */
(function () {
    var searchEngines = [];

    function setupSearchEngines() {
        var searchForm, searchBox, lStat;
        var createOption = function (label, formAction, formName, fieldName, fieldValue) {
            var opt = document.createElement('option');
            opt.appendChild(document.createTextNode(label));
            searchEngines.push({ formAction: formAction, formName: formName, fieldName: fieldName, fieldValue: fieldValue });
            return opt;
        };

        searchForm = document.forms.powersearch || document.forms.search;

        searchBox = searchForm.lsearchbox || searchForm.search;
        var selectBox = document.createElement('select');
        selectBox.id = 'searchEngine';
        searchForm.onsubmit = function () {
            var optSelected = searchEngines[document.getElementById('searchEngine').selectedIndex];
            searchForm.action = optSelected.formAction;
            searchBox.name = optSelected.formName;
            searchForm.title.name = optSelected.fieldName;
            searchForm.title.value = optSelected.fieldValue;
        };
        selectBox.appendChild(createOption('MediaWiki.org', mw.config.get( 'wgScript' ), 'search', 'title', 'Special:Search'));
        selectBox.appendChild(createOption('Google for wiki tech', 'https://www.google.com/cse', 'q', 'cx', '010768530259486146519:twowe4zclqy'));
        searchBox.style.marginLeft = '0px';
        if (document.getElementById('loadStatus')) {
            lStat = document.getElementById('loadStatus');
        } else {
            lStat = searchForm.title;
            if ( lStat && lStat.length ) {
                lStat = lStat[0];
            }
        }
        lStat.parentNode.insertBefore(selectBox, lStat);
    }

    if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Search' ) {
        // Only run on Special:Search
        $(setupSearchEngines);
    }
}());