/* JavaScript placed here customizes WikiLove, see https://www.mediawiki.org/wiki/Extension:WikiLove#Custom_configuration */
/**
 * This script extends WikiLove's own set of elements of some custom barnstars
 * requested by users.
 */
/*global jQuery:false */
/*jshint curly:false */
// jshint valid

( function ( $ ) {
'use strict';
  
$.extend(true, $.wikiLoveOptions, {
	types: {
		'barnstar': {
			// some new/own barnstars
			subtypes: {
				'translators': {
					fields: [ 'message' ],
					option: 'Translator Barnstar',
					descr: 'A Barnstar for our finest translators who help to translate mediawiki.org in so much languages.',
					header: 'A barnstar for you!',
					title: 'The Translator Barnstar',
					image: 'Translation-Barnstar.png'
				}
			}
		}
	}
});

}( jQuery ) );