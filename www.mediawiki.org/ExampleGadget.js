/**
 * This is an example gadget
 * It has no goal other than to show you techniques you might want to use in your own gadgets
 */
// 1: Module pattern
// This first line has two basic functions. Creating "functional scoping"
// and to rename and keep track of our our global variables
// mw and $ are also declared globally, but due to their length there is a higher
// chance of collisions and especially $ is often used by other libraries as well.
// Check the bottom of this script, for the last line, which closes off this statement
( function( mw, $ ) {
	// 2: Any variables you want to use for the duration of the module's lifetime
	// can be kept here. They are the global state of the module.
	var state;
	
	// 3: Functions that you use internally within the module should be defined
	// as named functions. Try to avoid declaring functions inside functions,
	// but especially avoid declaring them inside loops, because that is. 
	function aPrivateFunction() {
		
	}
	
	// Code that you execute here, runs immediately
	mw.log( 'Is executed immediately while this script is loading' );

	// 4: Waiting for the page to be ready	
	function pageReady() {
		// From here we can start reading and adding to the the webpage.
		// Before this point, the document will be available, but possibly incomplete
		mw.log( 'The document is ready' );
	}
	
	// There are multiple ways to wait for the page to be complete.
	// Variant 1:
	$( document ).ready( pageReady );
	// Variant 2: Shortest and most used, identical to the above
	$( pageReady );
	// Variant 3: Use this if you need to wait for the page to be ready,
	// but also need to wait or guarantee that other conditions are fulfilled
	// This uses promises to wait for both document ready to finish and for the
	// mediawiki.util library to finish loading before it executes pageReady
	$.when( $.ready, mw.loader.using( 'mediawiki.util' ) ).then( pageReady );
	
	// 5: ResourceLoader modules basics
	// MediaWiki uses a resource loader to segment Javascript, CSS,
	// DOM templates and translation messages into bundles, that can be loaded on
	// demand. MediaWiki has many modules and libraries (thousands), so on demand
	// loading a library ONLY when the user actually requires it is very important
	// and becoming somewhat familiar with how this works is therefor critical
	// 
	// The mediawiki.loader aka mw.loader takes care and provides all these options
	// The loader is always available
	//
	// 
	$.when( mw.loader.using( ['mediawiki.util'] ), $.ready ).then( function() {
		// Here we guarantee that the provided array of modules is loaded,
		// in this case only mediawiki.util, as well as that the document is available.
		
		// If the modules were already loaded by something else on the page, then
		// this is a noop and this function will load immediately.
		
		// We can now add a link to the toolboxes for instance
		mw.util.addPortletLink( '/wiki/MediaWiki:ExampleGadget.js', 'p-tb', 'Example Gadget' );
	} );
	
	
// TODO 
// 2: mw.hooks
// 3: addportletlink
// 4: resource loader modules (and loader)
// 5: require pattern
// 6: api requests
// 7: preferences
// 7: click handler preventDefault()
// 8: using external cdnjs libraries
// 9: Advanced resourceloader techniques
//   - load another gadget
//   - preloading

// 1: End of 1
// Here we execute the anonymous function preceding it, and pass it global
// variables that we want it to be able to use. These are supposed to be the
// 'only' global variables you use, making it easier to avoid accidently leaking
// variables into your global scope.
} )( mediaWiki, jQuery );