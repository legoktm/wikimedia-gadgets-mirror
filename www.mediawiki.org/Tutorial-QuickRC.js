/**
 * Tutorial script: QuickRC ("Quick Recent Changes")
 *
 * A tutorial user script which adds a "Quick changelog" link to the page skin's
 * toolbox, and when clicked it pops up a dialog with up to 25 recent edits.
 *
 * Demonstrates:
 * - Use of the API
 * - Use of jQuery
 * - Use of ResourceLoader and some of the default modules that come with it
 * - Use of localization
 *
 * (Be bold and improve it!)
 *
 * Authors:
 * Erik Moeller, 2011, public domain
 * Brion Vibber, 2012, public domain
 */

messages = {
    'en': {
        'quickchanges-title': 'Hello there!',
        'quickchanges-greeting': 'Welcome, $1!',
        'quickchanges-intro': 'The following pages have been recently modified:',
        'quickchanges-link': 'Quick changelog',
        'quickchanges-tooltip': 'Show a quick overview of changes'
    },
    'fr': {
        'quickchanges-title': 'Bonjour !',
        'quickchanges-greeting': 'Bienvenue, $1!',
        'quickchanges-intro': 'Ces pages ont été modifiées récemment :',
        'quickchanges-link': 'Modifications récentes'
        // Leave tooltip out to demonstrate fallback behavior
    }
};

mw.messages.set(messages['en']);
var lang = mw.config.get('wgUserLanguage');
if (lang && lang != 'en' && lang in messages) {
    mw.messages.set(messages[lang]);
}

// Import the jQuery dialog plugin before starting the rest of this script
mw.loader.using(['jquery.ui.dialog'], function() {

    function renderQuickRCDialog( pageLinks ) {
		var $dialog = $( '<div></div>' )
			.html(
				'<strong>' +
				mw.message('quickchanges-greeting', mw.user.getName()).escaped() +
				'</strong> ' +
				mw.message('quickchanges-intro').escaped() +
				'<br/><ul><li>' +
				pageLinks.join( '<br /><li>' ) + '</ul>'
			)
			.dialog({
				autoOpen: true,
				title: mw.message('quickchanges-title').plain(),
				width: '70%',
				modal: true
			});
	}

	function quickRC() {
		var myPageLinks = [];
		var myTitles = [];

		// Fetch recent changes from the API by one of jQuery's AJAX functions
		jQuery.getJSON(
			mw.util.wikiScript( 'api' ),
			{
				'format': 'json',
				'action': 'query',
				'list': 'recentchanges',
				'rclimit' : 25
			},
			function( data ) {

				// Build a unique array of links, using the mw.html library to format them.
				$.each ( data.query.recentchanges , function( index , rc ) {
					// Don't link to this title if we've seen this title already
					if ( $.inArray( rc.title, myTitles ) === -1 ) {
						myPageLinks.push(
							mw.html.element(
								'a', { href: mw.util.getUrl( rc.title ) }, rc.title
							)
						);
					}

					myTitles.push( rc.title );
				} ) ;

				renderQuickRCDialog( myPageLinks );
			}
		);
	}

	$(document).ready( function() {

		// Add a link to the toolbox
		var link = mw.util.addPortletLink(
			'p-tb',
			'#',
			mw.message('quickchanges-link').plain(),
			't-prettylinkwidget',
			mw.message('quickchanges-tooltip').plain(),
			'/',
			'#t-whatlinkshere'
		);

		// Create a jQuery object for this link so that we get
		// to use jQuery awesomeness like .click() for binding functions to events
		// and methods like e.preventDefault();
		$(link).click( function( e ) {
			// Avoid the browser going to '#'
			e.preventDefault();

			// Initiate quickRC!
			quickRC();
		});

	});

});