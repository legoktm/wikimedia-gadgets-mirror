/**
 * Utility gadget for CodeReview extension.
 * Adds button to interface to export a list of revision numbers,
 * based on the checkbox currently checked.
 * ! Beware: Code is hacky, quick and unstable but when it works,
 * ! it's a nice time-safer to do shell commands such as "svn merge -c ... "
 *
 * @author Roan Kattouw [[User:Catrope]], 2011
 * @author Timo Tijhof [[User:Krinkle]], 2011 
 */

/* console version */
window.checkedRevs = function( prefix, separator ) {
	if ( prefix === undefined ) {
 		prefix = 'r';
	}
	if ( separator === undefined ) {
		separator =  ', ';
	}
	var s = [];
	jQuery( '.TablePager_col_selectforchange input:checked' ).each( function() {
		s.push( prefix + jQuery( this ).val() );
	} );
	return s.reverse().join( separator );
}

/* GUI version */
window.checkedRevs_gui = function( e ) {
	var prefix = prompt( 'checkedRevs prefix', 'r' ),
		separator = prompt( 'checkedRevs separator', ', ' ),
		s = [];
	jQuery( '.TablePager_col_selectforchange input:checked' ).each( function() {
		s.push( prefix + jQuery( this ).val() );
	} );
	prompt( 'checkedRevs return', s.reverse().join( separator ) );
}

/* Add button for GUI version */
if ( mw.config.get( 'wgCanonicalSpecialPageName' ) === 'Code' ) {
	$( '.TablePager_nav' ).next( 'table' ).find( 'td:last' ).append(
		$('<input type="submit" value="Checked Revs" />' ).click( window.checkedRevs_gui )
	);
}