/**
 * Gadget to change the wiki page appearance
 * to appear as if it is opposite to what is considered by some the aspired convention.
 * For example, make titles appear title case instead of sentence case.
 */
mw.loader.using( 'mediawiki.Title', function () {
  var title = new mw.Title( mw.config.get( 'wgPageName' ) );
  var wipTitle = title.toText().replace(/(\b[^\b])/g, function ( str ) {
    return str.toUpperCase();
  } );
  $( '.firstHeading' ).text( wipTitle );
} );