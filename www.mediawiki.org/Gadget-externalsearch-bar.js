/**
 * Add a small dropdown menu to the search box to add the [[Wikimedia technical search]].
 *
 * Created by [[d:User:Bene*]]
 */
( function ( mw, $ ) {
	var i18nData = {
		'en': {
			'title': 'Google for wiki tech',
			'description': 'Use the custom Wikimedia technical Google search',
			'placeholder': 'Google for wiki tech'
		}
	};

	if( $( 'body' ).is( '.rtl' ) ) {
		i18nData.en.dir = 'rtl';
		i18nData.en.left = 'right';
		i18nData.en.right = 'left';
	}
	else {
		i18nData.en.dir = 'ltr';
		i18nData.en.left = 'left';
		i18nData.en.right = 'right';
	}

	var i18nLang = mw.config.get( 'wgUserLanguage' ),
		lang = i18nLang.split( '-' )[0];

	if( !( i18nLang in i18nData ) ) {
		i18nLang = 'en';
	}

	/**
	 * Returns the localized version of a message.
	 */
	function i18n( key ) {
		if( i18nData[ i18nLang ].hasOwnProperty( key ) ) {
			return i18nData[ i18nLang ][ key ];
		}
		else {
			return i18nData.en[ key ];
		}
	}

	function toggle() {
		$( '#improvedsearch' ).toggle( 'fast' );
		$( '.improvedsearch-page' ).focus();
	}

	function init() {
		var $improvedSearch = $( '<form>' )
		.attr( {
			id: 'improvedsearch',
			action: 'https://www.google.com/cse'
		} )
		.append(
			$( '<div>' )
			.attr( 'id', 'simpleSearch' ) // This is a hack.
			.append(
				$( '<input>' )
				.attr( {
					type: 'hidden',
					name: 'cx',
					value: '010768530259486146519:twowe4zclqy'
				} )
			)
			.append(
				$( '<input>' )
				.attr( {
					id: 'searchInput', // This is a hack.
					'class': 'improvedsearch-page',
					type: 'text',
					accesskey: 'g',
					title: i18n( 'title' ) + ' [g]',
					name: 'q',
					autocomplete: 'off',
					placeholder: i18n( 'placeholder' )
				} )
			)
			.append(
				$( '<input>' )
				.attr( {
					id: 'searchButton', // This is a hack.
					'class': 'searchButton',
					type: 'submit',
					title: i18n( 'description' ),
					value: i18n( 'title' )
				} )
			)
		);
		if( mw.config.get( 'skin' ) === 'vector' ) {
			$( '#p-search' ).append( $improvedSearch );
			$( '#improvedsearch' ).hide( 0 );
			var toggleCss = {
				width: '20px',
				height: '1.3em'
			};
			toggleCss['margin-' + i18n( 'left' )] = '21.7em';
			toggleCss['margin-' + i18n( 'right' )] = '-0.8em';
			toggleCss['margin-top'] = '-23px';
			toggleCss['background-image'] = 'linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz48c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjIyIiBoZWlnaHQ9IjE2Ij48cGF0aCBkPSJNMTUuNTAyIDYuMDAxbC01IDUuMDAxLTUtNS4wMDF6IiBmaWxsPSIjNzk3OTc5Ii8+PC9zdmc+")';
			toggleCss['background-position'] = 'center';
			$( '#searchform' ).after(
				$( '<div>' )
				.attr( {
					id: 'improvedsearch-toggle',
					'class': 'vectorMenu',
					title: i18n( 'title' )
				} )
				.css( toggleCss )
				.click( toggle )
			);
		} else if( $( '#searchBody' ).length ) {
			$( '#searchBody' ).append( '<br>' ).append( $improvedSearch );
		}
		// Call updateTooltipAccessKeys after inserting into DOM tree
		// to get a localized access key label in Firefox (bug 67946)
		$( '.improvedsearch-page' ).updateTooltipAccessKeys();
	}

	$( document ).ready( init );
} ( mediaWiki, jQuery ) );