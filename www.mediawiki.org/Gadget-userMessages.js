/*
==Automatic user messages ==
Installing this script as described on the talk page will give you links in the toolbox
for all pages in the User talk: namespace, that say 'Please tag', 'Please name', and
'Please link'. Clicking any of the buttons will add one of the following templates to
the user's talk page, signed by you:
*{{tl|Welcome}}
 
 
Main code by [[User:Siebrand]], derived from [[MediaWiki:Quick-delete.js]] by
[[:en:User:Jietse Niesen|Jietse Niesen]], [[User:pfctdayelise|pfctdayelise]] and
[[User:Alphax|Alphax]].
 
Code stolen from Commons by [[User:ShakataGaNai]] for Wikinews; and then stolen from Wikinews by [[User:Diego Grez]] for MediaWiki.org
 
===TODO===
* Do not add a trailing /<langcode> at the end of the subst'ed template, as they now (almost) all use the autotranslation mechanism (see [[Commons:Template_i18n/User_templates]])
*/
// <syntaxhighlight lang="javascript">
 
// Configuration
 
// Should the edits be saved automatically?
if(window.template_autosave !== false){ window.template_autosave = false; }
 
// Template Name
var uTemplate = [
	'Welcome',
	'Vandalism',
	'Spam',
	'Scope',
	'Test',
	'Not an encyclopedia',
	'Not wikibooks',
	'User:Jasper Deng/spambotblock|1={{subst:REVISIONUSER}}|2=',
	'User:Jasper Deng/indef|1=spamming|2='
];
 
// Text to be shown in Toolbox
var uText = [
	"Welcome",
	"Vandalism",
	"Spam warning",
	"Offtopic",
	"Test",
	"Not Wikipedia",
	"Not Wikibooks",
	"spamBot Block",
	"indef blocked"
];

// Mouseover help text for Toolbox
var uHelp = [
	"Welcome a new user",
	"Vandalism warning.",
	"Spam warning",
	"Adding off-topic content.",
	"Testing outside the sandbox",
	"Adding encyclopedic content",
	"Adding Wikibooks-esque content",
	"spamBot block",
	"indef blocked"
];

// Add the template
function template_mark(talkpage_fakeaction) {
	var editlk = document.getElementById('ca-edit').getElementsByTagName('a')[0].href;
	document.location = editlk + '&fakeaction=' + talkpage_fakeaction;
}
 
// Add template to user talk page
function template_addTemplate(template) {
	var txt = '{{subst:' + template + '}}';
	document.editform.wpTextbox1.value = document.editform.wpTextbox1.value + '\n' + txt + '\n~~~~';
	//  the edit summary for when you mark the image. You can change it if you want.
	document.editform.wpSummary.value = 'Adding template \"' + template + '\" to inform user. ([[User:Diego Grez/UserMessages.js|using UserMessages]])';
	if (template_autosave) document.editform.wpSave.click();
}

function makeVectorFancySection()  {
	//wrap this in a try. this might be somewhat delicate at the moment.
	var pNotify = document.createElement('div');
	pNotify.id = 'p-Notify';
	pNotify.className = 'vectorMenu';
	pNotify.innerHTML = ' <h3><span>Notify</span><a href="#"></a></h3> <div class="menu"> <ul> </ul> </div>';
	var rightNav = document.getElementById('right-navigation');
	var pViews = document.getElementById('p-views');
	pViewsCont = document.createElement('div');
	pViewsCont.id = 'p-views-continued';
	pViewsCont.className = 'vectorTabs';
	var pViewsUL = document.createElement('ul');
	pViewsCont.appendChild(pViewsUL);
	var pivot = (document.getElementById('ca-history') ? document.getElementById('ca-history') : document.getElementById('ca-addsection'));
	pivot = (pivot ? pivot : document.getElementById('ca-edit'));
	pViewsUL.appendChild(pivot);
	rightNav.insertBefore(pNotify, pViews.nextSibling);
	rightNav.insertBefore(pViewsCont, pNotify.nextSibling); 
}
 
// Add the menu, or add the template to the edit page
function template_onload() {
    try {
    if (mw.config.get('skin') === 'vector') {
        if (window.useFancyVectorDropdown || window.useFancyVectorDropdown === undefined) {
            try {
                makeVectorFancySection();
                useFancyVectorDropdown = 'done'; //this is for debug. can remove
            }
            catch (e) {
                document.getElementById('panel').innerHTML += '<div id="p-Notify" class="portal"><h3 lang="en" xml:lang="en">Notify</h3><div class="body">\n <ul> </ul> </div> </div>';
            }
        }
        else {
            document.getElementById('panel').innerHTML += '<div id="p-Notify" class="portal"><h3 lang="en" xml:lang="en">Notify</h3><div class="body">\n <ul> </ul> </div> </div>';
       }
    } else if (mw.config.get('skin') === 'monobook') {
        document.getElementById('column-one').innerHTML += '<div id="p-Notify" class="portlet"> <h3 lang="en" xml:lang="en">Notify</h3> <div class="pBody">	<ul></ul></div>	</div>';
    }
    } catch (e) {} //ignore errors and just use tb if they happen.
    var portlet = (document.getElementById('p-Notify') ? 'p-Notify' : 'p-tb');
 
    for( var i = 0; i < uText.length; i++ ) {
	    var node = mw.util.addPortletLink(portlet, '', uText[i], 'mark-warn', uHelp[i], null, null);
	    $( node ).click( { template_idx: i }, function(e) {
	    	e.preventDefault();
	    	template_mark( e.data.template_idx );	
	    } );
    }

	// On the edit page, check the fackaction param, to see if we need to add a template 
	var action_idx = -1;
	try {
		action_idx = parseInt (mw.util.getParamValue('fakeaction'), 10);
	} catch (some_error) {
		action_idx = -1;    
	}
	if ( uTemplate[ action_idx ] !== undefined ) {
		//may i take this moment to mention, this script is very very confusing
		if ( mw.config.get( 'wgNamespaceNumber' ) !== 3 ) { 
			alert("The user notify script has been disabled in this namespace for security reasons. If you see this message, please tell [[User:Diego Grez]] what linked you clicked on to get here.");
			throw new Error("Security error: wrong namespace for user notify gadget.");
		}
		template_addTemplate( uTemplate[ action_idx ] );
	}
}
 
// NS_USERTALK
if (
	mw.config.get('wgNamespaceNumber') === 3
	&& mw.config.get( 'wgPageContentModel' ) === 'wikitext'
) {
	// Load dependencies and wait for page to be ready
	$.when( mw.loader.using( [ 'mediawiki.util' ] ), $.ready )
		.done( template_onload );
}
// </syntaxhighlight>