// Author: Eee888
$(document).ready( function(){
	
		var initialDocTitle = document.title;
		var messageNotification = 0, alertNotification = 0;
	
		function addNotificationCounterToDocumentTitle(notificationType, newCountValue){
			
			if (!notificationType) { //first time the page is loaded
			
				var notificationElmArr = $('.mw-echo-notifications-badge');
				for(var elemIndex = 0; elemIndex < notificationElmArr.length ; elemIndex++){
					
					var elementNotificationNumber = parseInt($(notificationElmArr[elemIndex]).data('counter-num'));
					notificationElmArr[elemIndex].parentElement.id.indexOf('alert') > -1 ? 
						alertNotification += elementNotificationNumber : messageNotification += elementNotificationNumber;
				}
			} else {// it's an update
			
				'alert' == notificationType ? alertNotification = newCountValue : messageNotification = newCountValue;
			}
			var notificationToTitle = messageNotification + alertNotification;
			document.title = notificationToTitle !== 0 ? '('+mediaWiki.language.convertNumber(notificationToTitle)+') ' + initialDocTitle : initialDocTitle;
		}
		addNotificationCounterToDocumentTitle(); //for the initial page load
		mw.hook('ext.echo.badge.countChange').add(addNotificationCounterToDocumentTitle); //for any other notification counter change
});