jQuery( document ).ready( function ($) {
	var msgBlockLabel, msgBlockTooltip, title,
		lang = mw.config.get( 'wgUserLanguage' ),
		ns = mw.config.get( 'wgNamespaceNumber' );
	if ( ns !== 2 && ns !== 3 ) {
		// restrict to User and User talk
		return;
	}

	// @todo: Use MediaWiki message interface when Gadgets 2.0 is out and messages can be loaded through ResourceLoader
	msgBlockLabel = {
			en: 'Block',
			pl: 'Zablokuj'
	};
	msgBlockTooltip = {
			en: 'Block "$1"',
			pl: 'Zablokuj "$1"'
	};
	mw.messages.set( 'gadget-blocktab-label', msgBlockLabel[lang] || msgBlockLabel.en );
	mw.messages.set( 'gadget-blocktab-tooltip', msgBlockTooltip[lang] || msgBlockTooltip.en );

	title = mw.config.get( 'wgTitle' );
	// If / occurs in the title
	if ( title.indexOf( '/' ) !== -1) {
		// Strip it and anything after it from the title. This is to fix links from subpages.
		title = title.substr( 0, title.indexOf( '/' ) );
	}

	mw.util.addPortletLink(
		'p-cactions',
		new mw.Title( 'Special:Block/' + title ).getUrl(),
		mw.msg('gadget-blocktab-label', title),
		't-block',
		mw.msg('gadget-blocktab-tooltip', title)
	);
 
});