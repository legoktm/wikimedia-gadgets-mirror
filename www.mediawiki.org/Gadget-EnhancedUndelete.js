// Enhanced Undelete Tool
// Stolen from Wikibooks created & Maintained by [[b:User:Darklama]] and [[b:User:Mike.lifeguard]]
//
// Adds a "Select All" and "Invert Selection" button to Special:Undelete. 
// Used for administrators only.

if ( wgCanonicalSpecialPageName == 'Undelete' ) {
	jQuery( document ).ready( function() {
		if ( wgCanonicalSpecialPageName != 'Undelete' ) {
			return;
		}

		var fi = document.getElementsByTagName( 'input' );
		for ( i = 0; i < fi.length; i++ ) {
			if ( !fi[i].hasAttribute( 'type' ) ) {
				continue;
			}
			if ( fi[i].getAttribute( 'type' ) == 'reset' ) {
				var sa = document.createElement( 'input' );
				sa.setAttribute( 'type', 'button' );
				sa.setAttribute( 'value', 'Select All' );
				fi[i].parentNode.insertBefore( sa, fi[i].nextSibling );
				sa.onclick = function() {
					for ( var i = 0; i < fi.length; i++ ) {
						if ( fi[i].hasAttribute( 'type' ) && fi[i].getAttribute( 'type' ) == 'checkbox' ) {
							fi[i].checked = true;
						}
					}
				};

				// add invert selection button
				var inv = document.createElement( 'input' );
				inv.setAttribute( 'type', 'button' );
				inv.setAttribute( 'value', 'Invert All' );
				fi[i].parentNode.insertBefore( inv, fi[i].nextSibling );
				inv.onclick = function() {
					// if a deleted edit is checked, uncheck it, and vis-versa.
					for ( var i = 0; i < fi.length; i++ ) {
						if ( fi[i].hasAttribute( 'type' ) && fi[i].getAttribute( 'type' ) == 'checkbox' ) {
							fi[i].checked = !fi[i].checked;
						}
					}
				}
			} else if ( fi[i].getAttribute( 'type' ) == 'checkbox' ) {
				fi[i].checked = true;
			}
		}
	});
}