/**
 * Vector HeadAnchors
 * @source https://www.mediawiki.org/wiki/MediaWiki:Gadget-vector-headanchor.js
 * @revision 2015-03-07
 *
 * @license http://krinkle.mit-license.org/
 * @author Timo Tijhof, 2013–2015
 */
( function ( $ ) {

	$( '.mw-headline' ).each( function ( i, el ) {
		var id = el.id,
			$headline = $( el ),
			$header = $headline.parent();

		// If MediaWiki core's anchor is here, or if ours is here already, skip.
		if ( !id || $header.find( '.mw-headline-anchor,.mw-headline-headanchor').length ) {
			return;
		}

		$headline.removeAttr('id')
		$header.addClass( 'mw-header' ).prepend(
			$( '<a>' ).prop({
				id: id,
				href: '#' + id,
				title: 'Link to this section',
				className: 'mw-headline-headanchor'
			})
		);
	} );

}( jQuery ) );