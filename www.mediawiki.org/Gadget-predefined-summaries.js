if ( window.resumeDeluxe === undefined &&
        $.inArray( mw.config.get( 'wgAction' ), ['edit', 'submit']) > -1 &&
        mw.util.getParamValue( 'section' ) != 'new' ) {

    //Checker
    var resumeDeluxe = {
        titles: ["ortography", "- tipography", "- categorization",
                 "- order", "- translation", "- wikification",
                 "- image", "- redirect", "- style", "- comment", "- support", "- oppose",
                 "- revert", "- reorganization", "- answer",
                 "- maintenance", "- add", "- template", "- fix", "- links", "- warn", "- update"],
        inputs: ["ortography", "tipography", "categorization",
                 "order", "translation", "wikification",
                 "image", "redirect", "style", "comment", "support", "oppose",
                 "revert", "reorganization", "answer",
                 "maintenance", "add", "template", "fix", "links", "warn", "update"]
    };

    window.resumeDeluxe = resumeDeluxe;

    
    (function ( $, mw, undefined ) {
        'use strict';

        resumeDeluxe.addToSummary = function( str ) {
            var $summary = $( '#wpSummary' );
            if ( $summary.val() ) {
                $summary.val( $summary.val() + '; ' + str );
            } else {
                $summary.val( str );
            }
            return false;
        };

        function DeluxeSummary() {
            if ( $( '#wpSummaryLabel' )[0] !== undefined ) {
                var str = 'Predefined summaries: ',
                    numResumenes = resumeDeluxe.titles.length,
                    tmp,
                    i;
                $( '#wpSummary' ).width( '95%' );
                for ( i = 0; i < numResumenes; i++ ) {
                    tmp = resumeDeluxe.inputs[i];
                    str += "<a href=\"#\" onclick=\"return resumeDeluxe.addToSummary('" +
                           tmp.replace(/[']/g, "\\'") + "');\"" +
                           " class=\"sumLink\" title=\"Add to edit summary\">" +
                           resumeDeluxe.titles[i] + "</a> ";
                }
                $( '#wpSummaryLabel' ).prepend( str + '<br>' );
            }
        }
        $(document).ready( DeluxeSummary );

    })( jQuery, mediaWiki );
}
//</nowiki>