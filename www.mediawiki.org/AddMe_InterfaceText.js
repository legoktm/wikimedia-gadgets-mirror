{
	"@doc": "Configuration for the AddMe gadget",
   "devwish-vote":{
                    "title":"Vote for this proposal",
                    "button-submit":"Vote",
                    "button-cancel":"Cancel",
                    "summary":"Supported by",
                    "message-feedback":"Thank you for voting!",
                    "error-save":"There was an error, please try again",
                    "error-login":"You are not logged in"
    },
   "devwish-endorse":{
                    "title":"Endorse this proposal",
                    "message-description":"Your reason for endorsing the proposal",
                    "placeholder-comment":"If the problem that this proposal tries to solve has affected you personally (while developing, or helping/mentoring other developers), please explain how. If you feel you can speak for some group (user group, team/department, chapter, institution etc.), you are encouraged to do so.",
                    "button-submit":"Endorse",
                    "button-cancel":"Cancel",
                    "summary":"Endorsed by",
                    "message-feedback":"Thank you for adding your endorsement! Don't forget to vote also!",
                    "message-signature":"Your signature will be automatically added to your comment.",
                    "error-save":"There was an error, please try again",
                    "error-login":"You are not logged in"
    }
}